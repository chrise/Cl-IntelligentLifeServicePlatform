/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderServiceTest.java
 * History:
 *         2019年6月25日: Initially created, Chrise.
 */
package club.coderleague.ilsp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.beans.PaymentInterfaceResponse;
import club.coderleague.ilsp.common.domain.beans.PreferentialResult;
import club.coderleague.ilsp.service.interfaces.AbstractInterfaceService;
import club.coderleague.ilsp.service.orders.OrdersService;

/**
 * 订单服务测试。
 * @author Chrise
 */
@Service
public class OrderServiceTest {
	@Autowired
	private OrdersService ordersService;
	
	/**
	 * 核算优惠。
	 * @author Chrise 2019年7月11日
	 */
	public void calculatePreferential() {
		try {
			PreferentialResult<Integer> diamond = this.ordersService.calculateDiamondPreferential(0L, 0L, 0.0, false);
			System.out.println("diamond =====> " + diamond);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * 查询支付结果。
	 * @author Chrise 2019年6月26日
	 */
	public void queryPaymentResult() {
		try {
			PaymentInterfaceResponse pir = AbstractInterfaceService.queryTrade(0L);
			System.out.println("====================> " + pir);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * 申请退款。
	 * @author Chrise 2019年8月8日
	 */
	public void applyRefund() {
		try {
			AbstractInterfaceService.applyRefund(0L);
			Thread.sleep(60000L);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * 查询退款结果。
	 * @author Chrise 2019年8月8日
	 */
	public void queryRefundResult() {
		try {
			PaymentInterfaceResponse pir = AbstractInterfaceService.queryRefund(0L);
			System.out.println("====================> " + pir);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
