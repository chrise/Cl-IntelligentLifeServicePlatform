/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：NotifyTemplateConfigServiceTest.java
 * History:
 *         2019年6月9日: Initially created, Chrise.
 */
package club.coderleague.ilsp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.NotifyType;
import club.coderleague.ilsp.dao.NotifyTemplateConfigDao;
import club.coderleague.ilsp.entities.Notifytemplateconfigs;

/**
 * 通知模板配置服务测试。
 * @author Chrise
 */
@Service
public class NotifyTemplateConfigServiceTest {
	@Autowired
	private NotifyTemplateConfigDao ntConfigDao;
	
	/**
	 * 初始化通知模板配置。
	 * @author Chrise 2019年6月9日
	 */
	public void execInitNotifyTemplateConfig() {
		Notifytemplateconfigs phoneBind = new Notifytemplateconfigs(EntityState.VALID.getValue(), 
			NotifyType.PHONE_BIND.getValue(), "SMS_153055065", "{\"vercode\":\"vercode\",\"timeout\":\"timeout\"}");
		this.ntConfigDao.save(phoneBind);
	}
}
