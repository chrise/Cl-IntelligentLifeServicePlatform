/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SystemConfigServiceTest.java
 * History:
 *         2019年5月27日: Initially created, Chrise.
 */
package club.coderleague.ilsp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.SystemConfigDao;
import club.coderleague.ilsp.entities.Systemconfigs;

/**
 * 系统配置服务测试。
 * @author Chrise
 */
@Service
public class SystemConfigServiceTest {
	@Autowired
	private SystemConfigDao configDao;
	
	/**
	 * 初始化系统配置。
	 * @author Chrise 2019年5月27日
	 */
	public void execInitSystemConfig() {
		Systemconfigs entity = new Systemconfigs(EntityState.VALID.getValue(), 10, 20, 3, 7, 1, 30, 5, 
			true, "wxb21703d36322e58b", "9ce6cd849ce17b4e255f591bd9809040", "wxmerid", "wxapikey", null, "clilsptest", null, "欢迎使用顺山科技智慧生活服务平台！", 
			true, "2016093000627888", 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmAA97k0RWcBUVGSMqk7eCcY6JRuu4+jrKSoO/13Kv/p6MubUMM1vhAUaKodp6dsYO2dqQ99zmz2eWrHtTx5dVSa6SvT1AWlR95Vxad2dPda6jyMGVSSWOW3FvCGPrQHRIuYGC88LDSQ2KBh1B/hZWZTs/J/FJ/93XBzL7UUKN6M89Pw/Zm3QAXzAHINIsl/GXj7/qJW0V+aTFCIf177YvjxH0zvKixq0nEZDtE1xfWWCwpBE0M58OIKHWwIbHleZx0zko/vPE64PvFJwQ6nC0GkPiQ4Tm4BDOb9pDLQDcdA0tevSHdtWlDew8peco1NEAwwYa2qb5eAg8jscFJrqfQIDAQAB", 
			"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDP4ht72XeU5XZOQnTGKU/mN03vCEbBMAQvkUphm9B1vfbcv5dN3qw36iINF/cc0IECPe2gJEgfT95zmdQvuE2Qo+DgLc7/qHY9SXKDHJzc343rNLDUbPkhs0J6njChyUsoJKkZ9uE4zKdIG823HKzIVH+bckqmuzWeKH3onszUsnbd4RENgj/5Wrxi8FbO0c3ml9s+MKY52m5DygwTZji4wUdYAEEqwJAJ+EK56NdzS9M/jhLQCeEJEppJvcYIT4272g7TTEgss/HpVxud9ScbtXZfUkttBMyJX7aCVFbJeFzKj2dH0724a2lS0EVqjC+56z9srQNTTkacZqCUZeUlAgMBAAECggEAZXDEUI8x1yqRrGapbPlDz+c01GuJe0PfBwcy8KaD9rCYAehMAOejyjUqIJGNODZWfa2Sgw6xUvY6atapwA+Dag1N8pbn58Fg0U9B3dDcirtcGWZA06fJejRrQEJpIMmFLzWUfuMFzB2xk3Jd0/jeJ3N0QXpoCyxXYbIInra+5Rq+1qMvNZqSFGNsxTIkaD89JjIIskpCTheIaK60QLEz1LT9rcSvyRySyaKneyWd7SG7siGN2Undk8NOmbwlBwgTsgSJ/2wetbEkvSeYD64SeMHuKBuqGL0ptCaHuXWR6HHuMsEyiTfG7iNGsKmpjknV01rVuGq3Gus687H+MEgqEQKBgQD9oC8SUjrsFh2o8gYOprFg+UjLXY0ith5hliDLlbmF4kvNU3CqFp6+jCfLKTlKCJ7M+ECHVDiaLer6HnzER7QZ7vyrDvyILKlge41dZemnrBhWJXxqPw3PeCi1a/oU9xr8VMbZf12ZZU95GgEauu5ntRj6VuDVBNnttVApjs8mmwKBgQDR1E0eAkCzu1yTCS3UzTFSJZMU1oddBhLFj2jJw/qZXdU6uWQYM3HegYRg6aDpIc0R5yMFvs4+dCvdiwD6Csnsb1Ucx15VhOS7pRV+HuRiVtO6i8rIdPWHMOmtSoC33QdZkf91NFGCgx8aCmCdq5YxESTPELwkhvuuTwRj+9X/PwKBgBpR0jCzidvQG7nZi9lfYQ7wL6azCZ9tiM+VtceKvRrwte8T1qBAAq9BeiuyFnjhViFtLb0dd1ZAp57XJRl1W3JJg7z4rCoxgE475BqqJco5qLjf4P5hlrxN+uOA1g3w6sL+xTfy6LKvecTWdwDgHzdx6qqn82MHwtnRNAVsBYE7AoGBAJgO1vfpQAQ/wf2YCEuPi69xJV5TfPMPxKjAnXqh+EgONMkCsqPLHDjX+RvWEtL/uMHBk6LFPGcL/wHkiweoq94YbECXxigJT00gfCvSrUUKkRM93O1ZvNiEvMLlG9Sia4IgiFTiXjNLXE2duZhpUUUrnFYmVYHPBDK0doUSamk3AoGATm0dhwvsQHClrIK3P9h1TI4MO9Pd3c8HaGyQ1L+56u8/KPX9kPkmlvK8wuiG+n2Gc58/TwRojc1uaYCdOVW7hOTd8OaKSy5xkEdOMbNn/41ET7VTQm+CYVPdUWvTmgUQNd1wdKg5gf/72yIZu2K7fpBlYd2IY3KvzpSADo2oivk=", 
			true, "AccessKeyId", "AccessSecret", "顺山科技", 
			30);
		this.configDao.save(entity);
	}
}
