/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IlspApplicationTest.java
 * History:
 *         2019年5月27日: Initially created, Chrise.
 */
package club.coderleague.ilsp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import club.coderleague.ilsp.IlspApplication;
import club.coderleague.ilsp.service.NotifyTemplateConfigServiceTest;
import club.coderleague.ilsp.service.OrderServiceTest;
import club.coderleague.ilsp.service.SystemConfigServiceTest;

/**
 * 应用程序测试。
 * @author Chrise
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = IlspApplication.class)
public class IlspApplicationTest {
	@Autowired
	private SystemConfigServiceTest sConfigService;
	@Autowired
	private NotifyTemplateConfigServiceTest ntConfigService;
	@Autowired
	private OrderServiceTest orderService;
	
	/**
	 * 初始化系统配置。
	 * @author Chrise 2019年5月27日
	 */
	@Test
	public void initSystemConfig() {
		this.sConfigService.execInitSystemConfig();
	}
	
	/**
	 * 初始化通知模板配置。
	 * @author Chrise 2019年6月9日
	 */
	@Test
	public void initNotifyTemplateConfig() {
		this.ntConfigService.execInitNotifyTemplateConfig();
	}
	
	/**
	 * 核算优惠。
	 * @author Chrise 2019年7月11日
	 */
	@Test
	public void calculatePreferential() {
		this.orderService.calculatePreferential();
	}
	
	/**
	 * 查询支付结果。
	 * @author Chrise 2019年6月26日
	 */
	@Test
	public void queryPaymentResult() {
		this.orderService.queryPaymentResult();
	}
	
	/**
	 * 申请退款。
	 * @author Chrise 2019年8月8日
	 */
	@Test
	public void applyRefund() {
		this.orderService.applyRefund();
	}
	
	/**
	 * 查询退款结果。
	 * @author Chrise 2019年8月8日
	 */
	@Test
	public void queryRefundResult() {
		this.orderService.queryRefundResult();
	}
}
