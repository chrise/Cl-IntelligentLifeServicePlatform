/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AuthenticationService.java
 * History:
 *         2019年7月2日: Initially created, Chrise.
 */
package club.coderleague.ilsp.service;

/**
 * 身份验证服务。
 * @author Chrise
 */
public interface AuthenticationService {
	/**
	 * 验证。
	 * @author Chrise 2019年6月29日
	 * @param username 用户名。
	 * @param password 密码。
	 * @return 验证通过返回true，否则返回false。
	 */
	default boolean authenticate(String username, String password) {
		if ("admin".equals(username) && "admin".equals(password)) return true;
		return false;
	}
}
