/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileGoodsService.java
 * History:
 *         2019年7月31日: Initially created, wangjb.
 */
package club.coderleague.ilsp.service.mobile.goods;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.config.properties.FileUploadSettings;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.GoodSalesDao;
import club.coderleague.ilsp.dao.GoodsMgrDao;
import club.coderleague.ilsp.entities.Goodsales;

/**
 * -WeChat 商品管理业务处理。
 * @author wangjb
 */
@Service
public class MobileGoodsService {
	
	/**
	 * -商品管理数据接口。
	 */
	@Autowired GoodsMgrDao goodsMgrDao;
	
	/**
	 * -附件配置表。
	 */
	@Autowired FileUploadSettings upload;
	
	/**
	 * -商品销售。
	 */
	@Autowired GoodSalesDao saleDao;

	/**
	 * -WeChat 根据商户id查询商品管理数据。
	 * @author wangjb 2019年7月31日。
	 * @param merchantid 商户id。
	 * @return
	 */
	public Map<String, Object> getGroupAndMarketListDataService(Long merchantid) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> goodGroupList = this.goodsMgrDao.getGoodGroupListByMerchantidDao(merchantid);
		List<Map<String, Object>> marketList = this.goodsMgrDao.getMarketListByMerchantidDao(merchantid);
		result.put("goodGroupList", goodGroupList); // 商品分类数据。
		result.put("marketList", marketList); // 入驻市场数据。
		return result;
	}
	
	/**
	 * -WeCHat 获取商品管理数据。
	 * @author wangjb 2019年8月3日。
	 * @param merchantid 商户id。
	 * @return
	 */
	public Object getMobileGoodsMgrDataService(Long merchantid, Map<String, Object> params) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> goodsMgrList = this.goodsMgrDao.getGoodMgrMerchantidDao(merchantid, params); // 商品管理List。
		for (Map<String, Object> map : goodsMgrList) {
			if (map.get("photopath") != null && StringUtils.isNotBlank(map.get("photopath").toString())) { // 商品上册。
				map.put("photopath", this.upload.getVirtual() + this.upload.getGoodPhotosPath() +map.get("photopath").toString());
			}
			if (map.get("goodstock") != null && StringUtils.isNotBlank(map.get("goodstock").toString())) { // 商品库存。
				map.put("goodstock", Math.round((double)map.get("goodstock")) - (double)map.get("goodstock") == 0 ? (int)(double)map.get("goodstock") : map.get("goodstock")); // 库存。
			} else map.put("goodstock", 0);
			if (map.get("goodprice") != null && StringUtils.isNotBlank(map.get("goodprice").toString())) { // 商品价格。
				map.put("goodprice", new DecimalFormat("#.00").format(map.get("goodprice")));
			} else map.put("goodprice", "0.00");
		}
		result.put("goodsMgrList", goodsMgrList); // 商品管理数据。
		return result;
	}

	/**
	 * -WeChat 根据销售id修改商品库存。
	 * @author wangjb 2019年7月31日。
	 * @param saleid 销售id。
	 * @param goodstock 商品库存。
	 */
	public void executeModifyGoodstockBySaleidService(Long saleid, Double goodstock) {
		Goodsales sale = this.saleDao.getOne(saleid);
		if (EntityState.VALID.equalsValue(sale.getEntitystate())) sale.setEntitystate(EntityState.PUBLISHED.getValue());
		sale.setGoodstock(goodstock);
	}

	/**
	 * -WeChat 根据销售id修改商品单价。
	 * @author wangjb 2019年7月31日。
	 * @param saleid 销售id。
	 * @param goodprice 商品价格。
	 */
	public void executeModifyGoodpriceBySaleidService(Long saleid, Double goodprice) {
		Goodsales sale = this.saleDao.getOne(saleid);
		sale.setGoodprice(goodprice);
	}

	/**
	 * -WeChat 修改商品销售状态。
	 * @author wangjb 2019年8月3日。
	 * @param saleid 销售id。
	 * @param istate 销售状态。
	 */
	public void executeModifyGoodpriceBySaleidService(Long saleid, Integer istate) {
		Goodsales sale = this.saleDao.getOne(saleid);
		sale.setEntitystate(istate);
	}

}
