/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MainService.java
 * History:
 *         2019年5月15日: Initially created, CJH.
 */
package club.coderleague.ilsp.service.main;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.common.exception.MessageInfoException;
import club.coderleague.ilsp.common.exception.MessageWarnException;
import club.coderleague.ilsp.dao.FunctionsMgrDao;
import club.coderleague.ilsp.dao.UserDao;
import club.coderleague.ilsp.entities.Functions;
import club.coderleague.ilsp.entities.Users;
import club.coderleague.ilsp.util.SessionUtil;

/**
 * 主要Service
 * 
 * @author CJH
 */
@Service
public class MainService {
	
	/**
	 * 功能Dao
	 */
	private @Autowired FunctionsMgrDao functionsDao;
	
	/**
	 * 密码编码
	 */
	private @Autowired PasswordEncoder passwordEncoder;
	
	/**
	 * 用户Dao
	 */
	private @Autowired UserDao userDao;

	/**
	 * 根据父级功能主键和功能类型查询已授权功能
	 * 
	 * @author CJH 2019年5月15日
	 * @param parentfunc 父级功能主键
	 * @param functype 功能类型
	 * @return 功能
	 */
	public List<Functions> findAuthorizationByParentfuncAndFunctype(Long parentfunc, List<Integer> functype) {
		return functionsDao.findAuthorizationByParentfuncAndFunctype(parentfunc, functype);
	}

	/**
	 * 修改当前登录用户密码
	 * 
	 * @author CJH 2019年7月10日
	 * @param oldpassword 原密码
	 * @param newpassword 新密码
	 */
	public void updatePassword(String oldpassword, String newpassword) {
		if (StringUtils.isBlank(oldpassword)) {
			throw new ValidationException("原密码不能为空");
		}
		if (StringUtils.isBlank(newpassword)) {
			throw new ValidationException("新密码不能为空");
		}
		UserSession us = SessionUtil.getUserSession();
		if (us == null) {
			throw new MessageWarnException("用户会话不存在");
		}
		Users user = userDao.getOne(us.getId());
		if (!passwordEncoder.matches(oldpassword, user.getLoginpassword())) {
			throw new MessageInfoException("密码错误");
		}
		user.setLoginpassword(passwordEncoder.encode(newpassword));
	}

	/**
	 * 修改当前登录用户登录名
	 * 
	 * @author CJH 2019年8月7日
	 * @param loginname 登录名
	 * @return 结果信息
	 */
	public void updateLoginname(String loginname) {
		if (StringUtils.isBlank(loginname)) {
			throw new ValidationException("登录名不能为空");
		}
		UserSession us = SessionUtil.getUserSession();
		if (us == null) {
			throw new MessageWarnException("用户会话不存在");
		}
		Users user = userDao.getOne(us.getId());
		if (StringUtils.isNotBlank(user.getLoginname())) {
			throw new MessageWarnException("登录名已设置无法修改");
		}
		if (userDao.existsByLoginnameNotEntityid(loginname, us.getUserid())) {
			throw new MessageWarnException("登录名已存在");
		}
		user.setLoginname(loginname);
		HttpSession session = SessionUtil.getHttpSession();
		us.setLoginname(loginname);
		session.setAttribute(UserSession.SESSION_KEY, us);
	}

}
