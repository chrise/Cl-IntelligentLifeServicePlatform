/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomersService.java
 * History:
 *         2019年5月25日: Initially created, wangjb.
 */
package club.coderleague.ilsp.service.bigcustomers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.config.properties.FileUploadSettings;
import club.coderleague.ilsp.common.domain.beans.AttachBean;
import club.coderleague.ilsp.common.domain.beans.SettlementBigCustomerOrdersExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.controller.bigcustomers.bean.PersonsBean;
import club.coderleague.ilsp.controller.bigcustomers.bean.SaveBean;
import club.coderleague.ilsp.dao.BigCustomerAuditsDao;
import club.coderleague.ilsp.dao.BigCustomerPersonDao;
import club.coderleague.ilsp.dao.BigCustomersMgrDao;
import club.coderleague.ilsp.entities.Bigcustomeraudits;
import club.coderleague.ilsp.entities.Bigcustomerpersons;
import club.coderleague.ilsp.entities.Bigcustomers;
import club.coderleague.ilsp.service.capitalrecords.CapitalRecordsService;
import club.coderleague.ilsp.service.interfaces.AliyunInterfaceService;
import club.coderleague.security.AlgorithmBeanFactory;
import club.coderleague.security.algorithm.MD5Signer;

/**
 * 大客户管理业务处理。
 * @author wangjb
 */
@Service
public class BigCustomersMgrService {
	
	/**
	 * 大客户管理数据访问对象。
	 */
	@Autowired BigCustomersMgrDao BCMD;
	
	/**
	 * 大客户审核数据访问对象。
	 */
	@Autowired BigCustomerAuditsDao BCAD;
	
	/**
	 * 大客户人员。
	 */
	@Autowired BigCustomerPersonDao BCPD;
	
	/**
	 * 资金记录Service。
	 */
	@Autowired CapitalRecordsService capitalRecordsService;
	
	/**
	 * 短信提醒服务。
	 */
	@Autowired AliyunInterfaceService aliyunInterfaceService;
	
	/**
	 * 附件配置表。
	 */
	@Autowired FileUploadSettings upload;
	

	/**
	 * 获取大客户列表数据。
	 * @author wangjb 2019年5月26日。
	 * @param params 关键字参数查询。
	 * @return
	 * @throws Exception 抛出业务处理异常。
	 */
	public Page<Bigcustomers> getBigCustomersMgrService(Map<String, Object> params) throws Exception{
		Page<Bigcustomers> result = this.BCMD.getBigCustomersMgrDao(params);
		return result;
	}

	/**
	 * 根据大客户id获取数据。
	 * @author wangjb 2019年6月1日。
	 * @param entityid 大客户id。
	 * @return
	 */
	public Map<String, Object> getBigCustomersObjByEntityidService(Long entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		Bigcustomers bcc = this.BCMD.getOne(entityid);
		result.put("bcc", bcc);
		result.put("person", this.BCPD.getPersonByBgcidDao(entityid)); // 人员详情。
		List<AttachBean> list = new ArrayList<AttachBean>();
		AttachBean attachBean = new AttachBean("1", bcc.getBusinesslicense().substring(bcc.getBusinesslicense().lastIndexOf("/") + 1, bcc.getBusinesslicense().length()), 
				upload.getMerchantsPath() + bcc.getBusinesslicense());
		list.add(attachBean);
		result.put("businesslicense", list); // 营业执照。
		return result;
	}
	
	/**
	 * 新增/编辑大客户对象。
	 * @author wangjb 2019年6月1日。
	 * @param result 返回结果数据明。
	 * @param bigc 自定义对象。
	 * @throws Exception 抛出业务处理异常。
	 */
	public void insertOrUpdateBigCustomersObjService(Map<String, Object> result, SaveBean saveBean) throws Exception{
		Bigcustomers bigc = saveBean.getBigc();
		String customerhash = AlgorithmBeanFactory.getAlgorithmBean(MD5Signer.class).sign(bigc.getCustomername());
		Boolean repeat = this.BCMD.getRepeatByEntityidAndCustomerNameDao(customerhash, bigc.getEntityid());
		if (repeat) {
			result.put("state", false);
			result.put("msg", "已存在重复的名称!");
			return;
		}
		if (bigc.getEntityid() == null) {
			bigc.setCustomerhash(customerhash);
			this.BCMD.save(bigc);
		} else {
			Bigcustomers dataLibBigc = this.BCMD.getOne(bigc.getEntityid());
			dataLibBigc.setContactsphone(bigc.getContactsphone());
			bigc.setCustomerhash(customerhash);
			dataLibBigc.setCustomername(bigc.getCustomername());
			dataLibBigc.setCustomeraddress(bigc.getCustomeraddress());
			dataLibBigc.setBusinesslicense(bigc.getBusinesslicense());
		}
		
		/**
		 * 保存人员。
		 */
		List<String> delPhone = new ArrayList<String>();
		List<String> savePhone = new ArrayList<String>();
		for (PersonsBean person : saveBean.getPersons()) {
			String[] entityid = person.getEntityid().split("_");
			if (entityid.length > 1 && ("add").equals(entityid[0]) && ("del").equals(entityid[1])) continue;
			if (entityid.length > 1 && !("add").equals(entityid[0]) && ("del").equals(entityid[1])) { // 删除的数据。
				delPhone.add(person.getPersonphone());
				continue;
			}
			String phonehash = AlgorithmBeanFactory.getAlgorithmBean(MD5Signer.class).sign(person.getPersonphone());
			Bigcustomerpersons personObj = this.BCPD.getBigCustomerPersonsByCustomerIdAndPhoneHashDao(bigc.getEntityid(), phonehash);
			if (personObj == null) {
				personObj = new Bigcustomerpersons();
				personObj.setEntitystate(EntityState.VALID.getValue());
				personObj.setPersonname(person.getPersonname());
				personObj.setPersonphone(person.getPersonphone());
				personObj.setCustomerid(bigc.getEntityid());
				personObj.setPhonehash(phonehash);
				personObj.setManagerflag(person.getManagerflag());
				personObj.setPurchaserflag(person.getPurchaserflag());
				this.BCPD.save(personObj);
			} else {
				Bigcustomerpersons libObj = this.BCPD.getOne(Long.valueOf(entityid[0]));
				if (!phonehash.equals(libObj.getPhonehash())) libObj.setPersonweixin(null);
				personObj.setPersonname(person.getPersonname());
				personObj.setPersonphone(person.getPersonphone());
				personObj.setPhonehash(phonehash);
				personObj.setManagerflag(person.getManagerflag());
				personObj.setPurchaserflag(person.getPurchaserflag());
				personObj.setEntitystate(EntityState.VALID.getValue());
			}
			savePhone.add(person.getPersonphone());
//			if (("add").equals(entityid[0])) { // 新增。
//				obj.setEntitystate(EntityState.VALID.getValue());
//				obj.setPersonname(person.getPersonname());
//				obj.setPersonphone(person.getPersonphone());
//				obj.setCustomerid(bigc.getEntityid());
//				obj.setPhonehash(AlgorithmBeanFactory.getAlgorithmBean(MD5Signer.class).sign(person.getPersonphone()));
//				obj.setManagerflag(person.getManagerflag());
//				obj.setPurchaserflag(person.getPurchaserflag());
//				this.BCPD.save(obj);
//			} else if (entityid.length == 1 && !("add").equals(entityid[0])) { //修改。
//				Bigcustomerpersons libObj = this.BCPD.getOne(Long.valueOf(entityid[0]));
//				String phonehash = AlgorithmBeanFactory.getAlgorithmBean(MD5Signer.class).sign(person.getPersonphone());
//				if (!phonehash.equals(libObj.getPhonehash())) libObj.setPersonweixin(null);
//				libObj.setPersonname(person.getPersonname());
//				libObj.setPersonphone(person.getPersonphone());
//				libObj.setPhonehash(phonehash);
//				libObj.setManagerflag(person.getManagerflag());
//				libObj.setPurchaserflag(person.getPurchaserflag());
//			} else if (entityid.length > 1 && !("add").equals(entityid[0]) && ("del").equals(entityid[1])) { // 删除。
//				Bigcustomerpersons libObj = this.BCPD.getOne(Long.valueOf(entityid[0]));
//				libObj.setEntitystate(EntityState.INVALID.getValue());
//				libObj.setPersonweixin(null);
//			}
		}
		
		for (String phone : delPhone) { // 删除人员。
			boolean flag = true;
			for (String _p : savePhone) {
				if (phone.equals(_p)) {
					flag = false;
					break;
				}
			}
			if (flag) {
				Bigcustomerpersons personObj = this.BCPD.getBigCustomerPersonsByCustomerIdAndPhoneHashDao(bigc.getEntityid(), AlgorithmBeanFactory.getAlgorithmBean(MD5Signer.class).sign(phone));
				personObj.setEntitystate(EntityState.INVALID.getValue());
				personObj.setPersonweixin(null);
			}
		}
		result.put("state", true);
		result.put("msg", "操作成功");
	}

	/**
	 * 修改大客户状态。
	 * @author wangjb 2019年6月1日。
	 * @param entityid id。
	 * @param result 返回操作提示！
	 * @param entitystate
	 */
	public void updateEntitystateByEntityidService(String entityid, Map<String, Object> result, Integer entitystate) {
		String[] ids = entityid.split(","); 
		for (String bigcid : ids) {
			Bigcustomers dataLibBigc = this.BCMD.getOne(Long.valueOf(bigcid));
			if (dataLibBigc.getEntitystate() == EntityState.INVALID.getValue()) { // 回收站恢复数据。
				Boolean repeat = this.BCMD.getRepeatByEntityidAndCustomerNameDao(dataLibBigc.getCustomername(), dataLibBigc.getEntityid());
				if (repeat) {
					result.put("state", false);
					result.put("msg", "已存在重复的名称!");
					return;
				}
				dataLibBigc.setEntitystate(entitystate);
				continue;
			}
			
			dataLibBigc.setEntitystate(entitystate);
		}
		
		result.put("state", true);
		result.put("msg", "操作成功");
	}

	/**
	 * 审核待审核的大客户数据。
	 * @author wangjb 2019年6月1日。
	 * @param id 大客户id。
	 * @param istate 审核状态。
	 * @param record 审核记录。
	 * @param result 返回结果说明。
	 * @param userid 操作用户id。
	 */
	public void executeAuditBigCustomersObjService(Long id, Integer istate, String record, Map<String, Object> result, Long userid) {
		Bigcustomers dataLibBigc = this.BCMD.getOne(id);
		if (istate == 1) dataLibBigc.setEntitystate(EntityState.VALID.getValue());
		else dataLibBigc.setEntitystate(EntityState.NOTPASS.getValue());
		Bigcustomeraudits audit = new Bigcustomeraudits();
		audit.setEntitystate(EntityState.VALID.getValue());
		audit.setCustomerid(id);
		audit.setAuditor(userid);;
		audit.setAudittime(new Date());
		audit.setAuditrecord(record);
		this.BCAD.save(audit);
		result.put("state", true);
		result.put("msg", "操作成功");
	}

	/**
	 * 获取客户订单
	 * @author wangjb 2019年6月8日。
	 * @param params 页面查询参数。
	 * @return
	 */
	public Page<SettlementBigCustomerOrdersExtension> getBigCustomerOrdersMgrService(Map<String, Object> params) {
		Page<SettlementBigCustomerOrdersExtension> result = this.BCMD.getBigCustomerOrdersMgrDao(params);
		return result;
	}

	/**
	 * 执行结算操作。
	 * @author wangjb 2019年6月8日。
	 * @param ids 订单id。
	 * @param result 返回操作提示。
	 */
	public void executeSettlementByIdsObjService(String ids, Map<String, Object> result) {
		if (ids != null && !"".equals(ids)) {
//			List<Long> orderIdList = new ArrayList<Long>();
			for (String id : ids.split(",")) {
				this.capitalRecordsService.updateOrderRevenueCapitalByMerchantid(Long.valueOf(id)); // 使用金额。
			}
			
//			List<Map<String, Object>> merchantList = this.BCMD.getMerchantListByOrderIdDao(orderIdList);
//			for (Map<String, Object> map : merchantList) {
//				this.aliyunInterfaceService.sendAccountRemind(NotifyType.ARRACC_REMIND, Long.valueOf(map.get("merchantid").toString()), Double.valueOf(map.get("goodtotal").toString())); // 发送到账短信。
//			}
		}
		
		result.put("state", true);
		result.put("msg", "操作成功");
	}

	/**
	 * 获取大客户数据。
	 * @author wangjb 2019年8月30日。
	 * @return
	 */
	public List<Bigcustomers> getBigCustomerListService() {
		List<Bigcustomers> result = this.BCMD.getBigCustomerListDao();
		return result;
	}

}
