/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：FunctionsService.java
 * History:
 *         2019年5月10日: Initially created, wangjb.
 */
package club.coderleague.ilsp.service.functions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.FunctionsMgrDao;
import club.coderleague.ilsp.entities.Functions;

/**
 * 功能管理业务处理。
 * @author wangjb
 */
@Service
public class FunctionsMgrService {
	
	/**
	 * 功能管理业务处理。
	 */
	@Autowired FunctionsMgrDao funcMgrDao;

	/**
	  * 获取功能管理列表数据。
	 * @author wangjb 2019年5月11日。
	 * @param params 页面查询参数。
	 * @return
	 */
	public List<Map<String, Object>> getFuncMgrListService(Map<String, Object> params) {
		List<Map<String, Object>> result = this.funcMgrDao.getFuncMgrListDao(params);
		return result;
	}

	/**
	 * 根据功能id获取功能对象。
	 * @author wangjb 2019年5月11日。
	 * @param entityid 功能id。
	 * @return
	 */
	public Functions getFuncObjByFuncidService(Long entityid) {
		return this.funcMgrDao.getOne(entityid);
	}
	
	/**
	 * 获取父级功能
	 * @author wangjb 2019年5月11日。
	 * @return
	 */
	public List<Map<String, Object>> getParentFuncSelectListService() {
		List<Map<String, Object>> result = this.funcMgrDao.getParentFuncSelectListDao();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("entityid", "-1");
		map.put("funcname", "顶层功能");
		map.put("parentfunc", "");
		result.add(0, map);
		return result;
	}

	/**
	 * 新增或者编辑功能数据。
	 * @author wangjb 2019年5月11日。
	 * @param funcObj 功能对象。
	 * @param result 返回结果说明。
	 */
	public void insertOrUpdateFuncObjService(Functions funcObj, Map<String, Object> result) {
		Boolean repeat = this.funcMgrDao.getRepeatDataByFuncNameOrParentFuncOrFuncTypeDao(funcObj.getFuncname(), funcObj.getParentfunc(), funcObj.getFunctype(), funcObj.getEntityid());
		if (repeat) {
			result.put("state", false);
			result.put("msg", "已存在重复的名称!");
			return;
		}
		if (funcObj.getParentfunc() == null || funcObj.getParentfunc() == -1) funcObj.setParentfunc(null);
		this.funcMgrDao.save(funcObj);
		result.put("state", true);
		result.put("msg", "操作成功!");
	}
	
	/**
	 * 修改功能状态。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 功能id。
	 * @param result 返回操作提示。
	 * @param entitystate 功能状态。
	 */
	public void updateEntitystateByFuncidService(String entityid, Map<String, Object> result, Integer entitystate) {
		String[] ids = entityid.split(","); 
		for (String funcid : ids) {
			Functions funcObj = this.funcMgrDao.getOne(Long.parseLong(funcid));
			if (entitystate == EntityState.VALID.getValue()) { // 恢复数据。
				Boolean repeat = this.funcMgrDao.getRepeatDataByFuncNameOrParentFuncOrFuncTypeDao(funcObj.getFuncname(), funcObj.getParentfunc(), funcObj.getFunctype(), funcObj.getEntityid());
				if (repeat) {
					result.put("state", false);
					result.put("msg", "已存在重复的名称!");
					return;
				}
				if (funcObj.getParentfunc() != null) this.updateParentfuncEntitystateService(entitystate, funcObj.getParentfunc()); // 恢复父级。
			} else this.updateChildEntitystateService(entitystate, funcObj.getEntityid()); // 删除子级。
			funcObj.setEntitystate(entitystate);
		}
		
		result.put("state", true);
		result.put("msg", "操作成功!");
	}
	
	/**
	 * 恢复父级。
	 * @author wangjb 2019年5月12日。
	 * @param entitystate 功能状态。
	 * @param dataid 父级id。
	 */
	public void updateParentfuncEntitystateService(Integer entitystate, Long dataid) {
		while (true) {
			Functions funcObj = this.funcMgrDao.getOne(dataid);
			if (funcObj != null) {
				funcObj.setEntitystate(entitystate);
				if (funcObj.getEntitystate() == entitystate) break;
                if (funcObj.getParentfunc() != null) this.updateParentfuncEntitystateService(entitystate, funcObj.getParentfunc());
            }
            break;
		}
	}
	
	/**
	 * 删除子级数据。
	 * @author wangjb 2019年5月12日。
	 * @param entitystate 功能状态。
	 * @param dataid 子级的父级id。
	 */
	public void updateChildEntitystateService(Integer entitystate, Long dataid) {
        while (true) {
            List<Functions> funcList = this.funcMgrDao.findByParentfunc(dataid);
            if (funcList.size() > 0) {
                for (Functions func : funcList) {
    				func.setEntitystate(entitystate);
                    this.updateChildEntitystateService(entitystate, func.getEntityid());
                }
            }
            break;
        }
    }

	/**
	 * 查询功能详情数据。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 功能id。
	 * @return
	 */
	public Map<String, Object> getFunctionsDetailDataByFuncidService(String entityid) {
		Map<String, Object> result = this.funcMgrDao.getFunctionsDetailDataByFuncidDao(entityid);
		return result;
	}

}
