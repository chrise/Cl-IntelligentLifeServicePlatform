/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MarketsService.java
 * History:
 *         2019年5月11日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.service.markets;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.ResultMsg;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.GoodSalesDao;
import club.coderleague.ilsp.dao.MarketsDao;
import club.coderleague.ilsp.dao.MerchantSettlesDao;
import club.coderleague.ilsp.entities.Goodsales;
import club.coderleague.ilsp.entities.Markets;
import club.coderleague.ilsp.entities.Merchantsettles;

/**
 * 市场表
 * @author Liangjing
 */
@Service
public class MarketsService {
	
	@Autowired
	MarketsDao marketsDao;
	@Autowired
	MerchantSettlesDao merchantSettlesDao;
	@Autowired
	GoodSalesDao goodSalesDao;

	/**
	 * 获取所有的市场数据
	 * @author Liangjing 2019年5月11日
	 * @param pageIndex
	 * @param pageSize
	 * @param istate
	 * @param key
	 * @return
	 */
	public Page<Map<String,Object>> getTheAllMarkets(String pageIndex, String pageSize, boolean isrecycle, String key) {
		return marketsDao.getTheAllMarkets(Integer.valueOf(pageIndex), Integer.valueOf(pageSize), isrecycle, key);
	}
	
	/**
	 * 获取市场编辑页面数据
	 * @author Liangjing 2019年5月12日
	 * @param entityid
	 * @return
	 */
	public Markets getTheMarketEditPageData(Long entityid) {
		return marketsDao.findByEntityid(entityid);
	}
	
	/**
	 * 新增或者编辑市场
	 * @author Liangjing 2019年5月12日
	 * @param markets
	 * @return
	 */
	public ResultMsg updateTheMarket(Markets markets) {
		Markets marketsValidate = marketsDao.findByMarketnameAndEntitystate(markets.getMarketname(), EntityState.VALID.getValue());
		if(CommonUtil.isEmpty(markets.getEntityid())) {
			if(marketsValidate != null) {
				return new ResultMsg(false, "该市场已存在");
			}else {
				Markets marketsnew = new Markets(EntityState.VALID.getValue(), markets.getMarketname(), markets.getMarketdesc());
				marketsDao.save(marketsnew);
			}
		}else {
			if(marketsValidate != null) {
				if(marketsValidate.getEntityid().equals(markets.getEntityid())) {
					marketsValidate.setMarketname(markets.getMarketname());
					marketsValidate.setMarketdesc(markets.getMarketdesc());
				}else {
					return new ResultMsg(false, "该市场已存在");
				}
			}else {
				Markets marketsEdit = marketsDao.findByEntityid(markets.getEntityid());
				marketsEdit.setMarketname(markets.getMarketname());
				marketsEdit.setMarketdesc(markets.getMarketdesc());
			}
		}
		return new ResultMsg(true, "操作成功");
	}
	
	/**
	 * 修改市场的状态
	 * @author Liangjing 2019年5月12日
	 * @param marketids
	 * @param iState
	 * @return
	 */
	public ResultMsg updateTheMarketstateByIstate(List<Long> marketids,Integer iState) {
		boolean isgo = true;
		if(iState == EntityState.VALID.getValue()) {
			//恢复
			//判断是否有重复
			for (Long entityid : marketids) {
				Markets markets = marketsDao.findByEntityid(entityid);
				Markets marketsValidate = marketsDao.findByMarketnameAndEntitystate(markets.getMarketname(), EntityState.VALID.getValue());
				if(marketsValidate != null) {
					isgo = false;
					break;
				}
			}
		}
		if(isgo) {
			for (Long entityid : marketids) {
				this.updateTheRefState(entityid, iState);
				Markets markets = marketsDao.findByEntityid(entityid);
				markets.setEntitystate(iState);
			}
		}else {
			return new ResultMsg(false, "恢复数据存在重复");
		}
		return new ResultMsg(true, "操作成功");
	}
	
	/**
	 * 修改对应商户入驻和商品销售数据状态
	 * @author Liangjing 2019年7月30日
	 * @param marketids
	 */
	public void updateTheRefState(Long marketid, Integer istate) {
		if(istate == EntityState.INVALID.getValue()) {
			List<Merchantsettles> list = merchantSettlesDao.findByMarketidAndEntitystate(marketid, EntityState.VALID.getValue());
			for (Merchantsettles merchantsettles : list) {
				List<Goodsales> listGood = goodSalesDao.findBySettleidAndEntitystateIn(merchantsettles.getEntityid(), Arrays.asList(new Integer[] {EntityState.VALID.getValue(), EntityState.PUBLISHED.getValue()}));
				for (Goodsales goodsales : listGood) {
					goodsales.setOriginalstate(goodsales.getEntitystate());
					goodsales.setEntitystate(EntityState.CASCADEDELETED.getValue());
				}
				merchantsettles.setEntitystate(EntityState.CASCADEDELETED.getValue());
			}
		}else if(istate == EntityState.VALID.getValue()){
			List<Merchantsettles> list = merchantSettlesDao.findByMarketidAndEntitystate(marketid, EntityState.CASCADEDELETED.getValue());
			for (Merchantsettles merchantsettles : list) {
				List<Goodsales> listGood = goodSalesDao.findBySettleidAndEntitystateIn(merchantsettles.getEntityid(), Arrays.asList(new Integer[] {EntityState.CASCADEDELETED.getValue()}));
				for (Goodsales goodsales : listGood) {
					goodsales.setEntitystate(goodsales.getOriginalstate());
				}
				merchantsettles.setEntitystate(EntityState.VALID.getValue());
			}
		}
	}

}
