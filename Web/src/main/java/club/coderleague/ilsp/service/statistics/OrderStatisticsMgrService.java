/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomerOrdersService.java
 * History:
 *         2019年6月8日: Initially created, wangjb.
 */
package club.coderleague.ilsp.service.statistics;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.OrderStatisticsExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.OrderDeliveryState;
import club.coderleague.ilsp.dao.AreasDao;
import club.coderleague.ilsp.dao.OrdersDao;
import club.coderleague.ilsp.dao.RefundRecordsDao;
import club.coderleague.ilsp.entities.Orders;
import club.coderleague.ilsp.entities.Refundrecords;
import club.coderleague.ilsp.service.interfaces.AbstractInterfaceService;

/**
 * 大客户订单统计业务处理。
 * @author wangjb
 */
@Service
public class OrderStatisticsMgrService {
	
	/**
	 * -订单数据访问。
	 */
	@Autowired OrdersDao OD;
	
	/**
	 * -退款记录数据访问对象。
	 */
	@Autowired RefundRecordsDao refundRecordsDao;
	
	/**
	 * -区域数据访问对象。
	 */
	@Autowired AreasDao areasDao;

	/**
	 * 获取大客户订单统计列表数据。
	 * @author wangjb 2019年6月8日。
	 * @param params 页面查询参数。
	 * @return
	 */
	public Page<OrderStatisticsExtension> getOrderStatisticsMgrService(Map<String, Object> params) throws Exception{
		Page<OrderStatisticsExtension> result = this.OD.getOrderStatisticsMgrDao(params);
		return result;
	}

	/**
	 * -执行标记发货操作。
	 * @author wangjb 2019年8月30日。
	 * @param ids 订单id。
	 * @param result 返回结果提示。
	 */
	public void executeModifyOrderStateService(String ids, Map<String, Object> result) {
		if (ids != null && !"".equals(ids)) {
			for (String id : ids.split(",")) {
				Orders order = this.OD.getOne(Long.valueOf(id));
				order.setDeliverystate(OrderDeliveryState.SHIPPED.getValue());
				order.setSendtime(new Date());
			}
		}
		result.put("state", true);
		result.put("msg", "操作成功!");
	}
	
	/**
	 * -根据订单id获取退款信息。
	 * @author wangjb 2019年10月10日。
	 * @param orderid 订单id。
	 * @return
	 */
	public Map<String, Object> getRefundMessageByOrderIdService(Long orderid) {
		Map<String, Object> result = new HashMap<String, Object>();
		Orders order = this.OD.getOne(orderid);
		result.put("order", order);
		Map<String, Object> refundRecord = this.OD.getRefundRecordByOrderidDao(orderid);
		result.put("refundRecord", refundRecord);
		if (refundRecord.get("entityid") != null && StringUtils.isNotBlank(refundRecord.get("entityid").toString())) {
			List<Map<String, Object>> refundGood = this.OD.getRefundGoodByOrderidDao(orderid, refundRecord.get("entityid").toString());
			result.put("refundGood", refundGood);
		} else result.put("refundGood", null);
		
		return result;
	}

	/**
	 * -执行退款操作。
	 * @author wangjb 2019年8月30日。
	 * @param orderid 订单id。
	 * @param istate 状态。
	 * @param result 返回操作结果提示。
	 * @param handledesc 处理描述。
	 */
	public void executeRefundByOrderIdService(Long orderid, Integer istate, Map<String, Object> result, String handledesc) throws Exception {
		Refundrecords record = this.refundRecordsDao.findByOrderidAndEntitystate(orderid, EntityState.OBLIGATION.getValue());
		if (istate == 1) { // 通过。
			record.setEntitystate(EntityState.REFUNDING.getValue());
			record.setHandletime(new Date());
			record.setHandledesc(handledesc);
			AbstractInterfaceService.applyRefund(orderid);
		} else { // 否决。
			record.setEntitystate(EntityState.UNDO.getValue());
			record.setHandletime(new Date());
			record.setHandledesc(handledesc);
		}
		
		result.put("state", true);
		result.put("msg", "操作成功!");
	}
	
	/**
	 * 根据订单id获取订单详情。
	 * @author wangjb 2019年6月15日。
	 * @param id 订单id。
	 * @return
	 */
	public Map<String, Object> getOrderStatisticsDetailByOrderIdService(Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Orders order = this.OD.getOne(id);
		result.put("order", order);
		if (order.getProvince() != null) result.put("province", this.areasDao.getOne(order.getProvince()).getAreaname());
		if (order.getCity() != null) result.put("city", this.areasDao.getOne(order.getCity()).getAreaname());
		if (order.getCounty() != null) result.put("county", this.areasDao.getOne(order.getCounty()).getAreaname());
		if (order.getStreet() != null) result.put("street", this.areasDao.getOne(order.getStreet()).getAreaname());
		return result;
	}

	/**
	 * 根据订单id查询订单商品数据。
	 * @author wangjb 2019年6月15日。
	 * @param params 页面查询参数。
	 * @return
	 */
	public List<Map<String, Object>> getOrderGoodsByOrderIdService(Map<String, Object> params) {
		List<Map<String, Object>> result = this.OD.getOrderGoodsByOrderIdDao(params);
		return result;
	}

	/**
	 * -根据订单id获取退款记录数据。
	 * @author wangjb 2019年9月3日。
	 * @param orderid 订单id。
	 * @return
	 */
	public List<Refundrecords> getRefundRecordByOrderIdService(Long orderid) {
		List<Refundrecords> result = this.refundRecordsDao.findByOrderid(orderid);
		return result;
	}
}
