/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MembersService.java
 * History:
 *         2019年5月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.service.members;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.ReceivingAddressesSelectBean;
import club.coderleague.ilsp.dao.DiamondRecordsDao;
import club.coderleague.ilsp.dao.IntegralRecordsDao;
import club.coderleague.ilsp.dao.MemberBindDao;
import club.coderleague.ilsp.dao.MembersDao;
import club.coderleague.ilsp.dao.ReceivingAddressesDao;
import club.coderleague.ilsp.entities.Diamondrecords;
import club.coderleague.ilsp.entities.Integralrecords;
import club.coderleague.ilsp.entities.Memberbinds;
import club.coderleague.ilsp.entities.Members;

/**
 * 会员Service
 * 
 * @author CJH
 */
@Service
public class MembersService {
	/**
	 * 会员Dao
	 */
	private @Autowired MembersDao membersDao;
	@Autowired
	MemberBindDao memberBindDao;
	@Autowired
	IntegralRecordsDao integralRecordsDao;
	@Autowired
	DiamondRecordsDao diamondRecordsDao;
	@Autowired
	ReceivingAddressesDao receivingAddressesDao;
	
	/**
	 * 获取所有的会员
	 * @author Liangjing 2019年6月1日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<Members> getTheAllMembers(String pageIndex,String pageSize,String key) {
		return membersDao.getTheAllMembers(Integer.valueOf(pageIndex), Integer.valueOf(pageSize), key);
	}
	
	/**
	 * 获取对应会员绑定数据
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<Memberbinds> getTheAllMemberBinds(Long memberid,String pageIndex,String pageSize,String key) {
		return memberBindDao.getTheAllMemberBinds(memberid, Integer.valueOf(pageIndex), Integer.valueOf(pageSize), key);
	}
	
	/**
	 * 获取对应会员积分记录
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<Integralrecords> getTheAllIntegralRecords(Long memberid,String pageIndex,String pageSize,String key) {
		return integralRecordsDao.getTheAllIntegralRecords(memberid, Integer.valueOf(pageIndex), Integer.valueOf(pageSize), key);
	}
	
	/**
	 * 获取对应会员钻石记录
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<Diamondrecords> getTheAllDiamondRecords(Long memberid,String pageIndex,String pageSize,String key) {
		return diamondRecordsDao.getTheAllDiamondRecords(memberid, Integer.valueOf(pageIndex), Integer.valueOf(pageSize), key);
	}
	
	/**
	 * 获取会员对应收货地址
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<ReceivingAddressesSelectBean> getTheAllReceivingAddresses(Long memberid,String pageIndex,String pageSize,String key) {
		return receivingAddressesDao.getTheAllReceivingAddresses(memberid, Integer.valueOf(pageIndex), Integer.valueOf(pageSize), key);
	}
}
