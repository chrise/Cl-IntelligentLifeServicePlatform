/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：GoodGroupsService.java
 * History:
 *         2019年5月12日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.service.goodgroups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.config.properties.FileUploadSettings;
import club.coderleague.ilsp.common.domain.beans.AttachBean;
import club.coderleague.ilsp.common.domain.beans.ResultMsg;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.GoodGroupsDao;
import club.coderleague.ilsp.entities.Goodgroups;

/**
 * 商品分类
 * @author Liangjing
 */
@Service
public class GoodGroupsService {

	@Autowired
	GoodGroupsDao goodGroupsDao;
	@Autowired 
	FileUploadSettings upload;
	
	/**
	 * 获取所有的商品分类
	 * @author Liangjing 2019年5月12日
	 * @param key
	 * @param istate
	 * @return
	 */
	public List<Goodgroups> getTheAllGoodGroup(String key,boolean isrecycle) {
		return goodGroupsDao.getTheAllGoodGroup(key, isrecycle);
	}
	
	/**
	 * 获取商品分类编辑页面数据
	 * @author Liangjing 2019年5月12日
	 * @param entityid
	 * @return
	 */
	public Map<String,Object> getTheGoodGroupEditPageData(Long entityid) {
		Map<String, Object> map = new HashMap<String, Object>();
		Goodgroups goodgroup = goodGroupsDao.findByEntityid(entityid);
		map.put("formdata", goodgroup);
		if(!CommonUtil.isEmpty(goodgroup.getGrouppicture())) {
			map.put("fltp", this.getFileList(upload.getGoodGroupsPath() + goodgroup.getGrouppicture()));
		}else {
			map.put("fltp",this.getFileList(null));
		}
		return map;
	}
	
	/**
	 * 获取商品分类查看详情页面数据
	 * @author Liangjing 2019年5月12日
	 * @param entityid
	 * @return
	 */
	public Map<String,Object> getTheGoodGroupLookInfoPageData(String entityid) {
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> goodgroup = goodGroupsDao.getTheGoodGroupLookInfoPageData(entityid);
		map.put("formdata", goodgroup);
		if(goodgroup.get("grouppicture") != null && !"".equals(goodgroup.get("grouppicture").toString())) {
			map.put("fltp", this.getFileList(upload.getGoodGroupsPath() + goodgroup.get("grouppicture").toString()));
		}else {
			map.put("fltp",this.getFileList(null));
		}
		return map;
	}
	
	private List<AttachBean> getFileList(String path){
		List<AttachBean> list = new ArrayList<AttachBean>();
		if(!CommonUtil.isEmpty(path)) {
			AttachBean attachBean = new AttachBean("1", path.substring(path.lastIndexOf("/") + 1,path.length()), path);
			list.add(attachBean);
		}
		return list;
	}
	
	/**
	 * 新增或者编辑商品分类
	 * @author Liangjing 2019年5月12日
	 * @param goodgroups
	 * @return
	 */
	public ResultMsg updateTheGoodGroup(Goodgroups goodgroups) {
		if(goodgroups.getParentgroup() == -1) {
			goodgroups.setParentgroup(null);
		}
		Goodgroups goodgroupsValidate = goodGroupsDao.findByGroupnameAndParentgroupAndEntitystate(goodgroups.getGroupname(), goodgroups.getParentgroup(), EntityState.VALID.getValue());
		if(CommonUtil.isEmpty(goodgroups.getEntityid())) {
			if(goodgroupsValidate != null) {
				return new ResultMsg(false, "该商品分类已存在");
			}else {
				Goodgroups goodgroupsnew = new Goodgroups(EntityState.VALID.getValue(), goodgroups.getGroupname(), goodgroups.getParentgroup(), goodgroups.getGrouppicture(), goodgroups.getGroupdesc());
				goodGroupsDao.save(goodgroupsnew);
			}
		}else {
			if(goodgroupsValidate != null) {
				if(goodgroupsValidate.getEntityid().equals(goodgroups.getEntityid())) {
					goodgroupsValidate.setGroupname(goodgroups.getGroupname());
					goodgroupsValidate.setGroupdesc(goodgroups.getGroupdesc());
					goodgroupsValidate.setParentgroup(goodgroups.getParentgroup());
					if(!CommonUtil.isEmpty(goodgroups.getGrouppicture())) {
						goodgroupsValidate.setGrouppicture(goodgroups.getGrouppicture());
					}else {
						goodgroupsValidate.setGrouppicture(null);
					}
				}else {
					return new ResultMsg(false, "该商品分类已存在");
				}
			}else {
				Goodgroups goodgroupsEdit = goodGroupsDao.findByEntityid(goodgroups.getEntityid());
				goodgroupsEdit.setGroupname(goodgroups.getGroupname());
				goodgroupsEdit.setGroupdesc(goodgroups.getGroupdesc());
				goodgroupsEdit.setParentgroup(goodgroups.getParentgroup());
				if(!CommonUtil.isEmpty(goodgroups.getGrouppicture())) {
					goodgroupsEdit.setGrouppicture(goodgroups.getGrouppicture());
				}else {
					goodgroupsEdit.setGrouppicture(null);
				}
			}
		}
		return new ResultMsg(true, "操作成功");
	}
	
	/**
	 * 根据istate修改商品分类状态
	 * @author Liangjing 2019年5月12日
	 * @param entityids
	 * @param istate
	 * @return
	 */
	public ResultMsg updateTheGoodGroupStateByIstate(List<Long> entityids,Integer istate) {
		boolean isgo = true;
		if(istate == EntityState.VALID.getValue()) {
			for (Long entityid : entityids) {
				Goodgroups goodgroups = goodGroupsDao.findByEntityid(entityid);
				Goodgroups goodgroupsValidate = goodGroupsDao.findByGroupnameAndParentgroupAndEntitystate(goodgroups.getGroupname(), goodgroups.getParentgroup(), EntityState.VALID.getValue());
				if(goodgroupsValidate != null) {
					isgo = false;
					break;
				}
			}
		}
		if(isgo) {
			for (Long entityid : entityids) {
				Goodgroups goodgroups = goodGroupsDao.findByEntityid(entityid);
				goodgroups.setEntitystate(istate);
			}
		}else {
			return new ResultMsg(false, "恢复数据存在重复");
		}
		return new ResultMsg(true, "操作成功");
	}
	
	/**
	 * 获取商品分类编辑页面父级分类加载数据
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	public List<Map<String,Object>> getTheUpdatePageLoadParentGoods(String entityid) {
		List<Map<String,Object>> list = goodGroupsDao.getTheUpdatePageLoadParentGoods(entityid);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("id", "-1");
		map.put("name", "请选择");
		list.add(0,map);
		return list;
	}
}
