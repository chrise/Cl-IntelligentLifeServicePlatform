/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MobileGoodGroupsServers.java
 * History:
 *         2019年6月17日: Initially created, Liangj.
 */
package club.coderleague.ilsp.service.mobile.goodgroups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.config.properties.FileUploadSettings;
import club.coderleague.ilsp.common.domain.beans.mobile.MerchantsFilterConditions;
import club.coderleague.ilsp.common.domain.beans.mobile.SingleGoodsExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.common.util.RichTextEditorUtil;
import club.coderleague.ilsp.dao.GoodGroupsDao;
import club.coderleague.ilsp.dao.GoodSalesDao;
import club.coderleague.ilsp.dao.MarketsDao;
import club.coderleague.ilsp.dao.MerchantsDao;
import club.coderleague.ilsp.dao.ShoppingCartsDao;
import club.coderleague.ilsp.entities.Shoppingcarts;

/**
 * 手机-商品分类
 * @author Liangjing
 */
@Service
public class MobileGoodGroupsServers {

	@Autowired
	GoodGroupsDao goodGroupsDao;
	@Autowired 
	FileUploadSettings upload;
	@Autowired
	GoodSalesDao goodSalesDao;
	@Autowired
	ShoppingCartsDao shoppingCartsDao;
	@Autowired
	MerchantsDao merchantsDao;
	@Autowired
	MarketsDao marketsDao;
	@Autowired
	RichTextEditorUtil richTextEditorUtil;
	
	/**
	 * 获取手机需加载商品分类
	 * @author Liangjing 2019年6月17日
	 * @param key
	 * @return
	 */
	public List<Map<String,Object>> getTheAllMobileGoodGroups(Long entityid){
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		if(CommonUtil.isEmpty(entityid)) {
			list = goodGroupsDao.getTheMobileParentGoods();
			int i = 0;
			//只获取第一个父级的子级
			for (Map<String, Object> map : list) {
				List<Map<String,Object>> second = new ArrayList<Map<String,Object>>();
				if(i == 0) {
					second = goodGroupsDao.getTheMobileSecondSonGoods(Long.valueOf(map.get("entityid").toString()));
					for (Map<String, Object> map2 : second) {
						List<Map<String,Object>> three = goodGroupsDao.getTheMobileThreeSonGoods(Long.valueOf(map2.get("entityid").toString()), upload.getVirtual() + upload.getGoodGroupsPath());
						map2.put("three", three);
					}
					i++;
				}
				map.put("second",second);
			}
		}else {
			Map<String,Object> map = new HashMap<String, Object>();
			List<Map<String,Object>> second = goodGroupsDao.getTheMobileSecondSonGoods(entityid);
			for (Map<String, Object> map2 : second) {
				List<Map<String,Object>> three = goodGroupsDao.getTheMobileThreeSonGoods(Long.valueOf(map2.get("entityid").toString()), upload.getVirtual() + upload.getGoodGroupsPath());
				map2.put("three", three);
			}
			map.put("second",second);
			list.add(map);
		}
		return list;
	}
	/**
	 * 获取对应商品list
	 * @author Liangjing 2019年6月24日
	 * @param limitMin
	 * @param limitMax
	 * @param key
	 * @param goodgroupid
	 * @return
	 */
	public Map<String,Object> getTheSingleGoodsExtension(String limitMin, String limitMax, String key, Long goodgroupid) {
		Map<String,Object> map = new HashMap<String, Object>();
		List<SingleGoodsExtension> goodlist = goodSalesDao.getTheSingleGoodsExtension(Integer.valueOf(limitMin), Integer.valueOf(limitMax), key, goodgroupid, upload.getVirtual() + upload.getGoodPhotosPath(), null, null, null);
//		String goodgroupname = goodGroupsDao.getTheGoodGroupNameByEntityid(goodgroupid);
//		map.put("groupname", goodgroupname);
		map.put("goodlist", goodlist);
		return map;
	}
	
	/**
	 * 刷新手机商品列表页面
	 * @author Liangjing 2019年6月26日
	 * @param limitMin
	 * @param limitMax
	 * @param key
	 * @param goodgroupid
	 * @return
	 */
	public Map<String,Object> refreshmobilemainlist(String limitMin, String limitMax, String key, Long goodgroupid, String classSearch, String merchatSearch, String marketSearch) {
		Map<String,Object> map = new HashMap<String, Object>();
		List<SingleGoodsExtension> goodlist = goodSalesDao.getTheSingleGoodsExtension(Integer.valueOf(limitMin), Integer.valueOf(limitMax), key, goodgroupid, upload.getVirtual() + upload.getGoodPhotosPath(), CommonUtil.toLongArrays(classSearch), CommonUtil.toLongArrays(merchatSearch), CommonUtil.toLongArrays(marketSearch));
		map.put("goodlist", goodlist);
		return map;
	}
	/**
	 * 获取手机商品list-加载更多商品
	 * @author Liangjing 2019年6月25日
	 * @param limitMin
	 * @param limitMax
	 * @param goodgroupid
	 * @return
	 */
	public List<SingleGoodsExtension> getTheSingleGoodsList(String limitMin, String limitMax, Long goodgroupid, String classSearch, String merchatSearch, String marketSearch){
		return goodSalesDao.getTheSingleGoodsExtension(Integer.valueOf(limitMin), Integer.valueOf(limitMax), null, goodgroupid, upload.getVirtual() + upload.getGoodPhotosPath(), CommonUtil.toLongArrays(classSearch), CommonUtil.toLongArrays(merchatSearch), CommonUtil.toLongArrays(marketSearch));
	}
	
	/**
	 * 获取对应商品详情数据
	 * @author Liangjing 2019年7月2日
	 * @param goodentityid
	 * @param merchantsettleid
	 * @return
	 */
	public Map<String,Object> getTheGoodInfos(Long goodentityid,Long merchantsettleid){
		Map<String,Object> map = goodSalesDao.getTheGoodInfos(goodentityid, merchantsettleid);
		if(!CommonUtil.isEmpty(map.get("gooddesc"))) {
			map.put("gooddesc", richTextEditorUtil.replaceVisitImgPathUtil(map.get("gooddesc").toString()));
		}
		map.put("imgs", goodSalesDao.getTheGoodsAllPhotos(goodentityid, upload.getVirtual() + upload.getGoodPhotosPath()));
		return map;
	}
	
	/**
	 * 商品加载购物车并返回页面是否修改购物车上的数字
	 * @author Liangjing 2019年7月2日
	 * @param goodsalesid
	 * @return
	 */
	public boolean updateGoodIntoTheShoppingCartsBackIsAddNum(Long memberid,Long goodsalesid, Double num) {
		//删除购物车立即购买数据
		shoppingCartsDao.deleteShoppingCartsBuyNowData(memberid);
		
		boolean isadd = false;
		Shoppingcarts shoppingcarts = shoppingCartsDao.findByMemberidAndSaleid(memberid, goodsalesid);
		if(shoppingcarts != null) {
			shoppingcarts.setGoodnumber(shoppingcarts.getGoodnumber() + num);
		}else {
			isadd = true;
			shoppingcarts = new Shoppingcarts(EntityState.VALID.getValue(), memberid, goodsalesid, num, false, false);
			shoppingCartsDao.save(shoppingcarts);
		}
		return isadd;
	}
	
	/**
	 * 手机商品立即购买
	 * @author Liangjing 2019年7月22日
	 * @param goodsalesid
	 * @param num
	 */
	public void updatePayGoods(Long memberid, Long goodsalesid, Double num) {
		Shoppingcarts shoppingcarts = shoppingCartsDao.getTheShoppingCartsBuyNowIsTrue(memberid);
		if(shoppingcarts != null) {
			if(shoppingcarts.getEntityid() != goodsalesid) {
				//删除购物车立即购买数据
				shoppingCartsDao.deleteShoppingCartsBuyNowData(memberid);
				Shoppingcarts shoppingcartsNew = new Shoppingcarts(EntityState.VALID.getValue(), memberid, goodsalesid, num, true, true);
				shoppingCartsDao.save(shoppingcartsNew);
			}else {
				shoppingcarts.setGoodnumber(num);
			}
		}else {
			shoppingcarts = new Shoppingcarts(EntityState.VALID.getValue(), memberid, goodsalesid, num, true, true);
			shoppingCartsDao.save(shoppingcarts);
		}
	}
	
	/**
	 * 获取当前会员购物车数据
	 * @author Liangjing 2019年7月4日
	 * @param memberid
	 * @return
	 */
	public Long getTheUserGoodCarNum(Long memberid) {
		Long i = shoppingCartsDao.getTheUserGoodCarNum(memberid);
		return i != null && i > 0 ? i : 0;
	}
	/**
	 * 获取对应所有的筛选类型
	 * @author Liangjing 2019年7月7日
	 * @param goodgroupid
	 * @return
	 */
	public Map<String,Object> getTheAllFilterConditions(Long goodgroupid, String key){
		Map<String,Object> map = new HashMap<String, Object>();
		List<MerchantsFilterConditions> merchantList = merchantsDao.getTheAllMerchantFilterConditions(goodgroupid, key);
		List<Map<String,Object>> marketList = marketsDao.getTheAllMarketFilterConditions(goodgroupid, key);
		List<Map<String,Object>> goodgroupList = goodGroupsDao.getTheAllGoodGroupFilterConditions(goodgroupid, key);
		map.put("merchantList", merchantList);
		map.put("marketList", marketList);
		map.put("goodgroupList", goodgroupList);
		return map;
	}
	
	/**
	 * 获取对应所有商品规格
	 * @author Liangjing 2019年7月19日
	 * @param goodid
	 * @param merchantSettleid
	 * @return
	 */
	public List<Map<String,Object>> geTheAllGoodSpecs(Long goodid,Long merchantsettleid){
		return goodSalesDao.geTheAllGoodSpecs(goodid, merchantsettleid, upload.getVirtual() + upload.getGoodPhotosPath());
	}
}
