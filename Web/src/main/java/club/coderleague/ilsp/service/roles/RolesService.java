/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RolesService.java
 * History:
 *         2019年5月18日: Initially created, CJH.
 */
package club.coderleague.ilsp.service.roles;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.exception.MessageInfoException;
import club.coderleague.ilsp.dao.RoleAuthsDao;
import club.coderleague.ilsp.dao.RolesDao;
import club.coderleague.ilsp.entities.Roleauths;
import club.coderleague.ilsp.entities.Roles;

/**
 * 角色Service
 * 
 * @author CJH
 */
@Service
public class RolesService {
	/**
	 * 角色Dao
	 */
	private @Autowired RolesDao rolesDao;
	
	/**
	 * 角色授权Dao
	 */
	private @Autowired RoleAuthsDao roleAuthsDao;

	/**
	 * 分页查询角色
	 * 
	 * @author CJH 2019年7月2日
	 * @param pagingparameters 分页参数
	 * @param isrecycle 是否回收站
	 * @param keyword 关键字
	 * @return 角色
	 */
	public Page<Roles> findPageByParamsMap(PagingParameters pagingparameters, boolean isrecycle, String keyword) {
		return rolesDao.findPageByParamsMap(pagingparameters, isrecycle, keyword);
	}

	/**
	 * 新增角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param roles 角色
	 * @param funcids 功能主键
	 */
	public void insert(Roles roles, String funcids) {
		if (rolesDao.existsByRolenameNotEntityid(roles.getRolename(), null)) {
			throw new MessageInfoException("角色名称已存在");
		}
		roles.setEntitystate(EntityState.VALID.getValue());
		rolesDao.save(roles);
		
		if (StringUtils.isNotBlank(funcids)) {
			for (String funcidstr : funcids.split(",")) {
				Long funcid = Long.parseLong(funcidstr);
				roleAuthsDao.save(new Roleauths(EntityState.VALID.getValue(), roles.getEntityid(), funcid));
			}
		}
	}

	/**
	 * 更新角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param roles 角色
	 * @param funcids 功能主键
	 */
	public void update(Roles roles, String funcids) {
		if (rolesDao.existsByRolenameNotEntityid(roles.getRolename(), roles.getEntityid())) {
			throw new MessageInfoException("角色名称已存在");
		}
		Roles rolesSource = rolesDao.getOne(roles.getEntityid());
		rolesSource.setRolename(roles.getRolename());
		rolesSource.setRoledesc(roles.getRoledesc());
		
		List<Long> validentityids = new ArrayList<>();
		if (StringUtils.isNotBlank(funcids)) {
			for (String funcidstr : funcids.split(",")) {
				Long funcid = Long.parseLong(funcidstr);
				Roleauths roleauths = roleAuthsDao.findOneByRoleidAndFuncid(roles.getEntityid(), funcid);
				if (roleauths != null) {
					if (EntityState.INVALID.equalsValue(roleauths.getEntitystate())) {
						roleauths.setEntitystate(EntityState.VALID.getValue());
					}
					validentityids.add(roleauths.getEntityid());
				} else {
					roleauths = new Roleauths(EntityState.VALID.getValue(), roles.getEntityid(), funcid);
					roleAuthsDao.save(roleauths);
					validentityids.add(roleauths.getEntityid());
				}
			}
		}
		roleAuthsDao.updateEntitystateByRoleidNotInEntityids(EntityState.INVALID.getValue(), roles.getEntityid(), validentityids);
	}

	/**
	 * 根据角色主键查询角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityid 角色主键
	 * @return 角色
	 */
	public Map<String, Object> findMapByEntityid(Long entityid) {
		if (entityid == null) {
			throw new ValidationException("角色主键不能为空");
		}
		Map<String, Object> roles = rolesDao.findMapByEntityid(entityid);
		List<String> funcids = roleAuthsDao.findFuncidByRoleid(entityid);
		roles.put("funcids", funcids != null ? StringUtils.join(funcids, ",") : null);
		return roles;
	}
	
	/**
	 * 更新角色状态
	 * 
	 * @author CJH 2019年5月20日
	 * @param entityids 角色主键
	 * @param entitystate 状态
	 * @param isdistinct 是否去重
	 */
	public void updateEntitystate(String entityids, Integer entitystate, boolean isdistinct) {
		if (StringUtils.isBlank(entityids)) {
			throw new ValidationException("角色主键不能为空");
		}
		for (String entityid : entityids.split(",")) {
			Roles roles = rolesDao.getOne(Long.parseLong(entityid));
			if (roles == null) {
				continue;
			}
			if (isdistinct && rolesDao.existsByRolenameNotEntityid(roles.getRolename(), roles.getEntityid())) {
				throw new MessageInfoException("角色名称已存在");
			}
			roles.setEntitystate(entitystate);
		}
	}

	/**
	 * 根据角色主键查询授权功能
	 * 
	 * @author CJH 2019年5月21日
	 * @param roleid 角色主键
	 * @return 功能
	 */
	public List<Map<String, Object>> findAuthFunctionsByRoleid(Long roleid) {
		if (roleid == null) {
			throw new ValidationException("角色主键不能为空");
		}
		return rolesDao.findAuthFunctionsByRoleid(roleid);
	}
}
