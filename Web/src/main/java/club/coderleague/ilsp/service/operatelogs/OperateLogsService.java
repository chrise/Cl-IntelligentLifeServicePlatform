/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OperateLogsService.java
 * History:
 *         2019年5月11日: Initially created, CJH.
 */
package club.coderleague.ilsp.service.operatelogs;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.OperateLogsExtension;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.dao.OperateLogsDao;
import club.coderleague.ilsp.entities.Operatelogs;

/**
 * 操作日志Service
 * 
 * @author CJH
 */
@Service
public class OperateLogsService {
	
	/**
	 * 操作日志Dao
	 */
	private @Autowired OperateLogsDao operateLogsDao;
	
	/**
	 * 保存操作日志
	 * 
	 * @author CJH 2019年5月11日
	 * @param operatelogs 操作日志
	 */
	public void save(Operatelogs operatelogs) {
		operateLogsDao.save(operatelogs);
	}

	/**
	 * 分页查询操作日志
	 * 
	 * @author CJH 2019年7月2日
	 * @param pagingparameters 分页参数
	 * @param keyword 关键字
	 * @param starttime 开始时间
	 * @param endtime 结束时间
	 * @return 操作日志
	 */
	public Page<OperateLogsExtension> findPageByParamsMap(PagingParameters pagingparameters, String keyword, String starttime, String endtime) {
		return operateLogsDao.findPageByParamsMap(pagingparameters, keyword, starttime, endtime);
	}

	/**
	 * 根据操作日志主键查询操作日志
	 * 
	 * @author CJH 2019年5月21日
	 * @param entityid 操作日志主键
	 * @return 操作日志
	 */
	public OperateLogsExtension findExtensionByEntityid(Long entityid) {
		if (entityid == null) {
			throw new ValidationException("操作日志主键不能为空");
		}
		return operateLogsDao.findExtensionByEntityid(entityid);
	}
}
