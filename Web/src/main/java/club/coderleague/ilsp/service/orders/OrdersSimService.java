/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrdersSimService.java
 * History:
 *         2019年8月22日: Initially created, CJH.
 */
package club.coderleague.ilsp.service.orders;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.dao.GoodSalesDao;
import club.coderleague.ilsp.dao.MarketsDao;
import club.coderleague.ilsp.dao.MerchantsDao;
import club.coderleague.ilsp.dao.OrdersDao;
import club.coderleague.security.AlgorithmBeanFactory;
import club.coderleague.security.algorithm.AESEncryptor;
import club.coderleague.security.algorithm.MD5Signer;
import club.coderleague.security.support.CiphertextMode;
import club.coderleague.security.support.IllegalStringException;

/**
 * 订单模拟服务
 * 
 * @author CJH
 */
@Service
@ConditionalOnExpression("'${spring.profiles.active}'.indexOf('release') == -1")
public class OrdersSimService {
	
	private @Autowired MarketsDao marketsDao;
	
	private @Autowired MerchantsDao merchantsDao;

	private @Autowired GoodSalesDao goodSalesDao;
	
	private @Autowired OrdersDao ordersDao;
	
	/**
	 * 查询市场、商户和商品
	 * 
	 * @author CJH 2019年8月21日
	 * @return 市场、商户和商品
	 */
	public List<Map<String, Object>> findMarketsData() {
		List<Map<String, Object>> markets = marketsDao.findAllMap();
		if (markets != null && !markets.isEmpty()) {
			for (Map<String, Object> market : markets) {
				Long marketid = Long.valueOf(market.get("entityid").toString());
				List<Map<String, Object>> merchants = merchantsDao.findByMarketid(marketid);
				if (merchants != null && !merchants.isEmpty()) {
					for (Map<String, Object> merchant : merchants) {
						Long settleid = Long.valueOf(merchant.get("entityid").toString());
						try {
							String password = AlgorithmBeanFactory.getAlgorithmBean(MD5Signer.class).sign(merchant.get("merchantid").toString());
							String accountname = merchant.get("accountname").toString();
							String settlestall = merchant.get("settlestall").toString();
							String decryptaccountname = AlgorithmBeanFactory.getAlgorithmBean(AESEncryptor.class).decrypt(accountname, password, CiphertextMode.BASE64);
							decryptaccountname += "[" + settlestall + "]";
							merchant.put("accountname", decryptaccountname);
						} catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | IllegalStringException e) {
							e.printStackTrace();
						}
						List<Map<String, Object>> goods = goodSalesDao.findBySettleid(settleid);
						merchant.put("goods", goods);
					}
				}
				market.put("merchants", merchants);
			}
		}
		return markets;
	}
	
	/**
	 * 查询订单详情
	 * 
	 * @author CJH 2019年8月22日
	 * @param ordersid 订单标识
	 * @return 订单详情
	 */
	public Map<String, Object> findOrdersInfo(Long ordersid) {
		Map<String, Object> orders = ordersDao.findOrdersInfo(ordersid);
		Object memberidobj = orders.get("memberid");
		String memberid = memberidobj != null ? memberidobj.toString() : null;
		Object memberphoneobj = orders.get("memberphone");
		String memberphone = memberphoneobj != null ? memberphoneobj.toString() : null;
		if (StringUtils.isNoneBlank(memberid, memberphone)) {
			try {
				String password = AlgorithmBeanFactory.getAlgorithmBean(MD5Signer.class).sign(memberid);
				String memberphonedecrypt = AlgorithmBeanFactory.getAlgorithmBean(AESEncryptor.class).decrypt(memberphone, password, CiphertextMode.BASE64);
				orders.put("memberphone", memberphonedecrypt);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return orders;
	}
}
