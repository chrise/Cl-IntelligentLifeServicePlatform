/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSalesMgrService.java
 * History:
 *         2019年6月11日: Initially created, wangjb.
 */
package club.coderleague.ilsp.service.goodsales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.GoodSaleSettleIdExtension;
import club.coderleague.ilsp.common.domain.beans.GoodSalesExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.GoodSalesDao;
import club.coderleague.ilsp.dao.MerchantSettlesDao;
import club.coderleague.ilsp.entities.Goodsales;
import club.coderleague.ilsp.entities.Merchantsettles;

/**
 * 商品销售管理业务处理。
 * @author wangjb
 */
@Service
public class GoodSalesService {
	
	/**
	 * 商品销售管理数据访问对象。
	 */
	@Autowired GoodSalesDao goodSalesDao;
	
	/**
	 * 商户入驻数据访问对象。
	 */
	@Autowired MerchantSettlesDao settlesDao;

	/**
	 * 查询商户商品销售表。
	 * @author wangjb 2019年6月11日。
	 * @param params 页面查询参数。
	 * @return
	 */
	public Page<GoodSalesExtension> getGoodSalesMgrService(Map<String, Object> params) throws Exception{
		Page<GoodSalesExtension> result = this.goodSalesDao.getGoodSalesMgrDao(params);
		return result;
	}
	
	/**
	 * 根据入驻id查询入驻下的所有商品规格。
	 * @author wangjb 2019年6月14日。
	 * @param settleid 入驻id。
	 * @return
	 */
	public List<Map<String, Object>> getGoodSalesBySettleidService(Long settleid) {
		List<Map<String, Object>> resut = this.goodSalesDao.getGoodSalesBySettleidDao(settleid);
		return resut;
	}

	/**
	 * 获取商户入驻标识。
	 * @author wangjb 2019年6月12日。
	 * @return
	 */
	public List<Map<String, Object>> getSettleIdService() {
		List<GoodSaleSettleIdExtension> list = this.goodSalesDao.getSettleIdDao();
		List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		Map<String, Object> map = null;
		for (GoodSaleSettleIdExtension obj : list) {
			map = new HashMap<String, Object>();
			map.put("id", obj.getSettleid());
			map.put("mid", obj.getMid());
			map.put("name", obj.getMerchantname() + "【" + obj.getMarketname() + "】");
			result.add(map);
		}
		return result;
	}

	/**
	 * 根据商户id查询商品。
	 * @author wangjb 2019年6月12日。
	 * @param mid 商户id。
	 * @param settleid 市场id。
	 * @return
	 */
	public List<Map<String, Object>> getGoodByMerchantIdService(Long mid, Long settleid) {
		List<Map<String, Object>> result = this.goodSalesDao.getGoodByMerchantIdDao(mid, settleid);
		return result;
	}

	/**
	 * 修改商品销售对象数据。
	 * @author wangjb 2019年6月14日。
	 * @param result 返回操作提示。
	 * @param settleid 入驻标识。
	 * @param del 删除的数据。
	 * @param specid 规格标识。
	 */
	public void updateGoodSalesObjService(Map<String, Object> result, Long settleid, String specid, String del) {
		if (del != null && !"".equals(del)) { // 修改删除销售的状态。
			for (String id : del.split(",")) {
				Goodsales sale = this.goodSalesDao.getOne(Long.valueOf(id));
				sale.setEntitystate(EntityState.INVALID.getValue());
			}
		}
		Merchantsettles settleserials = this.settlesDao.getOne(settleid); // 获取入驻序号。
		String serial = Long.toString(settleserials.getSettleserials(), 36).toUpperCase();
		Integer goodserials = this.goodSalesDao.getGoodSerialsMaxValBySettleidDao(settleid); // 获取商品序号最大值。
		if (specid != null && !"".equals(specid)) {
			for (String id : specid.split(",")) {
				Goodsales sale = this.goodSalesDao.getGoodSalesObjBySettleIdAndSpecIdDao(settleid, Long.valueOf(id));
				if (sale != null) {
					sale.setEntitystate(EntityState.VALID.getValue());
					sale.setGoodprice(null);
					sale.setGoodstock(null);
					continue;
				}
				String goodnumber = serial + String.format("%04d", goodserials);
				sale = new Goodsales();
				sale.setGoodstock(null);
				sale.setSettleid(settleid);
				sale.setGoodnumber(goodnumber);
				sale.setGoodserials(goodserials);
				sale.setSpecid(Long.valueOf(id));
				sale.setEntitystate(EntityState.VALID.getValue());
				this.goodSalesDao.save(sale);
				goodserials += 1;
			}
		}
		
		result.put("state", true);
		result.put("msg", "操作成功!");
	}
	
	/**
	 * 根据入驻id获取商品销售详情。
	 * @author wangjb 2019年6月14日。
	 * @param param 页面查询详情。
	 * @return
	 */
	public List<Map<String, Object>> getGoodSalesDetailBySettleidService(Map<String, Object> param) {
		List<Map<String, Object>> result = this.goodSalesDao.getGoodSalesDetailBySettleidDao(param);
		return result;
	}

}
