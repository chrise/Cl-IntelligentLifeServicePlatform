/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：NotifyTemplateConfigsServer.java
 * History:
 *         2019年6月10日: Initially created, Liangj.
 */
package club.coderleague.ilsp.service.notifytemplateconfigs;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.ResultMsg;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.NotifyTemplateConfigDao;
import club.coderleague.ilsp.entities.Notifytemplateconfigs;

/**
 * 通过模板
 * @author Liangjing
 */
@Service
public class NotifyTemplateConfigsServer {

	@Autowired
	NotifyTemplateConfigDao notifyTemplateConfigDao;
	
	/**
	 * 获取所有的通知模板
	 * @author Liangjing 2019年6月10日
	 * @param key
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public Page<Notifytemplateconfigs> getTheAllTemplate(String key,boolean isrecycle,String pageIndex,String pageSize) {
		return notifyTemplateConfigDao.getTheAllTemplate(key, isrecycle, Integer.valueOf(pageIndex), Integer.valueOf(pageSize));
	}
	/**
	 * 获取通知模板编辑页面数据
	 * @author Liangjing 2019年6月10日
	 * @param entityid
	 * @return
	 */
	public Notifytemplateconfigs getTheEditPageData(Long entityid) {
		return notifyTemplateConfigDao.findByEntityid(entityid);
	}
	/**
	 * 获取通知模板查看详情页面数据
	 * @author Liangjing 2019年6月10日
	 * @param entityid
	 * @return
	 */
	public Map<String,Object> getTheLookInfoPageData(Long entityid) {
		return notifyTemplateConfigDao.getTheLookInfoPageData(entityid);
	}
	/**
	 * 更新通知模板
	 * @author Liangjing 2019年6月10日
	 * @param notifytemplateconfigs
	 * @return
	 */
	public ResultMsg updateTheTemplate(Notifytemplateconfigs notifytemplateconfigs) {
		Notifytemplateconfigs notifytemplateconfigsValidate = notifyTemplateConfigDao.findByNotifytypeAndEntitystate(notifytemplateconfigs.getNotifytype(), EntityState.VALID.getValue());
		if(CommonUtil.isEmpty(notifytemplateconfigs.getEntityid())) {
			if(notifytemplateconfigsValidate != null) {
				return new ResultMsg(false, "该类型的模板已经存在!");
			}
			Notifytemplateconfigs notifytemplateconfigsnew = new Notifytemplateconfigs(EntityState.VALID.getValue(), notifytemplateconfigs.getNotifytype(), notifytemplateconfigs.getTplcode(), notifytemplateconfigs.getTplparam());
			notifyTemplateConfigDao.save(notifytemplateconfigsnew);
		}else {
			if(notifytemplateconfigsValidate != null) {
				if(notifytemplateconfigsValidate.getEntityid().equals(notifytemplateconfigs.getEntityid())) {
					notifytemplateconfigsValidate.setNotifytype(notifytemplateconfigs.getNotifytype());
					notifytemplateconfigsValidate.setTplcode(notifytemplateconfigs.getTplcode());
					notifytemplateconfigsValidate.setTplparam(notifytemplateconfigs.getTplparam());
				}else {
					return new ResultMsg(false, "该类型的模板已经存在!");
				}
			}else {
				Notifytemplateconfigs notifytemplateconfigsEdit = notifyTemplateConfigDao.findByEntityid(notifytemplateconfigs.getEntityid());
				notifytemplateconfigsEdit.setNotifytype(notifytemplateconfigs.getNotifytype());
				notifytemplateconfigsEdit.setTplcode(notifytemplateconfigs.getTplcode());
				notifytemplateconfigsEdit.setTplparam(notifytemplateconfigs.getTplparam());
			}
		}
		return new ResultMsg(true, "操作成功!");
	}
	/**
	 * 更新通知模板状态
	 * @author Liangjing 2019年6月11日
	 * @param entityids
	 * @param istate
	 * @return
	 */
	public ResultMsg updateTheTemplateIstate(List<Long> entityids,Integer istate) {
		if(istate == EntityState.VALID.getValue()) {
			for (Long entityid : entityids) {
				Notifytemplateconfigs notifytemplateconfigs = notifyTemplateConfigDao.findByEntityid(entityid);
				Notifytemplateconfigs notifytemplateconfigsValidate = notifyTemplateConfigDao.findByNotifytypeAndEntitystate(notifytemplateconfigs.getNotifytype(), EntityState.VALID.getValue());
				if(notifytemplateconfigsValidate != null) {
					return new ResultMsg(false, "恢复数据存在重复!");
				}
			}
		}
		for (Long entityid : entityids) {
			Notifytemplateconfigs notifytemplateconfigs = notifyTemplateConfigDao.findByEntityid(entityid);
			notifytemplateconfigs.setEntitystate(istate);
		}
		return new ResultMsg(true, "操作成功!");
	}
}
