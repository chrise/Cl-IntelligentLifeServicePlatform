/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesService.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.service.merchantsettles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.MerchantSettlesPageBean;
import club.coderleague.ilsp.common.domain.beans.ResultMsg;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.MerchantSettlesDao;
import club.coderleague.ilsp.entities.Merchantsettles;

/**
 * 商户入驻
 * @author Liangjing
 */
@Service
public class MerchantSettlesService {

	@Autowired
	MerchantSettlesDao merchantSettlesDao;
	
	/**
	 * 获取所有的商户入驻数据
	 * @author Liangjing 2019年5月24日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @param istate
	 * @return
	 */
	public Page<MerchantSettlesPageBean> getTheAllMerchantSettles(String pageIndex,String pageSize,String key,String istate) {
		return merchantSettlesDao.getTheAllMerchantSettles(Integer.valueOf(pageIndex), Integer.valueOf(pageSize), key, istate);
	}
	
	/**
	 * 获取商户入驻编辑页面加载数据
	 * @author Liangjing 2019年5月24日
	 * @param entityid
	 * @return
	 */
	public Merchantsettles getTheMerchantSettlesEditPageData(Long entityid) {
		return merchantSettlesDao.findByEntityid(entityid);
	}
	
	/**
	 * 获取商户入驻查看详情页面加载数据
	 * @author Liangjing 2019年5月24日
	 * @param entityid
	 * @return
	 */
	public MerchantSettlesPageBean getTheMerchantSettlesLookInfoPageData(Long entityid) {
		return merchantSettlesDao.getTheMerchantSettlesLookInfoPageData(entityid);
	}
	
	/**
	 * 更新商户入驻
	 * @author Liangjing 2019年5月24日
	 * @param merchantsettles
	 * @return
	 */
	public ResultMsg updateTheMerchantSettles(Merchantsettles merchantsettles) {
		Merchantsettles merchantsettlesValidate = merchantSettlesDao.findByMarketidAndSettlestallAndEntitystate(merchantsettles.getMarketid(), merchantsettles.getSettlestall(), EntityState.VALID.getValue());
		if(CommonUtil.isEmpty(merchantsettles.getEntityid())) {
			if(merchantsettlesValidate != null) {
				return new ResultMsg(false, "该摊位已存在!");
			}else {
				Merchantsettles merchantsettlesNew = new Merchantsettles(EntityState.VALID.getValue(), merchantsettles.getMarketid(), merchantsettles.getMerchantid(), merchantSettlesDao.getTheMaxMerchantsNumber(), merchantsettles.getSettlestall());
				merchantSettlesDao.save(merchantsettlesNew);
			}
		}else {
			if(merchantsettlesValidate != null) {
				if(merchantsettlesValidate.getEntityid() != merchantsettles.getEntityid()) {
					return new ResultMsg(false, "该摊位已存在!");
				}else {
					merchantsettlesValidate.setMarketid(merchantsettles.getMarketid());
					merchantsettlesValidate.setMerchantid(merchantsettles.getMerchantid());
					merchantsettlesValidate.setSettlestall(merchantsettles.getSettlestall());
				}
			}else {
				Merchantsettles merchantsettlesEdit = merchantSettlesDao.findByEntityid(merchantsettles.getEntityid());
				merchantsettlesEdit.setMarketid(merchantsettles.getMarketid());
				merchantsettlesEdit.setMerchantid(merchantsettles.getMerchantid());
				merchantsettlesEdit.setSettlestall(merchantsettles.getSettlestall());
			}
		}
		return new ResultMsg(true, "操作成功!");
	}
	
	/**
	 * 修改商户入驻对象状态
	 * @author Liangjing 2019年5月24日
	 * @param entityids
	 * @param istate
	 * @return
	 */
	public ResultMsg updateTheMerchantSettlesIstate(String entityids,Integer istate) {
		if(!CommonUtil.isEmpty(entityids)) {
			if(istate == EntityState.VALID.getValue()) {
				for (Long entityid : CommonUtil.toLongArrays(entityids)) {
					Merchantsettles merchantsettles = merchantSettlesDao.findByEntityid(entityid);
					Merchantsettles merchantsettlesValidate = merchantSettlesDao.findByMarketidAndSettlestallAndEntitystate(merchantsettles.getMarketid(), merchantsettles.getSettlestall(), EntityState.VALID.getValue());
					if(merchantsettlesValidate != null) {
						return new ResultMsg(false, "恢复数据中的摊位已存在!");
					}
				}
			}
			for (Long entityid : CommonUtil.toLongArrays(entityids)) {
				Merchantsettles merchantsettles = merchantSettlesDao.findByEntityid(entityid);
				merchantsettles.setEntitystate(istate);
			}
		}
		return new ResultMsg(true, "操作成功!");
	}
	
	/**
	 * 获取所有的市场
	 * @author Liangjing 2019年5月24日
	 * @return
	 */
	public List<Map<String,Object>> getTheAllMarkets() {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("id", "-1");
		map.put("name", "请选择");
		List<Map<String,Object>> list = merchantSettlesDao.getTheAllMarkets();
		list.add(0, map);
		return list;
	}
	
	/**
	 * 获取所有的商户
	 * @author Liangjing 2019年5月24日
	 * @return
	 */
	public List<Map<String,Object>> getTheAllMerchants() {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("id", "-1");
		map.put("name", "请选择");
		List<Map<String,Object>> list = merchantSettlesDao.getTheAllMerchants();
		list.add(0, map);
		return list;
	}
}
