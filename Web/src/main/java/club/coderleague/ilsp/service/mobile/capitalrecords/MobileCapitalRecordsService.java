/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：CapitalRecordsService.java
 * History:
 *         2019年8月3日: Initially created, wangjb.
 */
package club.coderleague.ilsp.service.mobile.capitalrecords;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.enums.CapitalChangedReason;
import club.coderleague.ilsp.dao.CapitalRecordsDao;

/**
 * -WeChat 资金记录业务处理。
 * @author wangjb
 */
@Service
public class MobileCapitalRecordsService {

	/**
	 * -WeChat 资金记录访问数据接口。
	 */
	@Autowired CapitalRecordsDao CRD;

	/**
	 * -WeChat 根据商户id获取资金记录。
	 * @author wangjb 2019年8月3日。
	 * @param merchantid 商户id。
	 * @param params 页面查询参数。
	 * @return
	 */
	public Map<String, Object> getCapitalRecordListByMerchantidSetvice(Long merchantid, Map<String, Object> params) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Map<String, Object>> recordList = this.CRD.getCapitalRecordListByMerchantidDao(merchantid, params);
		for (Map<String, Object> map : recordList) {
			if (map.get("recordtime") != null && StringUtils.isNotBlank(map.get("recordtime").toString())) {
				map.put("recordtime", new SimpleDateFormat("yyyy-MM-dd").format(map.get("recordtime")));
	        }
		}
		result.put("recordList", recordList);
		return result;
	}

	/**
	 * -根据记录id获取记录详情。
	 * @author wangjb 2019年8月3日。
	 * @param recordid 记录id。
	 * @return
	 */
	public Map<String, Object> getRecordDetailByRecordidService(Long recordid) {
		Map<String, Object> record = this.CRD.getRecordDetailByRecordidDao(recordid);
		// 记录总额。
		if (record.get("totalamount") != null && StringUtils.isNotBlank(record.get("totalamount").toString()) && 
				!".00".equals(record.get("totalamount").toString()) && !"0.0".equals(record.get("totalamount").toString()) && !"0.00".equals(record.get("totalamount").toString())) {
			record.put("totalamount", new DecimalFormat("#.00").format(record.get("totalamount")));
		} else record.put("totalamount", "0.00");
		// 变化金额。
		if (record.get("changedamount") != null && StringUtils.isNotBlank(record.get("changedamount").toString()) && 
				!".00".equals(record.get("changedamount").toString()) && !"0.0".equals(record.get("changedamount").toString()) && !"0.00".equals(record.get("changedamount").toString())) {
			record.put("changedamount", new DecimalFormat("#.00").format(record.get("changedamount")));
		} else record.put("changedamount", "0.00");
		// 提成费率。
		if (record.get("royaltyrate") != null && StringUtils.isNotBlank(record.get("royaltyrate").toString()) && 
				!".00".equals(record.get("royaltyrate").toString()) && !"0.0".equals(record.get("royaltyrate").toString()) && !"0.00".equals(record.get("royaltyrate").toString())) {
			record.put("royaltyrate", new DecimalFormat("#.00").format(record.get("royaltyrate")));
		} else record.put("royaltyrate", "0.00");
		// 提成金额。
		if (record.get("royaltyamount") != null && StringUtils.isNotBlank(record.get("royaltyamount").toString()) && 
				!".00".equals(record.get("royaltyamount").toString()) && !"0.0".equals(record.get("royaltyamount").toString()) && !"0.00".equals(record.get("royaltyamount").toString())) {
			record.put("royaltyamount", new DecimalFormat("#.00").format(record.get("royaltyamount")));
		} else record.put("royaltyamount", "0.00");
		// 记录时间。
		if (record.get("recordtime") != null && StringUtils.isNotBlank(record.get("recordtime").toString())) {
			record.put("recordtime", new SimpleDateFormat("yyyy-MM-dd").format(record.get("recordtime")));
        }
		// 变化原因。
		if (record.get("changedreason") != null && StringUtils.isNotBlank(record.get("changedreason").toString())) {
			if (CapitalChangedReason.ORDER_REVENUE.getValue().equals(record.get("changedreason"))) record.put("reaso", CapitalChangedReason.ORDER_REVENUE.getText());
			else if (CapitalChangedReason.ORDER_REFUND.getValue().equals(record.get("changedreason"))) record.put("reaso", CapitalChangedReason.ORDER_REFUND.getText());
			else if (CapitalChangedReason.SETTLE_ACCOUNTS.getValue().equals(record.get("changedreason"))) record.put("reaso", CapitalChangedReason.SETTLE_ACCOUNTS.getText());
			else if (CapitalChangedReason.PAY.getValue().equals(record.get("changedreason"))) record.put("reaso", CapitalChangedReason.PAY.getText());
        }
		return record;
	}
	
}
