/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SystemConfigService.java
 * History:
 *         2019年5月23日: Initially created, Chrise.
 */
package club.coderleague.ilsp.service.systemconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.cache.CacheManager;
import club.coderleague.ilsp.common.domain.beans.ResultMsg;
import club.coderleague.ilsp.common.domain.beans.SystemConfig;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.SystemConfigDao;
import club.coderleague.ilsp.entities.Systemconfigs;

/**
 * 系统配置服务。
 * @author Chrise
 */
@Service
public class SystemConfigService {
	@Autowired
	private SystemConfigDao configDao;
	@Autowired
	CacheManager cacheManager;
	
	/**
	 * 查询缓存封装类。
	 * @author Chrise 2019年5月23日
	 * @return 缓存封装类对象。
	 */
	public SystemConfig queryCacheBean() {
		return this.configDao.queryCacheBean();
	}
	
	/**
	 * 获取系统配置
	 * @author Liangjing 2019年6月1日
	 * @return
	 */
	public Systemconfigs getTheSystemConfigs() {
		return configDao.findByEntitystate(EntityState.VALID.getValue());
	}
	
	/**
	 * 修改系统配置
	 * @author Liangjing 2019年6月1日
	 * @param systemconfigs
	 * @return
	 */
	public ResultMsg updateTheSystemConfigs(Systemconfigs systemconfigs) {
		Systemconfigs systemconfigsOld = configDao.findByEntitystate(EntityState.VALID.getValue());
		if(systemconfigsOld != null) {
			systemconfigsOld.setConfirmtimeout(systemconfigs.getConfirmtimeout());
			systemconfigsOld.setPaytimeout(systemconfigs.getPaytimeout());
			systemconfigsOld.setReceivetimeout(systemconfigs.getReceivetimeout());
			systemconfigsOld.setRefundtimeout(systemconfigs.getRefundtimeout());
			systemconfigsOld.setIntegraltimeout(systemconfigs.getIntegraltimeout());
			systemconfigsOld.setDiamondtimeout(systemconfigs.getDiamondtimeout());
			systemconfigsOld.setVercodetimeout(systemconfigs.getVercodetimeout());
			systemconfigsOld.setWxswitch(systemconfigs.getWxswitch());
			systemconfigsOld.setWxappid(systemconfigs.getWxappid());
			systemconfigsOld.setWxappkey(systemconfigs.getWxappkey());
			systemconfigsOld.setWxmerid(systemconfigs.getWxmerid());
			systemconfigsOld.setWxapikey(systemconfigs.getWxapikey());
			systemconfigsOld.setWxcerfile(systemconfigs.getWxcerfile());
			systemconfigsOld.setWxvertoken(systemconfigs.getWxvertoken());
			systemconfigsOld.setWxverfile(systemconfigs.getWxverfile());
			systemconfigsOld.setWxwelcome(systemconfigs.getWxwelcome());
			systemconfigsOld.setApswitch(systemconfigs.getApswitch());
			systemconfigsOld.setApappid(systemconfigs.getApappid());
			systemconfigsOld.setAppubkey(systemconfigs.getAppubkey());
			systemconfigsOld.setApmerprikey(systemconfigs.getApmerprikey());
			systemconfigsOld.setAynotify(systemconfigs.getAynotify());
			systemconfigsOld.setAyappid(systemconfigs.getAyappid());
			systemconfigsOld.setAyappkey(systemconfigs.getAyappkey());
			systemconfigsOld.setAysign(systemconfigs.getAysign());
			systemconfigsOld.setDiadeductratio(systemconfigs.getDiadeductratio());
		}else {
			Systemconfigs systemconfigsNew = new Systemconfigs(EntityState.VALID.getValue(), systemconfigs.getConfirmtimeout(), systemconfigs.getPaytimeout(), systemconfigs.getReceivetimeout(), systemconfigs.getRefundtimeout(), systemconfigs.getIntegraltimeout(), systemconfigs.getDiamondtimeout(), systemconfigs.getVercodetimeout(), systemconfigs.getWxswitch(), systemconfigs.getWxappid(), systemconfigs.getWxappkey(), systemconfigs.getWxmerid(), systemconfigs.getWxapikey(), systemconfigs.getWxcerfile(), systemconfigs.getWxvertoken(), systemconfigs.getWxverfile(), systemconfigs.getWxwelcome(), systemconfigs.getApswitch(), systemconfigs.getApappid(), systemconfigs.getAppubkey(), systemconfigs.getApmerprikey(), systemconfigs.getAynotify(), systemconfigs.getAyappid(), systemconfigs.getAyappkey(), systemconfigs.getAysign(), systemconfigs.getDiadeductratio());
			configDao.save(systemconfigsNew);
		}
		//检查更新系统配置缓存
		this.restCacheConfig();
		
		ResultMsg msg = new ResultMsg(true, "操作成功!");
		return msg;
	}
	
	/**
	 * 检查更新系统配置缓存
	 * @author Liangjing 2019年7月25日
	 */
	private void restCacheConfig() {
		SystemConfig systemConfig = this.queryCacheBean();
		
		// 检查缓存
		if(cacheManager.exits(SystemConfig.CACHE_KEY, false)) {
			Object cache = cacheManager.getObject(SystemConfig.CACHE_KEY, false);
			if (systemConfig.equals(cache)) return;
		}
		
		// 更新系统配置缓存
		cacheManager.setObject(SystemConfig.CACHE_KEY, systemConfig);
	}
}
