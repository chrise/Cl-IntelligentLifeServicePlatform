/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AreasService.java
 * History:
 *         2019年5月24日: Initially created, CJH.
 */
package club.coderleague.ilsp.service.areas;

import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.exception.MessageInfoException;
import club.coderleague.ilsp.common.exception.MessageWarnException;
import club.coderleague.ilsp.dao.AreasDao;
import club.coderleague.ilsp.entities.Areas;

/**
 * 区域Service
 * 
 * @author CJH
 */
@Service
public class AreasService {
	/**
	 * 区域Dao
	 */
	private @Autowired AreasDao areasDao;

	/**
	 * 分页查询区域
	 * 
	 * @author CJH 2019年7月2日
	 * @param isrecycle 是否回收站
	 * @param keyword 关键字
	 * @return 区域
	 */
	public List<Areas> findAllByParamsMap(boolean isrecycle, String keyword) {
		return areasDao.findAllByParamsMap(isrecycle, keyword);
	}

	/**
	 * 查询所有区域
	 * 
	 * @author CJH 2019年5月24日
	 * @return 区域
	 */
	public List<Map<String, Object>> findAll() {
		return areasDao.findAllMap();
	}

	/**
	 * 新增区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param areas 区域
	 */
	public void insert(Areas areas) {
		if (areas.getParentarea() != null && areas.getParentarea() == -1) {
			areas.setParentarea(null);
		}
		if (areasDao.existsByAreanameAndParentareaNotEntityid(areas.getAreaname(), areas.getParentarea(), null)) {
			throw new MessageInfoException("区域名称已存在");
		}
		areas.setEntitystate(EntityState.VALID.getValue());
		areasDao.save(areas);
	}

	/**
	 * 更新区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param areas 区域
	 */
	public void update(Areas areas) {
		if (areas.getEntityid() == null) {
			throw new MessageWarnException("区域主键不能为空");
		}
		if (areas.getParentarea() != null && areas.getParentarea() == -1) {
			areas.setParentarea(null);
		}
		if (areasDao.existsByAreanameAndParentareaNotEntityid(areas.getAreaname(), areas.getParentarea(), areas.getEntityid())) {
			throw new MessageInfoException("区域名称已存在");
		}
		Areas areasSource = areasDao.getOne(areas.getEntityid());
		areasSource.setAreaname(areas.getAreaname());
		areasSource.setAreadesc(areas.getAreadesc());
		areasSource.setParentarea(areas.getParentarea());
	}

	/**
	 * 根据区域主键查询区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityid 区域主键
	 * @return 区域
	 */
	public Areas findOneByEntityid(Long entityid) {
		if (entityid == null) {
			throw new ValidationException("区域主键不能为空");
		}
		return areasDao.getOne(entityid);
	}

	/**
	 * 更新区域状态
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityids 区域主键
	 * @param entitystate 状态
	 * @param isdistinct 是否去重
	 */
	public void updateEntitystate(String entityids, Integer entitystate, boolean isdistinct) {
		if (StringUtils.isBlank(entityids)) {
			throw new ValidationException("区域主键不能为空");
		}
		for (String entityid : entityids.split(",")) {
			Areas areas = areasDao.getOne(Long.parseLong(entityid));
			if (areas == null) {
				continue;
			}
			if (isdistinct && areasDao.existsByAreanameAndParentareaNotEntityid(areas.getAreaname(), areas.getParentarea(), areas.getEntityid())) {
				throw new MessageInfoException("区域名称已存在");
			}
			areas.setEntitystate(entitystate);
		}
	}

	/**
	 * 根据区域主键查询区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityid 区域主键
	 * @return 区域
	 */
	public Map<String, Object> findMapByEntityid(Long entityid) {
		if (entityid == null) {
			throw new ValidationException("区域主键不能为空");
		}
		return areasDao.findMapByEntityid(entityid);
	}
}
