/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AbstractService.java
 * History:
 *         2019年5月31日: Initially created, Chrise.
 */
package club.coderleague.ilsp.service;

import club.coderleague.ilsp.common.domain.enums.MemberBindType;
import club.coderleague.ilsp.common.domain.enums.MemberOrigin;
import club.coderleague.ilsp.common.domain.enums.WebAuthor;

/**
 * 服务基类。
 * @author Chrise
 */
public abstract class AbstractService {
	/**
	 * 授权人转换成绑定类型。
	 * @author Chrise 2019年5月31日
	 * @param author 授权人。
	 * @return 绑定类型。
	 */
	protected MemberBindType authorToBindType(WebAuthor author) {
		switch (author) {
			case WEIXIN:
				return MemberBindType.WEIXIN;
			case ALIPAY:
				return MemberBindType.ALIPAY;
			case UNIONPAY:
				return MemberBindType.UNIONPAY;
			default:
				return null;
		}
	}
	
	/**
	 * 授权人转换会员来源。
	 * @author Chrise 2019年6月2日
	 * @param author 授权人。
	 * @return 会员来源。
	 */
	protected MemberOrigin authorToMemberOrigin(WebAuthor author) {
		switch (author) {
			case WEIXIN:
				return MemberOrigin.WEIXIN_SCAN;
			case ALIPAY:
				return MemberOrigin.ALIPAY_SCAN;
			case UNIONPAY:
				return MemberOrigin.UNIONPAY_SCAN;
			default:
				return null;
		}
	}
}
