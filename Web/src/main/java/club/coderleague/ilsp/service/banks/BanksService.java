/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：BanksService.java
 * History:
 *         2019年7月26日: Initially created, Liangj.
 */
package club.coderleague.ilsp.service.banks;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.beans.ResultMsg;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.BanksDao;
import club.coderleague.ilsp.entities.Banks;

/**
 * 银行管理
 * @author Liangjing
 */
@Service
public class BanksService {

	@Autowired
	BanksDao banksDao;
	
	/**
	 * 获取对应所有银行信息
	 * @author Liangjing 2019年7月26日
	 * @param key
	 * @param isrecycle
	 * @return
	 */
	public List<Map<String,Object>> getTheAllBanks(String key,boolean isrecycle) {
		return banksDao.getTheAllBanks(key, isrecycle);
	}
	
	/**
	 * 获取所有银行父级信息
	 * @author Liangjing 2019年7月26日
	 * @return
	 */
	public List<Map<String,Object>> getTheAllParentBanks() {
		List<Map<String,Object>> list = banksDao.getTheAllParentBanks();
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("entityid", "-1");
		map.put("bankname", "请选择");
		list.add(0,map);
		return list;
	}
	
	/**
	 * 获取银行管理编辑页面数据
	 * @author Liangjing 2019年7月26日
	 * @param entityid
	 * @return
	 */
	public Banks getTheBankEditPageData(Long entityid) {
		return banksDao.findByEntityid(entityid);
	}
	
	/**
	 * 获取银行管理查看详情页面数据
	 * @author Liangjing 2019年7月26日
	 * @param entityid
	 * @return
	 */
	public Map<String,Object> getTheBankLookInfoPageData(Long entityid) {
		return banksDao.getTheBankLookInfoPageData(entityid);
	}
	
	/**
	 * 更新银行信息
	 * @author Liangjing 2019年7月26日
	 * @param banks
	 * @return
	 */
	public ResultMsg updateTheBanks(Banks banks) {
		if(banks.getParentbank() == -1) {
			banks.setParentbank(null);
		}
		Banks bankValidate = banksDao.findByParentbankAndBanknameAndEntitystate(banks.getParentbank(), banks.getBankname(), EntityState.VALID.getValue());
		if(CommonUtil.isEmpty(banks.getEntityid())) {
			if(bankValidate != null) {
				return new ResultMsg(false, "该银行已经存在!");
			}else {
				Banks banknew = new Banks(EntityState.VALID.getValue(), banks.getParentbank(), banks.getBankname());
				banksDao.save(banknew);
			}
		}else {
			if(bankValidate != null) {
				if(bankValidate.getEntityid() != banks.getEntityid()) {
					return new ResultMsg(false, "该银行已经存在!");
				}else {
					bankValidate.setParentbank(banks.getParentbank());
					bankValidate.setBankname(banks.getBankname());
				}
			}else {
				Banks bankEdit = banksDao.getOne(banks.getEntityid());
				bankEdit.setParentbank(banks.getParentbank());
				bankEdit.setBankname(banks.getBankname());
			}
		}
		return new ResultMsg(true, "操作成功!");
	}
	
	/**
	 * 更新银行信息状态
	 * @author Liangjing 2019年7月26日
	 * @param entityid
	 * @param istate
	 * @return
	 */
	public ResultMsg updateTheBanksState(List<Long> entityids, Integer istate) {
		if(istate == EntityState.VALID.getValue()) {
			for (Long entityid : entityids) {
				Banks banks = banksDao.getOne(entityid);
				Banks banksV = banksDao.findByParentbankAndBanknameAndEntitystate(banks.getParentbank(), banks.getBankname(), EntityState.VALID.getValue());
				if(banksV != null) {
					return new ResultMsg(false, "恢复数据中存在相同的银行!");
				}
			}
		}
		for (Long entityid : entityids) {
			Banks banks = banksDao.getOne(entityid);
			banks.setEntitystate(istate);
		}
		return new ResultMsg(true, "操作成功!");
	}
}
