/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrganizationsMgrService.java
 * History:
 *         2019年5月11日: Initially created, wangjb.
 */
package club.coderleague.ilsp.service.organizations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.OrganizationsMgrDao;
import club.coderleague.ilsp.entities.Organizations;

/**
 * 机构管理业务处理。
 * @author wangjb
 */
@Service
public class OrganizationsMgrService {

	/**
	 * 机构管理数据访问对象。
	 */
	@Autowired OrganizationsMgrDao orgmgrDao;
	
	/**
	 * 获取机构管理数据。
	 * @author wangjb 2019年5月12日。
	 * @param params 关键字查询。
	 * @return
	 */
	public List<Map<String, Object>> getOrgMgrListService(Map<String, Object> params) {
		List<Map<String, Object>> result = this.orgmgrDao.getOrgMgrListDao(params);
		return result;
	}

	/**
	 * 根据机构id获取机构对象。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @return
	 */
	public Organizations getOrgObjByOrgidService(Long entityid) {
		return this.orgmgrDao.getOne(entityid);
	}

	/**
	 * 获取父级机构数据。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	public List<Map<String, Object>> getParentOrgSelectListService() {
		List<Map<String, Object>> result = this.orgmgrDao.getParentOrgSelectListDao();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("entityid", "-1");
		map.put("orgname", "顶层机构");
		map.put("parentorg", "");
		result.add(0, map);
		return result;
	}

	/**
	 * 新增、编辑机构对象。
	 * @author wangjb 2019年5月12日。
	 * @param orgObj 机构对象。
	 * @param result 返回机构提示。
	 */
	public void insertOrUpdateOrgObjService(Organizations orgObj, Map<String, Object> result) {
		Boolean repeat = this.orgmgrDao.getRepeatDataByOrgNameOrParentOrgDao(orgObj.getOrgname(), orgObj.getParentorg(), orgObj.getEntityid());
		if (repeat) {
			result.put("state", false);
			result.put("msg", "已存在重复的名称!");
			return;
		}
		if (orgObj.getParentorg() == null) orgObj.setParentorg(null);
		this.orgmgrDao.save(orgObj);
		result.put("state", true);
		result.put("msg", "操作成功!");
	}

	/**
	 * 修改机构状态。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @param result 返回操作数据。
	 * @param entitystate
	 */
	public void updateEntitystateByOrgidService(String entityid, Map<String, Object> result, Integer entitystate) {
		String[] ids = entityid.split(","); 
		for (String orgid : ids) {
			Organizations orgObj = this.orgmgrDao.getOne(Long.parseLong(orgid));
			if (entitystate == EntityState.VALID.getValue()) { // 恢复数据。
				Boolean repeat = this.orgmgrDao.getRepeatDataByOrgNameOrParentOrgDao(orgObj.getOrgname(), orgObj.getParentorg(), orgObj.getEntityid());
				if (repeat) {
					result.put("state", false);
					result.put("msg", "已存在重复的名称!");
					return;
				}
				if (orgObj.getParentorg() != null) this.updateParentOrgEntitystateService(entitystate, orgObj.getParentorg()); // 恢复父级。
			} else this.updateChildEntitystateService(entitystate, orgObj.getEntityid()); // 删除子级。
			orgObj.setEntitystate(entitystate);
		}
		result.put("state", true);
		result.put("msg", "操作成功!");
	}

	/**
	 * 恢复父级。
	 * @author wangjb 2019年5月12日。
	 * @param entitystate 数据状态。
	 * @param dataid 数据id。
	 */
	private void updateParentOrgEntitystateService(Integer entitystate, Long dataid) {
		while (true) {
			Organizations orgObj = this.orgmgrDao.getOne(dataid);
			if (orgObj != null && orgObj.getEntitystate() != EntityState.VALID.getValue()) {
				orgObj.setEntitystate(entitystate);
				if (orgObj.getEntitystate() == entitystate) break;
                if (orgObj.getParentorg() != null) this.updateParentOrgEntitystateService(entitystate, orgObj.getParentorg());
            }
            break;
		}
	}
	
	/**
	 * 删除子级。
	 * @author wangjb 2019年5月12日。
	 * @param entitystate 数据状态。
	 * @param entityid 数据id。
	 */
	private void updateChildEntitystateService(Integer entitystate, Long dataid) {
		while (true) {
            List<Organizations> orgList = this.orgmgrDao.findByParentorg(dataid);
            if (orgList.size() > 0) {
                for (Organizations org : orgList) {
    				org.setEntitystate(entitystate);
                    this.updateChildEntitystateService(entitystate, org.getEntityid());
                }
            }
            break;
        }
	}

	/**
	 * 根据机构id查询机构详情。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @return
	 */
	public Map<String, Object> getOrganizationsDetailDataByOrgidService(String entityid) {
		return this.orgmgrDao.getOrganizationsDetailDataByOrgidDao(entityid);
	}
}
