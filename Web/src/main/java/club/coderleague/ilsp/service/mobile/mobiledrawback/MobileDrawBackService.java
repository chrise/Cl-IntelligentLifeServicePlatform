/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileDrawBackService.java
 * History:
 *         2019年8月23日: Initially created, wangjb.
 */
package club.coderleague.ilsp.service.mobile.mobiledrawback;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import club.coderleague.ilsp.common.config.properties.FileUploadSettings;
import club.coderleague.ilsp.common.domain.beans.mobile.MoblieOrderMerchantExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.OrderDeliveryState;
import club.coderleague.ilsp.common.domain.enums.OrderRefundApplyType;
import club.coderleague.ilsp.dao.OrderGoodsDao;
import club.coderleague.ilsp.dao.OrdersDao;
import club.coderleague.ilsp.dao.RefundGoodsDao;
import club.coderleague.ilsp.dao.RefundRecordsDao;
import club.coderleague.ilsp.entities.Ordergoods;
import club.coderleague.ilsp.entities.Refundgoods;
import club.coderleague.ilsp.entities.Refundrecords;

/**
 * WeChat 退款处理。
 * @author wangjb
 */
@Service
public class MobileDrawBackService {
	
	/**
	 * 格式化小数点。
	 */
	private DecimalFormat df = new DecimalFormat("#.00");
	
	/**
	 * -订单数据数据访问对象。
	 */
	@Autowired OrdersDao ordersDao;
	
	/**
	 * -订单商品数据访问对象。
	 */
	@Autowired OrderGoodsDao orderGoodsDao;
	
	/**
	 * -附件配置表。
	 */
	@Autowired FileUploadSettings upload;
	
	/**
	 * -退款记录数据访问对象。
	 */
	@Autowired RefundRecordsDao recordsDao;
	
	/**
	 * -退款商品数据访问对象。
	 */
	@Autowired RefundGoodsDao refundGoodsDao;

	/**
	 * -根据会员标识查询所有的已完成的订单数据。
	 * @author wangjb 2019年8月23日。
	 * @param memberid 会员标识。
	 * @param keyword 关键字查询。
	 * @return
	 */
	public Map<String, Object> getOrderDrawBackByMemberIdService(Long memberid, String keyword) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<MoblieOrderMerchantExtension> orderList = this.ordersDao.getMobileOrderByMemberIdDao(memberid, keyword);
		List<Ordergoods> orderGoodList = new ArrayList<Ordergoods>();
		for (MoblieOrderMerchantExtension mome : orderList) { // 订单下商品。
			int refundstate = 0; // 可以申请退。
			if (mome.getRecordcount() > 0) refundstate = 3; // 已申请售后。
			if (refundstate == 0 && !Boolean.valueOf(mome.getShowapply())) refundstate = 1; // 已超期。
			List<Ordergoods> orderGood = this.orderGoodsDao.findByOrderid(mome.getOrderid());
			Double paymenttotal = 0.0;
			boolean flag = false;
			for (Ordergoods good : orderGood) {
				paymenttotal += good.getPaymenttotal();
				if (good.getSupportrefund()) { // 支持退款。
					flag = true;
				}
			}
			mome.setPaymenttotal(Double.valueOf(df.format(paymenttotal)));	 // 支付金额。
			mome.setGoodnumber(orderGood.size()); // 购买件数。
			if (refundstate == 0 && !flag) refundstate = 2;
			mome.setRefundstate(refundstate);
			orderGoodList.addAll(orderGood);
		}
		result.put("orderList", orderList);
		result.put("orderGoodList", orderGoodList);
		result.put("img", this.upload.getVirtual() + this.upload.getGoodPhotosPath());
		return result;
	}

	/**
	 * -根据订单id获取订单商品。
	 * @author wangjb 2019年8月24日。
	 * @param orderid 订单id。
	 * @return
	 */
	public Map<String, Object> getRefundGoodByOrderIdService(Long orderid) {
		Map<String, Object> result = new HashMap<String, Object>();
		MoblieOrderMerchantExtension order = this.ordersDao.getOrderMerchantByOrderidDao(orderid);
		boolean orderstate = false;
		if (EntityState.OTS.equalsValue(order.getEntitystate()) && OrderDeliveryState.RECEIVED.equalsValue(order.getDeliverystate())) orderstate = true;
		List<Ordergoods> orderGood = this.orderGoodsDao.findByOrderid(orderid);
		result.put("order", order);
		result.put("orderGoodList", orderGood);
		result.put("img", this.upload.getVirtual() + this.upload.getGoodPhotosPath());
		result.put("orderstate", orderstate);
		return result;
	}

	/**
	 * -保存需退款商品。
	 * @author wangjb 2019年8月24日。
	 * @param result 返回结果提示。
	 * @param orderid 订单id。
	 * @param ordergoodid 订单商品id。
	 * @param applydesc 申请原因。
	 */
	public void executeRefundByOrderIdService(Map<String, Object> result, Long orderid, String ordergoodid, String applydesc) throws Exception{
		MoblieOrderMerchantExtension order = this.ordersDao.getOrderMerchantByOrderidDao(orderid);
		boolean orderstate = false;
		if (EntityState.OTS.equalsValue(order.getEntitystate()) && OrderDeliveryState.RECEIVED.equalsValue(order.getDeliverystate())) orderstate = true;
		Refundrecords recordObj = new Refundrecords();
		recordObj.setEntitystate(EntityState.OBLIGATION.getValue());
		recordObj.setOrderid(orderid);
		recordObj.setApplytype(OrderRefundApplyType.USER.getValue());
		recordObj.setApplytime(new Date());
		recordObj.setApplydesc(applydesc);
		Double refundtotal = 0.0;
		Integer integralrefund = 0;
		Integer diamondrefund = 0;
		List<Refundgoods> list = new ArrayList<Refundgoods>();
		for (String goodid : ordergoodid.split(",")) {
			Ordergoods good = this.orderGoodsDao.getOne(Long.valueOf(goodid));
			if (orderstate && !good.getSupportrefund()) {
				result.put("msg", "存在不可退款商品!");
				throw new ValidationException("该商品不支持退款!");
			}
			refundtotal += good.getPaymenttotal();
			integralrefund += good.getIntegralpay();
			diamondrefund += good.getDiamondpay();
			Refundgoods refundGood = new Refundgoods();
			refundGood.setGoodid(good.getEntityid());
			refundGood.setEntitystate(EntityState.VALID.getValue());
			
			list.add(refundGood);
		}
		recordObj.setRefundtotal(Double.valueOf(df.format(refundtotal)));
		recordObj.setIntegralrefund(integralrefund);
		recordObj.setDiamondrefund(diamondrefund);
		List<Ordergoods> orderGood = this.orderGoodsDao.findByOrderid(orderid);
		recordObj.setPartialrefund((orderGood.size() == ordergoodid.split(",").length) ? false : true);
		
		this.recordsDao.save(recordObj);
		
		for (Refundgoods good : list) {
			good.setRecordid(recordObj.getEntityid());
		}
		
		this.refundGoodsDao.saveAll(list);
	}

	/**
	 * -根据会员标识获取退款处理中得数据。
	 * @author wangjb 2019年8月24日。
	 * @param memberid 会员标识。
	 * @param keyword 关键字查询。
	 * @return
	 */
	public Map<String, Object> getRefundHandleByMemberIdService(Long memberid, String keyword) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Integer> refundstate = Arrays.asList(new Integer[] {EntityState.OBLIGATION.getValue(), EntityState.REFUNDING.getValue()});
		List<MoblieOrderMerchantExtension> refundOrderList = this.ordersDao.getRefundHandleByMemberIdDao(memberid, refundstate, keyword);
		List<Map<String, Object>> refundGoodList = new ArrayList<Map<String, Object>>();
		for (MoblieOrderMerchantExtension mome : refundOrderList) { // 退款商品。
			List<Map<String, Object>> refundGood = this.orderGoodsDao.getRefundGoodByRefundRecordIdDao(mome.getRefundrecordid());
			mome.setGoodnumber(refundGood.size()); // 退款件数。
			refundGoodList.addAll(refundGood);
		}
		result.put("refundOrderList", refundOrderList);
		result.put("refundGoodList", refundGoodList);
		result.put("img", this.upload.getVirtual() + this.upload.getGoodPhotosPath());
		return result;
	}

	/**
	 * -退款处理-详情。
	 * @author wangjb 2019年8月24日。
	 * @param orderid 订单id。
	 * @return
	 */
	public Map<String, Object> getRefundHandleByOrderIdService(Long orderid) {
		Map<String, Object> result = new HashMap<String, Object>();
		MoblieOrderMerchantExtension refundOrder = this.ordersDao.getRefundHandleByOrderIdDao(orderid);
		List<Map<String, Object>> refundGoodList = this.orderGoodsDao.getRefundGoodByRefundRecordIdDao(refundOrder.getRefundrecordid());
		refundOrder.setGoodnumber(refundGoodList.size());
		result.put("refundOrder", refundOrder);
		result.put("refundGoodList", refundGoodList);
		result.put("img", this.upload.getVirtual() + this.upload.getGoodPhotosPath());
		return result;
	}

	/**
	 * -退款记录数据查询。
	 * @author wangjb 2019年8月24日。
	 * @param memberid 会员标识id。
	 * @param keyword 关键字查询。
	 * @return
	 */
	public Map<String, Object> getRefundRecordByMemberIdService(Long memberid, String keyword) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Integer> refundstate = Arrays.asList(new Integer[] {EntityState.UNDO.getValue(), EntityState.REFUNDED.getValue()});
		List<MoblieOrderMerchantExtension> refundRecordList = this.ordersDao.getRefundHandleByMemberIdDao(memberid, refundstate, keyword);
		List<Map<String, Object>> refundRecordGoodList = new ArrayList<Map<String, Object>>();
		for (MoblieOrderMerchantExtension mome : refundRecordList) { // 退款商品。
			List<Map<String, Object>> refundRecordGood = this.orderGoodsDao.getRefundGoodByRefundRecordIdDao(mome.getRefundrecordid());
			mome.setGoodnumber(refundRecordGood.size()); // 退款件数。
			refundRecordGoodList.addAll(refundRecordGood);
		}
		result.put("refundRecordList", refundRecordList);
		result.put("recordGoodList", refundRecordGoodList);
		result.put("img", this.upload.getVirtual() + this.upload.getGoodPhotosPath());
		return result;
	}

	/**
	 * -退款记录详情查询。
	 * @author wangjb 2019年8月24日。
	 * @param orderid 订单id。
	 * @return
	 */
	public Object getRefundRecordByOrderIdService(Long orderid) {
		Map<String, Object> result = new HashMap<String, Object>();
		MoblieOrderMerchantExtension refundRecord = this.ordersDao.getRefundRecordByOrderIdDao(orderid);
		List<Map<String, Object>> recordGoodList = this.orderGoodsDao.getRefundGoodByRefundRecordIdDao(refundRecord.getRefundrecordid());
		refundRecord.setGoodnumber(recordGoodList.size());
		result.put("refundRecord", refundRecord);
		result.put("recordGoodList", recordGoodList);
		result.put("img", this.upload.getVirtual() + this.upload.getGoodPhotosPath());
		return result;
	}

}
