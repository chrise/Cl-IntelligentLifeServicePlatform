/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AliyunInterfaceService.java
 * History:
 *         2019年6月1日: Initially created, Chrise.
 */
package club.coderleague.ilsp.service.interfaces;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.profile.DefaultProfile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import club.coderleague.ilsp.cache.CacheManager;
import club.coderleague.ilsp.common.config.properties.AliyunSettings;
import club.coderleague.ilsp.common.domain.beans.MerchantRemind;
import club.coderleague.ilsp.common.domain.beans.SystemConfig;
import club.coderleague.ilsp.common.domain.enums.NotifyType;
import club.coderleague.ilsp.dao.NotifyTemplateConfigDao;
import club.coderleague.ilsp.entities.Notifytemplateconfigs;
import club.coderleague.ilsp.service.AbstractService;
import club.coderleague.ilsp.service.merchants.MerchantsService;
import club.coderleague.ilsp.util.CommonUtil;

/**
 * 阿里云接口服务。
 * @author Chrise
 */
@Service
@SuppressWarnings("unchecked")
public class AliyunInterfaceService extends AbstractService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AliyunInterfaceService.class);
	private static final Pattern PHONE_PATTERN = Pattern.compile("^1\\d{10}$");
	private static final String CACHE_KEY_PREFIX = "vercode_cache_";
	private static final String LOCAL_NOTIFY_KEY_PREFIX = "local_notify_";
	private static final String TPL_PARAM_VERCODE = "vercode";
	private static final String TPL_PARAM_TIMEOUT = "timeout";
	private static final String TPL_PARAM_AMOUNT = "amount";
	
	@Autowired
	private AliyunSettings aySettings;
	@Autowired
	private CacheManager cacheManager;
	@Autowired
	private ThreadPoolTaskScheduler taskScheduler;
	@Autowired
	private NotifyTemplateConfigDao ntConfigDao;
	@Autowired
	private MerchantsService merchantsService;
	
	/**
	 * 发送验证码。
	 * @author Chrise 2019年6月1日
	 * @param type 通知类型。
	 * @param phone 手机号。
	 * @param session 会话标识。
	 * @return 验证码发送成功返回true，否则返回false。
	 */
	public boolean sendVercode(NotifyType type, String phone, String session) {
		try {
			// 通知类型无效
			if (type != NotifyType.PHONE_BIND && type != NotifyType.IDENTITY_AUTH) return false;
			
			// 手机号无效
			if (!PHONE_PATTERN.matcher(phone).matches()) return false;
			
			// 启动验证码发送任务
			this.taskScheduler.schedule(new Runnable() {
				public void run() {
					sendVercodeInternal(type, phone, session);
				}
			}, new Date());
			
			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return false;
	}
	
	/**
	 * 发送账户提醒。
	 * @author Chrise 2019年6月9日
	 * @param type 通知类型。
	 * @param merchant 商户标识。
	 * @param amount 动账金额。
	 * @return 提醒发送成功返回true，否则返回false。
	 */
	public boolean sendAccountRemind(NotifyType type, long merchant, double amount) {
		try {
			// 通知类型无效
			if (type != NotifyType.ARRACC_REMIND && type != NotifyType.REFUND_REMIND && type != NotifyType.SETACC_REMIND) return false;
			
			// 启动账户提醒发送任务
			this.taskScheduler.schedule(new Runnable() {
				public void run() {
					sendAccountRemindInternal(type, merchant, amount);
				}
			}, new Date());
			
			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return false;
	}
	
	/**
	 * 获取缓存的验证码。
	 * @author Chrise 2019年6月5日
	 * @param session 会话对象。
	 * @return 缓存的验证码。
	 */
	public String getCachedVercode(HttpSession session) {
		// 获取缓存验证码
		String key = CACHE_KEY_PREFIX + session.getId();
		String cached = (String)this.cacheManager.getObject(key, true);
		return cached;
	}
	
	/**
	 * 清除缓存。
	 * @author Chrise 2019年6月5日
	 * @param session 会话对象。
	 */
	public void clearCache(HttpSession session) {
		// 清除缓存
		String key = CACHE_KEY_PREFIX + session.getId();
		this.cacheManager.setObject(key, null, 0L);
	}
	
	/**
	 * 获取本地通知。
	 * @author Chrise 2019年6月12日
	 * @param phone 手机号。
	 * @return 本地通知内容。
	 */
	public Object getLocalNotify(String phone) {
		Object notify = this.cacheManager.getObject(LOCAL_NOTIFY_KEY_PREFIX + phone, true);
		return (notify == null ? "" : notify);
	}
	
	/**
	 * 发送验证码。
	 * @author Chrise 2019年6月9日
	 * @param type 通知类型。
	 * @param phone 手机号。
	 * @param session 会话标识。
	 */
	private void sendVercodeInternal(NotifyType type, String phone, String session) {
		try {
			// 生成验证码
			String vercode = this.genVercode();
			
			// 获取系统配置
			SystemConfig sc = (SystemConfig)this.cacheManager.getObject(SystemConfig.CACHE_KEY, false);
			if (sc == null || sc.getVercodetimeout() == null) {
				LOGGER.error("The vercode [{}] send failed because system config not found.", vercode);
				return;
			}
			
			// 构造参数值集合
			Map<String, String> values = new HashMap<String, String>();
			values.put(TPL_PARAM_VERCODE, vercode);
			values.put(TPL_PARAM_TIMEOUT, String.valueOf(sc.getVercodetimeout()));
			
			// 生成短信内容
			Map<String, String> params = new HashMap<String, String>();
			String code = this.genSMSContent(type, values, params);
			if (CommonUtil.isEmpty(code)) {
				LOGGER.error("The vercode [{}] send failed because sms template not found.", vercode);
				return;
			}
			
			// 发送验证码短信
			if (this.aySettings.getLocalNotify()) {
				// 缓存通知参数
				this.cacheManager.setObject(LOCAL_NOTIFY_KEY_PREFIX + phone, params, this.aySettings.getLocalNotifyTimeout(), TimeUnit.MINUTES);
				
				LOGGER.info("The vercode is [{}] and sms content is [{}].", vercode, params);
			} else this.sendSMS(sc, phone, code, params);
			
			// 缓存验证码
			this.cacheManager.setObject(CACHE_KEY_PREFIX + session, vercode, sc.getVercodetimeout(), TimeUnit.MINUTES);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 发送账户提醒。
	 * @author Chrise 2019年6月9日
	 * @param type 通知类型。
	 * @param merchant 商户标识。
	 * @param amount 动账金额。
	 */
	private void sendAccountRemindInternal(NotifyType type, long merchant, double amount) {
		try {
			// 查询商户提醒
			MerchantRemind mr = this.merchantsService.queryMerchantRemind(merchant);
			if (mr == null) {
				LOGGER.error("The merchant [{}] does not exist.", merchant);
				return;
			}
			
			// 短信通知已关闭
			if (!mr.getSmnotify()) {
				LOGGER.error("The sms notify is closed for merchant [{}].", merchant);
				return;
			}
			
			// 获取系统配置
			SystemConfig sc = (SystemConfig)this.cacheManager.getObject(SystemConfig.CACHE_KEY, false);
			if (sc == null) {
				LOGGER.error("The account remind send failed for merchant [{}] because system config not found.", merchant);
				return;
			}
			
			// 构造参数值集合
			Map<String, String> values = new HashMap<String, String>();
			values.put(TPL_PARAM_AMOUNT, String.valueOf(amount));
			
			// 生成短信内容
			Map<String, String> params = new HashMap<String, String>();
			String code = this.genSMSContent(type, values, params);
			if (CommonUtil.isEmpty(code)) {
				LOGGER.error("The account remind send failed for merchant [{}] because sms template not found.", merchant);
				return;
			}
			
			// 发送提醒短信
			if (this.aySettings.getLocalNotify()) {
				// 缓存通知参数
				this.cacheManager.setObject(LOCAL_NOTIFY_KEY_PREFIX + mr.getMerchantphone(), params, this.aySettings.getLocalNotifyTimeout(), TimeUnit.MINUTES);
				
				LOGGER.info("The amount is [{}] and sms content is [{}].", amount, params);
			} else this.sendSMS(sc, mr.getMerchantphone(), code, params);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
	
	/**
	 * 生成验证码。
	 * @author Chrise 2019年6月1日
	 * @return 验证码字符串。
	 */
	private String genVercode() {
		StringBuilder builder = new StringBuilder();
		Random r = new Random(System.currentTimeMillis());
		for (int i = 0; i < 6; i ++) {
			builder.append(r.nextInt(10));
		}
		return builder.toString();
	}
	
	/**
	 * 生成短信内容。
	 * @author Chrise 2019年6月2日
	 * @param type 通知类型。
	 * @param values 参数值集合。
	 * @param params 模板参数集合。
	 * @return 模板代码。
	 * @throws IOException 输入输出错误。
	 */
	private String genSMSContent(NotifyType type, Map<String, String> values, Map<String, String> params) 
		throws IOException {
		// 查询模板配置
		Notifytemplateconfigs ntc = this.ntConfigDao.queryTemplate(type);
		if (ntc == null) return null;
		
		// 构造模板参数
		Map<String, String> temp = new ObjectMapper().readValue(ntc.getTplparam(), Map.class);
		Iterator<Entry<String, String>> it = temp.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> e = it.next();
			params.put(e.getKey(), values.get(e.getValue()));
		}
		
		return ntc.getTplcode();
	}
	
	/**
	 * 发送短信。
	 * @author Chrise 2019年6月2日
	 * @param config 系统配置对象。
	 * @param phone 手机号。
	 * @param code 模板代码。
	 * @param params 模板参数。
	 * @throws JsonProcessingException JSON处理错误。
	 * @throws ClientException 客户端错误。
	 * @throws IOException 输入输出错误。
	 */
	private void sendSMS(SystemConfig config, String phone, String code, Map<String, String> params) 
		throws JsonProcessingException, ClientException, IOException {
		String param = new ObjectMapper().writeValueAsString(params);
		
		DefaultProfile profile = DefaultProfile.getProfile("default", config.getAyappid(), config.getAyappkey());
		IAcsClient client = new DefaultAcsClient(profile);
		
		CommonRequest request = new CommonRequest();
		request.setSysProtocol(ProtocolType.HTTPS);
		request.setSysMethod(MethodType.POST);
		request.setSysDomain(this.aySettings.getDomain());
		request.setSysVersion(this.aySettings.getVersion());
		request.setSysAction(this.aySettings.getAction());
		request.putQueryParameter("PhoneNumbers", phone);
		request.putQueryParameter("SignName", config.getAysign());
		request.putQueryParameter("TemplateCode", code);
		request.putQueryParameter("TemplateParam", param);
		
		CommonResponse response = client.getCommonResponse(request);
		Map<String, String> result = new ObjectMapper().readValue(response.getData(), Map.class);
		if (!"OK".equals(result.get("Code"))) {
			LOGGER.error("The sms to [{}] send failed because [{}].", phone, result);
		}
	}
}
