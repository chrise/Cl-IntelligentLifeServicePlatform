/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileDrawBackController.java
 * History:
 *         2019年8月23日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.mobile.mobiledrawback;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.service.mobile.mobiledrawback.MobileDrawBackService;

/**
 * -WeChat 订单退款控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/mobiledrawback")
public class MobileDrawBackController extends IndexController{
	
	/**
	 * -WeChat 订单退款业务处理。
	 */
	@Autowired MobileDrawBackService drawBackService;

	/**
	 * -根据会员标识获取查询退款申请数据。
	 * @author wangjb 2019年8月23日。
	 * @param model
	 * @param keyword 关键字查询。
	 * @return
	 */
	@PreAuthorize("*")
	@RequestMapping("/getRefundApplyByMemberId.xhtml")
	@OperateLog("根据会员标识获取查询退款申请数据。 ")
	public String getRefundApplyByMemberIdController(Model model, String keyword) {
		model.addAttribute("refundapply", this.drawBackService.getOrderDrawBackByMemberIdService(super.getUserId(), keyword));
		return "/mobile/member/mobilepersonal/mobileDrawBack::refundapply_list";
	}
	
	/**
	 * -根据订单id获取商品数据。
	 * @author wangjb 2019年8月24日。
	 * @param model
	 * @param orderid 订单id。
	 * @return
	 */
	@PreAuthorize("*")
	@RequestMapping("/getRefundGoodByOrderId.xhtml")
	@OperateLog("根据订单id获取商品数据。 ")
	public String getRefundGoodByOrderIdController(Model model, Long orderid) {
		model.addAttribute("refundgood", this.drawBackService.getRefundGoodByOrderIdService(orderid));
		return "/mobile/member/mobilepersonal/moblieRefundWindow::refundgood_list";
	}
	
	/**
	 * -保存需退款商品。
	 * @author wangjb 2019年8月24日。
	 * @param orderid 订单id。
	 * @param ordergoodid 订单商品id。
	 * @param applydesc 退款原因。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog("保存需退款商品")
	@PostMapping("/executeRefundByOrderId.xhtml")
	public ModelMap executeRefundByOrderIdController(Long orderid, String ordergoodid, String applydesc){
		Map<String, Object> result = new HashMap<String, Object>();
		boolean flag = false;
		try {
			this.drawBackService.executeRefundByOrderIdService(result, orderid, ordergoodid, applydesc);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return super.createModel(flag, result);
	}
	
	/**
	 * -根据会员标识获取退款处理中得数据。
	 * @author wangjb 2019年8月24日。
	 * @param model
	 * @param keyword 关键字查询。
	 * @return
	 */
	@PreAuthorize("*")
	@RequestMapping("/getRefundHandleByMemberId.xhtml")
	@OperateLog("根据会员标识获取退款处理中得数据。 ")
	public String getRefundHandleByMemberIdController(Model model, String keyword) {
		model.addAttribute("refundhandle", this.drawBackService.getRefundHandleByMemberIdService(super.getUserId(), keyword));
		return "/mobile/member/mobilepersonal/mobileDrawBack::refundhandle_list";
	}
	
	/**
	 *- 根据会员标识获取退款记录数据。
	 * @author wangjb 2019年8月24日。
	 * @param model
	 * @param keyword 关键字查询。
	 * @return
	 */
	@PreAuthorize("*")
	@RequestMapping("/getRefundRecordByMemberId.xhtml")
	@OperateLog("根据会员标识获取退款记录数据。 ")
	public String getRefundRecordByMemberIdController(Model model, String keyword) {
		model.addAttribute("refundrecord", this.drawBackService.getRefundRecordByMemberIdService(super.getUserId(), keyword));
		return "/mobile/member/mobilepersonal/mobileDrawBack::refundrecord_list";
	}
}
