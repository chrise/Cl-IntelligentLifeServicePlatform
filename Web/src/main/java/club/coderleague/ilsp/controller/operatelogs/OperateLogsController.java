/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OperateLogsController.java
 * History:
 *         2019年5月12日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller.operatelogs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PassiveLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.service.operatelogs.OperateLogsService;

/**
 * 操作日志控制器
 * 
 * @author CJH
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/operatelogs")
public class OperateLogsController extends IndexController {
	/**
	 * 操作日志Service
	 */
	private @Autowired OperateLogsService operateLogsService;
	
	/**
	 * 前往操作日志页面
	 * 
	 * @author CJH 2019年5月12日
	 * @return 页面路径
	 */
	@GetMapping("/goOperateLogsPage.xhtml")
	@OperateLog("前往操作日志页面")
	@PreAuthorize("operatelogs")
	public String goOperateLogsPage() {
		return "/pc/operatelogs/operateLogsPage";
	}
	
	/**
	 * 分页查询操作日志
	 * 
	 * @author CJH 2019年7月2日
	 * @param pagingparameters 分页参数
	 * @param keyword 关键字
	 * @param starttime 开始时间
	 * @param endtime 结束时间
	 * @return 操作日志
	 */
	@PostMapping("/findPageByParamsMap.xhtml")
	@ResponseBody
	@PassiveLog("分页查询操作日志")
	@PreAuthorize("operatelogs")
	public ModelMap findPageByParamsMap(PagingParameters pagingparameters, String keyword, String starttime, String endtime) {
		return super.createModel(true, operateLogsService.findPageByParamsMap(pagingparameters, keyword, starttime, endtime));
	}
	
	/**
	 * 前往操作日志查看详情页面
	 * 
	 * @author CJH 2019年5月21日
	 * @return 页面路径
	 */
	@GetMapping("/goOperateLogsViewPage.xhtml")
	@OperateLog("前往操作日志查看详情页面")
	@PreAuthorize("operatelogs")
	public String goOperateLogsViewPage() {
		return "/pc/operatelogs/operateLogsViewPage";
	}
	
	/**
	 * 根据操作日志主键查询操作日志
	 * 
	 * @author CJH 2019年5月21日
	 * @param entityid 操作日志主键
	 * @return 操作日志
	 */
	@PostMapping("/findExtensionByEntityid.xhtml")
	@ResponseBody
	@PassiveLog("根据操作日志主键查询操作日志")
	@PreAuthorize("operatelogs")
	public ModelMap findExtensionByEntityid(Long entityid) {
		return super.createModel(true, operateLogsService.findExtensionByEntityid(entityid));
	}
}
