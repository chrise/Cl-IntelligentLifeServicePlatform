/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileCustomerController.java
 * History:
 *         2019年5月29日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 移动端大客户专区控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(MobileCustomerController.REQUEST_PREFIX)
public class MobileCustomerController extends MobileIndexController {
	/**
	 * 请求前缀。
	 */
	public static final String REQUEST_PREFIX = MobileIndexController.REQUEST_PREFIX + "/customer";
}
