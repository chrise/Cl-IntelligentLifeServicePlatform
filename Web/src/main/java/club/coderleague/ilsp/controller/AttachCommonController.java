/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：AttachCommonController.java
 * History:
 *         2019年6月6日: Initially created, Liangj.
 */
package club.coderleague.ilsp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.config.properties.FileUploadSettings;
import club.coderleague.ilsp.common.util.CommonUtil;

/**
 * 附件操作公共控制器
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/attach")
public class AttachCommonController extends IndexController {

	/**
	 * 上传对象。
	 */
	@Autowired FileUploadSettings upload;
	
	/**
	 * 浏览图片。
	 * @author Liangjing 2019年6月6日
	 * @param name
	 * @param path
	 * @param model
	 * @return
	 */
	@PreAuthorize("*")
	@OperateLog(value = "浏览图片")
	@RequestMapping("/vi.xhtml")
	public String viewImage(String name, String path, Model model) {
		model.addAttribute("imageName", name);
		model.addAttribute("imageUrl", this.upload.getVirtual() + path);
		return "/pc/attach/vi";
	}
	/**
	 * 下载图片
	 * @author Liangjing 2019年9月24日
	 * @param name
	 * @param path
	 * @param request
	 * @param response
	 * @throws Exception 
	 */
	@PreAuthorize("*")
	@PostMapping("/df.xhtml")
	@OperateLog(value = "下载图片")
	public void downloadTheImage(String name,String path, HttpServletRequest request, HttpServletResponse response) throws Exception {
		CommonUtil.downloadFiles(name, this.upload.getReal() + path, request, response);
	}
}
