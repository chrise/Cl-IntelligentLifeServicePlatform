/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrdersController.java
 * History:
 *         2019年6月6日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller.orders;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import club.coderleague.ilsp.controller.IndexController;

/**
 * 订单控制器 
 * 
 * @author CJH
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/orders")
public class OrdersController extends IndexController {
	
}
