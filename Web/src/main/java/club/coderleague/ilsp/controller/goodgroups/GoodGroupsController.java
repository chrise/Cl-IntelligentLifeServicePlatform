/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：GoodGroupsController.java
 * History:
 *         2019年5月12日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.controller.goodgroups;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Goodgroups;
import club.coderleague.ilsp.service.goodgroups.GoodGroupsService;

/**
 * 商品分类
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/goodgroups")
public class GoodGroupsController extends IndexController {

	@Autowired
	GoodGroupsService goodGroupsService;
	
	/**
	 * 获取所有的商品分类
	 * @author Liangjing 2019年5月12日
	 * @param key
	 * @param istate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodgroups","goodgroups.recycle"})
	@OperateLog(value = "获取所有的商品分类", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallgoodgroup.xhtml")
	public ModelMap getTheAllGoodGroup(String key,boolean isrecycle) {
		return super.createModel(true,goodGroupsService.getTheAllGoodGroup(key, isrecycle));
	}
	/**
	 * 获取商品分类编辑页面数据
	 * @author Liangjing 2019年5月12日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("goodgroups.edit")
	@OperateLog(value = "获取商品分类编辑页面数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthegoodgroupeditpagedata.xhtml")
	public ModelMap getTheGoodGroupEditPageData(Long entityid) {
		return super.createModel(true,goodGroupsService.getTheGoodGroupEditPageData(entityid));
	}
	/**
	 * 获取商品分类查看详情页面数据
	 * @author Liangjing 2019年5月12日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodgroups","goodgroups.recycle"})
	@OperateLog(value = "获取商品分类查看详情页面数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthegoodgrouplookinfopagedata.xhtml")
	public ModelMap getTheGoodGroupLookInfoPageData(String entityid) {
		return super.createModel(true,goodGroupsService.getTheGoodGroupLookInfoPageData(entityid));
	}
	
	/**
	 * 新增或者编辑商品分类
	 * @author Liangjing 2019年5月12日
	 * @param goodgroups
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodgroups.add","goodgroups.edit"})
	@OperateLog(value = "新增或者编辑商品分类")
	@RequestMapping("/updatethegoodgroup.xhtml")
	public ModelMap updateTheGoodGroup(Goodgroups goodgroups) {
		return super.createModel(true,goodGroupsService.updateTheGoodGroup(goodgroups));
	}
	/**
	 * 根据istate修改商品分类状态
	 * @author Liangjing 2019年5月12日
	 * @param entityids
	 * @param istate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodgroups.del","goodgroups.recycle"})
	@OperateLog(value = "根据istate修改商品分类状态")
	@RequestMapping("/updatethegoodgroupstatebyistate.xhtml")
	public ModelMap updateTheGoodGroupStateByIstate(String entityids,Integer istate) {
		return super.createModel(true,goodGroupsService.updateTheGoodGroupStateByIstate(CommonUtil.toLongArrays(entityids), istate));
	}
	/**
	 * 获取商品分类编辑页面父级分类加载数据
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodgroups.add","goodgroups.edit"})
	@OperateLog(value = "获取商品分类编辑页面父级分类加载数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheupdatepageloadpraentgoods.xhtml")
	public ModelMap getTheUpdatePageLoadParentGoods(String entityid) {
		return super.createModel(true,goodGroupsService.getTheUpdatePageLoadParentGoods(entityid));
	}
	/**
	 * 前往商品分类首页
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"goodgroups","goodgroups.recycle"})
	@OperateLog("前往商品分类首页")
	@RequestMapping("/gohomepage.xhtml")
	public String gohomepage() {
		return "/pc/goodgroups/homepage";
	}
	/**
	 * 前往商品分类修改页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"goodgroups.add","goodgroups.edit"})
	@OperateLog("前往商品分类修改页面")
	@RequestMapping("/goupdatepage.xhtml")
	public String goupdatepage(Model model,String entityid) {
		model.addAttribute("entityid", entityid);
		return "/pc/goodgroups/updatepage";
	}
	/**
	 * 前往商品分类查看详情页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"goodgroups","goodgroups.recycle"})
	@OperateLog("前往商品分类查看详情页面")
	@RequestMapping("/golookinfopage.xhtml")
	public String golookinfopage() {
		return "/pc/goodgroups/lookinfo";
	}
}
