/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AreasController.java
 * History:
 *         2019年5月24日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller.areas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PassiveLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Areas;
import club.coderleague.ilsp.service.areas.AreasService;

/**
 * 区域控制器
 * 
 * @author CJH
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/areas")
public class AreasController extends IndexController {
	/**
	 * 区域Service
	 */
	private @Autowired AreasService areasService;
	
	/**
	 * 前往区域管理页面
	 * 
	 * @author CJH 2019年5月24日
	 * @return 页面路径
	 */
	@GetMapping("/goAreasPage.xhtml")
	@OperateLog("前往区域管理页面")
	@PreAuthorize("areas")
	public String goAreasPage() {
		return "/pc/areas/areasPage";
	}
	
	/**
	 * 前往区域管理回收站页面
	 * 
	 * @author CJH 2019年5月24日
	 * @return 页面路径
	 */
	@GetMapping(value = "/goAreasPage.xhtml", params = "recycle=true")
	@OperateLog("前往区域管理回收站页面")
	@PreAuthorize("areas.recycle")
	public String goRolesRecyclePage() {
		return "/pc/areas/areasPage";
	}
	
	/**
	 * 分页查询区域
	 * 
	 * @author CJH 2019年7月2日
	 * @param isrecycle 是否回收站
	 * @param keyword 关键字
	 * @return 区域
	 */
	@PostMapping("/findAllByParamsMap.xhtml")
	@ResponseBody
	@PassiveLog("分页查询区域")
	@PreAuthorize({"areas", "areas.recycle"})
	public ModelMap findAllByParamsMap(boolean isrecycle, String keyword) {
		return super.createModel(true, areasService.findAllByParamsMap(isrecycle, keyword));
	}
	
	/**
	 * 前往区域管理编辑页面
	 * 
	 * @author CJH 2019年5月24日
	 * @return 页面路径
	 */
	@GetMapping("/goAreasEditPage.xhtml")
	@OperateLog("前往区域管理编辑页面")
	@PreAuthorize({"areas.add", "areas.edit"})
	public String goRolesEditPage() {
		return "/pc/areas/areasEditPage";
	}
	
	/**
	 * 查询所有区域
	 * 
	 * @author CJH 2019年5月24日
	 * @return 区域
	 */
	@PostMapping("/findAll.xhtml")
	@ResponseBody
	@PassiveLog("查询所有区域")
	@PreAuthorize({"areas.add", "areas.edit"})
	public ModelMap findAll() {
		return super.createModel(true, areasService.findAll());
	}
	
	/**
	 * 新增区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param areas 区域
	 * @return 结果信息
	 */
	@PostMapping(value = "/save.xhtml", params = "oper=add")
	@ResponseBody
	@OperateLog("新增区域")
	@PreAuthorize("areas.add")
	public ModelMap insert(Areas areas) {
		areasService.insert(areas);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 更新区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param areas 区域
	 * @return 结果信息
	 */
	@PostMapping(value = "/save.xhtml", params = "oper=edit")
	@ResponseBody
	@OperateLog("更新区域")
	@PreAuthorize("areas.edit")
	public ModelMap update(Areas areas) {
		areasService.update(areas);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据区域主键查询区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityid 区域主键
	 * @return 区域
	 */
	@PostMapping("/findOneByEntityid.xhtml")
	@ResponseBody
	@PassiveLog("根据区域主键查询区域")
	@PreAuthorize("areas.edit")
	public ModelMap findOneByEntityid(Long entityid) {
		return super.createModel(true, areasService.findOneByEntityid(entityid));
	}
	
	/**
	 * 根据区域主键删除区域到回收站
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityids 区域主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=del")
	@ResponseBody
	@OperateLog("根据区域主键删除区域到回收站")
	@PreAuthorize("areas.del")
	public ModelMap deleteByEntityids(String entityids) {
		areasService.updateEntitystate(entityids, EntityState.INVALID.getValue(), false);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据区域主键恢复区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityids 区域主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=restore")
	@ResponseBody
	@OperateLog("根据区域主键恢复区域")
	@PreAuthorize("areas.recycle")
	public ModelMap updateRestoreByEntityids(String entityids) {
		areasService.updateEntitystate(entityids, EntityState.VALID.getValue(), true);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据区域主键删除区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityids 区域主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=delete")
	@ResponseBody
	@OperateLog("根据区域主键删除区域")
	@PreAuthorize("areas.recycle")
	public ModelMap deleteRecycleByEntityids(String entityids) {
		areasService.updateEntitystate(entityids, EntityState.DELETED.getValue(), false);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 前往区域管理查看详情页面
	 * 
	 * @author CJH 2019年5月24日
	 * @return 页面路径
	 */
	@GetMapping("/goAreasViewPage.xhtml")
	@OperateLog("前往区域管理查看详情页面")
	@PreAuthorize({"areas", "areas.recycle"})
	public String goAreasViewPage() {
		return "/pc/areas/areasViewPage";
	}
	
	/**
	 * 根据区域主键查询区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityid 区域主键
	 * @return 区域
	 */
	@PostMapping("/findMapByEntityid.xhtml")
	@ResponseBody
	@PassiveLog("根据区域主键查询区域")
	@PreAuthorize({"areas", "areas.recycle"})
	public ModelMap findMapByEntityid(Long entityid) {
		return super.createModel(true, areasService.findMapByEntityid(entityid));
	}
}