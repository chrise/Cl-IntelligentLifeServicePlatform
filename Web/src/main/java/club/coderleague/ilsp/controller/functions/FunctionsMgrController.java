/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：FunctionsController.java
 * History:
 *         2019年5月10日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.functions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Functions;
import club.coderleague.ilsp.service.functions.FunctionsMgrService;

/**
    *  功能管理控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/funcmgr")
public class FunctionsMgrController extends IndexController {
	
	/**
	   * 功能管理业务处理。
	 */
	@Autowired FunctionsMgrService funcMgrService;
	
	/**
	 *  跳转至功能管理页面。
	 * @author wangjb 2019年5月10日。
	 * @return
	 */
	@PreAuthorize("funcmgr")
	@RequestMapping("/gofuncMgrPage.xhtml")
	@OperateLog("跳转至功能管理页面")
	public String goFuncMgrPageController() {
		return "/pc/functions/FunctionsMgrPage";
	}
	
	/**
	 * 跳转至功能管理回收站页面。
	 * @author wangjb 2019年5月30日。
	 * @return
	 */
	@PreAuthorize("funcmgr.recycle")
	@RequestMapping(value = "/gofuncMgrPage.xhtml", params = "recycle=true")
	@OperateLog("跳转至功能管理回收站页面")
	public String goFuncRecyclePageController() {
		return "/pc/functions/FunctionsMgrPage";
	}
	
	/**
	  * 获取功能列表数据。
	 * @author wangjb 2019年5月11日。
	 * @param params 页面参数。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"funcmgr", "funcmgr.recycle"})
	@PostMapping("/getFuncMgrList.xhtml")
	@OperateLog(value = "获取功能列表数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getFuncMgrListController(@RequestParam Map<String, Object> params) {
		List<Map<String, Object>> result = this.funcMgrService.getFuncMgrListService(params);
		return super.createModel(true, result);
		
	}
	
	/**
	 * 跳转至新增、编辑功能页面。
	 * @author wangjb 2019年5月11日。
	 * @return
	 */
	@PreAuthorize({"funcmgr.addFunc","funcmgr.editFunc"})
	@RequestMapping("/goUpdateFuncMgrPage.xhtml")
	@OperateLog("跳转至新增、编辑功能页面")
	public String goUpdateFuncMgrPageController() {
		return "/pc/functions/UpdateFunctionsPage";
	}
	
	/**
	 * 根据功能数据id获取对象。
	 * @author wangjb 2019年5月11日。
	 * @param entityid 功能数据id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"funcmgr.addFunc","funcmgr.editFunc"})
	@RequestMapping("/getFuncObjByFuncid.xhtml")
	@OperateLog(value = "根据功能数据id获取对象", type = OperateLogRequestType.PASSIVE)
	public ModelMap getFuncObjByFuncidController(Long entityid) {
		Functions result = this.funcMgrService.getFuncObjByFuncidService(entityid);
		return super.createModel(true, result);
	}
	
	/**
	 * 获取父级功能数据。
	 * @author wangjb 2019年5月11日。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"funcmgr.addFunc", "funcmgr.editFunc"})
	@RequestMapping("/getParentFuncSelectList.xhtml")
	@OperateLog(value = "获取父级功能数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getParentFuncSelectListController() {
		List<Map<String, Object>> result = this.funcMgrService.getParentFuncSelectListService();
		return super.createModel(true, result);
	}
	
	/**
	  *  新增功能对象。
	 * @author wangjb 2019年5月11日。
	 * @param func 表单功能json数据。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("funcmgr.addFunc")
	@RequestMapping(value = "/insertOrUpdateFuncObj.xhtml", params = "execute=add")
	@OperateLog("新增功能对象")
	public ModelMap insertFuncObjController(Functions data) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.funcMgrService.insertOrUpdateFuncObjService(data, result);
		return super.createModel(true, result);
	}
	
	/**
	  *  编辑功能对象。
	 * @author wangjb 2019年5月11日。
	 * @param func 表单功能json数据。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("funcmgr.editFunc")
	@RequestMapping(value = "/insertOrUpdateFuncObj.xhtml", params = "execute=edit")
	@OperateLog("编辑功能对象")
	public ModelMap updateFuncObjController(Functions data) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.funcMgrService.insertOrUpdateFuncObjService(data, result);
		return super.createModel(true, result);
	}
	
	/**
	 * 删除功能数据。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 功能id。
	 * @param entitystate 功能状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("funcmgr.delData")
	@RequestMapping(value = "/updateEntitystateByFuncid.xhtml", params = "execute=del")
	@OperateLog("删除功能数据")
	public ModelMap executeDeleteController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.funcMgrService.updateEntitystateByFuncidService(entityid, result, EntityState.INVALID.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 恢复功能数据。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 功能id。
	 * @param entitystate 功能状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("funcmgr.recycle")
	@RequestMapping(value = "/updateEntitystateByFuncid.xhtml", params = "execute=rest")
	@OperateLog("回收站执行恢复功能数据")
	public ModelMap executeRestoreController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.funcMgrService.updateEntitystateByFuncidService(entityid, result, EntityState.VALID.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 回收站执行删除功能数据。
	 * @author wangjb 2019年6月22日。
	 * @param entityid
	 * @param entitystate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("funcmgr.recycle")
	@RequestMapping(value = "/updateEntitystateByFuncid.xhtml", params = "execute=delRecyce")
	@OperateLog("回收站执行删除功能数据")
	public ModelMap executeDelRecyceController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.funcMgrService.updateEntitystateByFuncidService(entityid, result, EntityState.DELETED.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至功能详情页面。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	@PreAuthorize({"funcmgr", "funcmgr.recycle"})
	@RequestMapping("/goFunctionsDetailPage.xhtml")
	@OperateLog("跳转至功能详情页面")
	public String goFunctionsDetailPageController() {
		return "/pc/functions/FunctionsDetailPage";
	}
	
	
	/**
	 * 查询功能详情数据。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 功能id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"funcmgr", "funcmgr.recycle"})
	@RequestMapping("/getFunctionsDetailDataByFuncid.xhtml")
	@OperateLog(value = "查询功能详情数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getFunctionsDetailDataByFuncidController(String entityid) {
		Map<String, Object> result = this.funcMgrService.getFunctionsDetailDataByFuncidService(entityid);
		return super.createModel(true, result);
	}

}
