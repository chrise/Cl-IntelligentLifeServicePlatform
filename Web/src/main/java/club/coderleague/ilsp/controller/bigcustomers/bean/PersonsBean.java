/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PersonsBean.java
 * History:
 *         2019年6月6日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.bigcustomers.bean;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 人员bean
 * @author wangjb
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonsBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * id。
	 */
	private String entityid;
	
	/**
	 * 姓名。
	 */
	private String personname;
	
	/**
	 * 电话。
	 */
	private String personphone;
	
	/**
	 * 是否管理员。
	 */
	private Boolean managerflag;
	
	/**
	 * 是否采购员。
	 */
	private Boolean purchaserflag;

}
