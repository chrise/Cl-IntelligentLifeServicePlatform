/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SystemConfigController.java
 * History:
 *         2019年5月24日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller.systemconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Systemconfigs;
import club.coderleague.ilsp.service.systemconfig.SystemConfigService;

/**
 * 系统配置控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/system")
public class SystemConfigController extends IndexController {
	
	@Autowired
	SystemConfigService systemConfigService;
	
	/**
	 * 跳转至系统配置页面
	 * @author yj 2019年5月24日。
	 * @return
	 */
	@PreAuthorize("system")
	@RequestMapping("/gohome.xhtml")
	@OperateLog("跳转至系统配置页面")
	public String goSystemConfigController() {
		return "/pc/systemconfig/home";
	}
	
	
	/**
	 * 获取系统配置
	 * @author Liangjing 2019年6月1日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("system")
	@OperateLog(value = "获取系统配置")
	@RequestMapping("/getthesystemconfigs.xhtml")
	public ModelMap getTheSystemConfigs() {
		return super.createModel(true, systemConfigService.getTheSystemConfigs());
	}
	/**
	 * 修改系统配置
	 * @author Liangjing 2019年6月1日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("system")
	@OperateLog(value = "修改系统配置", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/updatesystemconfigs.xhtml")
	public ModelMap updateTheSystemConfigs(Systemconfigs systemconfigs) {
		return super.createModel(true, systemConfigService.updateTheSystemConfigs(systemconfigs));
	}
	
}
