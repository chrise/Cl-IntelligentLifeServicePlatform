/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomersController.java
 * History:
 *         2019年5月25日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.bigcustomers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.SettlementBigCustomerOrdersExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.controller.bigcustomers.bean.SaveBean;
import club.coderleague.ilsp.entities.Bigcustomers;
import club.coderleague.ilsp.service.bigcustomers.BigCustomersMgrService;

/**
 * 大客户管理控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/bcc")
public class BigCustomersMgrController extends IndexController {

	/**
	 * 大客户管理业务处理。
	 */
	@Autowired BigCustomersMgrService BCMS;
	
	/**
	 * 跳转至大客户管理页面。
	 * @author wangjb 2019年6月1日。
	 * @return
	 */
	@PreAuthorize("bigcustomers")
	@RequestMapping("/goBigCustomersMgrPage.xhtml")
	@OperateLog("跳转至大客户管理页面")
	public String goBigCustomersMgrPageController() {
		return "/pc/bigcustomers/BigCustomersMgrPage";
	}
	
	/**
	 * 跳转至大客户管理回收站页面。
	 * @author wangjb 2019年6月1日。
	 * @return
	 */
	@PreAuthorize("bigcustomers.recycle")
	@RequestMapping(value = "/goBigCustomersMgrPage.xhtml", params = "isrecycle=true")
	@OperateLog("跳转至大客户管理回收站页面。")
	public String goRecycleBigCustomersMgrPageController() {
		return "/pc/bigcustomers/BigCustomersMgrPage";
	}
	
	/**
	 * 获取大客户列表数据。
	 * @author wangjb 2019年5月26日。
	 * @param params 关键字查询参数
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"bigcustomers", "bigcustomers.recycle"})
	@RequestMapping("/getBigCustomersMgr.xhtml")
	@OperateLog(value="获取大客户列表数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getBigCustomersMgrController (@RequestParam Map<String, Object> params) throws Exception{
		Page<Bigcustomers> result = this.BCMS.getBigCustomersMgrService(params);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至新增大客户页面。
	 * @author wangjb 2019年6月1日。
	 * @return
	 */
	@PreAuthorize({"bigcustomers.add", "bigcustomers.edit"})
	@RequestMapping("/goUpdateBigCustomersPage.xhtml")
	@OperateLog("跳转至大客户新增页面")
	public String goUpdateBigCustomersPageController() {
		return "/pc/bigcustomers/UpdateBigCustomersPage";
	}
	
	/**
	 * 根据大客户id获取数据。
	 * @author wangjb 2019年6月1日。
	 * @param entityid 大客户id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"bigcustomers.add", "bigcustomers.edit"})
	@RequestMapping("/getBigCustomersObjByEntityid.xhtml")
	@OperateLog(value="根据id获取大客户数据。", type = OperateLogRequestType.PASSIVE)
	public ModelMap getBigCustomersObjByEntityidController (Long entityid) {
		Map<String, Object> result = this.BCMS.getBigCustomersObjByEntityidService(entityid);
		return super.createModel(true, result);
	}
	
	/**
	 * 新增大客户数据。
	 * @author wangjb 2019年6月1日。
	 * @param data json 数据。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.add")
	@RequestMapping(value = "/insertOrUpdateBigCustomersObj.xhtml", params = "execute=add")
	@OperateLog("新增大客户对象。")
	public ModelMap insertBigCustomersObjController(String data) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		SaveBean bigc = new ObjectMapper().readValue(data, SaveBean.class);
		this.BCMS.insertOrUpdateBigCustomersObjService(result, bigc);
		return super.createModel(true, result);
	}
	
	/**
	 * 编辑大客户数据。
	 * @author wangjb 2019年6月1日。
	 * @param data json 数据。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.edit")
	@RequestMapping(value = "/insertOrUpdateBigCustomersObj.xhtml", params = "execute=edit")
	@OperateLog("编辑大客户对象。")
	public ModelMap updateBigCustomersObjController(String data) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		SaveBean bigc = new ObjectMapper().readValue(data, SaveBean.class);
		this.BCMS.insertOrUpdateBigCustomersObjService(result, bigc);
		return super.createModel(true, result);
	}
	
	/**
	 * 删除大客户数据到回收站。
	 * @author wangjb 2019年6月1日。
	 * @param entityid id。
	 * @param entitystate 状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.del")
	@RequestMapping(value = "/updateEntitystateByEntityid.xhtml", params = "execute=del")
	@OperateLog("删除大客户数据到回收站。")
	public ModelMap executeDelController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCMS.updateEntitystateByEntityidService(entityid, result, EntityState.INVALID.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 回收站删除大客户数据。
	 * @author wangjb 2019年6月1日。
	 * @param entityid id。
	 * @param entitystate 状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.recycle")
	@RequestMapping(value = "/updateEntitystateByEntityid.xhtml", params = "execute=delRecyce")
	@OperateLog("回收站删除大客户数据。")
	public ModelMap executeRecycleDelController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCMS.updateEntitystateByEntityidService(entityid, result, EntityState.DELETED.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 回收站恢复大客户数据。
	 * @author wangjb 2019年6月1日。
	 * @param entityid id。
	 * @param entitystate 状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.recycle")
	@RequestMapping(value = "/updateEntitystateByEntityid.xhtml", params = "execute=rest")
	@OperateLog("回收站恢复大客户数据。")
	public ModelMap executeRecycleRestController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCMS.updateEntitystateByEntityidService(entityid, result, EntityState.VALID.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 冻结大客户数据。
	 * @author wangjb 2019年6月1日。
	 * @param entityid id。
	 * @param entitystate 状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.freeze")
	@RequestMapping(value = "/updateEntitystateByEntityid.xhtml", params = "execute=freeze")
	@OperateLog("冻结大客户数据。")
	public ModelMap executeFreezeController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCMS.updateEntitystateByEntityidService(entityid, result, EntityState.FROZEN.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 解冻大客户数据。
	 * @author wangjb 2019年6月1日。
	 * @param entityid id。
	 * @param entitystate 状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.unfreeze")
	@RequestMapping(value = "/updateEntitystateByEntityid.xhtml", params = "execute=unfreeze")
	@OperateLog("解冻大客户数据。")
	public ModelMap executeUnfreezeController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCMS.updateEntitystateByEntityidService(entityid, result, EntityState.VALID.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至审核大客户页面。
	 * @author wangjb 2019年6月1日。
	 * @return
	 */
	@PreAuthorize("bigcustomers.audit")
	@RequestMapping("/goAuditBigCustomersPage.xhtml")
	@OperateLog("跳转至审核大客户页面")
	public String goAuditBigCustomersPageController() {
		return "/pc/bigcustomers/AuditBigCustomersPage";
	}
	
	/**
	 * 审核待审核的大客户数据。
	 * @author wangjb 2019年6月1日。
	 * @param id 大客户id。
	 * @param istate 审核状态。
	 * @param record 审核记录。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.audit")
	@RequestMapping(value = "/executeAuditBigCustomersObj.xhtml", params = "execute=audit")
	@OperateLog("审核待审核的大客户数据。")
	public ModelMap executeAuditBigCustomersObjController(Long id, Integer istate, String record){
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCMS.executeAuditBigCustomersObjService(id, istate, record, result, super.getUserId());
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至大客户详情页面。
	 * @author wangjb 2019年6月6日。
	 * @return
	 */
	@PreAuthorize({"bigcustomers", "bigcustomers.recycle"})
	@RequestMapping("/goBigCustomersDetailPage.xhtml")
	@OperateLog("跳转至大客户详情页面！")
	public String goBigCustomersDetailPageController() {
		return "/pc/bigcustomers/BigCustomersDetailPage";
	}
	
	/**
	 * 根据大客户id获取详情数据。
	 * @author wangjb 2019年6月1日。
	 * @param entityid 大客户id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"bigcustomers", "bigcustomers.recycle"})
	@RequestMapping("/getBigCustomersDetailByEntityid.xhtml")
	@OperateLog(value="根据id获取大客户详情数据。", type = OperateLogRequestType.PASSIVE)
	public ModelMap getBigCustomersDetailByEntityidController (Long entityid) {
		Map<String, Object> result = this.BCMS.getBigCustomersObjByEntityidService(entityid);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至审核大客户结算页面。
	 * @author wangjb 2019年6月1日。
	 * @return
	 */
	@PreAuthorize("bigcustomers.settlement")
	@RequestMapping("/goSettlementBigCustomersPage.xhtml")
	@OperateLog("跳转至审核大客户页面")
	public String goSettlementBigCustomersPageController() {
		return "/pc/bigcustomers/SettlementBigCustomersPage";
	}
	
	/**
	 * 结算页面获取订单信息。
	 * @author wangjb 2019年6月8日。
	 * @param entityid 大客户id。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("bigcustomers.settlement")
	@RequestMapping("/getBigCustomerOrdersMgr.xhtml")
	@OperateLog(value="结算页面获取订单信息", type = OperateLogRequestType.PASSIVE)
	public ModelMap getBigCustomerOrdersMgrController (@RequestParam Map<String, Object> params) {
		Page<SettlementBigCustomerOrdersExtension> result = this.BCMS.getBigCustomerOrdersMgrService(params);
		return super.createModel(true, result);
	}
	
	@ResponseBody
	@PreAuthorize("bigcustomers.settlement")
	@RequestMapping("/executeSettlementByIds.xhtml")
	@OperateLog("执行结算操作。")
	public ModelMap executeSettlementByIdsController(String ids) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCMS.executeSettlementByIdsObjService(ids, result);
		return super.createModel(true, result);
	}
	
	/**
	 * 获取大客户数据。
	 * @author wangjb 2019年8月30日。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@RequestMapping("/getBigCustomerList.xhtml")
	@OperateLog(value="获取大客户数据。", type = OperateLogRequestType.PASSIVE)
	public ModelMap getBigCustomerListController() {
		List<Bigcustomers> result = this.BCMS.getBigCustomerListService();
		return super.createModel(true, result);
	}
}
