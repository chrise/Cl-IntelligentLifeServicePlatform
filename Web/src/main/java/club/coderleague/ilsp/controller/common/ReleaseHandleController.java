/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ReleaseHandleController.java
 * History:
 *         2019年7月2日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller.common;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.service.common.ReleaseHandleService;

/**
 * 发布处理控制器。
 * @author Chrise
 */
@Controller
public class ReleaseHandleController {
	@Autowired
	private ReleaseHandleService service;
	
	/**
	 * 请求路径后缀。
	 */
	public static final String REQUEST_PATH_SUFFIX = ".m";
	private static final String CONSOLE_REQUEST_PATH = "/console" + REQUEST_PATH_SUFFIX;
	
	/**
	 * 打开控制台页面。
	 * @author Chrise 2019年6月26日
	 * @param model 数据模型。
	 * @return 控制台页面地址。
	 */
	@GetMapping(CONSOLE_REQUEST_PATH)
	public String console(Model model) {
		model.addAttribute("supported", this.service.verify());
		return "/release/console";
	}
	
	/**
	 * 发布应用程序。
	 * @author Chrise 2019年6月28日
	 * @param request 请求对象。
	 * @return 发布结果。
	 */
	@ResponseBody
	@PostMapping(CONSOLE_REQUEST_PATH)
	public ModelMap release(HttpServletRequest request) throws Exception {
		String result = this.service.release(request);
		boolean status = (result == null);
		return new ModelMap().addAttribute("status", status).addAttribute("result", result);
	}
}
