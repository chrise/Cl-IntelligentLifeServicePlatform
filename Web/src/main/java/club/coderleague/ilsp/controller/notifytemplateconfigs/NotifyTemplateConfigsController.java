/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：NotifyTemplateConfigsController.java
 * History:
 *         2019年6月10日: Initially created, Liangj.
 */
package club.coderleague.ilsp.controller.notifytemplateconfigs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Notifytemplateconfigs;
import club.coderleague.ilsp.service.notifytemplateconfigs.NotifyTemplateConfigsServer;

/**
 * 通知模板
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/notifytemplateconfigs")
public class NotifyTemplateConfigsController extends IndexController {

	@Autowired
	NotifyTemplateConfigsServer notifyTemplateConfigsServer;
	
	/**
	 * 获取所有的通知模板
	 * @author Liangjing 2019年6月10日
	 * @param key
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"notifytemplateconfigs","notifytemplateconfigs.recycle"})
	@OperateLog(value = "获取所有的通知模板", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthealltemplate.xhtml")
	public ModelMap getTheAllTemplate(String key,boolean isrecycle,String pageIndex,String pageSize) {
		return super.createModel(true, notifyTemplateConfigsServer.getTheAllTemplate(key, isrecycle, pageIndex, pageSize));
	}
	/**
	 * 获取通知模板编辑页面数据
	 * @author Liangjing 2019年6月10日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"notifytemplateconfigs.edit"})
	@OperateLog(value = "获取通知模板编辑页面数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheeditpagedata.xhtml")
	public ModelMap getTheEditPageData(Long entityid) {
		return super.createModel(true, notifyTemplateConfigsServer.getTheEditPageData(entityid));
	}
	/**
	 * 获取通知模板查看详情页面数据
	 * @author Liangjing 2019年6月10日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"notifytemplateconfigs","notifytemplateconfigs.recycle"})
	@OperateLog(value = "获取通知模板查看详情页面数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthelookinfopagedata.xhtml")
	public ModelMap getTheLookInfoPageData(Long entityid) {
		return super.createModel(true, notifyTemplateConfigsServer.getTheLookInfoPageData(entityid));
	}
	/**
	 * 更新通知模板
	 * @author Liangjing 2019年6月10日
	 * @param notifytemplateconfigs
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"notifytemplateconfigs.edit"})
	@OperateLog(value = "更新通知模板")
	@RequestMapping("/updatethetemplate.xhtml")
	public ModelMap updateTheTemplate(Notifytemplateconfigs notifytemplateconfigs) {
		return super.createModel(true, notifyTemplateConfigsServer.updateTheTemplate(notifytemplateconfigs));
	}
	/**
	 * 更新通知模板状态
	 * @author Liangjing 2019年6月11日
	 * @param entityids
	 * @param istate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"notifytemplateconfigs.del","notifytemplateconfigs.recycle"})
	@OperateLog(value = "更新通知模板状态")
	@RequestMapping("/updatethetemplateistate.xhtml")
	public ModelMap updateTheTemplateIstate(String entityids,Integer istate) {
		return super.createModel(true, notifyTemplateConfigsServer.updateTheTemplateIstate(CommonUtil.toLongArrays(entityids), istate));
	}
	/**
	 * 前往通知模板首页
	 * @author Liangjing 2019年6月11日
	 * @return
	 */
	@PreAuthorize({"notifytemplateconfigs","notifytemplateconfigs.recycle"})
	@OperateLog("前往通知模板首页")
	@RequestMapping("/gohomepage.xhtml")
	public String gohomepage() {
		return "/pc/notifytemplateconfigs/homepage";
	}
	/**
	 * 前往通知模板修改页面
	 * @author Liangjing 2019年6月11日
	 * @return
	 */
	@PreAuthorize({"notifytemplateconfigs.add","notifytemplateconfigs.edit"})
	@OperateLog("前往通知模板修改页面")
	@RequestMapping("/goupdatepage.xhtml")
	public String goupdatepage() {
		return "/pc/notifytemplateconfigs/updatepage";
	}
	/**
	 * 前往通知模板查看详情页面
	 * @author Liangjing 2019年6月11日
	 * @return
	 */
	@PreAuthorize({"notifytemplateconfigs","notifytemplateconfigs.recycle"})
	@OperateLog("前往通知模板查看详情页面")
	@RequestMapping("/golookinfopage.xhtml")
	public String golookinfopage() {
		return "/pc/notifytemplateconfigs/lookinfo";
	}
}
