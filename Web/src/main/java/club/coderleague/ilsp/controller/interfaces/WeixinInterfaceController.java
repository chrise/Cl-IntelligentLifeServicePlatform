/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：WeixinInterfaceController.java
 * History:
 *         2019年5月26日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller.interfaces;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.domain.beans.WebAuthContext;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.common.domain.enums.WebAuthScene;
import club.coderleague.ilsp.common.domain.enums.WebAuthor;
import club.coderleague.ilsp.service.interfaces.WeixinInterfaceService;

/**
 * 微信接口控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(InterfaceController.REQUEST_PREFIX + "/weixin")
public class WeixinInterfaceController extends InterfaceController {
	private static final String EM_NOT_SUPPORTED = "系统暂不支持微信访问！";
	private static final String EM_AUTH_FAILED = "请求微信授权失败！";
	
	@Autowired
	private WeixinInterfaceService wxiService;
	
	/**
	 * @see club.coderleague.ilsp.controller.interfaces.InterfaceController#webAuth(club.coderleague.ilsp.common.domain.beans.WebAuthContext, org.springframework.ui.Model)
	 */
	@Override
	protected String webAuth(WebAuthContext context, Model model) {
		super.webAuth(context, model);
		
		String url = this.wxiService.getAuthUrl(getSession());
		
		if (url == null) model.addAttribute(EM_KEY, EM_AUTH_FAILED);
		else if ("".equals(url)) model.addAttribute(EM_KEY, EM_NOT_SUPPORTED);
		else return "redirect:" + url;
		
		return "/mobile/error/webauth";
	}
	
	/**
	 * 开发者身份验证。
	 * @author Chrise 2019年5月29日
	 * @param signature 签名。
	 * @param timestamp 时间戳。
	 * @param nonce 随机数。
	 * @param echostr 随机字符串。
	 */
	@ResponseBody
	@GetMapping("/push.xhtml")
	@OperateLog(value = "请求开发者身份验证", type = OperateLogRequestType.PASSIVE)
	public void developerIdentityVerify(String signature, String timestamp, String nonce, String echostr) {
		this.wxiService.verifyDeveloperIdentity(signature, timestamp, nonce, echostr, getResponse());
	}
	
	/**
	 * 处理消息。
	 * @author Chrise 2019年5月29日
	 */
	@ResponseBody
	@PostMapping("/push.xhtml")
	@OperateLog(value = "请求处理消息", type = OperateLogRequestType.PASSIVE)
	public void handleMessage() {
		this.wxiService.dispatchMessage(getRequest(), getResponse());
	}
	
	/**
	 * 会员商城。
	 * @author Chrise 2019年5月27日
	 * @param model 数据模型。
	 * @return 会员商城授权地址。
	 */
	@GetMapping("/membermall.xhtml")
	@OperateLog("请求会员商城")
	public String memberMall(Model model) {
		return webAuth(new WebAuthContext(WebAuthScene.MEMBER_MALL, WebAuthor.WEIXIN), model);
	}
	
	/**
	 * 大客户专区。
	 * @author Chrise 2019年5月31日
	 * @param model 数据模型。
	 * @return 大客户专区授权地址。
	 */
	@GetMapping("/customerarea.xhtml")
	@OperateLog("请求大客户专区")
	public String customerArea(Model model) {
		return webAuth(new WebAuthContext(WebAuthScene.CUSTOMER_AREA, WebAuthor.WEIXIN), model);
	}
	
	/**
	 * 商户控制台。
	 * @author Chrise 2019年5月31日
	 * @param model 数据模型。
	 * @return 商户控制台授权地址。
	 */
	@GetMapping("/merchantconsole.xhtml")
	@OperateLog("请求商户控制台")
	public String merchantConsole(Model model) {
		return webAuth(new WebAuthContext(WebAuthScene.MERCHANT_CONSOLE, WebAuthor.WEIXIN), model);
	}
	
	/**
	 * 网页授权回调。
	 * @author Chrise 2019年5月28日
	 * @param code 授权代码。
	 * @param state 状态。
	 * @param model 数据模型。
	 * @return 授权完成跳转地址。
	 */
	@GetMapping("/callback.xhtml")
	@OperateLog(value = "请求网页授权回调", type = OperateLogRequestType.PASSIVE)
	public String webAuthCallback(String code, String state, Model model) {
		String openId = this.wxiService.getOpenId(code, state, getSession());
		
		if (openId == null) model.addAttribute(EM_KEY, EM_AUTH_FAILED);
		else {
			String url = this.wxiService.onAuthSuccess(openId, getSession());
			if (url == null) model.addAttribute(EM_KEY, EM_AUTH_FAILED);
			else return "redirect:" + url;
		}
		
		return "/mobile/error/webauth";
	}
	
	/**
	 * 支付。
	 * @author Chrise 2019年6月10日
	 * @param order 订单标识。
	 * @param trade 交易号。
	 * @param model 数据模型。
	 * @return 支付页面地址。
	 */
	@GetMapping("/pay.xhtml")
	@OperateLog("请求支付")
	public String pay(String order, String trade, Model model) {
		Object result = this.wxiService.createTrade(order, trade, getUserSession(), getRequest());
		model.addAttribute("trade", result);
		
		if (result != null && result instanceof Map) return "/mobile/api/pay";
		else return "/mobile/error/orderpay";
	}
	
	/**
	 * 支付通知。
	 * @author Chrise 2019年6月23日
	 */
	@ResponseBody
	@PostMapping("/notify.xhtml")
	@OperateLog(value = "请求支付通知", type = OperateLogRequestType.PASSIVE)
	public void payNotify() {
		this.wxiService.handleTradeNotify(getRequest(), getResponse());
	}
	
	/**
	 * 退款通知。
	 * @author Chrise 2019年8月7日
	 */
	@ResponseBody
	@PostMapping("/refundNotify.xhtml")
	@OperateLog(value = "请求退款通知", type = OperateLogRequestType.PASSIVE)
	public void refundNotify() {
		this.wxiService.handleRefundNotify(getRequest(), getResponse());
	}
	
	/**
	 * 获取授权代码（模拟微信公众平台）。
	 * @author Chrise 2019年5月27日
	 * @param redirect_uri 重定向地址。
	 * @param state 状态。
	 * @param model 数据模型。
	 * @return 授权回调地址。
	 */
	@GetMapping("/oauth2/authorize")
	public String getAuthCode(String redirect_uri, String state, Model model) {
		String url = this.wxiService.getAuthCallbackUrl(redirect_uri, state, getRequest());
		
		if (url == null) model.addAttribute(EM_KEY, EM_AUTH_FAILED);
		else return "redirect:" + url;
		
		return "/mobile/error/webauth";
	}
	
	/**
	 * 获取开放标识（模拟微信公众平台）。
	 * @author Chrise 2019年5月28日
	 * @param code 授权代码。
	 * @return 开放标识。
	 */
	@ResponseBody
	@GetMapping("/oauth2/access_token")
	public Map<String, Object> getOpenId(String code) {
		return this.wxiService.getOpenId(code);
	}
}
