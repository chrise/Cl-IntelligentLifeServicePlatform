/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrganizationsMgrController.java
 * History:
 *         2019年5月11日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.organizations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Organizations;
import club.coderleague.ilsp.service.organizations.OrganizationsMgrService;

/**
 * 机构管理控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/orgmgr")
public class OrganizationsMgrController extends IndexController {
	
	/**
	 * 机构管理业务处理。
	 */
	@Autowired OrganizationsMgrService orgmgrService;
	
	/**
	 * 跳转至机构管理页面。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	@PreAuthorize("orgmgr")
	@RequestMapping("/goOrgMgrPage.xhtml")
	@OperateLog("跳转至机构管理页面")
	public String goOrgMgrPageController() {
		return "/pc/organizations/OrganizationsMgrPage";
	}
	
	/**
	 * 跳转至机构管理回收站页面。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	@PreAuthorize("orgmgr.recycle")
	@RequestMapping(value = "/goOrgMgrPage.xhtml", params = "recycle=true")
	@OperateLog("跳转至机构管理回收站页面")
	public String goOrgRecyclePageController() {
		return "/pc/organizations/OrganizationsMgrPage";
	}
	
	/**
	 * 获取机构管理数据。
	 * @author wangjb 2019年5月12日。
	 * @param params 关键字查询。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"orgmgr", "orgmgr.recycle"})
	@RequestMapping("/getOrgMgrList.xhtml")
	@OperateLog(value = "获取机构管理数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getOrgMgrListController(@RequestParam Map<String, Object> params) {
		List<Map<String, Object>> result = this.orgmgrService.getOrgMgrListService(params);
		return super.createModel(true, result);
		
	}
	
	/**
	 * 跳转至新增、编辑页面。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	@PreAuthorize({"orgmgr.addOrg", "orgmgr.editOrg"})
	@RequestMapping("/goUpdateOrgMgrPage.xhtml")
	@OperateLog("跳转至新增、编辑页面")
	public String goUpdateOrgMgrPageController() {
		return "/pc/organizations/UpdateOrganizationsPage";
	}
	
	/**
	 * 根据机构id获取机构对象。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"orgmgr.addOrg", "orgmgr.editOrg"})
	@RequestMapping("/getOrgObjByOrgid.xhtml")
	@OperateLog(value = "根据机构id获取机构对象", type = OperateLogRequestType.PASSIVE)
	public ModelMap getOrgObjByOrgidController(Long entityid) {
		Organizations result = this.orgmgrService.getOrgObjByOrgidService(entityid);
		return super.createModel(true, result);
	}
	
	/**
	 * 获取父级机构数据。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"orgmgr.addOrg", "orgmgr.editOrg"})
	@RequestMapping("/getParentOrgSelectList.xhtml")
	@OperateLog(value = "获取父级机构数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getParentOrgSelectController() {
		List<Map<String, Object>> result = this.orgmgrService.getParentOrgSelectListService();
		return super.createModel(true, result);
	}
	
	/**
	 * 新增机构对象。
	 * @author wangjb 2019年5月12日。
	 * @param data 机构对象。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orgmgr.addOrg")
	@RequestMapping(value = "/insertOrUpdateOrgObj.xhtml", params = "execute=add")
	@OperateLog(value = "新增机构对象", type = OperateLogRequestType.PASSIVE)
	public ModelMap insertOrgObjController(Organizations data) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.orgmgrService.insertOrUpdateOrgObjService(data, result);
		return super.createModel(true, result);
	}
	
	/**
	 * 新增机构对象。
	 * @author wangjb 2019年5月12日。
	 * @param data 机构对象。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orgmgr.addOrg")
	@RequestMapping(value = "/insertOrUpdateOrgObj.xhtml", params = "execute=edit")
	@OperateLog(value = "新增机构对象", type = OperateLogRequestType.PASSIVE)
	public ModelMap updateOrgObjController(Organizations data) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.orgmgrService.insertOrUpdateOrgObjService(data, result);
		return super.createModel(true, result);
	}
	
	/**
	 * 执行删除操作。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @param entitystate 机构状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orgmgr.delData")
	@RequestMapping(value = "/updateEntitystateByOrgid.xhtml", params = "execute=del")
	@OperateLog("执行删除操作")
	public ModelMap executeDeleteByOrgidController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.orgmgrService.updateEntitystateByOrgidService(entityid, result, EntityState.INVALID.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 执行回收站恢复操作。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @param entitystate 机构状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orgmgr.delData")
	@RequestMapping(value = "/updateEntitystateByOrgid.xhtml", params = "execute=rest")
	@OperateLog("执行回收站恢复操作")
	public ModelMap executeRestoreByOrgidController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.orgmgrService.updateEntitystateByOrgidService(entityid, result, EntityState.VALID.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 执行回收站删除操作。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @param entitystate 机构状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orgmgr.delData")
	@RequestMapping(value = "/updateEntitystateByOrgid.xhtml", params = "execute=delRecyce")
	@OperateLog("执行回收站删除操作")
	public ModelMap executeDelRecyceByOrgidController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.orgmgrService.updateEntitystateByOrgidService(entityid, result, EntityState.DELETED.getValue());
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至机构详情页面。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	@PreAuthorize({"orgmgr", "orgmgr.recycle"})
	@RequestMapping("/goOrganizationsDetailPage.xhtml")
	@OperateLog("跳转至机构详情页面")
	public String goOrganizationsDetailPageController() {
		return "/pc/organizations/OrganizationsDetailPage";
	}
	
	/**
	 * 根据机构id查询机构详情。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"orgmgr", "orgmgr.recycle"})
	@RequestMapping("/getOrganizationsDetailDataByOrgid.xhtml")
	@OperateLog(value = "根据机构id查询机构详情", type = OperateLogRequestType.PASSIVE)
	public ModelMap getOrganizationsDetailDataByOrgidController(String entityid) {
		Map<String, Object> result = this.orgmgrService.getOrganizationsDetailDataByOrgidService(entityid);
		return super.createModel(true, result);
	}

}
