/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileGoodsController.java
 * History:
 *         2019年7月31日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.mobile.goods;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.service.mobile.goods.MobileGoodsService;

/**
 * -WeChat 商品管理控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/mobilegoods")
public class MobileGoodsController extends IndexController{
	
	/**
	 * -WeChat 商品管理业务。
	 */
	@Autowired MobileGoodsService goodsService;
	
	/**
	 * -WeChat 修改商品销售库存。
	 * @author wangjb 2019年7月31日。
	 * @param saleid 销售id。
	 * @param goodstock 商品库存。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog("修改商品销售库存")
	@PostMapping("/executeModifyGoodstockBySaleid.xhtml")
	public ModelMap executeModifyGoodstockBySaleidController(Long saleid, Double goodstock) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.goodsService.executeModifyGoodstockBySaleidService(saleid, goodstock);
		return super.createModel(true, result);
	}
	
	/**
	 * -WeChat 修改商品单价库存。
	 * @author wangjb 2019年7月31日。
	 * @param saleid 销售id。
	 * @param goodprice 商品单价。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog("修改商品单价库存")
	@PostMapping("/executeModifyGoodpriceBySaleid.xhtml")
	public ModelMap executeModifyGoodpriceBySaleidController(Long saleid, Double goodprice) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.goodsService.executeModifyGoodpriceBySaleidService(saleid, goodprice);
		return super.createModel(true, result);
	}
	
	/**
	 * -WeChat 修改商品销售状态。
	 * @author wangjb 2019年8月3日。
	 * @param saleid 销售id。
	 * @param istate 销售状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog("修改商品单价库存")
	@PostMapping("/executeModifyGoodStateBySaleid.xhtml")
	public ModelMap executeModifyGoodStateBySaleidController(Long saleid, Integer istate) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.goodsService.executeModifyGoodpriceBySaleidService(saleid, istate);
		return super.createModel(true, result);
	}

}
