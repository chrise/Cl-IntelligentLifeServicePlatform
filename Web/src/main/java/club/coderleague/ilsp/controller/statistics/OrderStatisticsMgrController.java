/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderStatisticsMgrController.java
 * History:
 *         2019年6月8日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.statistics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.OrderStatisticsExtension;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Refundrecords;
import club.coderleague.ilsp.service.statistics.OrderStatisticsMgrService;

/**
 * 订单统计控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/orderstatistics")
public class OrderStatisticsMgrController extends IndexController{

	/**
	 * 订单统计业务处理。
	 */
	@Autowired OrderStatisticsMgrService BCOS;
	
	/**
	 * 跳转至大单统计页面。
	 * @author wangjb 2019年6月8日。
	 * @return
	 */
	@PreAuthorize("orderstatistics")
	@RequestMapping("/goOrderStatisticsMgrPage.xhtml")
	@OperateLog("跳转至订单统计页面")
	public String goOrderStatisticsMgrPageController() {
		return "/pc/statistics/orders/OrderStatisticsMgrPage";
	}
	
	/**
	 * 获取订单统计列表数据。
	 * @author wangjb 2019年6月8日。
	 * @param params 页面查询参数。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("orderstatistics")
	@RequestMapping("/getOrderStatisticsMgr.xhtml")
	@OperateLog(value="获取订单统计列表数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getOrderStatisticsMgrController (@RequestParam Map<String, Object> params) throws Exception{
		Page<OrderStatisticsExtension> result = this.BCOS.getOrderStatisticsMgrService(params);
		return super.createModel(true, result);
	}
	
	/**
	 * -执行标记发货操作。
	 * @author wangjb 2019年8月30日。
	 * @param ids 订单id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orderstatistics.delivery")
	@RequestMapping("/executeModifyOrderState.xhtml")
	@OperateLog("执行标记发货操作。")
	public ModelMap executeModifyOrderStateController(String ids) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCOS.executeModifyOrderStateService(ids, result);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至执行退款页面。
	 * @author wangjb 2019年6月15日。
	 * @return
	 */
	@PreAuthorize("orderstatistics.refund")
	@RequestMapping("/goExecuteRefundPage.xhtml")
	@OperateLog("跳转至执行退款页面")
	public String goExecuteRefundPageController() {
		return "/pc/statistics/orders/ExecuteRefundPage";
	}
	
	/**
	 * -根据订单id获取退款信息。
	 * @author wangjb 2019年10月10日。
	 * @param orderid 订单id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orderstatistics.refund")
	@RequestMapping("/getRefundMessageByOrderId.xhtml")
	@OperateLog(value="根据订单id获取退款信息", type = OperateLogRequestType.PASSIVE)
	public ModelMap getRefundMessageByOrderIdController (Long orderid){
		Map<String, Object> result = this.BCOS.getRefundMessageByOrderIdService(orderid);
		return super.createModel(true, result);
	}
	
	/**
	 * -执行退款操作。
	 * @author wangjb 2019年8月30日。
	 * @param orderid 订单id。
	 * @param istate 操作状态。
	 * @param handledesc 处理描述。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orderstatistics.refund")
	@RequestMapping("/executeRefundByOrderId.xhtml")
	@OperateLog("执行退款操作。")
	public ModelMap executeRefundByOrderIdController(Long orderid, Integer istate, String handledesc) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		this.BCOS.executeRefundByOrderIdService(orderid, istate, result, handledesc);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至订单统计详情页面。
	 * @author wangjb 2019年6月15日。
	 * @return
	 */
	@PreAuthorize("orderstatistics")
	@RequestMapping("/goOrderStatisticsDetailPage.xhtml")
	@OperateLog("跳转至订单统计详情页面")
	public String goOrderStatisticsDetailPageController() {
		return "/pc/statistics/orders/OrderStatisticsDetailPage";
	}
	
	/**
	 * 根据订单id获取订单商品数据
	 * @author wangjb 2019年6月15日。
	 * @param params 页面查询参数。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("orderstatistics")
	@RequestMapping("/getOrderGoodsByOrderId.xhtml")
	@OperateLog(value="根据订单id获取订单商品数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getOrderGoodsByOrderIdController (@RequestParam Map<String, Object> params) throws Exception{
		List<Map<String, Object>> result = this.BCOS.getOrderGoodsByOrderIdService(params);
		return super.createModel(true, result);
	}
	
	/**
	 * 根据订单id获取订单详情。
	 * @author wangjb 2019年6月15日。
	 * @param id 订单id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("orderstatistics")
	@RequestMapping("/getOrderStatisticsDetailByOrderId.xhtml")
	@OperateLog(value="获取订单统计列表数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getOrderStatisticsDetailByOrderIdController (Long id){
		Map<String, Object> result = this.BCOS.getOrderStatisticsDetailByOrderIdService(id);
		return super.createModel(true, result);
	}
	
	/**
	 * -根据订单id获取退款记录。
	 * @author wangjb 2019年9月3日。
	 * @param orderid 订单id。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("orderstatistics")
	@RequestMapping("/getRefundRecordByOrderId.xhtml")
	@OperateLog(value="根据订单id获取退款记录", type = OperateLogRequestType.PASSIVE)
	public ModelMap getRefundRecordByOrderIdController (Long orderid) throws Exception{
		List<Refundrecords> result = this.BCOS.getRefundRecordByOrderIdService(orderid);
		return super.createModel(true, result);
	}
}
