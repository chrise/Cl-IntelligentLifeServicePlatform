/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MainController.java
 * History:
 *         2019年5月15日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PassiveLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.service.main.MainService;

/**
 * 主要控制器
 * 
 * @author CJH
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/main")
public class MainController extends IndexController {
	
	/**
	 * 功能菜单Service
	 */
	private @Autowired MainService mainService;
	
	/**
	 * 根据父级功能主键和功能类型查询已授权功能
	 * 
	 * @author CJH 2019年5月15日
	 * @param parentfunc 父级功能主键
	 * @param functype 功能类型
	 * @return 功能
	 */
	@PostMapping("/findAuthByParentfuncAndFunctype.xhtml")
	@ResponseBody
	@PassiveLog("根据父级功能主键和功能类型查询已授权功能")
	@PreAuthorize("*")
	public ModelMap findAuthorizationByParentfuncAndFunctype(Long parentfunc, @RequestParam(value = "functype[]", required = false) List<Integer> functype) {
		return super.createModel(true, mainService.findAuthorizationByParentfuncAndFunctype(parentfunc, functype));
	}
	
	/**
	 * 前往修改密码页面
	 * 
	 * @author CJH 2019年7月10日
	 * @return 页面路径
	 */
	@GetMapping("/goPasswordEditPage.xhtml")
	@OperateLog("前往修改密码页面")
	@PreAuthorize("*")
	public String goPasswordEditPage() {
		return "/pc/main/passwordEditPage";
	}
	
	/**
	 * 修改当前登录用户密码
	 * 
	 * @author CJH 2019年7月10日
	 * @param oldpassword 原密码
	 * @param newpassword 新密码
	 * @return 结果信息
	 */
	@PostMapping("/updatePassword.xhtml")
	@ResponseBody
	@OperateLog("修改当前登录用户密码")
	@PreAuthorize("*")
	public ModelMap updatePassword(String oldpassword, String newpassword) {
		mainService.updatePassword(oldpassword, newpassword);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 前往修改登录名页面
	 * 
	 * @author CJH 2019年8月7日
	 * @return 页面路径
	 */
	@GetMapping("/goLoginnameEditPage.xhtml")
	@OperateLog("前往修改登录名页面")
	@PreAuthorize("*")
	public String goLoginnameEditPage() {
		return "/pc/main/loginnameEditPage";
	}
	
	/**
	 * 修改当前登录用户登录名
	 * 
	 * @author CJH 2019年8月7日
	 * @param loginname 登录名
	 * @return 结果信息
	 */
	@PostMapping("/updateLoginname.xhtml")
	@ResponseBody
	@OperateLog("修改当前登录用户登录名")
	@PreAuthorize("*")
	public ModelMap updateLoginname(String loginname) {
		mainService.updateLoginname(loginname);
		return super.createModelOnlyStatus(true);
	}
}
