/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSalesMgrController.java
 * History:
 *         2019年6月11日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.goodsales;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.GoodSalesExtension;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.service.goodsales.GoodSalesService;

/**
 * 商品销售控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/goodsales")
public class GoodSalesController extends IndexController{

	/**
	 * 商品销售管理业务处理。
	 */
	@Autowired GoodSalesService goodSalesService;
	
	/**
	 * 跳转至商品销售管理页面。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	@PreAuthorize("goodsales")
	@RequestMapping("/goGoodSalesMgrPage.xhtml")
	@OperateLog("跳转至商品销售管理页面")
	public String goGoodSalesMgrPageController() {
		return "/pc/goodsales/GoodSalesMgrPage";
	}
	
	/**
	 * 跳转至商品销售回收站管理页面。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	@PreAuthorize("goodsales.recycle")
	@RequestMapping(value = "/goGoodSalesMgrPage.xhtml", params = "recycle=true")
	@OperateLog("跳转至商品销售回收站管理页面")
	public String goGoodSalesRecycleMgrPageController() {
		return "/pc/goodsales/GoodSalesMgrPage";
	}
	
	/**
	 * 获取商户销量列表数据。
	 * @author wangjb 2019年6月11日。
	 * @param params 页面查询参数。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize({"goodsales", "goodsales.recycle"})
	@RequestMapping("/getGoodSalesMgr.xhtml")
	@OperateLog(value="获取商户销量列表数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGoodSalesMgrController (@RequestParam Map<String, Object> params) throws Exception{
		Page<GoodSalesExtension> result = this.goodSalesService.getGoodSalesMgrService(params);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至商品销售新增、编辑页面。
	 * @author wangjb 2019年6月12日。
	 * @return
	 */
	@PreAuthorize({"goodsales.add", "goodsales.edit"})
	@RequestMapping("/goUpdateGoodSalesPage.xhtml")
	@OperateLog("跳转至商品销售新增、编辑页面")
	public String goUpdateGoodSalesPageController() {
		return "/pc/goodsales/UpdateGoodSalesPage";
	}
	
	/**
	 * 根据入驻id查询入驻下的所有规格商品。
	 * @author wangjb 2019年6月14日。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodsales.add", "goodsales.edit"})
	@RequestMapping("/getGoodSalesBySettleid.xhtml")
	@OperateLog(value="获取商户入驻标识。", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGoodSalesBySettleidController(Long settleid) {
		List<Map<String, Object>> result = this.goodSalesService.getGoodSalesBySettleidService(settleid);
		return super.createModel(true, result);
	}
	
	/**
	 * 获取商户入驻标识。
	 * @author wangjb 2019年6月12日。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodsales.add", "goodsales.edit"})
	@RequestMapping("/getSettleIdContr.xhtml")
	@OperateLog(value="获取商户入驻标识。", type = OperateLogRequestType.PASSIVE)
	public ModelMap getSettleIdController() {
		List<Map<String, Object>> result = this.goodSalesService.getSettleIdService();
		return super.createModel(true, result);
	}
	
	/**
	 * 根据商户id获取商品。
	 * @author wangjb 2019年6月12日。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodsales.add", "goodsales.edit"})
	@RequestMapping("/getGoodByMerchantId.xhtml")
	@OperateLog(value="根据商户id获取商品。", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGoodByMerchantIdController(Long mid, Long settleid) {
		List<Map<String, Object>> result = this.goodSalesService.getGoodByMerchantIdService(mid, settleid);
		return super.createModel(true, result);
	}
	
	/**
	 * 新增商品销售对象。
	 * @author wangjb 2019年6月14日。
	 * @param settleid
	 * @param specid
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("goodsales.add")
	@RequestMapping(value = "/updateGoodSalesObj.xhtml", params = "execute=add")
	@OperateLog("新增商品销售对象。")
	public ModelMap insertGoodSalesObjController(Long settleid, String specid, String del) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		this.goodSalesService.updateGoodSalesObjService(result, settleid, specid, del);
		return super.createModel(true, result);
	}
	
	/**
	 * 编辑商品销售对象。
	 * @author wangjb 2019年6月14日。
	 * @param settleid
	 * @param specid
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("goodsales.edit")
	@RequestMapping(value = "/updateGoodSalesObj.xhtml", params = "execute=edit")
	@OperateLog("编辑商品销售对象。")
	public ModelMap updateGoodSalesObjController(Long settleid, String specid, String del) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		this.goodSalesService.updateGoodSalesObjService(result, settleid, specid, del);
		return super.createModel(true, result);
	}
	
	/**
	 * 商品销售查看详情。
	 * @author wangjb 2019年6月14日。
	 * @return
	 */
	@PreAuthorize({"goodsales", "goodsales.recycle"})
	@RequestMapping("/goGoodSalesDetailPage.xhtml")
	@OperateLog("商品销售查看详情。")
	public String goGoodSalesDetailPageController() {
		return "/pc/goodsales/GoodSalesDetailPage";
	}
	
	/**
	 * 根据入驻id获取商品销售详情。
	 * @author wangjb 2019年6月14日。
	 * @param param 页面查询参数。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"goodsales", "goodsales.recycle"})
	@RequestMapping("/getGoodSalesDetailBySettleid.xhtml")
	@OperateLog(value="根据入驻id获取商品销售详情。", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGoodSalesDetailBySettleidController(@RequestParam Map<String, Object> param) {
		List<Map<String, Object>> result = this.goodSalesService.getGoodSalesDetailBySettleidService(param);
		return super.createModel(true, result);
	}
}
