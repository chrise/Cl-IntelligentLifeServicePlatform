/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileIndexController.java
 * History:
 *         2019年5月11日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import club.coderleague.ilsp.common.annotation.OperateLog;

/**
 * 移动端默认控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(MobileIndexController.REQUEST_PREFIX)
public class MobileIndexController extends IndexController {
	/**
	 * 请求前缀。
	 */
	public static final String REQUEST_PREFIX = IndexController.REQUEST_PREFIX + "/mobile";
	/**
	 * 绑定请求后缀。
	 */
	public static final String BIND_REQUEST_SUFFIX = "/bind.xhtml";
	/**
	 * 过期请求后缀。
	 */
	public static final String EXPIRE_REQUEST_SUFFIX = "/expire.xhtml";
	
	/**
	 * 前往手机绑定页面。
	 * @author Chrise 2019年5月29日
	 * @return 手机绑定页面地址。
	 */
	@GetMapping(BIND_REQUEST_SUFFIX)
	@OperateLog("访问手机绑定页面")
	public String bind() {
		return "/mobile/bind/bind";
	}
}
