/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantsController.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.controller.merchants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.service.merchants.MerchantsService;

/**
 * 商户
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/merchants")
public class MerchantsController extends IndexController {

	@Autowired
	MerchantsService merchantsService;
	
	
	/**
	 * 获取所有的商户数据
	 * @author Liangjing 2019年5月24日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @param istate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants","merchants.recycle"})
	@OperateLog(value = "获取所有的商户数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallmerchants.xhtml")
	public ModelMap getTheAllMerchants(String pageIndex,String pageSize,Long searchnameentityid,String searchphonehash,boolean isrecycle) {
		return super.createModel(true,merchantsService.getTheAllMerchants(pageIndex, pageSize, searchnameentityid, searchphonehash, isrecycle));
	}
	/**
	 * 获取商户编辑页面加载数据
	 * @author Liangjing 2019年5月24日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants.edit","merchants.recycle"})
	@OperateLog(value = "获取商户编辑页面加载数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthemerchantseditpagedata.xhtml")
	public ModelMap getTheMerchantsEditPagedata(Long entityid) {
		return super.createModel(true,merchantsService.getTheMerchantsEditPagedata(entityid));
	}
	/**
	 * 获取商户查看详情页面加载数据
	 * @author Liangjing 2019年6月8日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants","merchants.recycle"})
	@OperateLog(value = "获取商户编辑页面加载数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthemerchantslookinfopagedata.xhtml")
	public ModelMap getTheMerchantsLookInfoPagedata(Long entityid) {
		return super.createModel(true,merchantsService.getTheMerchantsLookInfoPagedata(entityid));
	}
	/**
	 * 更新商户对象
	 * @author Liangjing 2019年5月24日
	 * @param data
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants.add","merchants.edit"})
	@OperateLog(value = "更新商户对象")
	@RequestMapping("/updatemerchants.xhtml")
	public ModelMap updateTheMerchants(String data,String array,String bank) throws Exception {
		return super.createModel(true,merchantsService.updateTheMerchants(data,array,bank));
	}
	/**
	 * 更新商户状态
	 * @author Liangjing 2019年5月24日
	 * @param entityids
	 * @param istate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants.start","merchants.ban","merchants.del","merchants.recycle"})
	@OperateLog(value = "更新商户状态")
	@RequestMapping("/updatemerchantsstate.xhtml")
	public ModelMap updateTheMerchantsState(String entityids,Integer istate) {
		return super.createModel(true,merchantsService.updateTheMerchantsState(entityids, istate));
	}
	
	/**
	 * 获取对应展开查询商户列表
	 * @author Liangjing 2019年7月25日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants","merchants.recycle"})
	@OperateLog(value = "获取对应展开查询商户列表", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallsearchmerchant.xhtml")
	public ModelMap getTheAllSearchMerchantName() {
		return super.createModel(true,merchantsService.getTheAllSearchMerchant());
	}
	/**
	 * 获取对应展开查询商户电话
	 * @author Liangjing 2019年7月25日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants","merchants.recycle"})
	@OperateLog(value = "获取对应展开查询商户电话", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallsearchmerchantphonehash.xhtml")
	public ModelMap getTheAllSearchMerchantPhoneHash() {
		return super.createModel(true,merchantsService.getTheAllSearchMerchantPhoneHash());
	}
	
	/**
	 * 获取所有开户行父级银行
	 * @author Liangjing 2019年7月26日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants.add","merchants.edit"})
	@OperateLog(value = "获取所有开户行父级银行", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallparentbanks.xhtml")
	public ModelMap getTheAllParentBanks() {
		return super.createModel(true,merchantsService.getTheAllParentBanks());
	}
	
	/**
	 * 获取所有开户行子级银行
	 * @author Liangjing 2019年7月26日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"merchants.add","merchants.edit"})
	@OperateLog(value = "获取所有开户行子级银行", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallsonbanks.xhtml")
	public ModelMap getTheAllSonBanks(Long entityid) {
		return super.createModel(true,merchantsService.getTheAllSonBanks(entityid));
	}
	
	/**
	 * 前往商户首页
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"merchants","merchants.recycle"})
	@OperateLog("前往商户首页")
	@RequestMapping("/gohomepage.xhtml")
	public String gohomepage() {
		return "/pc/merchants/homepage";
	}
	/**
	 * 前往商户修改页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"merchants.add","merchants.edit"})
	@OperateLog("前往商户修改页面")
	@RequestMapping("/goupdatepage.xhtml")
	public String goupdatepage(Model model) {
		model.addAttribute("marketlist", merchantsService.getTheAllMarkets());
		return "/pc/merchants/updatepage";
	}
	/**
	 * 前往商户查看详情页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"merchants","merchants.recycle"})
	@OperateLog("前往商户查看详情页面")
	@RequestMapping("/golookinfopage.xhtml")
	public String golookinfopage() {
		return "/pc/merchants/lookinfo";
	}
	
}
