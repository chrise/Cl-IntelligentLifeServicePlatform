/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MobileManagerController.java
 * History:
 *         2019年7月30日: Initially created, Liangj.
 */
package club.coderleague.ilsp.controller.mobile.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.controller.MobileIndexController;
import club.coderleague.ilsp.service.mobile.manager.MobileManagerService;
import club.coderleague.ilsp.service.orders.OrdersService;

/**
 * 手机管理界面
 * @author Liangjing
 */
@Controller
@RequestMapping(MobileIndexController.REQUEST_PREFIX + "/mobilemanager")
public class MobileManagerController extends MobileIndexController {

	@Autowired
	MobileManagerService mobileManagerService;
	@Autowired
	OrdersService ordersService;
	
	/**
	 * 更新商户短信通知或者自动结算
	 * @author Liangjing 2019年7月30日
	 * @param istate
	 * @param itype
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog(value = "更新商户短信通知或者自动结算")
	@RequestMapping("/updatethemerchantsmnotifyorautosettle.xhtml")
	public ModelMap updateTheMerchantSmNotifyOrAutoSettle(Boolean istate, Integer itype) {
		return super.createModel(true, mobileManagerService.updateTheMerchantSmNotifyOrAutoSettle(super.getUserId(), istate, itype));
	}
	
	/**
	 * 更新商户账户总额
	 * @author Liangjing 2019年7月30日
	 * @param moeny
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog(value = "更新商户账户总额")
	@RequestMapping("/updatethemerchantmoeny.xhtml")
	public ModelMap updateTheMerchantMoeny(Double moeny) throws Exception {
		return super.createModel(true, mobileManagerService.updateTheMerchantMoeny(super.getUserId(), moeny));
	}
	
	/**
	 * 获取对应结算数据详情
	 * @author Liangjing 2019年7月30日
	 * @param entityid
	 * @return
	 */
	@PreAuthorize({"*"})
	@OperateLog(value = "获取对应结算数据详情")
	@RequestMapping("/getthemerchantsettlementinfo.xhtml")
	public String getTheMerchantSettlementInfo(Model model, Long entityid) {
		model.addAttribute("info", mobileManagerService.getTheMerchantSettlementInfo(entityid));
		return "/mobile/merchant/main::info_refresh";
	}
	
	/**
	 * 撤销商户结算
	 * @author Liangjing 2019年7月30日
	 * @param entityid
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog(value = "撤销商户结算")
	@RequestMapping("/updatethemerchantsettlementstoback.xhtml")
	public ModelMap updateTheMerchantSettlementsToBack(Long entityid) throws Exception {
		return super.createModel(true, mobileManagerService.updateTheMerchantSettlementsToBack(entityid));
	}
	
	/**
	 * 修改会员昵称
	 * @author Liangjing 2019年8月19日
	 * @param nickname
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/updatethemembernickname.xhtml")
	@OperateLog("修改会员昵称 ")
	public ModelMap updateTheMemberNickName(String nickname) {
		return super.createModel(true, mobileManagerService.updateTheMemberNickName(super.getUserId(), nickname));
	}
	/**
	 * 解除会员的绑定
	 * @author Liangjing 2019年8月19日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/updatethemembertoremovebing.xhtml")
	@OperateLog("解除会员的绑定 ")
	public ModelMap updateTheMemberToRemoveBing(Long entityid) {
		return super.createModel(true, mobileManagerService.updateTheMemberToRemoveBing(entityid));
	}
	/**
	 * 获取会员对应tabindex的所有订单
	 * @author Liangjing 2019年8月29日
	 * @param model
	 * @param tabIndex
	 * @return
	 */
	@PreAuthorize({"*"})
	@OperateLog(value = "获取会员对应tabindex的所有订单")
	@RequestMapping("/getthesomeorders.xhtml")
	public String getTheSomeOrders(Model model, Integer tabIndex) {
		model.addAttribute("result", mobileManagerService.getTheOrdersByMemberidAndTabIndex(super.getUserId(), tabIndex));
		return "/mobile/member/mobilepersonal/mobileOrderManagement::order_refresh";
	}
	/**
	 * 删除订单
	 * @author Liangjing 2019年8月29日
	 * @param orderid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/deletetheorder.xhtml")
	@OperateLog("删除订单 ")
	public ModelMap deleteTheOrder(Long orderid) {
		return super.createModel(true, mobileManagerService.updateTheOrderStata(orderid, EntityState.DELETED.getValue()));
	}
	/**
	 * 取消订单
	 * @author Liangjing 2019年8月29日
	 * @param orderid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/cancletheorder.xhtml")
	@OperateLog("取消订单 ")
	public ModelMap cancleTheOrder(Long orderid) {
		return super.createModel(true, mobileManagerService.updateTheOrderStata(orderid, EntityState.CANC.getValue()));
	}
	/**
	 * 完成订单
	 * @author Liangjing 2019年8月30日
	 * @param orderid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/finishtheorder.xhtml")
	@OperateLog("完成订单")
	public ModelMap finishTheOrder(Long orderid) {
		ordersService.updateCompleteOrder(orderid);
		return super.createModel(true);
	}
	/**
	 * 订单再次购买
	 * @author Liangjing 2019年8月30日
	 * @param orderid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/payagainorder.xhtml")
	@OperateLog("订单再次购买 ")
	public ModelMap payAgainOrder(Long orderid) {
		return super.createModel(true, mobileManagerService.updateTheAgainPayCar(orderid, super.getUserId()));
	}
	/**
	 * 修改订单支付方式
	 * @author Liangjing 2019年9月12日
	 * @param orderid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/updatetheorderpaytype.xhtml")
	@OperateLog("修改订单支付方式 ")
	public ModelMap updateTheOrderPayType(Long orderid) {
		return super.createModel(true, mobileManagerService.updateTheOrderPayType(orderid));
	}
}
