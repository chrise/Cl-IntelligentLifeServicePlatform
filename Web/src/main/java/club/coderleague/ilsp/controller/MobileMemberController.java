/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileMemberController.java
 * History:
 *         2019年5月29日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.service.mobile.goodgroups.MobileGoodGroupsServers;
import club.coderleague.ilsp.service.mobile.manager.MobileManagerService;
import club.coderleague.ilsp.service.mobile.mobiledrawback.MobileDrawBackService;
import club.coderleague.ilsp.service.mobile.shoppingcarts.MobileShoppingCartService;

/**
 * 移动端会员商城控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(MobileMemberController.REQUEST_PREFIX)
public class MobileMemberController extends MobileIndexController {
	
	@Autowired
	MobileGoodGroupsServers mobileGoodGroupsServers;
	
	/**
	 * - 购物车业务处理器。
	 */
	@Autowired MobileShoppingCartService shoppingCartService;
	
	/**
	 * -退款业务处理。
	 */
	@Autowired MobileDrawBackService drawBackService;
	
	@Autowired
	MobileManagerService mobileManagerService;
	
	/**
	 * 请求前缀。
	 */
	public static final String REQUEST_PREFIX = MobileIndexController.REQUEST_PREFIX + "/member";
	
	/**
	 * 跳转到首页
	 * @author yj 2019年5月27日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/main.xhtml")
	@OperateLog("跳转到首页（移动端）")
	public String mobilemainController(Model model) {
		model.addAttribute("carnum", mobileGoodGroupsServers.getTheUserGoodCarNum(super.getUserId()));
		return "/mobile/member/mobilemain/main";
	}
	
	/**
	 * 跳转到菜单分类
	 * @author yj 2019年5月31日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileItem.xhtml")
	@OperateLog("跳转到菜单分类")
	public String mobilemainlistitemizeController(Model model) {
		model.addAttribute("goodgroups", mobileGoodGroupsServers.getTheAllMobileGoodGroups(null));
		model.addAttribute("carnum", mobileGoodGroupsServers.getTheUserGoodCarNum(super.getUserId()));
		return "/mobile/member/mobileitemize/mobileItemize";
	}
	/**
	 * 通过点击顶级分类加载手机端商品分类
	 * @author Liangjing 2019年6月17日
	 * @param model
	 * @param key
	 * @return
	 */
	@PreAuthorize("*")
	@RequestMapping("/reloadmobileItem.xhtml")
	@OperateLog("通过点击顶级分类加载手机端商品分类")
	public String reloadmobilemainlistitemizeController(Model model,Long entityid) {
		model.addAttribute("goodgroups", mobileGoodGroupsServers.getTheAllMobileGoodGroups(entityid));
		return "/mobile/member/mobileitemize/mobileItemize::goodgroups_refresh";
	}
	
	/**
	 * 跳转到分类列表
	 * @author yj 2019年5月29日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mainlist.xhtml")
	@OperateLog("跳转到分类列表")
	public String mobilemainlistController(Model model,String limitMin, String limitMax,Long goodgroupid, String key) {
		model.addAttribute("goods", mobileGoodGroupsServers.getTheSingleGoodsExtension(limitMin, limitMax, key, goodgroupid));
		model.addAttribute("filter", mobileGoodGroupsServers.getTheAllFilterConditions(goodgroupid, key));
		model.addAttribute("goodgroupid", !CommonUtil.isEmpty(goodgroupid) ? goodgroupid.toString() : null);
		model.addAttribute("key", key);
		return "/mobile/member/menulist/menuList";
	}
	/**
	 * 刷新手机商品列表页面
	 * @author Liangjing 2019年6月26日
	 * @param model
	 * @param limitMin
	 * @param limitMax
	 * @param goodgroupid
	 * @param key
	 * @return
	 */
	@PreAuthorize("*")
	@PostMapping("/refreshmobilemainlist.xhtml")
	@OperateLog("刷新手机商品列表页面")
	public String refreshmobilemainlist(Model model,String limitMin, String limitMax,Long goodgroupid,String key, String classSearch, String merchatSearch, String marketSearch) {
		model.addAttribute("goods", mobileGoodGroupsServers.refreshmobilemainlist(limitMin, limitMax, key, goodgroupid, classSearch, merchatSearch, marketSearch));
		return "/mobile/member/menulist/menuList::goods_refresh";
	}
	
	/**
	 * 跳转到菜单详情页面
	 * @author yj 2019年5月30日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mainlistd.xhtml")
	@OperateLog("跳转到菜单详情页面")
	public String mobilemainlistdetailsController() {
		return "/mobile/member/menulist/menuListDetails";
	}
	
	/**
	 * 跳转到购物车
	 * @author yj 2019年5月31日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileshoppingcart.xhtml")
	@OperateLog("跳转到购物车")
	public String mobileshoppingcartController(Model model) {
		Map<String, Object> result = this.shoppingCartService.getShoppingCartListByMemberIdService(super.getUserId());
		model.addAttribute("shoppingCart", result);
		model.addAttribute("carnum", mobileGoodGroupsServers.getTheUserGoodCarNum(super.getUserId()));
		return "/mobile/member/mobileshoppingcart/mobileShoppingCart";
	}
	
	/**
	 * 跳转到确认订单
	 * @author yj 2019年6月24日。
	 * @param model
	 * @param buynow 是否立即购买。
	 * @return
	 */
	@PreAuthorize("*")
	@RequestMapping("/mobilesubmito.xhtml")
	@OperateLog("跳转到确认订单")
	public String mobilesubmitoController(Model model, Boolean buynow) {
		if (buynow == null) buynow = true;
		Map<String, Object> settleOrder = this.shoppingCartService.getShoppingCartSettlementDataByParamService(buynow, super.getUserId());
		model.addAttribute("settleOrder", settleOrder);
		model.addAttribute("buynow", buynow);
		return "/mobile/member/mobileshoppingcart/mobileSubmitOrder";
	}
	
	/**
	 * 跳转到个人中心
	 * @author yj 2019年5月31日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobilepersonal.xhtml")
	@OperateLog("跳转到个人中心")
	public String mobilepersonalController(Model model) {
		Map<String, Object> result = this.mobileManagerService.getMemberInformationByMemberIdService(super.getUserId());
		model.addAttribute("result", result);
		model.addAttribute("carnum", mobileGoodGroupsServers.getTheUserGoodCarNum(super.getUserId()));
		return "/mobile/member/mobilepersonal/mobilePersonal";
	}
	
	/**
	 * 跳转到订单管理
	 * @author yj 2019年6月13日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileorderm.xhtml")
	@OperateLog("跳转到订单管理 ")
	public String mobileordermController(Model model,Integer tabindex) {
		model.addAttribute("waitpaynum", mobileManagerService.getTheMemberWaitPayOrders(super.getUserId()));
		model.addAttribute("waittakenum", mobileManagerService.getTheMemberWaitTakeOrders(super.getUserId()));
		model.addAttribute("tabindex", tabindex);
		return "/mobile/member/mobilepersonal/mobileOrderManagement";
	}
	/**
	 * 跳转到订单详情
	 * @author Liangjing 2019年8月30日
	 * @param model
	 * @param orderid
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileorderdetail.xhtml")
	@OperateLog("跳转到订单详情 ")
	public String mobileorderdetailController(Model model,Long orderid,Integer orderstate, Integer tabindex) {
		model.addAttribute("info", mobileManagerService.getTheMobileOrderDetailsByOrderidAndTabIndex(orderid, orderstate));
		model.addAttribute("tabindex", tabindex);
		return "/mobile/member/mobilepersonal/mobileOrderManagementDetails";
	}
	
	/**
	 * 跳转到待付款订单详情
	 * @author yj 2019年6月13日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileordermd.xhtml")
	@OperateLog("跳转到待付款订单详情 ")
	public String mobileordermdController() {
		return "/mobile/member/mobilepersonal/mobileOrderManagementDetails";
	}
	
	/**
	 * 跳转到待发货订单详情
	 * @author yj 2019年8月14日。
	 * @return
	 */
	//@PreAuthorize("*")
	//@GetMapping("/mobileshipped.xhtml")
	//@OperateLog("跳转到待发货订单详情")
	//public String mobileshippedController() {
		//return "/mobile/member/mobilepersonal/mobileOrderManagementShippedDetails";
	//}
	
	/**
	 * 跳转到待收货订单详情
	 * @author yj 2019年8月14日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobilereceived.xhtml")
	@OperateLog("跳转到待收货订单详情")
	public String mobilereceivedController() {
		return "/mobile/member/mobilepersonal/mobileOrderManagementReceivedDetails";
	}
	
	/**
	 * 跳转到已完成订单详情
	 * @author yj 2019年8月15日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobilefinished.xhtml")
	@OperateLog("跳转到已完成订单详情")
	public String mobilefinishedController() {
		return "/mobile/member/mobilepersonal/mobileOrderManagementFinishedDetails";
	}
	
	/**
	 * 跳转到已取消订单详情
	 * @author yj 2019年8月15日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobilecanceled.xhtml")
	@OperateLog("跳转到已取消订单详情")
	public String mobilecanceledController() {
		return "/mobile/member/mobilepersonal/mobileOrderManagementCanceledDetails";
	}
	
	/**
	 * 跳转到处理中退款详情
	 * @author yj 2019年8月19日。
	 * @param orderid 订单id。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobiledbd.xhtml")
	@OperateLog("跳转到处理中退款详情")
	public String mobiledrawbackdetailsController(Long orderid, Model model) {
		model.addAttribute("refundhandle", this.drawBackService.getRefundHandleByOrderIdService(orderid));
		return "/mobile/member/mobilepersonal/mobileDrawBackDetails.html";
	}
	
	/**
	 * 跳转到处理记录退款详情
	 * @author yj 2019年8月19日。
	 * @param orderid 订单id。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobiledbdr.xhtml")
	@OperateLog("跳转到处理记录退款详情")
	public String mobiledrawbackrdetailsController(Long orderid, Model model) {
		model.addAttribute("refundrecord", this.drawBackService.getRefundRecordByOrderIdService(orderid));
		return "/mobile/member/mobilepersonal/mobileDrawBackRecordDetails.html";
	}
	
	/**
	 * 跳转到待评价
	 * @author yj 2019年8月9日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileappraise.xhtml")
	@OperateLog("跳转到待评价 ")
	public String mobileappraiseController() {
		return "/mobile/member/mobilepersonal/mobileAppraise";
	}
 	
	/**
	 * 跳转到退款
	 * @author yj 2019年8月12日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobiledrawback.xhtml")
	@OperateLog("跳转到退款 ")
	public String mobiledrawbackController() {
		return "/mobile/member/mobilepersonal/mobileDrawBack";
	}
	
	/**
	 * 跳转到个人设置
	 * @author yj 2019年8月12日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileSetUp.xhtml")
	@OperateLog("跳转到个人设置 ")
	public String mobilesetupController(Model model) {
		model.addAttribute("info", mobileManagerService.getTheMobileUserInfo(super.getUserId()));
		return "/mobile/member/mobilepersonal/mobileSetUp";
	}
	
	/**
	 * 跳转到管理地址
	 * @author yj 2019年7月2日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileaddress.xhtml")
	@OperateLog("跳转到管理地址 ")
	public String mobileaddressController(Model model) {
		List<Map<String, Object>> receiviAreas = this.shoppingCartService.getWetChatReceiviAreasService();
		model.addAttribute("receiviAreas", receiviAreas);
		return "/mobile/member/mobileshoppingcart/mobileShoppingAddress";
	}
	
	/**
	 * 跳转到管理地址
	 * @author yj 2019年7月2日。
	 * @return
	 */
	@PreAuthorize("*")
	@PostMapping("/executeLoadAddress.xhtml")
	@OperateLog("跳转到管理地址 ")
	public String executeLoadAddressController(Model model) {
		Map<String, Object> result = this.shoppingCartService.getReceivingAddressesByMemberIdService(super.getUserId());
		model.addAttribute("addresses", result);
		return "/mobile/member/mobileshoppingcart/mobileShoppingAddress::address_list";
	}
	
	/**
	 * 跳转到添加新地址
	 * @author yj 2019年7月2日。
	 * @parma entityid 收货地址id。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobileaddressnew.xhtml")
	@OperateLog("跳转到添加新地址 ")
	public String mobileaddressnewController(Long entityid, Model model) {
		model.addAttribute("entityid", entityid);
		return "/mobile/member/mobileshoppingcart/mobileAddressAdd";
	}
	
	/**
	 * 跳转到搜索页面
	 * @author yj 2019年6月18日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/mobilesearch.xhtml")
	@OperateLog("跳转到搜索页面 ")
	public String mobilesearchController() {
		return "/mobile/member/mobilesearch/mobileSearch";
	}
}
