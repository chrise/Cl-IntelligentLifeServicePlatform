/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MobileGoodGroupsController.java
 * History:
 *         2019年6月17日: Initially created, Liangj.
 */
package club.coderleague.ilsp.controller.mobile.goodgroups;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.controller.MobileIndexController;
import club.coderleague.ilsp.service.mobile.goodgroups.MobileGoodGroupsServers;

/**
 * 手机-商品分类
 * @author Liangjing
 */
@Controller
@RequestMapping(MobileIndexController.REQUEST_PREFIX + "/mobilegoodgroups")
public class MobileGoodGroupsController extends MobileIndexController  {
	
	@Autowired
	MobileGoodGroupsServers mobileGoodGroupsServers;
	
	/**
	 * 获取手机商品list-加载更多商品
	 * @author Liangjing 2019年6月25日
	 * @param limitMin
	 * @param limitMax
	 * @param key
	 * @param goodgroupid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog(value = "获取手机商品list-加载更多商品")
	@RequestMapping("/getthesinglegoodsextension.xhtml")
	public ModelMap getTheSingleGoodsExtension(String limitMin, String limitMax, String key, Long goodgroupid, String classSearch, String merchatSearch, String marketSearch) {
		return super.createModel(true, mobileGoodGroupsServers.getTheSingleGoodsList(limitMin, limitMax, goodgroupid, classSearch, merchatSearch, marketSearch));
	}
	/**
	 * 刷新手机商品详情页面数据
	 * @author Liangjing 2019年7月2日
	 * @param model
	 * @param goodentityid
	 * @param merchantsettleid
	 * @return
	 */
	@PreAuthorize("*")
	@PostMapping("/refreshmobilegooddetailinfo.xhtml")
	@OperateLog("刷新手机商品详情页面数据")
	public String refreshmobilegooddetailinfo(Model model,Long goodentityid, Long merchantsettleid) {
		model.addAttribute("goodinfo", mobileGoodGroupsServers.getTheGoodInfos(goodentityid, merchantsettleid));
		model.addAttribute("carnum", mobileGoodGroupsServers.getTheUserGoodCarNum(super.getUserId()));
		return "/mobile/member/menulist/menuListDetails::goodDetail_refresh";
	}
	
	/**
	 * 更新手机购物车数据
	 * @author Liangjing 2019年7月4日
	 * @param memberid
	 * @param goodsalesid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/updategoodintotheshoppingcartsbackisaddnum.xhtml")
	@OperateLog("更新手机购物车数据")
	public ModelMap updateGoodIntoTheShoppingCartsBackIsAddNum(Long goodsalesid, Double num) {
		return super.createModel(true, mobileGoodGroupsServers.updateGoodIntoTheShoppingCartsBackIsAddNum(super.getUserId(), goodsalesid, num));
	}
	/**
	 * 手机商品立即购买
	 * @author Liangjing 2019年7月22日
	 * @param goodsalesid
	 * @param num
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/updatepaygoods.xhtml")
	@OperateLog("手机商品立即购买")
	public ModelMap updatePayGoods(Long goodsalesid, Double num) {
		mobileGoodGroupsServers.updatePayGoods(super.getUserId(), goodsalesid, num);
		return super.createModel(true);
	}
	/**
	 * 手机商品详情获取对应所有商品规格
	 * @author Liangjing 2019年7月19日
	 * @param goodid
	 * @param merchantSettleid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/getheallgoodspecs.xhtml")
	@OperateLog("手机商品详情获取对应所有商品规格")
	public ModelMap geTheAllGoodSpecs(Long goodid,Long merchantsettleid){
		return super.createModel(true, mobileGoodGroupsServers.geTheAllGoodSpecs(goodid, merchantsettleid));
	}
	
}
