/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：InterfaceController.java
 * History:
 *         2019年5月28日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller.interfaces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.domain.beans.WebAuthContext;
import club.coderleague.ilsp.common.domain.enums.WebAuthScene;
import club.coderleague.ilsp.common.domain.enums.WebAuthor;
import club.coderleague.ilsp.controller.NosessionRequestController;

/**
 * 接口控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(InterfaceController.REQUEST_PREFIX)
public class InterfaceController extends NosessionRequestController {
	/**
	 * 请求前缀。
	 */
	public static final String REQUEST_PREFIX = NosessionRequestController.REQUEST_PREFIX + "/api";
	
	/**
	 * 错误消息键。
	 */
	protected static final String EM_KEY = "errorMessage";
	
	@Autowired
	private WeixinInterfaceController wxiController;
	@Autowired
	private AlipayInterfaceController apiController;
	
	/**
	 * 网页授权。
	 * @author Chrise 2019年5月31日
	 * @param context 授权上下文。
	 * @param model 数据模型。
	 * @return 授权请求地址。
	 */
	protected String webAuth(WebAuthContext context, Model model) {
		getSession().setAttribute(WebAuthContext.SESSION_KEY, context);
		return null;
	}
	
	/**
	 * 扫码支付。
	 * @author Chrise 2019年5月28日
	 * @param order 支付订单。
	 * @param model 数据模型。
	 * @return 扫码支付授权地址。
	 */
	@GetMapping("/scanpay.xhtml")
	@OperateLog("请求扫码支付")
	public String scanPay(String order, Model model) {
		String agent = getRequest().getHeader("User-Agent").toLowerCase();
		if (agent.contains("micromessenger")) {
			return this.wxiController.webAuth(new WebAuthContext(WebAuthScene.SCAN_PAY, WebAuthor.WEIXIN, order), model);
		} else if (agent.contains("alipayclient")) {
			return this.apiController.webAuth(new WebAuthContext(WebAuthScene.SCAN_PAY, WebAuthor.ALIPAY, order), model);
		}
		throw new RuntimeException("Unknown endpoint.");
	}
}
