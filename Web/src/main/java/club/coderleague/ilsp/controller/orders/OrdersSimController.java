/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrdersSimController.java
 * History:
 *         2019年8月22日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller.orders;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PassiveLog;
import club.coderleague.ilsp.common.domain.beans.OfflineOrder;
import club.coderleague.ilsp.controller.NosessionRequestController;
import club.coderleague.ilsp.service.orders.OrdersService;
import club.coderleague.ilsp.service.orders.OrdersSimService;
import club.coderleague.ilsp.util.QrCodeUtils;

/**
 * 订单模拟控制器
 * 
 * @author CJH
 */
@Controller
@RequestMapping(NosessionRequestController.REQUEST_PREFIX + "/sim/orders")
@ConditionalOnExpression("'${spring.profiles.active}'.indexOf('release') == -1")
public class OrdersSimController extends NosessionRequestController {

	private @Autowired OrdersSimService ordersSimService;
	
	private @Autowired OrdersService ordersService;
	
	/**
	 * 前往线下订单页面
	 * 
	 * @author CJH 2019年8月21日
	 * @return 页面路径
	 */
	@GetMapping("/goOfflineOrderPage.xhtml")
	@OperateLog("前往线下订单页面")
	public String goOfflineOrderPage() {
		return "/pc/orders/offlineOrderPage";
	}
	
	/**
	 * 查询市场、商户和商品
	 * 
	 * @author CJH 2019年8月21日
	 * @return 市场、商户和商品
	 */
	@PostMapping("/findMarketsData.xhtml")
	@ResponseBody
	@PassiveLog("查询市场、商户和商品")
	public ModelMap findMarketsData() {
		return super.createModel(true, ordersSimService.findMarketsData());
	}
	
	/**
	 * 新增线下订单
	 * 
	 * @author CJH 2019年8月21日
	 * @return 结果信息
	 */
	@PostMapping("/insertOfflineOrders.xhtml")
	@ResponseBody
	@OperateLog("新增线下订单")
	public ModelMap insertOfflineOrders(OfflineOrder offlineorder) {
		return super.createModel(true, ordersService.insertOfflineOrder(offlineorder));
	}
	
	/**
	 * 生成二维码
	 * 
	 * @author CJH 2019年8月22日
	 * @param response 响应内容
	 * @param content 二维码内容
	 */
	@GetMapping("/createQR.xhtml")
	@OperateLog("生成二维码")
	public void createQR(HttpServletResponse response, String content) {
		try {
			ImageIO.write(QrCodeUtils.createImage(content), "JPG", response.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 前往订单详情页面
	 * 
	 * @author CJH 2019年8月21日
	 * @return 页面路径
	 */
	@GetMapping("/goOrdersViewPage.xhtml")
	@OperateLog("前往订单详情页面")
	public String goOrdersViewPage() {
		return "/pc/orders/ordersViewPage";
	}
	
	/**
	 * 查询订单详情
	 * 
	 * @author CJH 2019年8月22日
	 * @param ordersid 订单标识
	 * @return 订单详情
	 */
	@PostMapping("/findOrdersInfo.xhtml")
	@ResponseBody
	@OperateLog("查询订单详情")
	public ModelMap findOrdersInfo(Long ordersid) {
		return super.createModel(true, ordersSimService.findOrdersInfo(ordersid));
	} 
}
