/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileShoppingCartController.java
 * History:
 *         2019年6月27日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.mobile.shoppingcarts;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Receivingaddresses;
import club.coderleague.ilsp.service.mobile.shoppingcarts.MobileShoppingCartService;

/**
 * WeChat - 购物车控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/mobileshoppingcart")
public class MobileShoppingCartController extends IndexController{
	
	/**
	 * WeChat - 购物车业务处理。
	 */
	@Autowired MobileShoppingCartService shoppingCartService;
	
	/**
	 * WeChat提交订单。
	 * @author wangjb 2019年7月7日。
	 * @param addressid 收货地址id。
	 * @param mid 会员备注。
	 * @param dm 配送方式。
	 * @param pm 支付方式。
	 * @param dt 配送时间。
	 * @param buynow 是否立即购买。
	 * @param diamond 钻石数量。
	 * @param integral 积分数量。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog("提交提订单")
	@PostMapping("/executeSubmitOrder.xhtml")
	public ModelMap executeSubmitOrderController(String addressid, String mid, Integer dm, Integer pm, String dt, Boolean buynow, Integer diamond, Integer integral) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		this.shoppingCartService.executeSubmitOrderService(addressid, mid, super.getUserId(), dm, pm, dt, result, buynow, diamond, integral);
		return super.createModel(true, result);
	}
	
	/**
	 * 根据收货地址id查询对象。
	 * @author wangjb 2019年7月13日。
	 * @param entityid 收货地址id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"*"})
	@OperateLog("根据收货地址id查询对象")
	@RequestMapping("/getReceiverAddressById.xhtml")
	public ModelMap getReceiverAddressByIdController(Long entityid){
		Receivingaddresses result = this.shoppingCartService.getReceiverAddressByIdService(entityid, super.getUserId());
		return super.createModel(true, result);
	}

	/**
	 * 新增或编辑收货地址对象。
	 * @author wangjb 2019年7月13日。
	 * @param data 收货地址json。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@RequestMapping("/insertOrUpdateAddressesObj.xhtml")
	@OperateLog("新增/编辑收货地址。")
	public ModelMap insertOrUpdateAddressesObjController(String data) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		Receivingaddresses addresse = new ObjectMapper().readValue(data, Receivingaddresses.class);
		this.shoppingCartService.insertOrUpdateAddressesObjService(result, addresse, super.getUserId());
		return super.createModel(true, result);
	}
	
	/**
	 * 新增个人地址查询区域。
	 * @author wangjb 2019年7月15日。
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@RequestMapping("/getWetChatReceiviAreas.xhtml")
	@OperateLog("新增个人地址查询区域。")
	public ModelMap getWetChatReceiviAreasController() throws Exception{
		List<Map<String, Object>> result = this.shoppingCartService.getWetChatReceiviAreasService();
		return super.createModel(true, result);
	}
	
	/**
	 * 修改购物车商品选中状态。
	 * @author wangjb 2019年7月18日。
	 * @param shoppingcartid 购物车。
	 * @param selectstate 商品选中状态。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@RequestMapping("/executeAmendSelectState.xhtml")
	@OperateLog("修改购物车商品选中状态。")
	public ModelMap executeAmendSelectStateController(String shoppingcartid, Boolean selectstate) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		this.shoppingCartService.executeAmendSelectStateService(result, shoppingcartid, super.getUserId(), selectstate);
		return super.createModel(true, result);
	}
	
	/**
	 * 修改商品数量。
	 * @author wangjb 2019年7月18日。
	 * @param shoppingcartid 购物车id。
	 * @param goodnumber 商品数量。
	 * @param pagestate 页面判断（T、订单， false、购物车页面）
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@RequestMapping("/executeAmendGoodNumber.xhtml")
	@OperateLog("修改购物车商品选中状态。")
	public ModelMap executeAmendGoodNumberController(String shoppingcartid, Double goodnumber, Boolean pagestate) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		this.shoppingCartService.executeAmendGoodNumberService(result, shoppingcartid, goodnumber, super.getUserId(), pagestate);
		return super.createModel(true, result);
	}
	
	/**
	 * 删除购物车里的商品数据。
	 * @author wangjb 2019年7月20日。
	 * @param shoppingcartid 购物车id。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@RequestMapping("/executeDelShoppingCartGood.xhtml")
	@OperateLog("删除购物车里的商品数据。")
	public ModelMap executeDelShoppingCartGoodController(String shoppingcartid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.shoppingCartService.executeDelShoppingCartGoodService(result, shoppingcartid);
		return super.createModel(true, result);
	}
	
	/**
	 * -删除个人地址。
	 * @author wangjb 2019年9月16日。
	 * @param entityid 实体id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@RequestMapping("/deleteAddressesObjByEntityId.xhtml")
	@OperateLog("删除个人地址。")
	public ModelMap deleteAddressesObjByEntityIdController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.shoppingCartService.deleteAddressesObjByEntityIdService(result, entityid);
		return super.createModel(true, result);
	}
}
