/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MerchantSettlementsMgrController.java
 * History:
 *         2019年6月15日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.merchantsettlements;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.MerchantSettlementsExtension;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Merchants;
import club.coderleague.ilsp.service.merchantsettlements.MerchantSettlementsMgrService;

/**
 * 商户结算管理控制器。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/msmc")
public class MerchantSettlementsMgrController extends IndexController{

	/**
	 * 商户结算管理业务处理。
	 */
	@Autowired MerchantSettlementsMgrService MSMS;
	
	/**
	 * 跳转至商户结算管理页面。
	 * @author wangjb 2019年6月15日。
	 * @return
	 */
	@PreAuthorize("merchantsettlements")
	@RequestMapping("/goMerchantSettlementsMgrPage.xhtml")
	@OperateLog(" 跳转至商户结算管理页面")
	public String goMerchantSettlementsMgrPageController() {
		return "/pc/merchantsettlements/MerchantSettlementsMgrPage";
	}
	
	/**
	 * 获取商户结算管理列表。
	 * @author wangjb 2019年6月15日。
	 * @param params 页面查询参数。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("merchantsettlements")
	@RequestMapping("/getMerchantSettlementsMgr.xhtml")
	@OperateLog(value=" 获取商户结算管理列表", type = OperateLogRequestType.PASSIVE)
	public ModelMap getMerchantSettlementsMgrController (@RequestParam Map<String, Object> params) throws Exception{
		Page<MerchantSettlementsExtension> result = this.MSMS.getMerchantSettlementsMgrService(params);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至自动结算页面。
	 * @author wangjb 2019年6月15日。
	 * @return
	 */
	@PreAuthorize("merchantsettlements.automaticSettle")
	@RequestMapping("/goMerchantAutomaticSettlementsPage.xhtml")
	@OperateLog("跳转至自动结算页面")
	public String goMerchantAutomaticSettlementsPageController() {
		return "/pc/merchantsettlements/MerchantAutomaticSettlementsPage";
	}
	
	/**
	 * 获取自动结算的商户数据。
	 * @author wangjb 2019年6月15日。
	 * @param params 页面查询参数。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("merchantsettlements.automaticSettle")
	@RequestMapping("/getMerchanstList.xhtml")
	@OperateLog(value="获取自动结算的商户数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getMerchanstListController (@RequestParam Map<String, Object> params) throws Exception{
		Page<Merchants> result = this.MSMS.getMerchanstListService(params);
		return super.createModel(true, result);
	}
	
	/**
	 * -商户自动结算对象 导出商品数据。
	 * @author wangjb 2019年6月15日。
	 * @param ids 商户id。逗号隔开。
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("merchantsettlements.automaticSettle")
	@PostMapping("/executeSaveMerchantSettlement.xhtml")
	@OperateLog("-商户自动结算对象 导出商品数据。")
	public void executeSaveMerchantSettlementController(HttpServletResponse response,String ids) throws Exception{
		try {
			XSSFWorkbook workbook = this.MSMS.executeSaveMerchantSettlementService(ids, super.getUserId());
			String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	        response.setContentType("application/vnd.ms-excel");
	        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(date + "_自动商户结算表.xlsx", "utf-8"));
	        workbook.write(response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 执行手动结算。
	 * @author wangjb 2019年6月15日。
	 * @param ids 结算id。 
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("merchantsettlements.manualSettle")
	@PostMapping("/executeManualSettleByIds.xhtml")
	@OperateLog("执行手动结算。")
	public void executeManualSettleByIdsController(HttpServletResponse response,String ids) {
		try {
			XSSFWorkbook workbook = this.MSMS.executeManualSettleByIdsService(ids, super.getUserId());
			String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	        response.setContentType("application/vnd.ms-excel");
	        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(date + "_手动商户结算表.xlsx", "utf-8"));
	        workbook.write(response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 导出Excel文件。
	 * @author wangjb 2019年7月29日。
	 * @param response
	 * @param ids
	 */
	@ResponseBody
	@PreAuthorize("merchantsettlements.exportExcel")
	@PostMapping("/executeExportExcelByIds.xhtml")
	@OperateLog("执行手动结算。")
	public void exportFileExcelByIdsController(HttpServletResponse response,String ids) {
		try {
			XSSFWorkbook workbook = this.MSMS.executeExportExcelByIdsService(ids);
			String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	        response.setContentType("application/vnd.ms-excel");
	        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(date + "_商户结算表.xlsx", "utf-8"));
	        workbook.write(response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 跳转至商户结算完成页面。
	 * @author wangjb 2019年7月27日。
	 * @return
	 */
	@PreAuthorize("merchantsettlements.settleOver")
	@RequestMapping("/goMerchantSettlementOverPage.xhtml")
	@OperateLog("跳转至商户结算完成页面")
	public String goMerchantSettlementOverPageController() {
		return "/pc/merchantsettlements/MerchantSettlementOverPage";
	}
	
	/**
	 * 执行商户结算完成。
	 * @author wangjb 2019年7月27日。
	 * @param entityid 结算id。
	 * @param payevidence 付款凭证。
	 * @param paytime 支付时间。
	 * @return
	 * @throws Exception  
	 */
	@ResponseBody
	@PreAuthorize("merchantsettlements.settleOver")
	@RequestMapping("/executeSettleOver.xhtml")
	@OperateLog("执行商户结算完成。")
	public ModelMap executeSettleOverController(Long entityid, String payevidence, String paytime) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		this.MSMS.executeSettleOverService(result, entityid, payevidence, paytime, super.getUserId());
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至商户结算完成页面。
	 * @author wangjb 2019年7月27日。
	 * @return
	 */
	@PreAuthorize("merchantsettlements.settleCancel")
	@RequestMapping("/goMerchantSettlementCancelPage.xhtml")
	@OperateLog("跳转至商户结算完成页面")
	public String goMerchantSettlementCancelPageController() {
		return "/pc/merchantsettlements/MerchantSettlementCancelPage";
	}
	
	/**
	 * 执行商户结算完成。
	 * @author wangjb 2019年7月27日。
	 * @param ids 结算id。
	 * @param payevidence 取消原因。
	 * @param paytime 取消时间。
	 * @return
	 * @throws Exception  
	 */
	@ResponseBody
	@PreAuthorize("merchantsettlements.settleCancel")
	@RequestMapping("/executeSettleCancel.xhtml")
	@OperateLog("执行商户结算取消。")
	public ModelMap executeSettleCancelController(String ids, String cancelreason, String paytime) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		this.MSMS.executeSettleCancelService(result, ids, cancelreason, paytime, super.getUserId());
		return super.createModel(true, result);
	}
}
