/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AlipayInterfaceController.java
 * History:
 *         2019年5月28日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller.interfaces;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.domain.beans.WebAuthContext;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.service.interfaces.AlipayInterfaceService;

/**
 * 支付宝接口控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(InterfaceController.REQUEST_PREFIX + "/alipay")
public class AlipayInterfaceController extends InterfaceController {
	private static final String EM_NOT_SUPPORTED = "系统暂不支持支付宝访问！";
	private static final String EM_AUTH_FAILED = "请求支付宝授权失败！";
	
	@Autowired
	private AlipayInterfaceService apiService;
	
	/**
	 * @see club.coderleague.ilsp.controller.interfaces.InterfaceController#webAuth(club.coderleague.ilsp.common.domain.beans.WebAuthContext, org.springframework.ui.Model)
	 */
	@Override
	protected String webAuth(WebAuthContext context, Model model) {
		super.webAuth(context, model);
		
		String url = this.apiService.getAuthUrl(getSession());
		
		if (url == null) model.addAttribute(EM_KEY, EM_AUTH_FAILED);
		else if ("".equals(url)) model.addAttribute(EM_KEY, EM_NOT_SUPPORTED);
		else return "redirect:" + url;
		
		return "/mobile/error/webauth";
	}
	
	/**
	 * 网页授权回调。
	 * @author Chrise 2019年5月28日
	 * @param auth_code 授权代码。
	 * @param state 状态。
	 * @param model 数据模型。
	 * @return 授权完成跳转地址。
	 */
	@GetMapping("/callback.xhtml")
	@OperateLog(value = "请求网页授权回调", type = OperateLogRequestType.PASSIVE)
	public String webAuthCallback(String auth_code, String state, Model model) {
		String openId = this.apiService.getOpenId(auth_code, state, getSession());
		
		if (openId == null) model.addAttribute(EM_KEY, EM_AUTH_FAILED);
		else {
			String url = this.apiService.onAuthSuccess(openId, getSession());
			if (url == null) model.addAttribute(EM_KEY, EM_AUTH_FAILED);
			else return "redirect:" + url;
		}
		
		return "/mobile/error/webauth";
	}
	
	/**
	 * 支付。
	 * @author Chrise 2019年6月10日
	 * @param order 订单标识。
	 * @param trade 交易号。
	 * @param model 数据模型。
	 * @return 支付页面地址。
	 */
	@GetMapping("/pay.xhtml")
	@OperateLog("请求支付")
	public String pay(String order, String trade, Model model) {
		Object result = this.apiService.createTrade(order, trade, getUserSession());
		model.addAttribute("trade", result);
		
		if (result != null && result instanceof Map) return "/mobile/api/pay";
		else return "/mobile/error/orderpay";
	}
	
	/**
	 * 支付通知。
	 * @author Chrise 2019年6月13日
	 */
	@ResponseBody
	@PostMapping("/notify.xhtml")
	@OperateLog(value = "请求支付通知", type = OperateLogRequestType.PASSIVE)
	public void payNotify() {
		this.apiService.handleTradeNotify(getRequest(), getResponse());
	}
	
	/**
	 * 获取授权代码（模拟支付宝开放平台）。
	 * @author Chrise 2019年5月28日
	 * @param redirect_uri 重定向地址。
	 * @param state 状态。
	 * @param model 数据模型。
	 * @return 授权回调地址。
	 */
	@GetMapping("/oauth2/publicAppAuthorize.htm")
	public String getAuthCode(String redirect_uri, String state, Model model) {
		String url = this.apiService.getAuthCallbackUrl(redirect_uri, state, getRequest());
		
		if (url == null) model.addAttribute(EM_KEY, EM_AUTH_FAILED);
		else return "redirect:" + url;
		
		return "/mobile/error/webauth";
	}
	
	/**
	 * 获取开放标识（模拟支付宝开放平台）。
	 * @author Chrise 2019年5月28日
	 * @param code 授权代码。
	 */
	@ResponseBody
	@PostMapping("/gateway.do")
	public void getOpenId(String code) {
		this.apiService.writeToResponse(code, getResponse());
	}
}
