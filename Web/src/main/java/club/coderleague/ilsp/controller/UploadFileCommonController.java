/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：GoodGroupsController.java
 * History:
 *         2019年5月21日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.config.properties.FileUploadSettings;
import club.coderleague.ilsp.common.domain.beans.UploadFile;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.common.util.CommonUtil;


/**
 * 上传附件公共控制器。
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/commonUpload")
public class UploadFileCommonController extends IndexController {
	
	/**
	 * 上传对象。
	 */
	@Autowired FileUploadSettings upload;
	
	/**
	 * 商户表附件上传附件
	 * @author Liangjing 2019年5月22日
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "商户表附件上传附件")
	@RequestMapping("/uploadMerchantsFile.xhtml")
	public ModelMap uploadMerchantsFile(HttpServletRequest request) throws Exception {
		List<UploadFile> list = null;
		List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
		list = CommonUtil.saveFiles(files, this.upload.getReal() + this.upload.getMerchantsPath(), this.upload.getMerchantsType(), this.upload.getCommonAttachSize(), true);
		return super.createModel(true,list);
	}
	
	/**
	 * 商品相册附件上传。
	 * @author wangjb 2019年6月6日。
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "上传商品相册附件", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/uploadGoodPhotosFile.xhtml")
	public ModelMap uploadGoodPhotosFile(HttpServletRequest request) throws Exception {
		List<UploadFile> list = null;
		List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
		list = CommonUtil.saveFiles(files, this.upload.getReal() + this.upload.getGoodPhotosPath(), this.upload.getGoodPhotosType(), this.upload.getCommonAttachSize(), true);
		return super.createModel(true,list);
	}
	
	/**
	 * 商品新增编辑富文本编辑器上传附件。
	 * @author wangjb 2019年7月9日。
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "上传商品相册附件", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/uploadGoodtextareaFile.xhtml")
	public Map<String, Object> uploadGoodtextareaFile(HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		List<UploadFile> list = null;
		List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
		try {
			list = CommonUtil.saveFiles(files, this.upload.getReal() + this.upload.getGoodPhotosPath(), this.upload.getGoodPhotosType(), this.upload.getCommonAttachSize(), true);
			result.put("code", 0);
			result.put("msg", "上传成功!");
			for (UploadFile uf : list) {
				Map<String, Object> data = new HashMap<String, Object>();
				data.put("src", this.upload.getVirtual() + this.upload.getGoodPhotosPath() + uf.getSavePath());
				data.put("title", uf.getLocalName());
				result.put("data", data);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", 1);
			result.put("msg", "上传失败!");
		}
		return result;
	}
	
	/**
	 * 上传大客户营业执照附件。
	 * @author wangjb 2019年6月6日。
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "上传大客户营业执照附件", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/uploadBigCustomersFile.xhtml")
	public ModelMap uploadBigCustomersFile(HttpServletRequest request) throws Exception {
		List<UploadFile> list = null;
		List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
		list = CommonUtil.saveFiles(files, this.upload.getReal() + this.upload.getBigCustomersPath(), this.upload.getBigCustomersType(), this.upload.getCommonAttachSize(), true);
		return super.createModel(true,list);
	}
	/**
	 * 上传商品分类图片
	 * @author Liangjing 2019年6月17日
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "上传商品分类图片")
	@RequestMapping("/uploadGoodGroupsFile.xhtml")
	public ModelMap uploadGoodGroupsFile(HttpServletRequest request) throws Exception {
		List<UploadFile> list = null;
		List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
		list = CommonUtil.saveFiles(files, this.upload.getReal() + this.upload.getGoodGroupsPath(), this.upload.getGoodGroupsType(), this.upload.getCommonAttachSize(), true);
		return super.createModel(true,list);
	}
	
	/**
	 * 上传商品结算付款凭证图片。
	 * @author wangjb 2019年7月27日。
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "上传商品结算付款凭证图片")
	@RequestMapping("/uploadMerchantSettlementsFile.xhtml")
	public ModelMap uploadMerchantSettlementsFile(HttpServletRequest request) throws Exception {
		List<UploadFile> list = null;
		List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
		list = CommonUtil.saveFiles(files, this.upload.getReal() + this.upload.getMerchantSettlementsPath(), this.upload.getMerchantSettlementsType(), this.upload.getCommonAttachSize(), true);
		return super.createModel(true,list);
	}
	/**
	 * 上传系统配置文件
	 * @author Liangjing 2019年8月8日
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@PreAuthorize("system")
	@OperateLog(value = "上传系统配置文件")
	@RequestMapping("/uploadthesystemfiles.xhtml")
	public ModelMap uploadTheSystemFiles(HttpServletRequest request) throws Exception {
		List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
		CommonUtil.saveSystemFiles(files, this.upload.getReal());
		return super.createModel(true);
	}
}
