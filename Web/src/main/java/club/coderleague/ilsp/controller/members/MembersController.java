/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MembersController.java
 * History:
 *         2019年6月1日: Initially created, Liangj.
 */
package club.coderleague.ilsp.controller.members;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.service.members.MembersService;

/**
 * 会员表
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/members")
public class MembersController extends IndexController {

	@Autowired
	MembersService membersService;
	
	/**
	 * 获取所有的会员
	 * @author Liangjing 2019年6月1日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("members")
	@OperateLog(value = "获取所有的会员", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallmembers.xhtml")
	public ModelMap getTheAllMembers(String pageIndex,String pageSize,String key) {
		return super.createModel(true, membersService.getTheAllMembers(pageIndex, pageSize, key));
	}
	
	/**
	 * 获取对应会员绑定数据
	 * @author Liangjing 2019年6月1日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("members")
	@OperateLog(value = "获取对应会员绑定数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallmemberbinds.xhtml")
	public ModelMap getTheAllMemberBinds(Long memberid,String pageIndex,String pageSize,String key) {
		return super.createModel(true, membersService.getTheAllMemberBinds(memberid, pageIndex, pageSize, key));
	}
	
	/**
	 * 获取对应会员积分记录
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("members")
	@OperateLog(value = "获取对应会员积分记录", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallintegralrecords.xhtml")
	public ModelMap getTheAllIntegralRecords(Long memberid,String pageIndex,String pageSize,String key) {
		return super.createModel(true, membersService.getTheAllIntegralRecords(memberid, pageIndex, pageSize, key));
	}
	
	/**
	 * 获取对应会员钻石记录
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("members")
	@OperateLog(value = "获取对应会员钻石记录", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthealldiamondrecords.xhtml")
	public ModelMap getTheAllDiamondRecords(Long memberid,String pageIndex,String pageSize,String key) {
		return super.createModel(true, membersService.getTheAllDiamondRecords(memberid, pageIndex, pageSize, key));
	}
	
	/**
	 * 获取会员对应收货地址
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("members")
	@OperateLog(value = "获取会员对应收货地址", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallreceivingaddresses.xhtml")
	public ModelMap getTheAllReceivingAddresses(Long memberid,String pageIndex,String pageSize,String key) {
		return super.createModel(true, membersService.getTheAllReceivingAddresses(memberid, pageIndex, pageSize, key));
	}
	
	/**
	 * 前往会员首页
	 * @author Liangjing 2019年6月1日
	 * @return
	 */
	@PreAuthorize("members")
	@OperateLog("前往会员首页")
	@RequestMapping("/gohomepage.xhtml")
	public String gohomepage() {
		return "/pc/members/homepage";
	}
	/**
	 * 前往会员绑定查看页面
	 * @author Liangjing 2019年6月1日
	 * @return
	 */
	@PreAuthorize("members")
	@OperateLog("前往会员绑定查看页面")
	@RequestMapping("/gomemberbindspage.xhtml")
	public String goMemberBindsPage(Long memberid, Model model) {
		model.addAttribute("memberid", memberid);
		return "/pc/members/memberbindspage";
	}
	/**
	 * 前往会员积分记录查看页面
	 * @author Liangjing 2019年6月1日
	 * @return
	 */
	@PreAuthorize("members")
	@OperateLog("前往会员积分记录查看页面")
	@RequestMapping("/gointegralrecordspage.xhtml")
	public String gointegralrecordspage(Long memberid, Model model) {
		model.addAttribute("memberid", memberid);
		return "/pc/members/integralrecordspage";
	}
	/**
	 * 前往会员钻石记录查看页面
	 * @author Liangjing 2019年6月1日
	 * @return
	 */
	@PreAuthorize("members")
	@OperateLog("前往会员钻石记录查看页面")
	@RequestMapping("/godiamondrecordspage.xhtml")
	public String diamondrecordspage(Long memberid, Model model) {
		model.addAttribute("memberid", memberid);
		return "/pc/members/diamondrecordspage";
	}
	/**
	 * 前往收货地址查看页面
	 * @author Liangjing 2019年6月1日
	 * @return
	 */
	@PreAuthorize("members")
	@OperateLog("前往收货地址查看页面")
	@RequestMapping("/goreceivingaddressespage.xhtml")
	public String receivingaddressespage(Long memberid, Model model) {
		model.addAttribute("memberid", memberid);
		return "/pc/members/receivingaddressespage";
	}
}
