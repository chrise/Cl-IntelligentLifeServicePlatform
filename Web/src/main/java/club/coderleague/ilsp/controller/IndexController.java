/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IndexController.java
 * History:
 *         2019年5月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.UserSession;

/**
 * 默认控制器。
 * @author CJH
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX)
public class IndexController {
	/**
	 * 请求前缀。
	 */
	public static final String REQUEST_PREFIX = "";
	/**
	 * 登录请求后缀。
	 */
	public static final String LOGIN_REQUEST_SUFFIX = "/login.xhtml";
	/**
	 * 主页请求后缀。
	 */
	public static final String MAIN_REQUEST_SUFFIX = "/main.xhtml";
	
	@Autowired
	private HttpSession session;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;
	
	/**
	 * 获取会话。
	 * @author Chrise 2019年5月12日
	 * @return 会话对象。
	 */
	protected HttpSession getSession() {
		return this.session;
	}
	
	/**
	 * 获取请求。
	 * @author Chrise 2019年5月26日
	 * @return 请求对象。
	 */
	protected HttpServletRequest getRequest() {
		return this.request;
	}
	
	/**
	 * 获取响应。
	 * @author Chrise 2019年5月26日
	 * @return 响应对象。
	 */
	protected HttpServletResponse getResponse() {
		return this.response;
	}
	
	/**
	 * 创建用于返回数据的模型。
	 * @author wangjb 2019年5月10日。
	 * @return 模型对象。
	 */
	protected ModelMap createModel() {
		return createModel(false, null);
	}
	
	/**
	 * 创建用于返回数据的模型。
	 * @author wangjb 2019年5月10日。
	 * @param status 处理状态，true表示成功，false表示失败。
	 * @return 模型对象。
	 */
	protected ModelMap createModel(boolean status) {
		return createModel(status, null);
	}
	
	/**
	 * 创建用于返回数据的模型。
	 * @author wangjb 2019年5月10日。
	 * @param status 处理状态，true表示成功，false表示失败。
	 * @param result 处理结果。
	 * @return 模型对象。
	 */
	protected ModelMap createModel(boolean status, Object result) {
		return new ModelMap().addAttribute("status", status)
							 .addAttribute("result", result);
	}
	
	/**
	 * 创建用于返回数据的模型，仅包含状态。
	 * @author Chrise 2019年6月2日
	 * @param status 处理状态，true表示成功，false表示失败。
	 * @return 模型对象。
	 */
	protected ModelMap createModelOnlyStatus(boolean status) {
		return new ModelMap().addAttribute("status", status);
	}
	
	/**
	 * 添加处理结果到数据模型。
	 * @author Chrise 2019年6月2日
	 * @param result 处理结果。
	 * @param model 数据模型。
	 */
	protected void addResultTo(Object result, ModelMap model) {
		model.addAttribute("result", result);
	}
	
	/**
	 * 获取用户会话。
	 * @author Chrise 2019年6月12日
	 * @return 用户会话对象。
	 */
	protected UserSession getUserSession() {
		return (UserSession)getSession().getAttribute(UserSession.SESSION_KEY);
	}
	
	/**
	 * 获取用户id。
	 * @author wangjb 2019年6月1日。
	 * @return
	 */
	protected Long getUserId() {
		UserSession us = (UserSession) this.getSession().getAttribute(UserSession.SESSION_KEY);
		return us.getId();
	}
	
	/**
	 * 首页。
	 * @author Chrise 2019年5月11日
	 * @return 跳转路径。
	 */
	@GetMapping("/")
	@OperateLog("访问系统首页")
	public String index() {
		return "forward:" + REQUEST_PREFIX + LOGIN_REQUEST_SUFFIX;
	}
	
	/**
	 * 前往登录页面。
	 * @author CJH 2019年5月10日
	 * @return 页面路径。
	 */
	@GetMapping(LOGIN_REQUEST_SUFFIX)
	@OperateLog("访问系统登录页面")
	public String login() {
		return "/pc/login/login";
	}
	
	/**
	 * 前往登陆成功主页页面。
	 * @author CJH 2019年5月10日
	 * @return 页面路径。
	 */
	@GetMapping(MAIN_REQUEST_SUFFIX)
	@OperateLog("访问系统主页面")
	@PreAuthorize("*")
	public String main(HttpServletRequest request) {
		return "/pc/main/main";
	}
	
	/**
	 * 退出登录
	 * @author CJH 2019年5月23日
	 * @return 前往登录页面处理
	 */
	@GetMapping("/logout.xhtml")
	@OperateLog("退出登录")
	@PreAuthorize("*")
	public String logout() {
		getSession().invalidate();
		return "redirect:/login.xhtml";
	}
}
