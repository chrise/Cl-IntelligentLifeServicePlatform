/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondRecordsController.java
 * History:
 *         2019年5月31日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller.diamondrecords;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import club.coderleague.ilsp.controller.IndexController;

/**
 * 钻石记录控制器
 * 
 * @author CJH
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/diamondrecords")
public class DiamondRecordsController extends IndexController {
	
//	private @Autowired DiamondRecordsService diamondRecordsService;
}
