/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AliyunInterfaceController.java
 * History:
 *         2019年6月6日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller.interfaces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.domain.enums.NotifyType;
import club.coderleague.ilsp.service.interfaces.AliyunInterfaceService;

/**
 * 阿里云接口控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(InterfaceController.REQUEST_PREFIX + "/aliyun")
public class AliyunInterfaceController extends InterfaceController {
	@Autowired
	private AliyunInterfaceService ayiService;
	
	/**
	 * 获取手机绑定验证码。
	 * @author Chrise 2019年6月6日
	 * @param phone 手机号。
	 * @return 验证码发送结果。
	 */
	@ResponseBody
	@PostMapping("/bindvercode.xhtml")
	@OperateLog("请求获取手机绑定验证码")
	public ModelMap getBindVercode(String phone) {
		boolean result = this.ayiService.sendVercode(NotifyType.PHONE_BIND, phone, getSession().getId());
		return createModel(result);
	}
	
	/**
	 * 获取身份认证验证码。
	 * @author Chrise 2019年6月6日
	 * @param phone 手机号。
	 * @return 验证码发送结果。
	 */
	@ResponseBody
	@PostMapping("/authvercode.xhtml")
	@OperateLog("请求获取身份认证验证码")
	public ModelMap getAuthVercode(String phone) {
		boolean result = this.ayiService.sendVercode(NotifyType.IDENTITY_AUTH, phone, getSession().getId());
		return createModel(result);
	}
}
