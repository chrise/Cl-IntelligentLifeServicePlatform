/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodsController.java
 * History:
 *         2019年5月18日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.goods;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.GoodsExtension;
import club.coderleague.ilsp.common.domain.beans.GoodsSettleExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.controller.goods.bean.SaveGoodsBean;
import club.coderleague.ilsp.service.good.GoodsMgrService;

/**
 * 商品管理控制层。
 * @author wangjb
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/goods")
public class GoodsMgrController extends IndexController {
	
	/**
	 * 商品管理业务处理。
	 */
	@Autowired GoodsMgrService goodService;

	/**
	 * 跳转至商品管理页面。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	@PreAuthorize("good")
	@RequestMapping("/goGoodsMgrPage.xhtml")
	@OperateLog("跳转至商品管理页面")
	public String goGoodsMgrPageController() {
		return "/pc/goods/GoodsMgrPage";
	}
	
	/**
	 * 跳转至商品管理回收站页面。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	@PreAuthorize("good.recycle")
	@RequestMapping(value = "/goGoodsMgrPage.xhtml", params = "recycle=true")
	@OperateLog("跳转至商品管理回收站页面")
	public String goGoodsRecyclePageController() {
		return "/pc/goods/GoodsMgrPage";
	}
	
	/**
	 * 获取商品管理分页数据。
	 * @author wangjb 2019年5月18日。
	 * @param params 页面查询参数。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"good", "good.recycle"})
	@RequestMapping("/getGoodsMgr.xhtml")
	@OperateLog(value="获取商品列表数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGoodsMgrController (@RequestParam Map<String, Object> params) throws Exception{
		Page<GoodsExtension> result = this.goodService.getGoodsMgrService(params);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至商品新增、编辑页面。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	@PreAuthorize({"good.addGoods", "good.editGoods"})
	@RequestMapping("/goUpdateGoodsPage.xhtml")
	@OperateLog("跳转至商品新增、编辑页面")
	public String goUpdateGoodsPageController() {
		return "/pc/goods/UpdateGoodsPage";
	}
	
	/**
	 * 根据商品id获取商品对象。
	 * @author wangjb 2019年5月18日。
	 * @param entityid 商品id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"good.addGoods", "good.editGoods"})
	@RequestMapping("/getGoodsObjByEntityid.xhtml")
	@OperateLog(value="根据商品id获取商品对象", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGoodsObjByEntityidController(Long entityid) {
		Map<String, Object> result = this.goodService.getGoodsObjByEntityidService(entityid);
		return super.createModel(true, result);
	}
	
	/**
	 * 获取商户标识。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"good.addGoods", "good.editGoods"})
	@RequestMapping("/getMerchantIdList.xhtml")
	@OperateLog(value="获取商户入驻标识", type = OperateLogRequestType.PASSIVE)
	public ModelMap getMerchantIdListController() {
		List<GoodsSettleExtension> result = this.goodService.getMerchantIdListService();
		return super.createModel(true, result);
	}
	
	/**
	 * 获取商品分类标识。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"good.addGoods", "good.editGoods"})
	@RequestMapping("/getGroupIdList.xhtml")
	@OperateLog(value="获取商品分类标识", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGroupIdListController() {
		List<Map<String, Object>> result = this.goodService.getGroupIdListService();
		return super.createModel(true, result);
	}
	
	/**
	 * 新增商品对象。
	 * @author wangjb 2019年5月18日。
	 * @param data 页面表单数据。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("good.addGoods")
	@RequestMapping(value = "/updateGoodsObj.xhtml", params = "execute=add")
	@OperateLog("新增商品对象")
	public ModelMap insertGoodsObjController(String data) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		SaveGoodsBean goodsBean = new ObjectMapper().readValue(data, SaveGoodsBean.class);
		this.goodService.updateGoodsObjService(result, goodsBean);
		return super.createModel(true, result);
	}
	
	/**
	 * 编辑商品对象。
	 * @author wangjb 2019年5月18日。
	 * @param data 页面表单数据。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("good.editGoods")
	@RequestMapping(value = "/updateGoodsObj.xhtml", params = "execute=edit")
	@OperateLog("编辑商品对象")
	public ModelMap updateGoodsObjController(String data) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		SaveGoodsBean goodsBean = new ObjectMapper().readValue(data, SaveGoodsBean.class);
		this.goodService.updateGoodsObjService(result, goodsBean);
		return super.createModel(true, result);
	}
	
	/**
	 * 列表主页删除。
	 * @author wangjb 2019年5月19日。
	 * @param entityid 商品id。
	 * @param entitystate 商品状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("good.delData")
	@RequestMapping(value = "/updateEntitystateByGoodId.xhtml", params = "execute=del")
	@OperateLog("列表主页删除")
	public ModelMap executeDeleteByGoodIdController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.goodService.updateEntitystateByGoodIdService(entityid, EntityState.INVALID.getValue(), result);
		return super.createModel(true, result);
	}
	
	/**
	 * 回收站恢复。
	 * @author wangjb 2019年5月19日。
	 * @param entityid 商品id。
	 * @param entitystate 商品状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("good.recycle")
	@RequestMapping(value="/updateEntitystateByGoodId.xhtml", params = "execute=rest")
	@OperateLog("回收站恢复")
	public ModelMap executeRestByGoodIdController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.goodService.updateEntitystateByGoodIdService(entityid, EntityState.VALID.getValue(), result);
		return super.createModel(true, result);
	}
	
	/**
	 * 回收站删除。
	 * @author wangjb 2019年5月19日。
	 * @param entityid 商品id。
	 * @param entitystate 商品状态。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("good.recycle")
	@RequestMapping(value="/updateEntitystateByGoodId.xhtml", params = "execute=restDel")
	@OperateLog("回收站删除")
	public ModelMap executeRestDelByGoodIdController(String entityid) {
		Map<String, Object> result = new HashMap<String, Object>();
		this.goodService.updateEntitystateByGoodIdService(entityid, EntityState.DELETED.getValue(), result);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至商品详情页面
	 * @author wangjb 2019年5月19日。
	 * @return
	 */
	@PreAuthorize({"good", "good.recycle"})
	@RequestMapping("/goGoodsDetailPage.xhtml")
	@OperateLog("跳转至商品详情页面！")
	public String goGoodsDetailPageController() {
		return "/pc/goods/GoodsDetailPage";
	}
	
	/**
	 * 根据商品id查询商品详情。
	 * @author wangjb 2019年5月19日。
	 * @param entityid 商品id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"good", "good.recycle"})
	@RequestMapping("/getGoodsDetailByEntityid.xhtml")
	@OperateLog(value="根据商品id查询商品详情", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGoodsDetailByEntityidController(Long entityid) {
		Map<String, Object> result = this.goodService.getGoodsDetailByEntityidService(entityid);
		return super.createModel(true, result);
	}
	
	/**
	 * 跳转至商品的详情页面
	 * @author wangjb 2019年5月19日。
	 * @return
	 */
	@PreAuthorize({"good", "good.recycle"})
	@RequestMapping("/goGoodGraphicDetailPage.xhtml")
	@OperateLog("跳转至商品的详情页面！")
	public String goGoodGraphicDetailPageController() {
		return "/pc/goods/GoodGraphicDetailPage";
	}
	
	/**
	 * 根据商品id查询商品详情。
	 * @author wangjb 2019年5月19日。
	 * @param entityid 商品id。
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"good", "good.recycle"})
	@RequestMapping("/getGraphicDetailById.xhtml")
	@OperateLog(value="根据商品id查询商品详情", type = OperateLogRequestType.PASSIVE)
	public ModelMap getGraphicDetailByIdController(Long entityid) {
		String result = this.goodService.getGraphicDetailByIdService(entityid);
		return super.createModel(true, result);
	}
}
