/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：NosessionRequestController.java
 * History:
 *         2019年5月11日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.service.bind.BoundService;
import club.coderleague.ilsp.service.users.UserService;
import club.coderleague.ilsp.util.CommonUtil;

/**
 * 无会话请求控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(NosessionRequestController.REQUEST_PREFIX)
public class NosessionRequestController extends IndexController {
	/**
	 * 请求前缀。
	 */
	public static final String REQUEST_PREFIX = IndexController.REQUEST_PREFIX + "/nr";
	
	private static final String VERICODE_KEY = "vericode_key";
	
	@Autowired
	private UserService userService;
	@Autowired
	private BoundService boundService;
	
	/**
	 * 生成验证码。
	 * @author Chrise 2019年5月12日
	 * @return 验证码数据流。
	 */
	@ResponseBody
	@GetMapping("/vericode.xhtml")
	@OperateLog("请求生成验证码")
	public StreamingResponseBody genVericode() {
		Map<String, BufferedImage> vericode = CommonUtil.genVericode();
		
		String codenum = vericode.keySet().iterator().next();
		getSession().setAttribute(VERICODE_KEY, codenum);
		
		BufferedImage codeimg = vericode.get(codenum);
		return new StreamingResponseBody() {
			public void writeTo(OutputStream outputStream) throws IOException {
				ImageIO.write(codeimg, "jpeg", outputStream);
			}
		};
	}
	
	/**
	 * 登录。
	 * @author Chrise 2019年5月12日
	 * @param username 用户名。
	 * @param password 密码。
	 * @param vericode 验证码。
	 * @return 登录结果。
	 * @throws Exception 登录处理异常。
	 */
	@ResponseBody
	@PostMapping("/login.xhtml")
	@OperateLog("请求登录系统")
	public ModelMap login(String username, String password, String vericode) throws Exception {
		HttpSession session = getSession();
		String cachecode = (String)session.getAttribute(VERICODE_KEY);
		
		String message = this.userService.userLogin(username, password, vericode, cachecode, session);
		if (message != null) return createModel(false, message);
		
		return createModel(true);
	}
	
	/**
	 * 绑定手机号。
	 * @author Chrise 2019年6月2日
	 * @param phone 手机号。
	 * @param code 验证码。
	 * @return 绑定结果。
	 * @throws Exception 绑定异常。
	 */
	@ResponseBody
	@PostMapping("/bind.xhtml")
	@OperateLog("请求绑定手机号")
	public ModelMap bind(String phone, String code) throws Exception {
		ModelMap mm = createModelOnlyStatus(false);
		
		String result = this.boundService.execBindPhone(phone, code, getSession(), mm);
		addResultTo(result, mm);
		
		return mm;
	}
}
