/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SaveBean.java
 * History:
 *         2019年6月6日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.bigcustomers.bean;

import java.io.Serializable;
import java.util.List;

import club.coderleague.ilsp.entities.Bigcustomers;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 大客户保存bean
 * @author wangjb
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SaveBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 大客户对象。
	 */
	private Bigcustomers bigc;
	/**
	 * 人员对象。
	 */
	private List<PersonsBean> persons;
}
