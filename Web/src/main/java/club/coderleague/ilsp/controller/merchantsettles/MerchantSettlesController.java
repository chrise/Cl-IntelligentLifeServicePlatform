/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesController.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.controller.merchantsettles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Merchantsettles;
import club.coderleague.ilsp.service.merchantsettles.MerchantSettlesService;

/**
 * 商户入驻
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/merchantsettles")
public class MerchantSettlesController extends IndexController {
	
	@Autowired
	MerchantSettlesService merchantSettlesService;
	
	/**
	 * 获取所有的商户入驻数据
	 * @author Liangjing 2019年5月24日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @param istate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "获取所有的商户入驻数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallmerchantsettles.xhtml")
	public ModelMap getTheAllMerchantSettles(String pageIndex,String pageSize,String key,String istate) {
		return super.createModel(true, merchantSettlesService.getTheAllMerchantSettles(pageIndex, pageSize, key, istate));
	}
	/**
	 * 获取商户入驻编辑页面加载数据
	 * @author Liangjing 2019年5月24日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "获取商户入驻编辑页面加载数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthemerchantsettleseditpagedata.xhtml")
	public ModelMap getTheMerchantSettlesEditPageData(Long entityid) {
		return super.createModel(true, merchantSettlesService.getTheMerchantSettlesEditPageData(entityid));
	}
	/**
	 * 获取商户入驻查看详情页面加载数据
	 * @author Liangjing 2019年5月24日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "获取商户入驻查看详情页面加载数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthemerchantsettleslookinfopagedata.xhtml")
	public ModelMap getTheMerchantSettlesLookInfoPageData(Long entityid) {
		return super.createModel(true, merchantSettlesService.getTheMerchantSettlesLookInfoPageData(entityid));
	}
	
	/**
	 * 更新商户入驻
	 * @author Liangjing 2019年5月24日
	 * @param merchantsettles
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "更新商户入驻")
	@RequestMapping("/updatethemerchantsettles.xhtml")
	public ModelMap updateTheMerchantSettles(Merchantsettles merchantsettles) {
		return super.createModel(true, merchantSettlesService.updateTheMerchantSettles(merchantsettles));
	}
	
	/**
	 * 修改商户入驻对象状态
	 * @author Liangjing 2019年5月24日
	 * @param entityids
	 * @param istate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "修改商户入驻对象状态")
	@RequestMapping("/updatethemerchantsettlesistate.xhtml")
	public ModelMap updateTheMerchantSettlesIstate(String entityids,Integer istate) {
		return super.createModel(true, merchantSettlesService.updateTheMerchantSettlesIstate(entityids, istate));
	}
	
	/**
	 * 获取所有的市场
	 * @author Liangjing 2019年5月24日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "获取所有的市场", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallmarkets.xhtml")
	public ModelMap getTheAllMarkets() {
		return super.createModel(true, merchantSettlesService.getTheAllMarkets());
	}
	
	/**
	 * 获取所有的商户
	 * @author Liangjing 2019年5月24日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize("*")
	@OperateLog(value = "获取所有的商户", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallmerchants.xhtml")
	public ModelMap getTheAllMerchants() {
		return super.createModel(true, merchantSettlesService.getTheAllMerchants());
	}
	
	/**
	 * 前往商户入驻首页
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize("*")
	@OperateLog("前往商户入驻首页")
	@RequestMapping("/gohomepage.xhtml")
	public String gohomepage() {
		return "/pc/merchantsettles/homepage";
	}
	/**
	 * 前往商户入驻修改页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize("*")
	@OperateLog("前往商户入驻修改页面")
	@RequestMapping("/goupdatepage.xhtml")
	public String goupdatepage() {
		return "/pc/merchantsettles/updatepage";
	}
	/**
	 * 前往商户入驻查看详情页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize("*")
	@OperateLog("前往商户入驻查看详情页面")
	@RequestMapping("/golookinfopage.xhtml")
	public String golookinfopage() {
		return "/pc/merchantsettles/lookinfo";
	}
}
