/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：LocalNotifyController.java
 * History:
 *         2019年6月12日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller.local;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.configure.condition.ConditionalOnProperty;
import club.coderleague.ilsp.controller.NosessionRequestController;
import club.coderleague.ilsp.service.interfaces.AliyunInterfaceService;

/**
 * 本地通知控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(NosessionRequestController.REQUEST_PREFIX + "/local")
@ConditionalOnProperty(path = "${custom.aliyun.configLocation}", name = "aliyun.localNotify", havingValue = "true")
public class LocalNotifyController extends NosessionRequestController {
	@Autowired
	private AliyunInterfaceService ayiService;
	
	/**
	 * 查询通知。
	 * @author Chrise 2019年6月13日
	 * @param phone 手机号。
	 * @return 通知内容。
	 */
	@ResponseBody
	@GetMapping("/notify.xhtml")
	public Object notify(String phone) {
		return this.ayiService.getLocalNotify(phone);
	}
}
