/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileMemberOrderController.java
 * History:
 *         2019年6月9日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller.orders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.PayOrder;
import club.coderleague.ilsp.controller.MobileMemberController;
import club.coderleague.ilsp.service.orders.OrdersService;

/**
 * 移动端会员商城订单控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(MobileMemberController.REQUEST_PREFIX + "/order")
public class MobileMemberOrderController extends MobileMemberController {
	@Autowired
	private OrdersService ordersService;
	
	/**
	 * 访问订单确认页面。
	 * @author Chrise 2019年6月9日
	 * @param api 支付接口。
	 * @param order 订单标识。
	 * @param model 数据模型。
	 * @return 订单确认页面地址。
	 */
	@PreAuthorize("*")
	@GetMapping("/confirm.xhtml")
	@OperateLog("访问订单确认页面")
	public String confirm(String api, String order, Model model) {
		Object result = this.ordersService.queryOrderForConfirm(api, order, getUserId());
		if (result instanceof PayOrder) {
			model.addAttribute("order", result);
			return "/mobile/member/order/confirm";
		}
		
		model.addAttribute("retry", result);
		return "/mobile/error/orderconfirm";
	}
	
	/**
	 * 确认订单。
	 * @author Chrise 2019年6月20日
	 * @return 确认结果。
	 */
	@ResponseBody
	@PreAuthorize("*")
	@PostMapping("/confirm.xhtml")
	@OperateLog("请求确认订单")
	public ModelMap confirmOrder(long order, int diamondpay) throws Exception {
		this.ordersService.updateConfirmedOfflineOrder(order, 0, diamondpay, getUserId());	//TODO 暂无积分优惠始终传0
		return createModel(true);
	}
}
