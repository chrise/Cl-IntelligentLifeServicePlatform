/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：BanksController.java
 * History:
 *         2019年7月26日: Initially created, Liangj.
 */
package club.coderleague.ilsp.controller.banks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Banks;
import club.coderleague.ilsp.service.banks.BanksService;

/**
 * 银行管理
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/banks")
public class BanksController extends IndexController {

	@Autowired
	BanksService banksService;
	
	/**
	 * 获取对应所有银行信息
	 * @author Liangjing 2019年7月26日
	 * @param key
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"banks", "banks.recycle"})
	@RequestMapping("/gettheallbanks.xhtml")
	@OperateLog(value="获取对应所有银行信息", type = OperateLogRequestType.PASSIVE)
	public ModelMap getTheAllBanks(String key,boolean isrecycle) {
		return super.createModel(true, banksService.getTheAllBanks(key, isrecycle));
	}
	/**
	 * 获取所有银行父级信息
	 * @author Liangjing 2019年7月26日
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"banks.add", "banks.edit"})
	@RequestMapping("/gettheallparentbanks.xhtml")
	@OperateLog(value="获取所有银行父级信息", type = OperateLogRequestType.PASSIVE)
	public ModelMap getTheAllParentBanks() {
		return super.createModel(true, banksService.getTheAllParentBanks());
	}
	/**
	 * 获取银行管理编辑页面数据
	 * @author Liangjing 2019年7月26日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"banks.edit"})
	@RequestMapping("/getthebankeditpagedata.xhtml")
	@OperateLog(value="获取银行管理编辑页面数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getTheBankEditPageData(Long entityid) {
		return super.createModel(true, banksService.getTheBankEditPageData(entityid));
	}
	
	/**
	 * 获取银行管理查看详情页面数据
	 * @author Liangjing 2019年7月26日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"banks", "banks.recycle"})
	@RequestMapping("/getthebanklookinfopagedata.xhtml")
	@OperateLog(value="获取银行管理查看详情页面数据", type = OperateLogRequestType.PASSIVE)
	public ModelMap getTheBankLookInfoPageData(Long entityid) {
		return super.createModel(true, banksService.getTheBankLookInfoPageData(entityid));
	}
	
	/**
	 * 更新银行信息
	 * @author Liangjing 2019年7月26日
	 * @param banks
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"banks.add","banks.edit"})
	@RequestMapping("/updatethebanks.xhtml")
	@OperateLog(value="更新银行信息")
	public ModelMap updateTheBanks(Banks banks) {
		return super.createModel(true, banksService.updateTheBanks(banks));
	}
	
	/**
	 * 更新银行信息状态
	 * @author Liangjing 2019年7月26日
	 * @param entityids
	 * @param istate
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"banks.edit","banks.del","banks.recycle"})
	@RequestMapping("/updatethebanksstate.xhtml")
	@OperateLog(value="更新银行信息状态")
	public ModelMap updateTheBanksState(String entityids, Integer istate) {
		return super.createModel(true, banksService.updateTheBanksState(CommonUtil.toLongArrays(entityids), istate));
	}
	
	/**
	 * 前往银行管理首页
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"banks","banks.recycle"})
	@OperateLog("前往银行管理首页")
	@RequestMapping("/gohomepage.xhtml")
	public String gohomepage() {
		return "/pc/banks/homepage";
	}
	
	/**
	 * 前往银行管理修改页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"banks.add","banks.edit"})
	@OperateLog("前往银行管理修改页面")
	@RequestMapping("/goupdatepage.xhtml")
	public String goupdatepage(Model model,String entityid) {
		model.addAttribute("entityid", entityid);
		return "/pc/banks/updatepage";
	}
	
	/**
	 * 前往银行管理查看详情页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"banks","banks.recycle"})
	@OperateLog("前往银行管理查看详情页面")
	@RequestMapping("/golookinfopage.xhtml")
	public String golookinfopage() {
		return "/pc/banks/lookinfo";
	}
}
