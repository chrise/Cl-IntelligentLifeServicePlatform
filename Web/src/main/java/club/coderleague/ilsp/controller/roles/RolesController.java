/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RolesController.java
 * History:
 *         2019年5月18日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller.roles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PassiveLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Roles;
import club.coderleague.ilsp.service.roles.RolesService;

/**
 * 角色控制器
 * 
 * @author CJH
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/roles")
public class RolesController extends IndexController {
	/**
	 * 角色Service
	 */
	private @Autowired RolesService rolesService;
	
	/**
	 * 前往角色管理页面
	 * 
	 * @author CJH 2019年5月18日
	 * @return 页面路径
	 */
	@GetMapping("/goRolesPage.xhtml")
	@OperateLog("前往角色管理页面")
	@PreAuthorize("roles")
	public String goRolesPage() {
		return "/pc/roles/rolesPage";
	}
	
	/**
	 * 前往角色管理回收站页面
	 * 
	 * @author CJH 2019年5月18日
	 * @return 页面路径
	 */
	@GetMapping(value = "/goRolesPage.xhtml", params = "recycle=true")
	@OperateLog("前往角色管理回收站页面")
	@PreAuthorize("roles.recycle")
	public String goRolesRecyclePage() {
		return "/pc/roles/rolesPage";
	}
	
	/**
	 * 分页查询角色
	 * 
	 * @author CJH 2019年7月2日
	 * @param pagingparameters 分页参数
	 * @param isrecycle 是否回收站
	 * @param keyword 关键字
	 * @return 角色
	 */
	@PostMapping("/findPageByParamsMap.xhtml")
	@ResponseBody
	@PassiveLog("分页查询角色")
	@PreAuthorize({"roles", "roles.recycle"})
	public ModelMap findPageByParamsMap(PagingParameters pagingparameters, boolean isrecycle, String keyword) {
		return super.createModel(true, rolesService.findPageByParamsMap(pagingparameters, isrecycle, keyword));
	}
	
	/**
	 * 前往角色管理编辑页面
	 * 
	 * @author CJH 2019年5月18日
	 * @return 页面路径
	 */
	@GetMapping("/goRolesEditPage.xhtml")
	@OperateLog("前往角色管理编辑页面")
	@PreAuthorize({"roles.add", "roles.edit"})
	public String goRolesEditPage() {
		return "/pc/roles/rolesEditPage";
	}
	
	/**
	 * 新增角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param roles 角色
	 * @param funcids 功能主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/save.xhtml", params = "oper=add")
	@ResponseBody
	@OperateLog("新增角色")
	@PreAuthorize("roles.add")
	public ModelMap insert(Roles roles, String funcids) {
		rolesService.insert(roles, funcids);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 更新角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param roles 角色
	 * @param funcids 功能主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/save.xhtml", params = "oper=edit")
	@ResponseBody
	@OperateLog("更新角色")
	@PreAuthorize("roles.edit")
	public ModelMap update(Roles roles, String funcids) {
		rolesService.update(roles, funcids);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据角色主键查询角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityid 角色主键
	 * @return 角色
	 */
	@PostMapping("/findMapByEntityid.xhtml")
	@ResponseBody
	@PassiveLog("根据角色主键查询角色")
	@PreAuthorize("roles.edit")
	public ModelMap findMapByEntityid(Long entityid) {
		return super.createModel(true, rolesService.findMapByEntityid(entityid));
	}
	
	/**
	 * 根据角色主键删除角色到回收站
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityids 角色主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=del")
	@ResponseBody
	@OperateLog("根据角色主键删除角色到回收站")
	@PreAuthorize("roles.del")
	public ModelMap deleteByEntityids(String entityids) {
		rolesService.updateEntitystate(entityids, EntityState.INVALID.getValue(), false);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据角色主键恢复角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityids 角色主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=restore")
	@ResponseBody
	@OperateLog("根据角色主键恢复角色")
	@PreAuthorize("roles.recycle")
	public ModelMap updateRestoreByEntityids(String entityids) {
		rolesService.updateEntitystate(entityids, EntityState.VALID.getValue(), true);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据角色主键删除角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityids 角色主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=delete")
	@ResponseBody
	@OperateLog("根据角色主键删除角色")
	@PreAuthorize("roles.recycle")
	public ModelMap deleteRecycleByEntityids(String entityids) {
		rolesService.updateEntitystate(entityids, EntityState.DELETED.getValue(), false);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 前往角色管理查看详情页面
	 * 
	 * @author CJH 2019年5月21日
	 * @return 页面路径
	 */
	@GetMapping("/goRolesViewPage.xhtml")
	@OperateLog("前往角色管理查看详情页面")
	@PreAuthorize({"roles", "roles.recycle"})
	public String goRolesViewPage() {
		return "/pc/roles/rolesViewPage";
	}
	
	/**
	 * 根据角色主键查询授权功能
	 * 
	 * @author CJH 2019年5月21日
	 * @param roleid 角色主键
	 * @return 功能
	 */
	@PostMapping("findAuthFunctionsByRoleid.xhtml")
	@ResponseBody
	@OperateLog("根据角色主键查询授权功能")
	@PreAuthorize({"roles", "roles.recycle"})
	public ModelMap findAuthFunctionsByRoleid(Long roleid) {
		return super.createModel(true, rolesService.findAuthFunctionsByRoleid(roleid));
	}
}
