/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SaveGoodsBean.java
 * History:
 *         2019年6月7日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.goods.bean;

import java.io.Serializable;
import java.util.List;

import club.coderleague.ilsp.common.domain.beans.UploadFile;
import club.coderleague.ilsp.entities.Goods;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品保存自定义对象。
 * @author wangjb
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SaveGoodsBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 商品表。
	 */
	private Goods goods;
	
	/**
	 * 商品销售表。
	 */
	private List<GoodSpecsBean> goodSpecsBean;
	
	/**
	 * 商品普通图片。
	 */
	private List<UploadFile> goodphoto;
	
	/**
	 * 商品封面。
	 */
	private List<UploadFile> coverphoto;
	
	/**
	 * 附件id。
	 */
	private String del;
	

}
