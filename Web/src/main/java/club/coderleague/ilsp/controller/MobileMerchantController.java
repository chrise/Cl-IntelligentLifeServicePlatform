/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MobileMerchantController.java
 * History:
 *         2019年5月29日: Initially created, Chrise.
 */
package club.coderleague.ilsp.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.mobile.MoblieMerchantInfo;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.service.mobile.capitalrecords.MobileCapitalRecordsService;
import club.coderleague.ilsp.service.mobile.goods.MobileGoodsService;
import club.coderleague.ilsp.service.mobile.manager.MobileManagerService;

/**
 * 移动端商户控制台控制器。
 * @author Chrise
 */
@Controller
@RequestMapping(MobileMerchantController.REQUEST_PREFIX)
public class MobileMerchantController extends MobileIndexController {
	/**
	 * 请求前缀。
	 */
	public static final String REQUEST_PREFIX = MobileIndexController.REQUEST_PREFIX + "/merchant";
	
	@Autowired
	MobileManagerService mobileManagerService;
	
	/**
	 * -WeChat 商品管理。
	 */
	@Autowired MobileGoodsService goodsService;
	
	/**
	 * -WeChat 资金记录。
	 */
	@Autowired MobileCapitalRecordsService recordsService;
	
	/**
	 * 跳转到首页
	 * @author yj 2019年6月21日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/main.xhtml")
	@OperateLog("跳转到首页（移动端）")
	public String mobilemainController() {
		return "/mobile/merchant/main";
	}
	
	/**
	 * 跳转到商户信息
	 * @author yj 2019年7月29日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/merchantinfo.xhtml")
	@OperateLog("跳转到商户信息")
	public String mobilemerchantinfoController(Model model) {
		MoblieMerchantInfo moblieMerchantInfo = mobileManagerService.getTheMerchantInfo(super.getUserId());
		model.addAttribute("info", moblieMerchantInfo);
		return "/mobile/merchant/merchantinfo/merchantinfo";
	}
	
	/**
	 * 跳转到商品管理
	 * @author yj 2019年7月29日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/merchantitem.xhtml")
	@OperateLog("跳转到商品管理")
	public String mobilemerchantitemController(Model model) {
		model.addAttribute("select", this.goodsService.getGroupAndMarketListDataService(super.getUserId()));
		return "/mobile/merchant/merchantitem/merchantitem";
	}
	
	/**
	 * -根据商户id获取商品数据。
	 * @author wangjb 2019年8月3日。
	 * @param model
	 * @param params
	 * @return
	 */
	@PreAuthorize("*")
	@PostMapping("/getGoodMgrByMerchantid.xhtml")
	@OperateLog("获取商品数据")
	public String getGoodMgrByMerchantidController(Model model, @RequestParam Map<String, Object> params) {
		model.addAttribute("goodMgr", this.goodsService.getMobileGoodsMgrDataService(super.getUserId(), params));
		return "/mobile/merchant/merchantitem/merchantitem::goodMgr_List";
	}
	
	/**
	 * 跳转到商户结算
	 * @author yj 2019年7月29日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/merchantaccount.xhtml")
	@OperateLog("跳转到商户结算")
	public String mobilemerchantaccountController(Model model,String istate) throws Exception {
		List<String> checkistate = null;
		if(!CommonUtil.isEmpty(istate)) {
			checkistate = Arrays.asList(istate.split(","));
		}
		List<Map<String,Object>> list = mobileManagerService.getTheAllMerchantSettlements(super.getUserId(), checkistate);
		model.addAttribute("list", list);
		return "/mobile/merchant/merchantaccount/merchantaccount";
	}
	
	/**
	 * 跳转到资金记录
	 * @author yj 2019年7月29日。
	 * @return
	 */
	@PreAuthorize("*")
	@GetMapping("/merchantfund.xhtml")
	@OperateLog("跳转到资金记录")
	public String mobilemerchantfundController() {
		return "/mobile/merchant/merchantfund/merchantfund";
	}
	
	/**
	 * -根据商户id获取资金记录。
	 * @author wangjb 2019年8月3日。
	 * @param model
	 * @param params 页面参数。
	 * @return
	 */
	@PreAuthorize("*")
	@PostMapping("/getCapitalRecordListByMerchantid.xhtml")
	@OperateLog("根据商户id获取资金记录。")
	public String getCapitalRecordListByMerchantidController(Model model, @RequestParam Map<String, Object> params) {
		model.addAttribute("record", this.recordsService.getCapitalRecordListByMerchantidSetvice(super.getUserId(), params));
		return "/mobile/merchant/merchantfund/merchantfund::record_List";
	}
	
	/**
	 * -根据记录id获取记录详情。
	 * @author wangjb 2019年8月3日。
	 * @param model
	 * @param recordid 记录id。
	 * @return
	 */
	@PreAuthorize("*")
	@PostMapping("/getRecordDetailByRecordId.xhtml")
	@OperateLog("根据记录id获取记录详情")
	public String getRecordDetailByRecordIdController(Model model, Long recordid) {
		model.addAttribute("recordDetail", this.recordsService.getRecordDetailByRecordidService(recordid));
		return "/mobile/merchant/main::RecordDetail";
	}
}
