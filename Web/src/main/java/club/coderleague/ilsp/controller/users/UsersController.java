/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UsersController.java
 * History:
 *         2019年5月16日: Initially created, CJH.
 */
package club.coderleague.ilsp.controller.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PassiveLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Users;
import club.coderleague.ilsp.service.users.UserService;

/**
 * 用户控制器
 * 
 * @author CJH
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/users")
public class UsersController extends IndexController {
	/**
	 * 用户Service
	 */
	private @Autowired UserService userService;
	
	/**
	 * 前往用户管理页面
	 * 
	 * @author CJH 2019年5月16日
	 * @return 页面路径
	 */
	@GetMapping("/goUsersPage.xhtml")
	@OperateLog("前往用户管理页面")
	@PreAuthorize("users")
	public String goUsersPage() {
		return "/pc/users/usersPage";
	}
	
	/**
	 * 前往用户管理回收站页面
	 * 
	 * @author CJH 2019年5月18日
	 * @return 页面路径
	 */
	@GetMapping(value = "/goUsersPage.xhtml", params = "recycle=true")
	@OperateLog("前往用户管理回收站页面")
	@PreAuthorize("users.recycle")
	public String goUsersRecyclePage() {
		return "/pc/users/usersPage";
	}
	
	/**
	 * 分页查询用户
	 * 
	 * @author CJH 2019年7月2日
	 * @param pagingparameters 分页参数
	 * @param isrecycle 是否回收站
	 * @param keyword 关键字
	 * @param usernametext 用户名
	 * @param username 用户名对应用户主键
	 * @param userphonetext 用户电话
	 * @param userphone 用户电话对应用户主键
	 * @return 用户
	 */
	@PostMapping("/findPageByParamsMap.xhtml")
	@ResponseBody
	@PassiveLog("分页查询用户")
	@PreAuthorize({"users", "users.recycle"})
	public ModelMap findPageByParamsMap(PagingParameters pagingparameters, boolean isrecycle, String keyword, String usernametext, String username, String userphonetext, String userphone) {
		return super.createModel(true, userService.findPageByParamsMap(pagingparameters, isrecycle, keyword, usernametext, username, userphonetext, userphone));
	}
	
	/**
	 * 前往用户编辑页面
	 * 
	 * @author CJH 2019年5月17日
	 * @return 页面路径
	 */
	@GetMapping("/goUsersEditPage.xhtml")
	@OperateLog("前往用户编辑页面")
	@PreAuthorize({"users.add", "users.edit"})
	public String goUsersEditPage() {
		return "/pc/users/usersEditPage";
	}
	
	/**
	 * 新增用户
	 * 
	 * @author CJH 2019年5月17日
	 * @param users 用户
	 * @param roleids 角色主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/save.xhtml", params = "oper=add")
	@ResponseBody
	@OperateLog("新增用户")
	@PreAuthorize("users.add")
	public ModelMap insert(Users users, String roleids) {
		userService.insert(users, roleids);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 更新用户
	 * 
	 * @author CJH 2019年5月17日
	 * @param users 用户
	 * @param roleids 角色主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/save.xhtml", params = "oper=edit")
	@ResponseBody
	@OperateLog("更新用户")
	@PreAuthorize("users.edit")
	public ModelMap update(Users users, String roleids) {
		userService.update(users, roleids);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 查询所有机构
	 * 
	 * @author CJH 2019年5月20日
	 * @return 机构
	 */
	@PostMapping("/findAllOrganizations.xhtml")
	@ResponseBody
	@PassiveLog("查询所有机构")
	@PreAuthorize({"users.add", "users.edit"})
	public ModelMap findAllOrganizations() {
		return super.createModel(true, userService.findAllOrganizations());
	}
	
	/**
	 * 查询授权角色
	 * 
	 * @author CJH 2019年5月20日
	 * @return 角色
	 */
	@PostMapping("/findAuthRoles.xhtml")
	@ResponseBody
	@PassiveLog("查询授权角色")
	@PreAuthorize({"users.add", "users.edit"})
	public ModelMap findAuthRoles() {
		return super.createModel(true, userService.findAuthRoles());
	}
	
	/**
	 * 根据用户主键查询用户
	 * 
	 * @author CJH 2019年5月20日
	 * @param entityid 用户主键
	 * @return 用户
	 */
	@PostMapping("/findExtensionByEntityid.xhtml")
	@ResponseBody
	@PassiveLog("根据用户主键查询用户")
	@PreAuthorize({"users.edit", "users", "users.recycle"})
	public ModelMap findExtensionByEntityid(Long entityid) {
		return super.createModel(true, userService.findExtensionByEntityid(entityid));
	}
	
	/**
	 * 根据用户主键删除用户到回收站
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityids 用户主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=del")
	@ResponseBody
	@OperateLog("根据用户主键删除用户到回收站")
	@PreAuthorize("users.del")
	public ModelMap deleteByEntityids(String entityids) {
		userService.updateEntitystate(entityids, EntityState.INVALID.getValue(), false);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据用户主键恢复用户
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityids 用户主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=restore")
	@ResponseBody
	@OperateLog("根据用户主键恢复用户")
	@PreAuthorize("users.recycle")
	public ModelMap updateRestoreByEntityids(String entityids) {
		userService.updateEntitystate(entityids, EntityState.VALID.getValue(), true);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据用户主键删除用户
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityids 用户主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=delete")
	@ResponseBody
	@OperateLog("根据用户主键删除用户")
	@PreAuthorize("users.recycle")
	public ModelMap deleteRecycleByEntityids(String entityids) {
		userService.updateEntitystate(entityids, EntityState.DELETED.getValue(), false);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据用户主键冻结用户
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityids 用户主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=frozen")
	@ResponseBody
	@OperateLog("根据用户主键冻结用户")
	@PreAuthorize("users.frozen")
	public ModelMap updateFrozenByEntityids(String entityids) {
		userService.updateEntitystate(entityids, EntityState.FROZEN.getValue(), false);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 根据用户主键解冻用户
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityids 用户主键
	 * @return 结果信息
	 */
	@PostMapping(value = "/updateEntitystate.xhtml", params = "oper=thaw")
	@ResponseBody
	@OperateLog("根据用户主键解冻用户")
	@PreAuthorize("users.thaw")
	public ModelMap updateThawByEntityids(String entityids) {
		userService.updateEntitystate(entityids, EntityState.VALID.getValue(), false);
		return super.createModelOnlyStatus(true);
	}
	
	/**
	 * 前往用户管理查看详情页面
	 * 
	 * @author CJH 2019年5月21日
	 * @return 页面路径
	 */
	@GetMapping("/goUsersViewPage.xhtml")
	@OperateLog("前往用户管理查看详情页面")
	@PreAuthorize({"users", "users.recycle"})
	public String goUsersViewPage() {
		return "/pc/users/usersViewPage";
	}
	
	/**
	 * 查询所有用户名和用户电话
	 * 
	 * @author CJH 2019年5月22日
	 * @param isrecycle 是否回收站
	 * @return 用户
	 */
	@PostMapping("/findAllUsernameAndUserphone.xhtml")
	@ResponseBody
	@PassiveLog("查询所有用户名和用户电话")
	@PreAuthorize({"users", "users.recycle"})
	public ModelMap findAllUsernameAndUserphone(boolean isrecycle) {
		return super.createModel(true, userService.findAllUsernameAndUserphone(isrecycle));
	}
}