/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSalesBean.java
 * History:
 *         2019年6月7日: Initially created, wangjb.
 */
package club.coderleague.ilsp.controller.goods.bean;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品规格自定义bean。
 * @author wangjb
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GoodSpecsBean implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * id。
	 */
	private String entityid;
	
	/**
	 *  商品规格。
	 */
	private String specdefine;

	/**
	 *  计量单位。
	 */
	private String meterunit;

}
