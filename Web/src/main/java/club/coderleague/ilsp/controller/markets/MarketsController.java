/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MarketsController.java
 * History:
 *         2019年5月11日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.controller.markets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import club.coderleague.ilsp.common.annotation.OperateLog;
import club.coderleague.ilsp.common.annotation.PreAuthorize;
import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.controller.IndexController;
import club.coderleague.ilsp.entities.Markets;
import club.coderleague.ilsp.service.markets.MarketsService;

/**
 * 市场表
 * @author Liangjing
 */
@Controller
@RequestMapping(IndexController.REQUEST_PREFIX + "/market")
public class MarketsController extends IndexController {
	
	@Autowired
	MarketsService marketsService;
	
	/**
	 * 获取所有的市场数据
	 * @author Liangjing 2019年5月11日
	 * @param pageIndex
	 * @param pageSize
	 * @param istate
	 * @param key
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"market","market.recycle"})
	@OperateLog(value = "获取所有的市场数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/gettheallmarkets.xhtml")
	public ModelMap getTheAllMarkets(String pageIndex, String pageSize, boolean isrecycle, String key) {
		return super.createModel(true,marketsService.getTheAllMarkets(pageIndex, pageSize, isrecycle, key));
	}
	
	/**
	 * 获取市场编辑页面数据
	 * @author Liangjing 2019年5月12日
	 * @param entityid
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"market.edit","market","market.recycle"})
	@OperateLog(value = "获取市场编辑页面数据", type = OperateLogRequestType.PASSIVE)
	@RequestMapping("/getthemarketeditpagedata.xhtml")
	public ModelMap getTheMarketEditPageData(Long entityid) {
		return super.createModel(true,marketsService.getTheMarketEditPageData(entityid));
	}
	
	/**
	 * 新增或者编辑市场
	 * @author Liangjing 2019年5月12日
	 * @param markets
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"market.add","market.edit"})
	@OperateLog(value = "新增或者编辑市场")
	@RequestMapping("/updateTheMarket.xhtml")
	public ModelMap updateTheMarket(Markets markets) {
		return super.createModel(true,marketsService.updateTheMarket(markets));
	}
	
	/**
	 * 修改市场的状态
	 * @author Liangjing 2019年5月12日
	 * @param marketids
	 * @param iState
	 * @return
	 */
	@ResponseBody
	@PreAuthorize({"market.del","market.recycle"})
	@OperateLog(value = "修改市场的状态")
	@RequestMapping("/updatethemarketstate.xhtml")
	public ModelMap updateTheMarketstateByIstate(String marketids,Integer iState) {
		return super.createModel(true,marketsService.updateTheMarketstateByIstate(CommonUtil.toLongArrays(marketids), iState));
	}
	
	/**
	 * 前往市场首页
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"market","market.recycle"})
	@OperateLog("前往市场首页")
	@RequestMapping("/gohomepage.xhtml")
	public String gohomepage() {
		return "/pc/markets/homepage";
	}
	/**
	 * 前往市场修改页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"market.add","market.edit"})
	@OperateLog("前往市场修改页面")
	@RequestMapping("/goupdatepage.xhtml")
	public String goupdatepage() {
		return "/pc/markets/updatepage";
	}
	/**
	 * 前往市场查看详情页面
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	@PreAuthorize({"market","market.recycle"})
	@OperateLog("前往市场查看详情页面")
	@RequestMapping("/golookinfopage.xhtml")
	public String golookinfopage() {
		return "/pc/markets/lookinfo";
	}
}
