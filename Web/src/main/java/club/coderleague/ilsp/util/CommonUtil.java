/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：CommonUtil.java
 * History:
 *         2019年5月12日: Initially created, Chrise.
 */
package club.coderleague.ilsp.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 公共工具类。
 * @author Chrise
 */
public class CommonUtil {
	private static final char[] VC_CHARS = { '1', '2', '3', '4', '5', '6','7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
	private static final int VC_SIZE = 4;
	private static final int VC_LINES = 5;
	private static final int VC_WIDTH = 80;
	private static final int VC_HEIGHT = 28;
	private static final int VC_FONT_SIZE = 24;
	
	/**
	 * 检查字符串是否为空。
	 * @author Chrise 2019年5月12日
	 * @param s 待检查字符串。
	 * @return 字符串为空返回true，否则返回false。
	 */
	public static boolean isEmpty(String s) {
		return (s == null || "".equals(s));
	}
	
	/**
	 * 检查字符串是否不为空。
	 * @author Chrise 2019年6月10日
	 * @param s 待检查字符串。
	 * @return 字符串不为空返回true，否则返回false。
	 */
	public static boolean isNotEmpty(String s) {
		return (s != null && !"".equals(s));
	}
	
	/**
	 * 生成验证码。
	 * @author Chrise 2019年5月12日
	 * @return 验证码图片。
	 */
	public static Map<String, BufferedImage> genVericode() {
		Random r = new Random();
		
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < VC_SIZE; i ++) {
			sb.append(VC_CHARS[r.nextInt(VC_CHARS.length)]);
		}
		String code = sb.toString();
		
		BufferedImage image = new BufferedImage(VC_WIDTH, VC_HEIGHT, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphic = image.createGraphics();
		
		graphic.setColor(Color.WHITE);
		graphic.fillRect(0, 0, VC_WIDTH, VC_HEIGHT);
		
		graphic.setColor(Color.GRAY.brighter());
		for (int i = 0; i < VC_LINES; i ++) {
			graphic.drawLine(r.nextInt(VC_WIDTH), r.nextInt(VC_HEIGHT), r.nextInt(VC_WIDTH), r.nextInt(VC_HEIGHT));
		}
		
		graphic.translate(0, VC_HEIGHT - 2);
		graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		Font font = new Font("微软雅黑", Font.BOLD|Font.ITALIC, VC_FONT_SIZE);
		GlyphVector vector = font.createGlyphVector(graphic.getFontMetrics(font).getFontRenderContext(), code);
		Shape shape = vector.getOutline(4, -2);
		
		graphic.setColor(new Color(51, 98, 118));
		graphic.setStroke(new BasicStroke(1.0f));
		graphic.draw(shape);
		
		Map<String, BufferedImage> map = new HashMap<String, BufferedImage>();
		map.put(code, image);
		return map;
	}
}
