/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：StreamUtil.java
 * History:
 *         2019年6月25日: Initially created, Chrise.
 */
package club.coderleague.ilsp.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 流工具。
 * @author Chrise
 */
public class StreamUtil {
	private static final String DEF_ENCODING = "UTF-8";
	
	/**
	 * 使用默认编码从流中读取字符串，默认编码为UTF-8。
	 * @author Chrise 2019年6月25日
	 * @param in 输入流。
	 * @return 字符串对象。
	 * @throws IOException 输入输出错误。
	 */
	public static String readStringFromStream(InputStream in) throws IOException {
		return readStringFromStream(in, DEF_ENCODING);
	}
	
	/**
	 * 使用指定编码从流中读取字符串。
	 * @author Chrise 2019年6月25日
	 * @param in 输入流。
	 * @param encoding 字符编码。
	 * @return 字符串对象。
	 * @throws IOException 输入输出错误。
	 */
	public static String readStringFromStream(InputStream in, String encoding) throws IOException {
		ByteArrayOutputStream out = null;
		
		try {
			out = new ByteArrayOutputStream();
			
			int read = -1;
			byte[] buffer = new byte[1024];
			while ((read = in.read(buffer)) != -1) {
				out.write(buffer, 0, read);
			}
			
			return new String(out.toByteArray(), encoding);
		} finally {
			if (out != null) out.close();
		}
	}
}
