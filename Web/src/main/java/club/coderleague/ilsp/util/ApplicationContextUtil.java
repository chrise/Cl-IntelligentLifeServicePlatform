/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ApplicationContextUtil.java
 * History:
 *         2019年8月16日: Initially created, Chrise.
 */
package club.coderleague.ilsp.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 应用上下文工具。
 * @author Chrise
 */
@Component
public class ApplicationContextUtil implements ApplicationContextAware {
	private static ApplicationContext context;
	
	/**
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ApplicationContextUtil.context = applicationContext;
	}
	
	/**
	 * 获取应用上下文。
	 * @author Chrise 2019年8月16日
	 * @return 应用上下文对象。
	 */
	public static ApplicationContext getContext() {
		return context;
	}
}
