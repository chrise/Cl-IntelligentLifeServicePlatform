/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MessageQueueUtil.java
 * History:
 *         2019年9月12日: Initially created, Chrise.
 */
package club.coderleague.ilsp.util;

import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息队列工具。
 * @author Chrise
 */
public class MessageQueueUtil {
	private static List<String> delayDefine;
	static {
		delayDefine = Arrays.asList("10m", "15m", "20m", "3d", "5d");
	}
	
	/**
	 * 获取延迟时间级别。
	 * @author Chrise 2019年9月12日
	 * @param delay 延迟时间。
	 * @param unit 时间单位。
	 * @return 延迟时间级别，延迟时间未定义时返回即时级别。
	 */
	public static int getLevel(int delay, TimeUnit unit) {
		int index = delayDefine.indexOf(String.format("%d%s", delay, unit.getValue()));
		return index + 1;
	}
	
	/**
	 * 时间单位。
	 * @author Chrise
	 */
	@Getter
	@AllArgsConstructor
	public enum TimeUnit {
		/**
		 * 秒。
		 */
		SECOND("s"),
		/**
		 * 分。
		 */
		MINUTE("m"),
		/**
		 * 时。
		 */
		HOUR("h"),
		/**
		 * 天。
		 */
		DAY("d");
		
		private String value;
	}
}
