/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MessageHandler.java
 * History:
 *         2019年5月30日: Initially created, Chrise.
 */
package club.coderleague.ilsp.handler.wxmp;

import club.coderleague.ilsp.common.domain.beans.wxmp.AbstractMessage;

/**
 * 消息处理器。
 * @author Chrise
 */
public interface MessageHandler {
	/**
	 * 获取关联的消息类型。
	 * @author Chrise 2019年5月31日
	 * @return 关联的消息类型。
	 */
	String type();
	
	/**
	 * 处理消息。
	 * @author Chrise 2019年5月30日
	 * @param message 消息对象。
	 * @return 处理结果。
	 */
	String handle(AbstractMessage message);
}
