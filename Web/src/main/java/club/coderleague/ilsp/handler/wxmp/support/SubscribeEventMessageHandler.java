/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SubscribeEventMessageHandler.java
 * History:
 *         2019年5月30日: Initially created, Chrise.
 */
package club.coderleague.ilsp.handler.wxmp.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import club.coderleague.ilsp.cache.CacheManager;
import club.coderleague.ilsp.common.domain.beans.SystemConfig;
import club.coderleague.ilsp.common.domain.beans.wxmp.AbstractMessage;
import club.coderleague.ilsp.common.domain.beans.wxmp.reply.TextReplyMessage;
import club.coderleague.ilsp.handler.wxmp.MessageHandler;

/**
 * 关注事件消息处理器。
 * @author Chrise
 */
public class SubscribeEventMessageHandler implements MessageHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(SubscribeEventMessageHandler.class);
	
	@Autowired
	private CacheManager cacheManager;
	
	/**
	 * @see club.coderleague.ilsp.handler.wxmp.MessageHandler#type()
	 */
	@Override
	public String type() {
		return AbstractMessage.SUBSCRIBE;
	}
	
	/**
	 * @see club.coderleague.ilsp.handler.wxmp.MessageHandler#handle(club.coderleague.ilsp.common.domain.beans.wxmp.AbstractMessage)
	 */
	@Override
	public String handle(AbstractMessage message) {
		// 获取系统配置
		SystemConfig sc = (SystemConfig)this.cacheManager.getObject(SystemConfig.CACHE_KEY, false);
		if (sc == null || !sc.getWxswitch()) {
			LOGGER.warn("Received subscribe event message but interface is closed. -> [{}]", sc);
			return "";
		}
		
		// 回复欢迎消息
		TextReplyMessage reply = new TextReplyMessage(message.getFromUserName(), message.getToUserName(), sc.getWxwelcome());
		return reply.serialize();
	}
}
