/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesPageBean.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans.mobile;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.KeyField;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 手机分类点击后单个商品List展示扩展对象
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class SingleGoodsExtension {
	
	/**
	 * 商品id
	 */
	private String goodentityid;
	/**
	 * 商品名称
	 */
	private String goodname;
	/**
	 * 商品单价
	 */
	private Double goodprice;
	/**
	 * 市场名称
	 */
	private String marketname;
	/**
	 * 市场id
	 */
	private String marketentityid;
	/**
	 * 商户入驻id
	 */
	private String merchantsettleid;
	/**
	 * 商户id
	 */
	@KeyField
	private String merchantsentityid;
	/**
	 * 商户名称
	 */
	@SecureField("merchantsentityid")
	private String merchantname;
	/**
	 * 商品封面图片
	 */
	private String goodphoto;
	/**
	 * 已销售数量
	 */
	private Double salenumber;
	
}
