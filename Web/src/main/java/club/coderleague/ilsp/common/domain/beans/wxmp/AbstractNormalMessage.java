/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AbstractNormalMessage.java
 * History:
 *         2019年5月30日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans.wxmp;

/**
 * 普通消息基类。
 * @author Chrise
 */
public abstract class AbstractNormalMessage extends AbstractMessage {
	private static final long serialVersionUID = -2755218231266407637L;
}
