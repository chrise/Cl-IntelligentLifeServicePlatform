/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ReceivingAddressesExtension.java
 * History:
 *         2019年7月17日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.ilsp.entities.Receivingaddresses;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 收货地址扩展对象。
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class ReceivingAddressesExtension extends Receivingaddresses{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 省份。
	 */
	private String provincename;
	/**
	 * 城市。
	 */
	private String cityname;
	/**
	 * 区县。
	 */
	private String countyname;
	/**
	 * 街道。
	 */
	private String streetname;

}
