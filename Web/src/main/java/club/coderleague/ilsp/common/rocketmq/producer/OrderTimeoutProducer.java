/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderTimeoutProducer.java
 * History:
 *         2019年7月21日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.rocketmq.producer;

import org.springframework.stereotype.Component;

/**
 * 订单超时消息生产者
 * 
 * @author CJH
 */
@Component
public class OrderTimeoutProducer extends BaseDefaultMQProducer {
	
	/**
	 * 生产者组
	 */
	public final static String PRODUCER_GROUP = "ORDER_TIMEOUT_PRODUCER";

	/**
	 * 构造方法
	 */
	public OrderTimeoutProducer() {
		super(PRODUCER_GROUP);
	}

	
}
