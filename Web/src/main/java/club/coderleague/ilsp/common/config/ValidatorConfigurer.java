/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ValidatorConfigurer.java
 * History:
 *         2019年7月2日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.config;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;

/**
 * 校验配置
 * 
 * @author CJH
 */
public class ValidatorConfigurer {
	/**
	 * 配置校验为快速返回模式
	 * 
	 * @author CJH 2019年7月2日
	 * @return 校验
	 */
	@Bean
    public Validator validator() {
		ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)
                .buildValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        return validator;
	}
}
