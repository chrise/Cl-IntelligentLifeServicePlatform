/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MerchantRemind.java
 * History:
 *         2019年6月9日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.util.List;
import java.util.Map;

import javax.persistence.Id;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 手机用户个人设置数据封装
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MobileUser {
	/**
	 * 会员id
	 */
	@Id
	private Long memberid;
	/**
	 * 会员昵称
	 */
	private String nickname;
	/**
	 * 会员电话
	 */
	@SecureField
	private String memberphone;
	/**
	 * 会员绑定信息
	 */
	private List<Map<String,Object>> bindinfos;
}
