/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MultipleFileUploadBean.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 多文件上传封装类
 * @author Liangjing
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MultipleFileUploadBean<T> {
	/**
	 * 数据对象
	 */
	private T dataObj;
	/**
	 * 文件上传List
	 */
	private List<UploadFileBean> filelist;
}
