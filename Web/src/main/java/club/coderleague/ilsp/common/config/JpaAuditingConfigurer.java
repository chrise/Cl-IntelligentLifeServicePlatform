/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：JpaAuditingConfigurer.java
 * History:
 *         2019年5月8日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.config;

import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import club.coderleague.data.jpa.domain.Auditor;

/**
 * JPA审计配置器。
 * @author Chrise
 */
@EnableJpaAuditing
public class JpaAuditingConfigurer implements Auditor<Long> {

}
