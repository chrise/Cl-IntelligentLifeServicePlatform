/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MessageInfoException.java
 * History:
 *         2019年6月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.exception;

import club.coderleague.ilsp.common.domain.enums.ResultCode;

/**
 * 信息提示异常
 * 
 * @author CJH
 */
public class MessageInfoException extends MessageException {
	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 异常信息描述
	 * 
	 * @param message 异常描述
	 */
	public MessageInfoException(String message) {
        super(ResultCode.INFO, message);
    }
}
