/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OperateLogsExtension.java
 * History:
 *         2019年5月20日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.beans;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.KeyField;
import club.coderleague.data.jpa.annotation.SecureField;
import club.coderleague.ilsp.entities.Operatelogs;
import lombok.Getter;
import lombok.Setter;

/**
 * 操作日志扩展
 * 
 * @author CJH
 */
@Getter
@Setter
@CustomBean
@EnableHibernateEncryption
public class OperateLogsExtension extends Operatelogs {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 操作人ID
	 */
	@KeyField
	private Long operatorid;
	
	/**
	 * 操作人用户名
	 */
	@SecureField("operatorid")
	private String operatorusername;
}
