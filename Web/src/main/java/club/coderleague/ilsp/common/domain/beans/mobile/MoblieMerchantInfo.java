/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MoblieMerchantInfo.java
 * History:
 *         2019年7月30日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans.mobile;

import javax.persistence.Id;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 手机管理商户信息
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MoblieMerchantInfo {
	/**
	 * 商户id
	 */
	@Id
	private Long entityid;
	/**
	 * 商户名称
	 */
	@SecureField
	private String merchantname;
	/**
	 * 商户电话
	 */
	@SecureField
	private String merchantphone;
	/**
	 * 商户身份证号
	 */
	@SecureField
	private String idnumber;
	/**
	 * 开户银行
	 */
	private String bankkfname;
	/**
	 * 开户网点
	 */
	private String bankwdname;
	/**
	 * 银行账户名称
	 */
	@SecureField
	private String accountname;
	/**
	 * 银行卡号
	 */
	@SecureField
	private String bcnumber;
	/**
	 * 身份证正面照
	 */
	@SecureField
	private String idpositive;
	/**
	 * 身份证反面照
	 */
	@SecureField
	private String idnegative;
	/**
	 * 营业执照照片
	 */
	@SecureField
	private String businesslicense;
	/**
	 * 账户剩余金额（账户总额）
	 */
	private Double surplusmoeny;
	/**
	 * 实际可结算金额
	 */
	private Double realsurplusmoeny;
	/**
	 * 是否短信通知
	 */
	private Boolean smnotify;
	/**
	 * 是否自动结算
	 */
	private Boolean autosettle;

}
