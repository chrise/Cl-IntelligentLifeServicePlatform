/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：UploadFileBean.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 文件上传封装类
 * @author Liangjing
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileBean {
	/**
	 * 上传key
	 */
	private String keyname;
	/**
	 * 上传文件
	 */
	private List<UploadFile> uploadFile;
	/**
	 * 删除文件
	 */
	private String dels;
}
