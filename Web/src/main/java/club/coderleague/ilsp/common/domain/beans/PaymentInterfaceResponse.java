/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PaymentInterfaceResponse.java
 * History:
 *         2019年8月7日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 支付接口响应。
 * @author Chrise
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentInterfaceResponse implements Serializable {
	private static final long serialVersionUID = 9184412062390168621L;
	
	/**
	 * 订单标识。
	 */
	private Long orderid;
	/**
	 * 交易号。
	 */
	private String tradeno;
	/**
	 * 交易时间。
	 */
	private Date tradetime;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
