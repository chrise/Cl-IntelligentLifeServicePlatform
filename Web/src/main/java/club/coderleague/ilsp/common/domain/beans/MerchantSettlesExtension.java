/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesExtension.java
 * History:
 *         2019年6月8日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商户入驻扩展
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MerchantSettlesExtension {
	/**
	 * 商户入驻Id
	 */
	private Long entityid;
	/**
	 * 商户Id
	 */
	private Long marketid;
	/**
	 * 市场名称
	 */
	private String marketname;
	/**
	 * 入驻摊位
	 */
	private String settlestall;
}
