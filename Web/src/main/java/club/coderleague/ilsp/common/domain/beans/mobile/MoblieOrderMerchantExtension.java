/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MoblieOrderMerchantExtension.java
 * History:
 *         2019年8月23日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans.mobile;

import java.util.Date;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * -WeChat订单退款自定义扩展类。
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MoblieOrderMerchantExtension {
	
	/**
	 * -商户id
	 */
	@Id
	private Long merchantid;
	
	/**
	 * -商户名称
	 */
	@SecureField
	private String merchantname;
	
	/**
	 * -商户电话
	 */
	@SecureField
	private String merchantphone;
	
	/**
	 * -商户状态。
	 */
	private Integer merchantstate;
	
	/**
	 * -订单id。
	 */
	private Long orderid;
	
	/**
	 * -显示申请(true- 显示申请退， false-不显示申请退 )。
	 */
	private java.lang.String showapply;
	
	/**
	 * -商品数量。
	 */
	private Integer goodnumber; 
	
	/**
	 * -支付总额。
	 */
	private Double paymenttotal;
	
	/**
	 * -显示状态(0-可退，1-已超期，2-不可退，3-已申请售后)。
	 */
	private Integer refundstate;
	
	/**
	 * -退款记录id。
	 */
	private Long refundrecordid;
	
	/**
	 * -退款总额
	 */
	private Double refundtotal;
	
	/**
	 * -申请原因
	 */
	private String applydesc;
	
	/**
	 * -申请时间。
	 */
	private Date applytime;
	
	/**
	 * -退款时间。
	 */
	private Date refundtime;
	
	/**
	 * -交易号。
	 */
	private String tradeno;
	
	/**
	 * -记录总数。
	 */
	private Long recordcount;
	
	/**
	 * -订单状态。
	 */
	private Integer entitystate;
	
	/**
	 * -配送状态。
	 */
	private Integer deliverystate;
	
	/**
	 * -退款原因。
	 */
	private String refunddesc;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}

}
