/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AbstractEventMessage.java
 * History:
 *         2019年5月30日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans.wxmp;

import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import club.coderleague.ilsp.common.domain.beans.wxmp.event.SubscribeEventMessage;
import lombok.Getter;
import lombok.Setter;

/**
 * 事件消息基类。
 * @author Chrise
 */
@Getter
@Setter
public abstract class AbstractEventMessage extends AbstractMessage {
	private static final long serialVersionUID = 6147133501072994324L;
	private static final Logger logger = LoggerFactory.getLogger(AbstractEventMessage.class);
	
	private static final String tagEvent = "Event";
	
	private String event;
	
	/**
	 * @see club.coderleague.ilsp.common.domain.beans.wxmp.AbstractMessage#parseInternal(org.dom4j.Element)
	 */
	@Override
	protected void parseInternal(Element root) {
		super.parseInternal(root);
		
		this.event = root.elementTextTrim(tagEvent);
	}
	
	/**
	 * 解析。
	 * @author Chrise 2019年5月30日
	 * @param root XML根节点对象。
	 * @return 事件消息对象。
	 */
	protected static AbstractEventMessage parse(Element root) {
		AbstractEventMessage msg = null;
		
		String event = root.elementTextTrim(tagEvent);
		switch (event) {
			case SUBSCRIBE:
				msg = new SubscribeEventMessage();
				break;
			default:
				logger.warn("Unknown event [{}].", event);
				return null;
		}
		
		msg.parseInternal(root);
		return msg;
	}
}
