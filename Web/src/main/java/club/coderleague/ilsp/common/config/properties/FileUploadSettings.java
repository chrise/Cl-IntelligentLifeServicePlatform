/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：GoodGroupsController.java
 * History:
 *         2019年5月21日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.common.config.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 文件上传设置。
 * @author Liangjing
 */
@Getter
@Setter
@Component
@PropertySource("classpath:config/file-upload-settings.properties")
@ConfigurationProperties(prefix = "system.upload", ignoreUnknownFields = false)
public class FileUploadSettings {
	/**
	 * WEB预览附件及下载附件时程序调用的路径。
	 */
	@Value("${custom.resource.root.real}")
	private String real;
	/**
	 * WEB上传文件程序调用的路径。
	 */
	@Value("${custom.resource.root.virtual}")
	private String virtual;
	
	/**
	 * 公共系统附件上传大小设置。
	 */
	private Integer commonAttachSize;
	
	/**
	 * 商户表附件上传路径。
	 */
	private String merchantsPath;
	
	/**
	 * 商户表附件上传类型。
	 */
	private String merchantsType;
	
	/**
	 * 商品相册表上传文件夹名称。
	 */
	private String goodPhotosPath;
	
	/**
	 * 商品相册表上传类型。
	 */
	private String goodPhotosType;
	
	/**
	 * 大客户上传文件路径。
	 */
	private String bigCustomersPath;
	
	/**
	 * 大客户上传文件类型。
	 */
	private String bigCustomersType;
	
	/**
	 * 商品分类附件上传路径。
	 */
	private String goodGroupsPath;
	
	/**
	 * 商品分类附件上传类型。
	 */
	private String goodGroupsType;
	
	/**
	 * 商户结算付款凭证附件上传路径。
	 */
	private String merchantSettlementsPath;
	
	/**
	 * 商户结算付款凭证附件上传类型。
	 */
	private String merchantSettlementsType;
	
}
