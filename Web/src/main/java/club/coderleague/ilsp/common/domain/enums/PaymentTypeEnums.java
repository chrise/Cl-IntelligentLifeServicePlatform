/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PaymentTypeEnums.java
 * History:
 *         2019年7月26日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 付款类型（1、人工打款，2、自动转账）枚举。
 * @author wangjb
 */
@Getter
@AllArgsConstructor
public enum PaymentTypeEnums {
	/**
	 * 人工打款。
	 */
	ARTIFICIAL(1, "人工打款"),
	/**
	 * 自动转账。
	 */
	AUTOMATIC(2, "自动转账");
	
	private int value;
	private String text;
}
