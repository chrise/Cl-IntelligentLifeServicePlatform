/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MemberBindType.java
 * History:
 *         2019年5月31日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 会员绑定类型。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum MemberBindType {
	/**
	 * 微信。
	 */
	WEIXIN(1, "微信"),
	/**
	 * 支付宝。
	 */
	ALIPAY(2, "支付宝"),
	/**
	 * 银联。
	 */
	UNIONPAY(3, "银联");
	
	private int value;
	private String text;
	
	/**
	 * 比较会员绑定类型。
	 * @author Chrise 2019年5月31日
	 * @param type 会员绑定类型对象。
	 * @return 会员绑定类型相等时返回true，否则返回false。
	 */
	public boolean equals(MemberBindType type) {
		return (this.value == type.value);
	}
	
	/**
	 * 比较会员绑定类型值。
	 * @author Chrise 2019年5月31日
	 * @param value 会员绑定类型值。
	 * @return 会员绑定类型值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
