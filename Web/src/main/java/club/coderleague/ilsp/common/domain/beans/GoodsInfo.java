/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodsInfo.java
 * History:
 *         2019年7月25日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 商品信息
 * 
 * @author CJH
 */
@Getter
@Setter
public class GoodsInfo implements Serializable {

	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 商户标识
	 */
	private Long merchantid;
	
	/**
	 * 市场标识
	 */
	private Long marketid;
	
	/**
	 * 商品名称
	 */
	private String goodname;
	
	/**
	 * 商品产地
	 */
	private String goodorigin;
	
	/**
	 * 商品封面图片
	 */
	private String goodpicture;

	/**
	 * 商品规格
	 */
	private String goodspec;
	
	
}
