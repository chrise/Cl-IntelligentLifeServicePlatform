/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：WebAuthScene.java
 * History:
 *         2019年5月31日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

/**
 * 网页授权场景。
 * @author Chrise
 */
public enum WebAuthScene {
	/**
	 * 扫码支付。
	 */
	SCAN_PAY,
	/**
	 * 会员商城。
	 */
	MEMBER_MALL,
	/**
	 * 大客户专区。
	 */
	CUSTOMER_AREA,
	/**
	 * 商户控制台。
	 */
	MERCHANT_CONSOLE;
}
