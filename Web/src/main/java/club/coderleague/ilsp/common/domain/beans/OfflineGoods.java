/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OfflineGoods.java
 * History:
 *         2019年8月21日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 线下订单商品
 * 
 * @author CJH
 */
@Getter
@Setter
public class OfflineGoods implements Serializable {
	
	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 销售标识
	 */
	private Long saleid;
	
	/**
	 * 商品数量
	 */
	private Double goodnumber;
}
