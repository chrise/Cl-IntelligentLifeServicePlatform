/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderCompleteProducer.java
 * History:
 *         2019年9月12日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.rocketmq.producer;

/**
 * 订单完成生产者
 * 
 * @author CJH
 */
public class OrderCompleteProducer extends BaseDefaultMQProducer {

	/**
	 * 生产者组
	 */
	public final static String PRODUCER_GROUP = "ORDER_COMPLETE_PRODUCER";

	/**
	 * 构造方法
	 */
	public OrderCompleteProducer() {
		super(PRODUCER_GROUP);
	}
}
