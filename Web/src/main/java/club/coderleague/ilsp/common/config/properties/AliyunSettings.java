/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AliyunSettings.java
 * History:
 *         2019年6月9日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 阿里云设置。
 * @author Chrise
 */
@Getter
@Setter
@Component
@PropertySource("${custom.aliyun.configLocation}")
@ConfigurationProperties(prefix = "aliyun", ignoreUnknownFields = false)
public class AliyunSettings {
	/**
	 * 域名。
	 */
	private String domain;
	/**
	 * 版本。
	 */
	private String version;
	/**
	 * 方法。
	 */
	private String action;
	/**
	 * 本地通知。
	 */
	private Boolean localNotify;
	/**
	 * 本地通知超时。
	 */
	private Integer localNotifyTimeout;
}
