/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：WebAuthContext.java
 * History:
 *         2019年5月31日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.ilsp.common.domain.enums.WebAuthScene;
import club.coderleague.ilsp.common.domain.enums.WebAuthor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 网页授权上下文。
 * @author Chrise
 */
@Getter
@Setter
@NoArgsConstructor
public class WebAuthContext implements Serializable {
	private static final long serialVersionUID = -968218837189257762L;
	
	/**
	 * 会话键。
	 */
	public static final String SESSION_KEY = "web_auth_context";
	
	/**
	 * 授权场景。
	 */
	private WebAuthScene scene;
	/**
	 * 授权人。
	 */
	private WebAuthor author;
	/**
	 * 支付订单。
	 */
	private String order;
	/**
	 * 开放标识。
	 */
	private String openid;
	/**
	 * 重定向地址。
	 */
	private String redirect;
	
	/**
	 * 网页授权上下文构造方法。
	 * @param scene 授权场景。
	 * @param author 授权人。
	 */
	public WebAuthContext(WebAuthScene scene, WebAuthor author) {
		this(scene, author, null);
	}
	
	/**
	 * 网页授权上下文构造方法。
	 * @param scene 授权场景。
	 * @param author 授权人。
	 * @param order 支付订单。
	 */
	public WebAuthContext(WebAuthScene scene, WebAuthor author, String order) {
		this.scene = scene;
		this.author = author;
		this.order = order;
		this.openid = null;
		this.redirect = null;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
