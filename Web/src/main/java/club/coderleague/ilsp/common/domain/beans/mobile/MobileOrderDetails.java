/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesPageBean.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans.mobile;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.KeyField;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 手机端-订单详情
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MobileOrderDetails {
	
	/**
	 * 订单id
	 */
	@KeyField
	private String orderid;
	/**
	 * 订单状态
	 */
	private String orderstate;
	/**
	 * 支付方式（只有待付款订单 才会有值）
	 */
	private String paymodel;
	/**
	 * 取消原因
	 */
	private String canclereason;
	/**
	 * 剩余时间
	 */
	private String surplustime;
	/**
	 * 收货人
	 */
	@SecureField("orderid")
	private String receivername;
	/**
	 * 收货人电话
	 */
	@SecureField("orderid")
	private String receivephone;
	/**
	 * 收货地址
	 */
	private String address;
	/**
	 * 收货详细地址
	 */
	@SecureField("orderid")
	private String detailaddress;
	/**
	 * 商户id
	 */
	@KeyField
	private Long merchantid;
	/**
	 * 商户名称
	 */
	@SecureField("merchantid")
	private String merchantname;
	/**
	 * 订单商品
	 */
	private List<Map<String,Object>> goodlist;
	/**
	 * 订单编号
	 */
	private String ordercode;
	/**
	 * 下单时间
	 */
	private String ordertime;
	/**
	 * 支付方式
	 */
	private String paymentmode;
	/**
	 * 配送方式
	 */
	private String deliverymode;
	/**
	 * 期望配送日期
	 */
	private String deliverytime;
	/**
	 * 商品总额
	 */
	private Double goodtotal;
	/**
	 * 配送费用
	 */
	private Double deliverycost;
	/**
	 * 订单总额
	 */
	private Double ordertotal;
	/**
	 * 钻石抵扣
	 */
	private Integer diamondpay;
	/**
	 * 支付总额
	 */
	private Double paymenttotal;
	/**
	 *不可退款类型（1.超时不可退   2.订单内所有商品不可退   3.已经申请退款  0.可以退款）
	 */
	private Long isnorefundtype;
	/**
	 * 订单来源（1、线下  2、线上）
	 */
	private Integer orderorigin;
	
}
