/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderStatisticsExtension.java
 * History:
 *         2019年6月15日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.KeyField;
import club.coderleague.data.jpa.annotation.SecureField;
import club.coderleague.ilsp.entities.Orders;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 订单统计扩展对象。
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class OrderStatisticsExtension extends Orders{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 大客户id。
	 */
	@KeyField
	private Long customerid;
	
	/**
	 * 大客户名称。
	 */
	@SecureField("customerid")
	private String customername;
	
	/**
	 * 采购员 id。
	 */
	@KeyField
	private Long buyer;
	
	/**
	 * 采购员姓名。
	 */
	@SecureField("buyer")
	private String buyername;
	
	/**
	 * 商户id。
	 */
	@KeyField
	private Long mid;
	
	/**
	 * 商户名称。
	 */
	@SecureField("mid")
	private String merchantname;
	
	/**
	 * 退款状态。
	 */
	private Long refundstate;
	
	/**
	 * 市场名称。
	 */
	private String marketname;
	
	/**
	 * 会员id。
	 */
	@KeyField
	private Long memberid;
	
	/**
	 * 会员名称。
	 */
	@SecureField("memberid")
	private String memberphone;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}

}
