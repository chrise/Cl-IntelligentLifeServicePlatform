/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PassiveLog.java
 * History:
 *         2019年6月20日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;

/**
 * 被动操作日志
 * 
 * @author CJH
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@OperateLog(type = OperateLogRequestType.PASSIVE)
public @interface PassiveLog {
	/**
	 * 日志描述
	 */
	@AliasFor(annotation = OperateLog.class)
	String value() default "";
}
