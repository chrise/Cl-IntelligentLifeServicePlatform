/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MessageErrorException.java
 * History:
 *         2019年6月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.exception;

import club.coderleague.ilsp.common.domain.enums.ResultCode;

/**
 * 信息错误异常
 * 
 * @author CJH
 */
public class MessageErrorException extends MessageException {
	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 异常信息描述
	 * 
	 * @param message 异常描述
	 */
	public MessageErrorException(String message) {
        super(ResultCode.ERROR, message);
    }
}
