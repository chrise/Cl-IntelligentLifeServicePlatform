/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondRecordType.java
 * History:
 *         2019年5月28日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 钻石记录类型
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum DiamondRecordType {
	/**
	 * 收入
	 */
	INCOME(1, "收入"),
	
	/**
	 * 支出
	 */
	PAY(2, "支出");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
}
