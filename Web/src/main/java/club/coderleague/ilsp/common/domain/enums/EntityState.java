/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：EntityState.java
 * History:
 *         2019年5月8日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 实体状态。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum EntityState {
	/**
	 * 有效。
	 */
	VALID(1, "有效"),
	/**
	 * 无效。
	 */
	INVALID(2, "无效"),
	/**
	 * 冻结。
	 */
	FROZEN(3, "冻结"),
	/**
	 * 上架。
	 */
	PUBLISHED(4, "上架"),
	/**
	 * 待确认。
	 */
	TBD(5, "待确认"),
	/**
	 * 已确认。
	 */
	CONFIRMED(6, "已确认"),
	/**
	 * 已完成。
	 */
	OTS(7, "已完成"),
	/**
	 * 已拆分。
	 */
	HBS(8, "已拆分"),
	/**
	 * 已取消。
	 */
	CANC(9, "已取消"),
	/**
	 * 待审核
	 */
	STAYAUDIT(10, "待审核"),
	/**
	 * 未通过
	 */
	NOTPASS(11, "未通过"),
	/**
	 * 待处理。
	 */
	OBLIGATION(12, "待处理"),
	/**
	 *处理中。
	 */
	PAYMENT(13, "处理中"),
	/**
	 * 撤销。
	 */
	UNDO(14, "已撤销"),
	/**
	 * 结算完成。
	 */
	SETTLEOVER(15, "结算完成"),
	/**
	 * 结算取消。
	 */
	SETTLECANCEL(16, "结算取消"),
	/**
	 * 待退款。
	 */
	REFUNDING(17, "待退款"),
	/**
	 * 已退款。
	 */
	REFUNDED(18, "已退款"),
	/**
	 * 级联删除。
	 */
	CASCADEDELETED(98, "级联删除"),
	/**
	 * 删除。
	 */
	DELETED(99, "删除");
	
	private int value;
	private String text;
	
	/**
	 * 比较实体状态。
	 * @author Chrise 2019年5月8日
	 * @param state 实体状态对象。
	 * @return 实体状态相等时返回true，否则返回false。
	 */
	public boolean equals(EntityState state) {
		return (this.value == state.value);
	}
	
	/**
	 * 比较实体状态值。
	 * @author Chrise 2019年5月8日
	 * @param value 实体状态值。
	 * @return 实体状态值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
