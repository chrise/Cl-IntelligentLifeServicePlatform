/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MemberOrigin.java
 * History:
 *         2019年6月2日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 会员来源。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum MemberOrigin {
	/**
	 * 微信关注。
	 */
	WEIXIN_SUBSCRIBE(1, "微信关注"),
	/**
	 * 微信扫码。
	 */
	WEIXIN_SCAN(2, "微信扫码"),
	/**
	 * 支付宝扫码。
	 */
	ALIPAY_SCAN(3, "支付宝扫码"),
	/**
	 * 银联扫码。
	 */
	UNIONPAY_SCAN(4, "银联扫码");
	
	private int value;
	private String text;
	
	/**
	 * 比较会员来源。
	 * @author Chrise 2019年6月2日
	 * @param origin 会员来源对象。
	 * @return 会员来源相等时返回true，否则返回false。
	 */
	public boolean equals(MemberOrigin origin) {
		return (this.value == origin.value);
	}
	
	/**
	 * 比较会员来源值。
	 * @author Chrise 2019年6月2日
	 * @param value 会员来源值。
	 * @return 会员来源值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
