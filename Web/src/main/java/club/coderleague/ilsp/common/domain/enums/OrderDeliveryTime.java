/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderDeliveryTime.java
 * History:
 *         2019年7月14日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单配送时间枚举
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderDeliveryTime implements BaseEnum {
	/**
	 * 标准时间
	 */
	STANDARD(1, "标准时间"),
	
	/**
	 * 特定时间
	 */
	CUSTOMIZED(2, "特定时间");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * @see club.coderleague.ilsp.common.domain.enums.BaseEnum#getPrimaryKey()
	 */
	@Override
	public String getPrimaryKey() {
		return String.valueOf(value);
	}
	
	/**
	 * 比较值是否相等
	 * 
	 * @author CJH 2019年7月14日
	 * @param value 比较值
	 * @return 是否相等
	 */
	public boolean equalsValue(Integer value) {
		return this.value.intValue() == value.intValue();
	}
}
