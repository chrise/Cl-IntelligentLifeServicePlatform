/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondExpireScheduled.java
 * History:
 *         2019年5月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.scheduled;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import club.coderleague.ilsp.service.diamondrecords.DiamondRecordsService;
import lombok.extern.slf4j.Slf4j;

/**
 * 钻石到期任务
 * 
 * @author CJH
 */
@Slf4j
@Component
public class DiamondExpireScheduled {
	/**
	 * 定时任务过期时间
	 */
	@Value("${custom.scheduled.diamond.duration}")
	private String duration;
	
	/**
	 * Redis模板
	 */
	private @Autowired RedisTemplate<String, Object> redisTemplate;
	
	/**
	 * Redis保存任务状态key
	 */
	private static final String LOCK_KEY = "diamond.expire.scheduled.starttime";
	
	/**
	 * 钻石记录Service
	 */
	private @Autowired DiamondRecordsService diamondRecordsService;
	
	/**
	 * 定时清理过期钻石
	 * 
	 * @author CJH 2019年5月27日
	 */
	@Scheduled(cron = "${custom.scheduled.diamond.cron}")
	public void scheduler() {
		try {
			if (redisTemplate.opsForValue().setIfAbsent(LOCK_KEY, System.currentTimeMillis(), Duration.parse(duration))) {
				log.info("开始执行清理过期钻石任务...");
				diamondRecordsService.updateCleanExpireDiamond();
			} else {
				log.info("清理过期钻石任务正在执行中...");
			}
		} finally {
			redisTemplate.delete(LOCK_KEY);
		}
	}
}
