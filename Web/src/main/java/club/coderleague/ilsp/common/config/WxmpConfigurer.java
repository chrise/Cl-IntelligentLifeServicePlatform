/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：WxmpConfigurer.java
 * History:
 *         2019年5月31日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import club.coderleague.ilsp.handler.wxmp.support.SubscribeEventMessageHandler;
import club.coderleague.ilsp.service.interfaces.WeixinInterfaceService;

/**
 * 微信公众平台配置器。
 * @author Chrise
 */
public class WxmpConfigurer {
	@Autowired
	private WeixinInterfaceService wiService;
	
	/**
	 * 注册关注事件消息处理器。
	 * @author Chrise 2019年5月31日
	 * @return 关注事件消息处理器对象。
	 */
	@Bean
	public SubscribeEventMessageHandler registerSubscribeEventMessageHandler() {
		SubscribeEventMessageHandler handler = new SubscribeEventMessageHandler();
		this.wiService.addMessageHandler(handler);
		return handler;
	}
}
