/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MySQL5726Dialect.java
 * History:
 *         2019年5月8日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.dialect;

import java.sql.Types;

import org.hibernate.dialect.MySQL57Dialect;
import org.hibernate.type.StandardBasicTypes;

/**
 * MySQL5726 方言。
 * @author Chrise
 */
public class MySQL5726Dialect extends MySQL57Dialect {
	/**
	 * MySQL5726 方言构造方法。
	 */
	public MySQL5726Dialect() {
		super();
		
		registerColumnType(Types.INTEGER, "mediumint");
		registerColumnType(Types.INTEGER, "int");
		registerColumnType(Types.LONGVARCHAR, "text");
		
		registerHibernateType(Types.BIT, StandardBasicTypes.BOOLEAN.getName());
		registerHibernateType(Types.TINYINT, StandardBasicTypes.INTEGER.getName());
		registerHibernateType(Types.SMALLINT, StandardBasicTypes.INTEGER.getName());
		registerHibernateType(Types.INTEGER, StandardBasicTypes.INTEGER.getName());
		registerHibernateType(Types.BIGINT, StandardBasicTypes.LONG.getName());
		registerHibernateType(Types.DECIMAL, StandardBasicTypes.DOUBLE.getName());
		registerHibernateType(Types.NUMERIC, StandardBasicTypes.DOUBLE.getName());
		registerHibernateType(Types.CLOB, StandardBasicTypes.STRING.getName());
	}
}
