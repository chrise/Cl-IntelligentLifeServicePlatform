/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomerOrdersExtension.java
 * History:
 *         2019年6月8日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import club.coderleague.ilsp.entities.Orders;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 大客户订单扩展对象
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class BigCustomerOrdersExtension extends Orders{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 大客户id
	 */
	@Id
	private Long customerid;
	
	/**
	 * 大客户名称。
	 */
	@SecureField
	private String customername;
	
//	/**
//	 * 采购人id。
//	 */
//	@Id
//	private Long userid;
//	
//	/**
//	 * 采购人姓名。
//	 */
//	@SecureField
//	private String username;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}

}
