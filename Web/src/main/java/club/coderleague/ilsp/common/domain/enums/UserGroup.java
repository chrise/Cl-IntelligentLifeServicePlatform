/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UserGroup.java
 * History:
 *         2019年5月31日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用户组。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum UserGroup {
	/**
	 * 管理员。
	 */
	MANAGER(1, "管理员"),
	/**
	 * 会员。
	 */
	MEMBER(2, "会员"),
	/**
	 * 大客户。
	 */
	CUSTOMER(3, "大客户"),
	/**
	 * 商户。
	 */
	MERCHANT(4, "商户");
	
	private int value;
	private String text;
	
	/**
	 * 比较用户组。
	 * @author Chrise 2019年5月31日
	 * @param group 用户组对象。
	 * @return 用户组相等时返回true，否则返回false。
	 */
	public boolean equals(UserGroup group) {
		return (this.value == group.value);
	}
	
	/**
	 * 比较用户组值。
	 * @author Chrise 2019年5月31日
	 * @param value 用户组值。
	 * @return 用户组值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
