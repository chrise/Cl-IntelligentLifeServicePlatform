/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesPageBean.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans.mobile;

import java.util.List;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.KeyField;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 手机端-订单
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MobileOrders {
	
	/**
	 * 订单id
	 */
	private String orderid;
	/**
	 * 商户id
	 */
	@KeyField
	private Long merchantid;
	/**
	 * 商户名称
	 */
	@SecureField("merchantid")
	private String merchantname;
	/**
	 * 订单合计
	 */
	private Double ordermoney;
	/**
	 * 订单件数
	 */
	private Long ordernum;
	/**
	 * 订单状态文字描述
	 */
	private String orderstatestr;
	/**
	 * 订单状态
	 */
	private String orderstate;
	/**
	 * 支付方式（只有待付款订单 才会有值）
	 */
	private String paymodel;
	/**
	 *不可退款类型（1.超时不可退   2.订单内所有商品不可退   3.已经申请退款  0.可以退款）
	 */
	private Long isnorefundtype;
	/**
	 * 订单来源（1、线下  2、线上）
	 */
	private Integer orderorigin;
	/**
	 * 订单内商品图片
	 */
	private List<String> piclist;
	
}
