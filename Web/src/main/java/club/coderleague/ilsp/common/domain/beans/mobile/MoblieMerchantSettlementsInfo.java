/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MoblieMerchantInfo.java
 * History:
 *         2019年7月30日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans.mobile;

import javax.persistence.Id;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 手机商户结算表信息
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MoblieMerchantSettlementsInfo {
	/**
	 * 商户结算ID
	 */
	private Long entityid;
	/**
	 * 结算状态
	 */
	private String entitystate;
	/**
	 * 结算类型
	 */
	private String settlementtype;
	/**
	 * 商结算金额
	 */
	private Double settlementamount;
	/**
	 * 申请时间
	 */
	private String applytime;
	/**
	 * 撤销时间
	 */
	private String revoketime;
	/**
	 * 付款类型
	 */
	private String paymenttype;
	/**
	 * 付款时间
	 */
	private String paytime;
	/**
	 * 处理人用户ID
	 */
	@Id
	private Long userentityid;
	/**
	 * 处理人用户名称
	 */
	@SecureField
	private String username;

}
