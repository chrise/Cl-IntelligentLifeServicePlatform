/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：Result.java
 * History:
 *         2019年5月11日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
  * 操作结果。
 * @author Liangjing
 */
@Data
@AllArgsConstructor
public class ResultMsg implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 结果状态
	 */
	private boolean status;

	/**
	 * 信息。
	 */
	private String msg;
	
}
