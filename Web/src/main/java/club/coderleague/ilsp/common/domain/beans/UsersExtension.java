/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UsersExtension.java
 * History:
 *         2019年5月20日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.beans;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.ilsp.entities.Users;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户扩展
 * 
 * @author CJH
 */
@Getter
@Setter
@CustomBean
@EnableHibernateEncryption
public class UsersExtension extends Users {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 机构名称
	 */
	private String orgname;
	
	/**
	 * 角色ID
	 */
	private String roleids;
	
	/**
	 * 角色名称
	 */
	private String rolenames;
}
