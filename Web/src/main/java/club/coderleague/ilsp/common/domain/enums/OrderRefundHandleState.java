/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderRefundHandleState.java
 * History:
 *         2019年7月25日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 退款处理状态
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderRefundHandleState implements BaseEnum {
	
	/**
	 * 未申请
	 */
	NO_APPLY(1, "未申请"),
	
	/**
	 * 待处理
	 */
	PROCESSING(2, "待处理"),
	
	/**
	 * 已处理
	 */
	PROCESSED(3, "已处理");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * @see club.coderleague.ilsp.common.domain.enums.BaseEnum#getPrimaryKey()
	 */
	@Override
	public String getPrimaryKey() {
		return String.valueOf(value);
	}
	
	/**
	 * 比较值是否相等
	 * 
	 * @author CJH 2019年7月25日
	 * @param value 比较值
	 * @return 是否相等
	 */
	public boolean equalsValue(Integer value) {
		return this.value.intValue() == value.intValue();
	}
}