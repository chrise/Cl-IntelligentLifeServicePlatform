/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantsPageBean.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans.mobile;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.KeyField;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 筛选信息-商户
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MerchantsFilterConditions {
	/**
	 * 商户id
	 */
	@KeyField
	private String entityid;
	/**
	 * 商户名称
	 */
	@SecureField("entityid")
	private String name;
}
