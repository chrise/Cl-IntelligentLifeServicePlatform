/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PagingParameters.java
 * History:
 *         2019年7月4日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 分页参数
 * 
 * @author CJH
 */
@Getter
@Setter
public class PagingParameters implements Serializable {

	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 当前页数
	 */
	private Integer pageIndex;
	
	/**
	 * 每页大小
	 */
	private Integer pageSize;
	
	/**
	 * 排序字段
	 */
	private String sortField;
	
	/**
	 * 排序方式
	 */
	private String sortOrder;
}
