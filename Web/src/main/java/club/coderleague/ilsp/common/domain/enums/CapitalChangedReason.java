/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：CapitalChangedReason.java
 * History:
 *         2019年6月17日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 资金变化原因
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum CapitalChangedReason {
	/**
	 * 订单收入
	 */
	ORDER_REVENUE(1, "订单收入", CapitalRecordType.INCOME),
	
	/**
	 * 订单退款
	 */
	ORDER_REFUND(2, "订单退款", CapitalRecordType.PAY),
	
	/**
	 * 结算支出
	 */
	SETTLE_ACCOUNTS(3, "结算支出", CapitalRecordType.PAY),
	
	/**
	 * 支付货款
	 */
	PAY(4, "支付货款", CapitalRecordType.PAY);
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * 类型
	 */
	private CapitalRecordType type;
}
