/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ThymeleafConfigurer.java
 * History:
 *         2019年5月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.config;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.thymeleaf.spring5.view.ThymeleafViewResolver;

/**
 * Thymeleaf配置器
 * 
 * @author CJH
 */
public class ThymeleafConfigurer {
	private static final String KEY_CTX = "ctx";
	private static final String KEY_MOBILE_MEMBER_CTX = "memberctx";
	private static final String KEY_MOBILE_CUSTOMER_CTX = "customerctx";
	private static final String KEY_MOBILE_MERCHANT_CTX = "merchantctx";
	private static final String KEY_LIB = "lib";
	private static final String KEY_CSS = "css";
	private static final String KEY_IMAGE = "image";
	private static final String KEY_JS = "js";
	
	@Resource
	private ServletContext sc;
	
	/**
	 * 注册静态变量
	 * 
	 * @author CJH 2019年5月10日
	 * @param resolver 视图解析器
	 */
	@Resource
	private void registerStaticVariable(ThymeleafViewResolver resolver) {
		String ctx = this.sc.getContextPath();
		
		resolver.addStaticVariable(KEY_CTX, ctx);
		resolver.addStaticVariable(KEY_MOBILE_MEMBER_CTX, ctx + "/mobile/member");
		resolver.addStaticVariable(KEY_MOBILE_CUSTOMER_CTX, ctx + "/mobile/customer");
		resolver.addStaticVariable(KEY_MOBILE_MERCHANT_CTX, ctx + "/mobile/merchant");
		resolver.addStaticVariable(KEY_LIB, ctx + "/static/library");
		resolver.addStaticVariable(KEY_CSS, ctx + "/static/css");
		resolver.addStaticVariable(KEY_IMAGE, ctx + "/static/image");
		resolver.addStaticVariable(KEY_JS, ctx + "/static/js");
	}
}
