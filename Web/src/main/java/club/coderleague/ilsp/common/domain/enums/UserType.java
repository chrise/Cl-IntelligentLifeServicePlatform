/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UserType.java
 * History:
 *         2019年5月8日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用户类型。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum UserType {
	/**
	 * 管理员。
	 */
	ADMIN(1, "管理员"),
	/**
	 * 普通用户。
	 */
	NORMAL(2, "普通用户");
	
	private int value;
	private String text;
	
	/**
	 * 比较用户类型。
	 * @author Chrise 2019年5月8日
	 * @param type 用户类型对象。
	 * @return 用户类型相等时返回true，否则返回false。
	 */
	public boolean equals(UserType type) {
		return (this.value == type.value);
	}
	
	/**
	 * 比较用户类型值。
	 * @author Chrise 2019年5月8日
	 * @param value 用户类型值。
	 * @return 用户类型值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
