/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IntegralChangedReason.java
 * History:
 *         2019年5月30日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 积分变化原因
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum IntegralChangedReason {
	/**
	 * 过期清零
	 */
	EXPIRE(1, "过期清零", IntegralRecordType.PAY),
	
	/**
	 * 订单收入
	 */
	ORDER_REVENUE(2, "订单收入", IntegralRecordType.INCOME),
	
	/**
	 * 订单支出
	 */
	PAY(3, "订单支出", IntegralRecordType.PAY);
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * 类型
	 */
	private IntegralRecordType type;
}
