/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderType.java
 * History:
 *         2019年6月10日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单类型。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum OrderType {
	/**
	 * 普通。
	 */
	NORMAL(1, "普通"),
	/**
	 * 活动。
	 */
	ACTIVITY(2, "活动"),
	/**
	 * 充值。
	 */
	RECHARGE(3, "充值"),
	/**
	 * 大客户。
	 */
	CUSTOMER(4, "大客户");
	
	private int value;
	private String text;
	
	/**
	 * 比较订单类型。
	 * @author Chrise 2019年6月10日
	 * @param type 订单类型对象。
	 * @return 订单类型相等时返回true，否则返回false。
	 */
	public boolean equals(OrderType type) {
		return (this.value == type.value);
	}
	
	/**
	 * 比较订单类型值。
	 * @author Chrise 2019年6月10日
	 * @param value 订单类型值。
	 * @return 订单类型值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
