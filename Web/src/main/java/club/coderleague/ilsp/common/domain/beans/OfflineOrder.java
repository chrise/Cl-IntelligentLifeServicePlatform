/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OfflineOrder.java
 * History:
 *         2019年7月7日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 线下订单
 * 
 * @author CJH
 */
@Getter
@Setter
public class OfflineOrder implements Serializable {
	
	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 购物商品
	 */
	private List<OfflineGoods> offlinegoods;
}
