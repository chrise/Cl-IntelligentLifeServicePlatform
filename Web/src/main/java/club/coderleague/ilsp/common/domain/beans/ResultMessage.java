/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ResultMessage.java
 * History:
 *         2019年5月11日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 操作结果信息
 * 
 * @author CJH
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultMessage implements Serializable {
	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 编码
	 */
	private Integer code;
	
	/**
	 * 信息
	 */
	private String message;
}
