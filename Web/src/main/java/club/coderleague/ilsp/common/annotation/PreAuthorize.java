/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PreAuthorize.java
 * History:
 *         2019年5月17日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 定义前置鉴权。
 * @author Chrise
 */
@Target(METHOD)
@Retention(RUNTIME)
public @interface PreAuthorize {
	/**
	 * 授权标识。
	 * @author Chrise 2019年5月17日
	 * @return 授权标识。
	 */
	String[] value() default {};
}
