/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ThreadPoolConfigurer.java
 * History:
 *         2019年5月21日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * 线程池配置器。
 * @author Chrise
 */
public class ThreadPoolConfigurer {
	@Value("${custom.thread-pool.min-size}")
	private int minThreadPoolSize;
	@Value("${custom.thread-pool.max-size}")
	private int maxThreadPoolSize;
	
	/**
	 * 注册任务调度器。
	 * @author Chrise 2019年5月21日
	 * @return 任务调度器对象。
	 */
	@Bean
	public ThreadPoolTaskScheduler registerTaskScheduler() {
		ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
		scheduler.setPoolSize(this.maxThreadPoolSize);
		return scheduler;
	}
	
	/**
	 * 注册任务执行器。
	 * @author Chrise 2019年5月21日
	 * @return 任务执行器对象。
	 */
	@Bean
	public ThreadPoolTaskExecutor registerTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(this.minThreadPoolSize);
		executor.setMaxPoolSize(this.maxThreadPoolSize);
		return executor;
	}
}
