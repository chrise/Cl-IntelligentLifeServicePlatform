/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IntegralRecordsType.java
 * History:
 *         2019年5月30日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 积分记录类型
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum IntegralRecordType {
	/**
	 * 收入
	 */
	INCOME(1, "收入"),
	
	/**
	 * 支出
	 */
	PAY(2, "支出");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
}
