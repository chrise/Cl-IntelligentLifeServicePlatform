/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MerchantRemind.java
 * History:
 *         2019年6月9日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商户提醒。
 * @author Chrise
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MerchantRemind {
	/**
	 * 商户标识。
	 */
	@Id
	private Long merchantid;
	/**
	 * 商户电话。
	 */
	@SecureField
	private String merchantphone;
	/**
	 * 短信通知。
	 */
	private Boolean smnotify;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
