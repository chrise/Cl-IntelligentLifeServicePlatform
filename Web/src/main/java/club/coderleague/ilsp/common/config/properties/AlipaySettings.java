/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AlipaySettings.java
 * History:
 *         2019年5月28日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 支付宝设置。
 * @author Chrise
 */
@Getter
@Setter
@Component
@PropertySource("${custom.alipay.configLocation}")
@ConfigurationProperties(prefix = "alipay", ignoreUnknownFields = false)
public class AlipaySettings {
	/**
	 * 域名。
	 */
	private String domain;
	/**
	 * 网关。
	 */
	private String gateway;
	/**
	 * 本地公钥。
	 */
	private String localPublicKey;
	/**
	 * 本地私钥。
	 */
	private String localPrivateKey;
	/**
	 * 网页授权代码请求。
	 */
	private String webAuthCodeRequest;
	/**
	 * 网页授权回调请求。
	 */
	private String webAuthCallbackRequest;
	/**
	 * 网页授权回调超时。
	 */
	private Long webAuthCallbackTimeout;
	/**
	 * 网页授权模拟器开关。
	 */
	private Boolean webAuthSimulatorEnabled;
	/**
	 * 通知地址。
	 */
	private String notifyUrl;
	/**
	 * 本地支付开关。
	 */
	private Boolean localPayEnabled;
}
