/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PayOrder.java
 * History:
 *         2019年6月10日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 支付订单。
 * @author Chrise
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PayOrder implements Serializable {
	private static final long serialVersionUID = -3838015039643024385L;
	
	/**
	 * 订单标识。
	 */
	private Long orderid;
	/**
	 * 订单类型。
	 */
	private Integer ordertype;
	/**
	 * 订单名称。
	 */
	private String ordername;
	/**
	 * 订单总额。
	 */
	private Double ordertotal;
	/**
	 * 订单状态。
	 */
	private Integer orderstate;
	/**
	 * 支付接口。
	 */
	private String paymentapi;
	/**
	 * 支付总额。
	 */
	private Double paymenttotal;
	/**
	 * 支付状态。
	 */
	private Integer paymentstate;
	/**
	 * 支付方式。
	 */
	private Integer paymentmode;
	/**
	 * 支付交易号。
	 */
	private String paymenttradeno;
	/**
	 * 支付创建时间。
	 */
	private Date tradecreatetime;
	/**
	 * 钻石数量。
	 */
	private Integer diamondnum;
	/**
	 * 支付钻石。
	 */
	private Integer diamondpay;
	/**
	 * 需要确认。
	 */
	private Boolean needconfirm;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
