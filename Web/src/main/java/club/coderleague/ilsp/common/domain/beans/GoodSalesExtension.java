/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSalesExtension.java
 * History:
 *         2019年6月11日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品销售管理扩展对象。
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class GoodSalesExtension implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	private Long mid;
	private Long settleid;
	private Long marketid;
	@SecureField
	private String merchantname;
	private String marketname;
	private Long validNum;
	private Long putawayNum;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
