/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderPaymentState.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单支付状态
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderPaymentState {
	/**
	 * 待付款
	 */
	UNPAID(1, "待付款"),
	
	/**
	 * 已付款
	 */
	PAID(2, "已付款"),
	
	/**
	 * 已退款
	 */
	REFUNDED(3, "已退款");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * 比较订单支付状态。
	 * @author Chrise 2019年6月10日
	 * @param state 订单支付状态对象。
	 * @return 订单支付状态相等时返回true，否则返回false。
	 */
	public boolean equals(OrderPaymentState state) {
		return (this.value.intValue() == state.value.intValue());
	}
	
	/**
	 * 比较订单支付状态值。
	 * @author Chrise 2019年6月10日
	 * @param value 订单支付状态值。
	 * @return 订单支付状态值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value.intValue() == value.intValue());
	}
}
