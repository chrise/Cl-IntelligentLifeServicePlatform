/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OnlineOrder.java
 * History:
 *         2019年7月21日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import club.coderleague.ilsp.common.domain.enums.OrderDeliveryMode;
import club.coderleague.ilsp.common.domain.enums.OrderDeliveryTime;
import club.coderleague.ilsp.common.domain.enums.OrderPaymentMode;
import lombok.Getter;
import lombok.Setter;

/**
 * 线上订单
 * 
 * @author CJH
 */
@Getter
@Setter
public class OnlineOrder implements Serializable {

	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 订单商户信息
	 */
	private List<OrderMerchant> ordermerchants;
	
	/**
	 * 收货地址标识
	 */
	private Long receivingaddressesid;
	
	/**
	 * 会员标识
	 */
	private Long memberid;
	
	/**
	 * 配送方式
	 */
	private OrderDeliveryMode deliverymode;
	
	/**
	 * 配送时间
	 */
	private OrderDeliveryTime deliverytime;
	
	/**
	 * 起送时间
	 */
	private Date deliverystarttime;
	
	/**
	 * 止送时间
	 */
	private Date deliveryendtime;
	
	/**
	 * 支付方式
	 */
	private OrderPaymentMode paymentmode;
	
	/**
	 * 积分支付
	 */
	private Integer integralpay;
	
	/**
	 * 钻石支付
	 */
	private Integer diamondpay;
	
	/**
	 * 立即购买
	 */
	private Boolean buynow;
	
	/**
	 * 订单商户信息
	 * 
	 * @author CJH
	 */
	@Getter
	@Setter
	public class OrderMerchant implements Serializable {
		
		/**
		 * 序列化版本号
		 */
		private static final long serialVersionUID = 1L;
	
		/**
		 * 商户标识
		 */
		private Long merchantid;
		
		/**
		 * 订单备注
		 */
		private String orderremark;
	
		/**
		 * 配送费用
		 */
		private Double deliverycost;
		
		/**
		 * 购买商品信息
		 */
		private List<OnlineGood> onlinegoods;
	}
	
	/**
	 * 线上购买商品
	 * 
	 * @author CJH
	 */
	@Getter
	@Setter
	public class OnlineGood implements Serializable {
		
		/**
		 * 序列化版本号
		 */
		private static final long serialVersionUID = 1L;

		/**
		 * 销售标识
		 */
		private Long saleid;
		
		/**
		 * 商品价格
		 */
		private Double goodprice;
		
		/**
		 * 商品数量
		 */
		private Double goodnumber;
	}
}
