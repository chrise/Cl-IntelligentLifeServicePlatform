/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OperateLog.java
 * History:
 *         2019年5月11日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import club.coderleague.ilsp.common.domain.enums.OperateLogRequestType;

/**
 * 操作日志注解
 * 
 * @author CJH
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface OperateLog {
	/**
	 * 日志描述
	 */
	String value() default "";
	
	/**
	 * 操作类型
	 */
	OperateLogRequestType type() default OperateLogRequestType.ACTIVE;
}
