/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SessionConfigurer.java
 * History:
 *         2019年5月20日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSessionListener;

import org.springframework.context.annotation.Bean;
import org.springframework.session.web.http.SessionEventHttpSessionListenerAdapter;

import club.coderleague.ilsp.common.session.SessionListener;
import club.coderleague.ilsp.common.session.SessionManager;
import club.coderleague.ilsp.interceptor.SessionInterceptor;

/**
 * 会话配置器。
 * @author Chrise
 */
public class SessionConfigurer {
	/**
	 * 注册会话监听器。
	 * @author Chrise 2019年5月20日
	 * @return 会话事件适配器对象。
	 */
	@Bean
	public SessionEventHttpSessionListenerAdapter registerSessionListener() {
		List<HttpSessionListener> listeners = new ArrayList<HttpSessionListener>();
		listeners.add(new SessionListener(registerSessionManager()));
		
		return new SessionEventHttpSessionListenerAdapter(listeners);
	}
	
	/**
	 * 注册会话管理器。
	 * @author Chrise 2019年5月20日
	 * @return 会话管理器对象。
	 */
	@Bean
	public SessionManager registerSessionManager() {
		return new SessionManager();
	}
	
	/**
	 * 注册会话拦截器。
	 * @author Chrise 2019年5月21日
	 * @return 会话拦截器对象。
	 */
	@Bean
	public SessionInterceptor registerSessionInterceptor() {
		return new SessionInterceptor();
	}
}
