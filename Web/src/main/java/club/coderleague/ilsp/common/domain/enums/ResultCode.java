/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ResultCode.java
 * History:
 *         2019年5月11日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 结果信息编码
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum ResultCode {
	/**
	 * 成功
	 */
	SUCCESS(1, "操作成功", true),
	
	/**
	 * 提示
	 */
	INFO(2, null, false),
	
	/**
	 * 警告
	 */
	WARN(3, null, false),
	
	/**
	 * 错误
	 */
	ERROR(4, "系统错误", false);
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * 状态
	 */
	private Boolean status; 
}
