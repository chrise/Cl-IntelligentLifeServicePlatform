/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SessionListener.java
 * History:
 *         2019年5月20日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.session;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * 会话监听器。
 * @author Chrise
 */
public class SessionListener implements HttpSessionListener {
	private SessionManager sessionManager;
	
	/**
	 * 会话监听器构造方法。
	 * @param sessionManager
	 */
	public SessionListener(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}
	
	/**
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		this.sessionManager.add(se.getSession().getId());
	}
	
	/**
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		this.sessionManager.remove(se.getSession().getId());
	}
}
