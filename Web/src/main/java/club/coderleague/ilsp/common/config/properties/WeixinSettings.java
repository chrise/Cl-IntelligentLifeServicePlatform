/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：WeixinSettings.java
 * History:
 *         2019年5月27日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 微信设置。
 * @author Chrise
 */
@Getter
@Setter
@Component
@PropertySource("${custom.weixin.configLocation}")
@ConfigurationProperties(prefix = "weixin", ignoreUnknownFields = false)
public class WeixinSettings {
	/**
	 * 域名。
	 */
	private String domain;
	/**
	 * 网页授权代码请求。
	 */
	private String webAuthCodeRequest;
	/**
	 * 网页授权令牌请求。
	 */
	private String webAuthTokenRequest;
	/**
	 * 网页授权回调请求。
	 */
	private String webAuthCallbackRequest;
	/**
	 * 网页授权回调超时。
	 */
	private Long webAuthCallbackTimeout;
	/**
	 * 网页授权模拟器开关。
	 */
	private Boolean webAuthSimulatorEnabled;
	/**
	 * 支付接口。
	 */
	private String payApi;
	/**
	 * 支付查询接口。
	 */
	private String payQueryApi;
	/**
	 * 退款接口。
	 */
	private String refundApi;
	/**
	 * 退款查询接口。
	 */
	private String refundQueryApi;
	/**
	 * 通知地址。
	 */
	private String notifyUrl;
	/**
	 * 退款通知地址。
	 */
	private String refundNotifyUrl;
	/**
	 * 预支付超时（秒）。
	 */
	private Long prepayTimeout;
	/**
	 * 本地支付开关。
	 */
	private Boolean localPayEnabled;
}
