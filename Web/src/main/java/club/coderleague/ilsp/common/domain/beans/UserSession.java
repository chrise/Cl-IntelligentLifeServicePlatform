/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UserSession.java
 * History:
 *         2019年5月11日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.util.List;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import club.coderleague.data.jpa.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 用户会话。
 * @author Chrise
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class UserSession implements User<Long> {
	private static final long serialVersionUID = -2713766000271350561L;
	
	@Id
	private Long userid;
	private Integer usergroup;
	private String password;
	private String orgname;
	@SecureField
	private String username;
	private Integer usertype;
	private Integer membertype;
	private String nickname;
	private String openid;
	private Boolean manager;
	private Boolean purchaser;
	private String loginname;
	private List<String> authorities;
	
	/**
	 * 获取授权标识。
	 * @author Chrise 2019年5月20日
	 * @return 授权标识集合。
	 */
	public synchronized List<String> getAuthorities() {
		return this.authorities;
	}
	
	/**
	 * 设置授权标识。
	 * @author Chrise 2019年5月20日
	 * @param authorities 授权标识集合。
	 */
	public synchronized void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}
	
	/**
	 * @see com.cl.data.jpa.domain.User#getId()
	 */
	@Override
	public Long getId() {
		return this.userid;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
