/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ShoppingCartMerchantExtension.java
 * History:
 *         2019年6月29日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * -购物车结算商户名称扩展对象。
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class ShoppingCartSettlementMerchantExtension implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
	 *商户id。
	 */
	@Id
	private Long entityid;
	/**
	 * 商户名称。
	 */
	@SecureField
	private String merchantname;
	/**
	 * 选中状态。
	 */
	private Boolean selectstate;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
