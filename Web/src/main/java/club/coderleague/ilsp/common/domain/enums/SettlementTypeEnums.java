/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SettlementTypeEnums.java
 * History:
 *         2019年7月26日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * -结算类型（1、自动结算，2、手动结算）枚举。
 * @author wangjb
 */
@Getter
@AllArgsConstructor
public enum SettlementTypeEnums {
	/**
	 * 自动结算。
	 */
	AUTOMATIC(1, "自动结算"),
	/**
	 * 手动结算。
	 */
	MANUAL(2, "手动结算");
	
	private int value;
	private String text;
}
