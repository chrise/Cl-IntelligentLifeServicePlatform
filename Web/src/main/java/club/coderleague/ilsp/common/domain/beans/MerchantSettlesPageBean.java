/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesPageBean.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans;


import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.KeyField;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商户入驻分页封装类
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MerchantSettlesPageBean {

	/**
	 * 商户入驻Id
	 */
	private Long entityid;
	/**
	 * 商户Id
	 */
	@KeyField
	private Long merchantid;
	/**
	 * 入驻摊位
	 */
	private String settlestall;
	/**
	 * 市场名称
	 */
	private String marketname;
	/**
	 * 商户名称
	 */
	@SecureField("merchantid")
	private String merchantname;
}
