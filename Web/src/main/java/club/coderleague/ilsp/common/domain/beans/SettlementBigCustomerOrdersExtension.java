/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SettlementBigCustomerOrdersExtension.java
 * History:
 *         2019年6月8日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import club.coderleague.ilsp.entities.Orders;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 结算大客户列表页访问扩展
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class SettlementBigCustomerOrdersExtension extends Orders{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 采购人id。
	 */
	@Id
	private Long userid;
	
	/**
	 * 采购人姓名。
	 */
	@SecureField
	private String username;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}

}
