/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：AttachBean.java
 * History:
 *         2019年6月6日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 附件封装类
 * @author Liangjing
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AttachBean {
	/**
	 * 附件实体id
	 */
	private String entityid;
	/**
	 * 附件名称
	 */
	private String attachname;
	/**
	 * 附件路径
	 */
	private String attachpath;
	
}
