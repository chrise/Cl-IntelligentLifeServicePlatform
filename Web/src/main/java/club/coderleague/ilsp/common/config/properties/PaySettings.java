/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PaySettings.java
 * History:
 *         2019年6月10日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/**
 * 支付设置。
 * @author Chrise
 */
@Getter
@Setter
@Component
@PropertySource(value = "${custom.pay.configLocation}", encoding = "UTF-8")
@ConfigurationProperties(prefix = "pay", ignoreUnknownFields = false)
public class PaySettings {
	/**
	 * 购物订单名称。
	 */
	private String shoppingName;
	/**
	 * 充值订单名称。
	 */
	private String rechargeName;
}
