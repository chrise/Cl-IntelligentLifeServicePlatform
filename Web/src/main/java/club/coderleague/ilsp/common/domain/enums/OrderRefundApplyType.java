/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderRefundApplyType.java
 * History:
 *         2019年8月8日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 退款申请类型
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderRefundApplyType implements BaseEnum {
	/**
	 * 商户申请
	 */
	MERCHANT(1, "商户申请"),
	
	/**
	 * 用户申请
	 */
	USER(2, "用户申请"),
	
	/**
	 * 后台申请
	 */
	BACKSTAGE(3, "后台申请"),
	
	/**
	 * 系统申请
	 */
	SYSTEM(4, "系统申请");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * @see club.coderleague.ilsp.common.domain.enums.BaseEnum#getPrimaryKey()
	 */
	@Override
	public String getPrimaryKey() {
		return String.valueOf(value);
	}
	
	/**
	 * 比较值是否相等
	 * 
	 * @author CJH 2019年8月8日
	 * @param value 比较值
	 * @return 是否相等
	 */
	public boolean equalsValue(Integer value) {
		return this.value.intValue() == value.intValue();
	}
}
