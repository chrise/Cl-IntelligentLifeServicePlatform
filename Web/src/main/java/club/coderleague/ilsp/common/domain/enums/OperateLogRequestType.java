/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OperateLogRequestType.java
 * History:
 *         2019年5月11日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 操作日志请求类型
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OperateLogRequestType {
	/**
	 * 主动
	 */
	ACTIVE(1, "主动"),
	
	/**
	 * 被动
	 */
	PASSIVE(2, "被动");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
}
