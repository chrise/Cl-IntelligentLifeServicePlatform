/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：TextReplyMessage.java
 * History:
 *         2019年5月30日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans.wxmp.reply;

import org.dom4j.Element;

import club.coderleague.ilsp.common.domain.beans.wxmp.AbstractMessage;
import lombok.Getter;
import lombok.Setter;

/**
 * 文本回复消息。
 * @author Chrise
 */
@Getter
@Setter
public class TextReplyMessage extends AbstractMessage {
	private static final long serialVersionUID = 9199886977562429906L;
	
	private static final String tagContent = "Content";
	
	private String content;
	
	/**
	 * 文本回复消息构造方法。
	 * @param toUserName 接收者。
	 * @param fromUserName 发送者。
	 * @param content 文本内容。
	 */
	public TextReplyMessage(String toUserName, String fromUserName, String content) {
		super(toUserName, fromUserName, TEXT);
		
		this.content = content;
	}
	
	/**
	 * @see club.coderleague.ilsp.common.domain.beans.wxmp.AbstractMessage#getMsgType()
	 */
	@Override
	public String getMsgType() {
		return TEXT;
	}
	
	/**
	 * @see club.coderleague.ilsp.common.domain.beans.wxmp.AbstractMessage#addToXml(org.dom4j.Element)
	 */
	@Override
	protected void addToXml(Element root) {
		super.addToXml(root);
		
		root.addElement(tagContent).addCDATA(this.content);
	}
}
