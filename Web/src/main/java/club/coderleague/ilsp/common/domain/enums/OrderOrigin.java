/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderOrigin.java
 * History:
 *         2019年6月6日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单来源
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderOrigin implements BaseEnum {
	/**
	 * 线下
	 */
	OFFLINE(1, "线下"),
	
	/**
	 * 线上
	 */
	ONLINE(2, "线上");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * @see club.coderleague.ilsp.common.domain.enums.BaseEnum#getPrimaryKey()
	 */
	@Override
	public String getPrimaryKey() {
		return String.valueOf(value);
	}
	
	/**
	 * 比较值是否相等
	 * 
	 * @author CJH 2019年6月14日
	 * @param value 比较值
	 * @return 是否相等
	 */
	public boolean equalsValue(Integer value) {
		return this.value.intValue() == value.intValue();
	}
}
