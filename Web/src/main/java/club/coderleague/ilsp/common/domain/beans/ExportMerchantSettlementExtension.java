/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ExportMerchantSettlementExtension.java
 * History:
 *         2019年7月26日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.ilsp.entities.Merchants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * -导出商户结算扩展对象。
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class ExportMerchantSettlementExtension extends Merchants{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 开户银行。
	 */
	private String bankname;
	
	/**
	 * 开户网点。
	 */
	private String branche;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}

}
