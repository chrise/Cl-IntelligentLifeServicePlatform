/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：NotifyType.java
 * History:
 *         2019年6月6日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通知类型。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum NotifyType {
	/**
	 * 手机绑定。
	 */
	PHONE_BIND(1, "手机绑定"),
	/**
	 * 身份认证。
	 */
	IDENTITY_AUTH(2, "身份认证"),
	/**
	 * 到账提醒。
	 */
	ARRACC_REMIND(3, "到账提醒"),
	/**
	 * 退款提醒
	 */
	REFUND_REMIND(4, "退款提醒"),
	/**
	 * 结算提醒。
	 */
	SETACC_REMIND(5, "结算提醒");
	
	private int value;
	private String text;
	
	/**
	 * 比较通知类型。
	 * @author Chrise 2019年6月6日
	 * @param type 通知类型对象。
	 * @return 通知类型相等时返回true，否则返回false。
	 */
	public boolean equals(NotifyType type) {
		return (this.value == type.value);
	}
	
	/**
	 * 比较通知类型值。
	 * @author Chrise 2019年6月6日
	 * @param value 通知类型值。
	 * @return 通知类型值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
