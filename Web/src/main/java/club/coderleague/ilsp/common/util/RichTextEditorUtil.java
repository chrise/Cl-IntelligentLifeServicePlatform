/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RichTextEditorService.java
 * History:
 *         2019年7月25日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import club.coderleague.ilsp.common.config.properties.FileUploadSettings;

/**
 * 富文本编辑器路劲替换处理。
 * @author wangjb
 */
@Component
public class RichTextEditorUtil {
	
	/**
	 * 图片路径替换字符串。
	 */
	public final static String REPLACE_IMG_PATH = "{{@ContextImgPaht}}";

	/**
	 * 上传对象。
	 */
	@Autowired FileUploadSettings upload;
	
	/**
	 * 【富文本编辑器】替换访问服务器图片地址。
	 * @author wangjb 2019年7月25日。
	 * @param gooddesc 商品描述。
	 * @return
	 */
	public String replaceVisitImgPathUtil(String gooddesc) {
		if (gooddesc != null && !"".equals(gooddesc)) gooddesc = gooddesc.replace(RichTextEditorUtil.REPLACE_IMG_PATH, this.upload.getVirtual());
		return gooddesc;
	}
	
	
	/**
	 * 【富文本编辑器】替换存入数据库图片地址。
	 * @author wangjb 2019年7月25日。
	 * @param gooddesc 商品描述。
	 * @return
	 */
	public String replaceDepositLibPathUtil(String gooddesc) {
		if (gooddesc != null && !"".equals(gooddesc)) gooddesc = gooddesc.replace(this.upload.getVirtual(), RichTextEditorUtil.REPLACE_IMG_PATH);
		return gooddesc;
	}
}
