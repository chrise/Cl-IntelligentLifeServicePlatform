/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RefundOrder.java
 * History:
 *         2019年8月1日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 退款订单。
 * @author Chrise
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RefundOrder implements Serializable {
	private static final long serialVersionUID = 3814027991207865572L;
	
	/**
	 * 订单标识。
	 */
	private Long orderid;
	/**
	 * 支付总额。
	 */
	private Double paymenttotal;
	/**
	 * 退款总额。
	 */
	private Double refundtotal;
	/**
	 * 退款交易号。
	 */
	private String refundtradeno;
	/**
	 * 订单状态。
	 */
	private Integer orderstate;
	/**
	 * 订单类型。
	 */
	private Integer ordertype;
	/**
	 * 支付状态。
	 */
	private Integer paymentstate;
	/**
	 * 支付方式。
	 */
	private Integer paymentmode;
	/**
	 * 退款截止时间。
	 */
	private Date refundendtime;
	/**
	 * 退款标识。
	 */
	private Long refundid;
	/**
	 * 部分退款。
	 */
	private Boolean partialrefund;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
