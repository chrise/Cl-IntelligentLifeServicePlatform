/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：FuncType.java
 * History:
 *         2019年5月12日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能类型枚举。
 * @author wangjb
 */
@Getter
@AllArgsConstructor
public enum FuncType {

	/**
	 * 分类。
	 */
	CLASSIFY(1,"分类"),
	
	/**
	 * 模块。
	 */
	MODULE(2,"模块"),
	
	/**
	 * 节点。
	 */
	NODE(3,"节点"),
	
	/**
	 * 按钮。
	 */
	BUTTON(4,"按钮");
	
	private Integer value;
	private String text;

}
