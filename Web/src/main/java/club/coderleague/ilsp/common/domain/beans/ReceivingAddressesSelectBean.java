/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：ReceivingAddressesSelectBean.java
 * History:
 *         2019年6月3日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans;

import javax.persistence.Id;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 会员查看收货地址查询页面封装类
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class ReceivingAddressesSelectBean {
	/**
	 * 商户id
	 */
	@Id
	private Long entityid;
	/**
	 * 会员标识
	 */
	private Long memberid;
	/**
	 * 收货人
	 */
	@SecureField
	private String receiver;
	/**
	 * 城市
	 */
	private String cityname;
	/**
	 * 区县
	 */
	private String countyname;
	/**
	 * 街道
	 */
	private String streetname;
	/**
	 * 详细地址
	 */
	@SecureField
	private String detailaddress;
	/**
	 * 手机号码
	 */
	@SecureField
	private String mobilephone;
}
