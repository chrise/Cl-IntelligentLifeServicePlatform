/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：CommonUtil.java
 * History:
 *         2019年5月21日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 上传文件文件封装类。
 * @author Liangjing
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UploadFile implements Serializable {
	private static final long serialVersionUID = 3889844856514338681L;
	
	/**
	 * 附件路径。
	 */
	private String savePath;
	
	/**
	 * 附件名称。
	 */
	private String localName;
}
