/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderPaymentMode.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单支付方式
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderPaymentMode implements BaseEnum {
	/**
	 * 微信
	 */
	WECHAT(1, "微信", "weixin"),
	
	/**
	 * 支付宝
	 */
	ALIPAY(2, "支付宝", "alipay"),
	
	/**
	 * 银联
	 */
	UNIONPAY(3, "银联", "unionpay");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * 关键字。
	 */
	private String key;

	/**
	 * @see club.coderleague.ilsp.common.domain.enums.BaseEnum#getPrimaryKey()
	 */
	@Override
	public String getPrimaryKey() {
		return String.valueOf(value);
	}
	
	/**
	 * 比较订单支付方式。
	 * @author Chrise 2019年6月24日
	 * @param mode 订单支付方式对象。
	 * @return 订单支付方式相等时返回true，否则返回false。
	 */
	public boolean equals(OrderPaymentMode mode) {
		return (this.value.intValue() == mode.value.intValue());
	}
	
	/**
	 * 比较订单支付方式值。
	 * @author Chrise 2019年6月24日
	 * @param value 订单支付方式值。
	 * @return 订单支付方式值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value.intValue() == value.intValue());
	}
}
