/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SubscribeEventMessage.java
 * History:
 *         2019年5月30日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans.wxmp.event;

import org.dom4j.Element;

import club.coderleague.ilsp.common.domain.beans.wxmp.AbstractEventMessage;
import lombok.Getter;
import lombok.Setter;

/**
 * 关注事件消息。
 * @author Chrise
 */
@Getter
@Setter
public class SubscribeEventMessage extends AbstractEventMessage {
	private static final long serialVersionUID = 7625298960606499290L;
	
	private static final String tagEventKey = "EventKey";
	private static final String tagTicket = "Ticket";
	
	private String eventKey;
	private String ticket;
	
	/**
	 * @see club.coderleague.ilsp.common.domain.beans.wxmp.AbstractMessage#getMsgType()
	 */
	@Override
	public String getMsgType() {
		return SUBSCRIBE;
	}
	
	/**
	 * @see club.coderleague.ilsp.common.domain.beans.wxmp.AbstractEventMessage#parseInternal(org.dom4j.Element)
	 */
	@Override
	protected void parseInternal(Element root) {
		super.parseInternal(root);
		
		Element eKey = root.element(tagEventKey);
		if (eKey != null) this.eventKey = eKey.getTextTrim();
		Element eTicket = root.element(tagTicket);
		if (eTicket != null) this.ticket = eTicket.getTextTrim();
	}
}
