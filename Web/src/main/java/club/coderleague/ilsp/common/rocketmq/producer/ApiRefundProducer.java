/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ApiRefundProducer.java
 * History:
 *         2019年8月5日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.rocketmq.producer;

import org.springframework.stereotype.Component;

/**
 * 接口退款消息生产者。
 * @author Chrise
 */
@Component
public class ApiRefundProducer extends BaseDefaultMQProducer {
	/**
	 * 生产者组。
	 */
	public final static String PRODUCER_GROUP = "API_REFUND_PRODUCER";
	
	/**
	 * 接口退款生产者构造方法。
	 */
	public ApiRefundProducer() {
		super(PRODUCER_GROUP);
	}
}
