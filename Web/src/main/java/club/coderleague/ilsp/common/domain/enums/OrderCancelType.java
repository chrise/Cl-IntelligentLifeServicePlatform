/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderCancelType.java
 * History:
 *         2019年6月24日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单取消类型
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderCancelType implements BaseEnum {
	/**
	 * 商户取消
	 */
	MERCHANT(1, "商户取消"),
	
	/**
	 * 用户取消
	 */
	USER(2, "用户取消"),
	
	/**
	 * 后台取消
	 */
	BACKSTAGE(3, "后台取消"),
	
	/**
	 * 系统取消
	 */
	SYSTEM(4, "系统取消");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;

	/**
	 * @see club.coderleague.ilsp.common.domain.enums.BaseEnum#getPrimaryKey()
	 */
	@Override
	public String getPrimaryKey() {
		return String.valueOf(this.value);
	}
}
