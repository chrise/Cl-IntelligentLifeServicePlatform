/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodExtension.java
 * History:
 *         2019年6月6日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品扩展对象。
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class GoodsExtension implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long mid;
	private Long entityid;
	private Long goodsid;
	private String goodname;
	private String goodorigin;
	private String gooddesc;
	@SecureField
	private String merchantname;
	private String groupname;
	private Integer entitystate;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
