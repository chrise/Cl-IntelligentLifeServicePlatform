/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MessageType.java
 * History:
 *         2019年5月30日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans.wxmp;

/**
 * 消息类型。
 * @author Chrise
 */
public interface MessageType {
	/**
	 * 普通文本消息。
	 */
	String TEXT = "text";
	/**
	 * 事件推送消息。
	 */
	String EVENT = "event";
	/**
	 * 关注事件消息。
	 */
	String SUBSCRIBE = "subscribe";
}
