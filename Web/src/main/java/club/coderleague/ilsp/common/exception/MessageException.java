/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MessageException.java
 * History:
 *         2019年6月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.exception;

import club.coderleague.ilsp.common.domain.enums.ResultCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 信息异常
 * 
 * @author CJH
 */
@Getter
@Setter
public class MessageException extends RuntimeException {
	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 结果编码
	 */
	private ResultCode resultcode;
	
	/**
	 * 异常信息描述
	 * 
	 * @param message 异常描述
	 */
	public MessageException(String message) {
        super(message);
    }
	
	/**
	 * 异常信息描述
	 * 
	 * @param resultcode 结果编码
	 * @param message 异常描述
	 */
	public MessageException(ResultCode resultcode, String message) {
		super(message);
		this.resultcode = resultcode;
    }
}
