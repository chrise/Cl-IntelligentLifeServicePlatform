/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderDeliveryMode.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单配送方式
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderDeliveryMode {
	/**
	 * 自提
	 */
	NO_DELIVERY(1, "自提"),
	
	/**
	 * 市场配送
	 */
	MARKET_DELIVERY(2, "市场配送");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
}
