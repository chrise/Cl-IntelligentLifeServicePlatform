/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BaseEnum.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

/**
 * 枚举基础接口
 * 
 * @author CJH
 */
public interface BaseEnum {
	/**
	 * 获取主键
	 * 
	 * @author CJH 2019年6月10日
	 * @return 主键
	 */
	public String getPrimaryKey();
}
