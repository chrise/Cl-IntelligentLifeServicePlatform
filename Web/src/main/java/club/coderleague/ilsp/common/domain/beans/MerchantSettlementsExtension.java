/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MerchantSettlementsExtension.java
 * History:
 *         2019年6月15日: Initially created, wangjb.
 */
package club.coderleague.ilsp.common.domain.beans;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.KeyField;
import club.coderleague.data.jpa.annotation.SecureField;
import club.coderleague.ilsp.entities.Merchantsettlements;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商户结算扩展对象。
 * @author wangjb
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MerchantSettlementsExtension extends Merchantsettlements{

	private static final long serialVersionUID = 1L;
	
	@KeyField
	private Long mid;
	@SecureField("mid")
	private String merchantname;
	@SecureField("mid")
	private String merchantphone;
	@SecureField("mid")
	private String idnumber;
	@SecureField("mid")
	private String accountname;
	@SecureField("mid")
	private String bcnumber;
	@KeyField
	private Long userid;
	@SecureField("userid")
	private String username;
	/**
	 * 开户银行。
	 */
	private String bankname;
	
	/**
	 * 开户网点。
	 */
	private String bankcard;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
