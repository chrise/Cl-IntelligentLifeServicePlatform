/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondChangedReason.java
 * History:
 *         2019年5月28日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 钻石变化原因
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum DiamondChangedReason {
	/**
	 * 过期失效
	 */
	EXPIRE(1, "过期失效", DiamondRecordType.PAY),
	
	/**
	 * 订单收入
	 */
	BUY(2, "订单收入", DiamondRecordType.INCOME),
	
	/**
	 * 订单支出
	 */
	PAY(3, "订单支出", DiamondRecordType.PAY);
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * 类型
	 */
	private DiamondRecordType type;
}
