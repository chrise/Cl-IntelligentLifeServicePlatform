/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SystemConfig.java
 * History:
 *         2019年5月23日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import javax.persistence.Id;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import club.coderleague.ilsp.util.MessageQueueUtil;
import club.coderleague.ilsp.util.MessageQueueUtil.TimeUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 系统配置。
 * @author Chrise
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class SystemConfig implements Serializable {
	private static final long serialVersionUID = -1852112104488541405L;
	
	/**
	 * 缓存键。
	 */
	public static final String CACHE_KEY = "systemconfig_cache";
	
	@Id
	private Long secretkey;
	private Integer confirmtimeout;
	private Integer paytimeout;
	private Integer receivetimeout;
	private Integer refundtimeout;
	private Integer vercodetimeout;
	private Boolean wxswitch;
	@SecureField
	private String wxappid;
	@SecureField
	private String wxappkey;
	@SecureField
	private String wxmerid;
	@SecureField
	private String wxapikey;
	@SecureField
	private String wxcerfile;
	@SecureField
	private String wxvertoken;
	private String wxwelcome;
	private Boolean apswitch;
	@SecureField
	private String apappid;
	@SecureField
	private String appubkey;
	@SecureField
	private String apmerprikey;
	private Boolean aynotify;
	@SecureField
	private String ayappid;
	@SecureField
	private String ayappkey;
	private String aysign;
	private Integer diadeductratio;
	
	/**
	 * 获取确认超时级别。
	 * @author Chrise 2019年9月12日
	 * @return 确认超时级别。
	 */
	public int getConfirmtimeoutLevel() {
		return MessageQueueUtil.getLevel(this.confirmtimeout, TimeUnit.MINUTE);
	}
	
	/**
	 * 获取支付超时级别。
	 * @author Chrise 2019年9月12日
	 * @return 支付超时级别。
	 */
	public int getPaytimeoutLevel() {
		return MessageQueueUtil.getLevel(this.paytimeout, TimeUnit.MINUTE);
	}
	
	/**
	 * 获取收货超时级别。
	 * @author Chrise 2019年9月12日
	 * @return 收货超时级别。
	 */
	public int getReceivetimeoutLevel() {
		return MessageQueueUtil.getLevel(this.receivetimeout, TimeUnit.DAY);
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
