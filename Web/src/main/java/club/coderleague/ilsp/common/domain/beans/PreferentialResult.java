/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：PreferentialResult.java
 * History:
 *         2019年7月11日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import lombok.Getter;

/**
 * 优惠结果。
 * @author Chrise
 */
@Getter
public class PreferentialResult<A> implements Serializable {
	private static final long serialVersionUID = -3100916301240073379L;
	
	private A available;
	private A total;
	private Map<Long, A> subitems;
	
	/**
	 * 优惠结果构造方法。
	 * @param available 可用优惠。
	 * @param total 总体优惠。
	 */
	public PreferentialResult(A available, A total) {
		this.available = available;
		this.total = total;
		this.subitems = new HashMap<Long, A>();
	}
	
	/**
	 * 添加分项优惠。
	 * @author Chrise 2019年7月11日
	 * @param good 商品标识。
	 * @param amount 优惠数额。
	 */
	public void addSubitemPreferential(Long good, A amount) {
		this.subitems.put(good, amount);
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
