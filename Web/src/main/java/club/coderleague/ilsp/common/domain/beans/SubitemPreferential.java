/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SubitemPreferential.java
 * History:
 *         2019年7月11日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.beans;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 分项优惠。
 * @author Chrise
 */
@Getter
@Setter
@AllArgsConstructor
public class SubitemPreferential<A> implements Serializable {
	private static final long serialVersionUID = -5408371831148959267L;
	
	private Long good;
	private Double ratio;
	private Double total;
	private A amount;
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}
}
