/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderDeliveryState.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 订单配送状态
 * 
 * @author CJH
 */
@Getter
@AllArgsConstructor
public enum OrderDeliveryState {
	/**
	 * 待发货
	 */
	UNSHIPPED(1, "待发货"),
	
	/**
	 * 已发货
	 */
	SHIPPED(2, "已发货"),
	
	/**
	 * 已收货
	 */
	RECEIVED(3, "已收货"),
	
	/**
	 * 已退货
	 */
	REFUNDED(4, "已退货");
	
	/**
	 * 主键
	 */
	private Integer value;
	
	/**
	 * 描述
	 */
	private String text;
	
	/**
	 * -比较实体状态。
	 * @author wangjb 2019年9月5日。
	 * @param state
	 * @return
	 */
	public boolean equals(OrderDeliveryState state) {
		return (this.value == state.value);
	}
	
	/**
	 * -比较实体状态值。
	 * @author wangjb 2019年9月5日。
	 * @param value
	 * @return
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
