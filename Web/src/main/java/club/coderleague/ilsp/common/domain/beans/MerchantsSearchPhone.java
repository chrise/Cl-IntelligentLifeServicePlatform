/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantsPageBean.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.common.domain.beans;

import javax.persistence.Id;

import club.coderleague.data.jpa.annotation.CustomBean;
import club.coderleague.data.jpa.annotation.EnableHibernateEncryption;
import club.coderleague.data.jpa.annotation.SecureField;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 展开查询商户电话
 * @author Liangjing
 */
@Getter
@Setter
@CustomBean
@NoArgsConstructor
@AllArgsConstructor
@EnableHibernateEncryption
public class MerchantsSearchPhone {
	/**
	 * 商户id
	 */
	@Id
	private Long entityid;
	/**
	 * 商户电话
	 */
	@SecureField
	private String merchantphone;
	/**
	 * 商户电话哈希
	 */
	private String phonehash;
}
