/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：WebAuthor.java
 * History:
 *         2019年5月31日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 网页授权人。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum WebAuthor {
	/**
	 * 微信。
	 */
	WEIXIN("weixin"),
	/**
	 * 支付宝。
	 */
	ALIPAY("alipay"),
	/**
	 * 银联。
	 */
	UNIONPAY("unionpay");
	
	private String name;
}
