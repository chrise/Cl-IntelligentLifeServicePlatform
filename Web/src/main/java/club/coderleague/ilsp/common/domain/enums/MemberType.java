/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MemberType.java
 * History:
 *         2019年6月2日: Initially created, Chrise.
 */
package club.coderleague.ilsp.common.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 会员类型。
 * @author Chrise
 */
@Getter
@AllArgsConstructor
public enum MemberType {
	/**
	 * 普通会员。
	 */
	NORMALS(1, "普通会员"),
	/**
	 * 超级会员。
	 */
	SUPERS(2, "超级会员");
	
	private int value;
	private String text;
	
	/**
	 * 比较会员类型。
	 * @author Chrise 2019年6月2日
	 * @param type 会员类型对象。
	 * @return 会员类型相等时返回true，否则返回false。
	 */
	public boolean equals(MemberType type) {
		return (this.value == type.value);
	}
	
	/**
	 * 比较会员类型值。
	 * @author Chrise 2019年6月2日
	 * @param value 会员类型值。
	 * @return 会员类型值相等时返回true，否则返回false。
	 */
	public boolean equalsValue(Integer value) {
		return (value != null && this.value == value);
	}
}
