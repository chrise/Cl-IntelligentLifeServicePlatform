/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderCompleteConsumer.java
 * History:
 *         2019年9月12日: Initially created, CJH.
 */
package club.coderleague.ilsp.common.rocketmq.consumer;

import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;

import club.coderleague.ilsp.common.exception.MessageInfoException;
import club.coderleague.ilsp.service.orders.OrdersService;

/**
 * 订单完成消费者
 * 
 * @author CJH
 */
public class OrderCompleteConsumer extends BaseDefaultMQPushConsumer {
	
	/**
	 * 消费者组
	 */
	public final static String CONSUMER_GROUP = "ORDER_COMPLETE_CONSUMER";
	
	/**
	 * 订阅主题
	 */
	public final static String TOPIC = "ORDER_COMPLETE_TOPIC";
	
	private @Autowired OrdersService ordersService;

	/**
	 * 构造方法。
	 */
	public OrderCompleteConsumer() {
		super(CONSUMER_GROUP, TOPIC);
	}

	/**
	 * @see club.coderleague.ilsp.common.rocketmq.consumer.BaseDefaultMQPushConsumer#messageHandle(org.apache.rocketmq.common.message.MessageExt, org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext)
	 */
	@Override
	protected void messageHandle(MessageExt messageExt, ConsumeConcurrentlyContext consumeConcurrentlyContext)
			throws Exception {
		String orderidstr = new String(messageExt.getBody());
		Long orderid = Long.valueOf(orderidstr);
		try {
			ordersService.updateCompleteOrder(orderid);
		} catch (MessageInfoException e) {
		}
	}

}
