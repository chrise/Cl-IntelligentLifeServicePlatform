/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：CacheManager.java
 * History:
 *         2019年5月23日: Initially created, Chrise.
 */
package club.coderleague.ilsp.cache;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import club.coderleague.ilsp.common.domain.beans.SystemConfig;
import club.coderleague.ilsp.service.systemconfig.SystemConfigService;

/**
 * 缓存管理器。
 * @author Chrise
 */
@Component
public class CacheManager {
	private static final String CACHE_ATTR_PREFIX = "cacheAttr:";
	private static final String CACHE_PERMANENT_KEY = "system:permanent:caches";
	private static final String CACHE_TEMPORARY_PREFIX = "system:temporary:caches:";
	
	@Autowired
	private SystemConfigService configService;
	@Resource
	private RedisTemplate<String, Object> redisTemplate;
	
	/**
	 * 初始化。
	 * @author Chrise 2019年5月25日
	 */
	public void initialize() {
		// 获取系统配置
		Object systemConfig = this.configService.queryCacheBean();
		if (systemConfig == null) return;
		
		// 检查缓存
		if (exits(SystemConfig.CACHE_KEY, false)) {
			Object cache = getObject(SystemConfig.CACHE_KEY, false);
			if (systemConfig.equals(cache)) return;
		}
		
		// 更新系统配置缓存
		setObject(SystemConfig.CACHE_KEY, systemConfig);
	}
	
	/**
	 * 检查缓存是否存在。
	 * @author Chrise 2019年5月25日
	 * @param key 缓存键。
	 * @param temporary 检查临时缓存。
	 * @return 缓存存在时返回true，否则返回false。
	 */
	public boolean exits(String key, boolean temporary) {
		if (temporary) return (this.redisTemplate.opsForValue().size(CACHE_TEMPORARY_PREFIX + key) > 0L);
		return this.redisTemplate.opsForHash().hasKey(CACHE_PERMANENT_KEY, CACHE_ATTR_PREFIX + key);
	}
	
	/**
	 * 获取缓存对象。
	 * @author Chrise 2019年5月25日
	 * @param key 缓存键。
	 * @param temporary 检查临时缓存。
	 * @return 缓存对象，缓存不存在时返回null。
	 */
	public Object getObject(String key, boolean temporary) {
		if (temporary) return this.redisTemplate.opsForValue().get(CACHE_TEMPORARY_PREFIX + key);
		return this.redisTemplate.opsForHash().get(CACHE_PERMANENT_KEY, CACHE_ATTR_PREFIX + key);
	}
	
	/**
	 * 设置缓存对象。
	 * @author Chrise 2019年5月25日
	 * @param key 缓存键。
	 * @param value 缓存对象，对象为null时将删除缓存。
	 */
	public void setObject(String key, Object value) {
		BoundHashOperations<String, Object, Object> bho = this.redisTemplate.boundHashOps(CACHE_PERMANENT_KEY);
		
		key = CACHE_ATTR_PREFIX + key;
		if (value == null) bho.delete(key);
		else bho.put(key, value);
	}
	
	/**
	 * 设置缓存对象。
	 * @author Chrise 2019年6月2日
	 * @param key 缓存键。
	 * @param value 缓存对象。
	 * @param timeout 缓存失效时间，为0时删除缓存对象。
	 */
	public void setObject(String key, Object value, long timeout) {
		setObject(key, value, timeout, null);
	}
	
	/**
	 * 设置缓存对象。
	 * @author Chrise 2019年6月1日
	 * @param key 缓存键。
	 * @param value 缓存对象。
	 * @param timeout 缓存失效时间，为0时删除缓存对象。
	 * @param unit 时间单位。
	 */
	public void setObject(String key, Object value, long timeout, TimeUnit unit) {
		if (unit == null) unit = TimeUnit.MILLISECONDS;
		
		BoundValueOperations<String, Object> bvo = this.redisTemplate.boundValueOps(CACHE_TEMPORARY_PREFIX + key);
		bvo.set(value);
		bvo.expire(timeout, unit);
	}
}
