/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IntegralRecordRefsDaoImpl.java
 * History:
 *         2019年8月25日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.dao.extension.IntegralRecordRefsDaoExtension;
import club.coderleague.ilsp.entities.Integralrecordrefs;

/**
 * 积分记录关系DaoImpl
 * 
 * @author CJH
 */
public class IntegralRecordRefsDaoImpl extends AbstractDataRepositoryExtension<Integralrecordrefs, Long, SnowflakeV4> implements IntegralRecordRefsDaoExtension {

}
