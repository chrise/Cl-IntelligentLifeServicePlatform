/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomersDaoImpl.java
 * History:
 *         2019年5月25日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.SettlementBigCustomerOrdersExtension;
import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.UserGroup;
import club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension;
import club.coderleague.ilsp.entities.Bigcustomers;

/**
 * 大客户管理数据访问扩展对象实现。
 * @author wangjb
 */
public class BigCustomersMgrDaoImpl extends AbstractDataRepositoryExtension<Bigcustomers, Long, SnowflakeV4> implements BigCustomersMgrDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension#getBigCustomersMgrDao(java.util.Map)
	 */
	@Override
	public Page<Bigcustomers> getBigCustomersMgrDao(Map<String, Object> params) {
		Map<String, Object> param = new HashMap<String, Object>();
		String qsql = "from Bigcustomers where entitystate in (:entitystate) ";
		String csql = "select count(*) from Bigcustomers where  entitystate in (:entitystate) ";
		boolean isrecycle = Boolean.valueOf((String) params.get("isrecycle"));
		if (!isrecycle) param.put("entitystate", Arrays.asList(new Integer[]{EntityState.VALID.getValue(), EntityState.FROZEN.getValue(), 
				EntityState.STAYAUDIT.getValue(), EntityState.NOTPASS.getValue()}));
		else param.put("entitystate", EntityState.INVALID.getValue());
		if (params.get("keyword") != null && StringUtils.isNotBlank(params.get("keyword").toString())) {
			List<Long> ids = new ArrayList<Long>();
			for (String id : (params.get("keyword").toString()).split(",")) {
				ids.add(Long.valueOf(id));
			}
			qsql += "and entityid in (:keyword) ";
			csql += "and entityid in (:keyword) ";
			param.put("keyword", ids);
		}
		qsql += "order by modifytime desc";
		return super.queryEntityPage(csql, qsql, Integer.parseInt(params.get("pageIndex").toString()), Integer.parseInt(params.get("pageSize").toString()), param);
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension#queryUserSession(java.lang.String)
	 */
	@Override
	public UserSession queryUserSession(String openId) {
		String sql = "select bcp.entityid as userid, bcp.personname as username, bcp.managerflag as manager, bcp.purchaserflag as purchaser " 
			+ "from bigcustomers bc, bigcustomerpersons bcp where bc.entityid = bcp.customerid and bc.entitystate = ?0 and bcp.entitystate = ?1 and bcp.personweixin = ?2";
		
		UserSession us = super.queryCustomBeanBySql(UserSession.class, sql, EntityState.VALID.getValue(), EntityState.VALID.getValue(), openId);
		if (us != null) us.setUsergroup(UserGroup.CUSTOMER.getValue());
		
		return us;
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension#updateLoginTime(long)
	 */
	@Override
	public void updateLoginTime(long person) {
		String sql = "update bigcustomerpersons set lastlogin = ?0 where entityid = ?1";
		super.executeUpdateBySql(sql, new Date(), person);
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension#getRepeatByEntityidAndCustomerNameDao(java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean getRepeatByEntityidAndCustomerNameDao(String customerhash, Long entityid) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select count(entityid) from Bigcustomers where customerhash = :customerhash and entitystate in (:entitystate) ";
		param.put("customerhash", customerhash);
		param.put("entitystate", Arrays.asList(new Integer[]{EntityState.VALID.getValue(), EntityState.FROZEN.getValue(), 
				EntityState.STAYAUDIT.getValue(), EntityState.NOTPASS.getValue()}));
		if (entityid != null) {
			sql += "and entityid != :entityid ";
			param.put("entityid", entityid);
		}
		Long count = super.queryLongBySql(sql, param);
		return (count > 0);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension#getBigCustomerOrdersMgrDao(Map)
	 */
	@Override
	public Page<SettlementBigCustomerOrdersExtension> getBigCustomerOrdersMgrDao(Map<String, Object> params) {
		Map<String, Object> param = new HashMap<String, Object>();
		String qsql = "select o.entityid as entityid, o.ordertotal as ordertotal, o.goodtotal as goodtotal, o.deliverycost as deliverycost, o.paymenttotal as paymenttotal, o.ordertime as ordertime, o.confirmtime as confirmtime, o.sendtime as sendtime, o.receivetime as receivetime, o.completetime as completetime, o.customerclose as customerclose, o.customerclosetime as customerclosetime," + 
				"bcp.entityid as userid, bcp.personname as username from Orders o, Bigcustomerpersons bcp where o.purchaserid = bcp.entityid and o.entitystate = :entitystate and bcp.entitystate = :istate and bcp.customerid = :customerid and o.customerclose = 0 ";
		String csql = "select count(o.entityid) from Orders o, Bigcustomerpersons bcp where o.purchaserid = bcp.entityid and o.entitystate = :entitystate and bcp.entitystate = :istate and bcp.customerid = :customerid and o.customerclose = 0 ";
		param.put("entitystate", EntityState.OTS.getValue());
		param.put("istate", EntityState.VALID.getValue());
		param.put("customerid", Long.valueOf(params.get("entityid").toString()));
		
		if (params.get("keyword") != null && StringUtils.isNotBlank(params.get("keyword").toString())) {
			List<Long> ids = new ArrayList<Long>();
			for (String id : (params.get("keyword").toString()).split(",")) {
				ids.add(Long.valueOf(id));
			}
			qsql += "and bcp.customerid in (:keyword) ";
			csql += "and bcp.customerid in (:keyword) ";
			param.put("keyword", ids);
		}
		return super.queryCustomBeanPageBySql(SettlementBigCustomerOrdersExtension.class, csql, qsql, Integer.parseInt(params.get("pageIndex").toString()), Integer.parseInt(params.get("pageSize").toString()), param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension#getMerchantListByOrderIdDao(java.util.List)
	 */
	@Override
	public List<Map<String, Object>> getMerchantListByOrderIdDao(List<Long> orderIdList) {
		String sql = "select merchantid, sum(goodtotal) as goodtotal from Orders entityid in (?0) group by merchantid ";
		return super.queryMapListBySql(sql, orderIdList);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension#getBigCustomerListDao()
	 */
	@Override
	public List<Bigcustomers> getBigCustomerListDao() {
		String sql = "from Bigcustomers where entitystate = ?0";
		return super.queryEntityList(sql, EntityState.VALID.getValue());
	}
}
