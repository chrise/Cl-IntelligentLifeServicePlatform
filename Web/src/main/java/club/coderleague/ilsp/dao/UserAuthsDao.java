/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UserAuthsDao.java
 * History:
 *         2019年5月19日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.UserAuthsDaoExtension;
import club.coderleague.ilsp.entities.Userauths;

/**
 * 用户授权Dao
 * 
 * @author CJH
 */
public interface UserAuthsDao extends DataRepository<Userauths, Long>, UserAuthsDaoExtension {

	/**
	 * 根据用户主键和角色主键查询用户授权
	 * 
	 * @author CJH 2019年5月20日
	 * @param userid 用户主键
	 * @param roleid 角色主键
	 * @return 用户授权
	 */
	public Userauths findOneByUseridAndRoleid(Long userid, Long roleid);

}
