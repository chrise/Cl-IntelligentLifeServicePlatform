/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：GoodGroupsDao.java
 * History:
 *         2019年5月12日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.GoodGroupsDaoExtension;
import club.coderleague.ilsp.entities.Goodgroups;

/**
 * 商品分类
 * @author Liangjing
 */
public interface GoodGroupsDao extends DataRepository<Goodgroups, String>, GoodGroupsDaoExtension {
	
	/**
	 * 通过名称和父级id得到对应商品分类
	 * @author Liangjing 2019年5月12日
	 * @param name
	 * @param parentid
	 * @return
	 */
	public Goodgroups findByGroupnameAndParentgroupAndEntitystate(String name,Long parentid,Integer entitystate);
	/**
	 * 通过id获取商品分类
	 * @author Liangjing 2019年5月17日
	 * @param id
	 * @return
	 */
	public Goodgroups findByEntityid(Long id);
}
