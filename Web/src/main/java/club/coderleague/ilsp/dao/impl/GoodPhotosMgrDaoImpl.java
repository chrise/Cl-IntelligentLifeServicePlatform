/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodPhotosMgrDaoImpl.java
 * History:
 *         2019年5月28日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.GoodPhotosMgrDaoExtension;
import club.coderleague.ilsp.entities.Goodphotos;

/**
 * 商品相册管理数据访问扩展对象实现。
 * @author wangjb
 */
public class GoodPhotosMgrDaoImpl extends AbstractDataRepositoryExtension<Goodphotos, Long, SnowflakeV4> implements GoodPhotosMgrDaoExtension{

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodPhotosMgrDaoExtension#getGoodPhotoListByGoodidDao(Long, boolean)
	 */
	@Override
	public List<Goodphotos> getGoodPhotoListByGoodidDao(Long goodid, boolean flag) {
		Map<String, Object> param = new  HashMap<String, Object>();
		String jpql = "from Goodphotos where goodid = :goodid and entitystate = :entitystate ";
		param.put("goodid", goodid);
		param.put("entitystate", EntityState.VALID.getValue());
		if (!flag) {
			jpql += " and coverphoto = :coverphoto ";
			param.put("coverphoto", true);
		}
		return super.queryEntityList(jpql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodPhotosMgrDaoExtension#getCoverphotoByGoodid(java.lang.Long)
	 */
	@Override
	public Goodphotos getCoverphotoByGoodid(Long goodid) {
		String jpql = "from Goodphotos gp where gp.goodid = ?0 and gp.entitystate = ?1 and gp.coverphoto = ?2";
		return super.queryEntity(jpql, goodid, EntityState.VALID.getValue(), true);
	}

}
