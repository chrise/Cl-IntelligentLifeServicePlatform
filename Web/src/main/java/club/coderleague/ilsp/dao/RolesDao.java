/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RolesDao.java
 * History:
 *         2019年5月18日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.RolesDaoExtension;
import club.coderleague.ilsp.entities.Roles;

/**
 * 角色Dao
 * 
 * @author CJH
 */
public interface RolesDao extends DataRepository<Roles, Long>, RolesDaoExtension {

}
