/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：FunctionsDaoExtension.java
 * History:
 *         2019年5月10日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.ilsp.entities.Functions;

/**
 * 功能管理数据访问扩展对象。
 * @author wangjb
 */
public interface FunctionsMgrDaoExtension {
	
	/**
	 * 获取功能管理数据。
	 * @author wangjb 2019年5月12日。
	 * @param params 关键字查询。
	 * @return
	 */
	List<Map<String, Object>> getFuncMgrListDao(Map<String, Object> params);
	
	/**
	 * 查询重复。
	 * @author wangjb 2019年5月12日。
	 * @param funcname 功能名称。
	 * @param parentfunc 父级功能。
	 * @param functype 功能类型。
	 * @param entityid 功能id。
	 * @return
	 */
	Boolean getRepeatDataByFuncNameOrParentFuncOrFuncTypeDao(String funcname, Long parentfunc, Integer functype,
			Long entityid);
	
	/**
	 * 查询父级功能。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	List<Map<String, Object>> getParentFuncSelectListDao();
	
	/**
	 * 查询功能详情。
	 * @author wangjb 2019年5月12日。
	 * @param entityid
	 * @return
	 */
	Map<String, Object> getFunctionsDetailDataByFuncidDao(String entityid);

	/**
	 * 根据父级功能主键和功能类型查询已授权功能
	 * 
	 * @author CJH 2019年5月15日
	 * @param parentfunc 父级功能主键
	 * @param functype 功能类型
	 * @return 功能
	 */
	public List<Functions> findAuthorizationByParentfuncAndFunctype(Long parentfunc, List<Integer> functype);
}
