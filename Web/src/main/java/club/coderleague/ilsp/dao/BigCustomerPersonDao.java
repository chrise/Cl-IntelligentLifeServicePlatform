/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomerPersonDao.java
 * History:
 *         2019年6月3日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.BigCustomerPersonDaoExtension;
import club.coderleague.ilsp.entities.Bigcustomerpersons;

/**
 * 大客户人员数据访问对象。
 * @author Chrise
 */
public interface BigCustomerPersonDao extends DataRepository<Bigcustomerpersons, Long>, BigCustomerPersonDaoExtension {

}
