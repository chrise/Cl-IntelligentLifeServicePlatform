/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomerPersonDaoImpl.java
 * History:
 *         2019年6月3日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.BigCustomerPersonDaoExtension;
import club.coderleague.ilsp.entities.Bigcustomerpersons;

/**
 * 大客户人员数据访问对象实现。
 * @author Chrise
 */
public class BigCustomerPersonDaoImpl extends AbstractDataRepositoryExtension<Bigcustomerpersons, Long, SnowflakeV4> implements BigCustomerPersonDaoExtension {
	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomerPersonDaoExtension#queryBoundInfo(java.lang.String)
	 */
	@Override
	public Map<String, Object> queryBoundInfo(String openId) {
		String jpql = "select bc.entitystate as boundstate, bcp.entityid as boundid, bcp.phonehash as boundphone from Bigcustomers bc, Bigcustomerpersons bcp " 
			+ "where bc.entityid = bcp.customerid and (bc.entitystate = ?0 or bc.entitystate = ?1 or bc.entitystate = ?2 or bc.entitystate = ?3) " 
			+ "and bcp.entitystate = ?4 and bcp.personweixin = ?5";
		return super.queryMap(jpql, EntityState.VALID.getValue(), EntityState.INVALID.getValue(), EntityState.FROZEN.getValue(), EntityState.DELETED.getValue(), 
			EntityState.VALID.getValue(), openId);
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomerPersonDaoExtension#queryExistsPersion(java.lang.String)
	 */
	@Override
	public Bigcustomerpersons queryExistsPersion(String phoneHash) {
		String jpql = "select bcp from Bigcustomers bc, Bigcustomerpersons bcp " 
			+ "where bc.entityid = bcp.customerid and bc.entitystate = ?0 and bcp.entitystate = ?1 and bcp.phonehash = ?2";
		return super.queryEntity(jpql, EntityState.VALID.getValue(), EntityState.VALID.getValue(), phoneHash);
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomerPersonDaoExtension#clearBound(java.lang.String)
	 */
	@Override
	public void clearBound(String boundId) {
		String sql = "update bigcustomerpersons set personweixin = null where entityid = ?0";
		super.executeUpdateBySql(sql, boundId);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomerPersonDaoExtension#getPersonByBgcidDao(java.lang.Long)
	 */
	@Override
	public List<Bigcustomerpersons> getPersonByBgcidDao(Long customerid) {
		String sql = "from Bigcustomerpersons where customerid = ?0 and entitystate = ?1 ";
		return super.queryEntityList(sql, customerid, EntityState.VALID.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.BigCustomerPersonDaoExtension#getBigCustomerPersonsByCustomerIdAndPhoneHashDao(java.lang.Long, java.lang.String)
	 */
	@Override
	public Bigcustomerpersons getBigCustomerPersonsByCustomerIdAndPhoneHashDao(Long customerid, String phonehash) {
		String sql = "from Bigcustomerpersons where customerid = ?0 and phonehash = ?1 ";
		return super.queryEntity(sql, customerid, phonehash);
	}
}
