/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomerAuditsDaoExtension.java
 * History:
 *         2019年6月1日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

/**
 * 大客户审核记录数据访问扩展对象。
 * @author wangjb
 */
public interface BigCustomerAuditsDaoExtension{

}
