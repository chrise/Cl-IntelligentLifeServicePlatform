/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesDao.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.MerchantSettlesDaoExtension;
import club.coderleague.ilsp.entities.Merchantsettles;

/**
 * 商户入驻
 * @author Liangjing
 */
public interface MerchantSettlesDao extends DataRepository<Merchantsettles, Long>, MerchantSettlesDaoExtension {

	/**
	 * 获取商户入驻对象
	 * @author Liangjing 2019年5月24日
	 * @param entityid
	 * @return
	 */
	public Merchantsettles findByEntityid(Long entityid);
	/**
	 * 根据参数查询对应的商户入驻对象
	 * @author Liangjing 2019年5月24日
	 * @param marketid
	 * @param merchantid
	 * @param settlestall
	 * @param EntityState
	 * @return
	 */
	public Merchantsettles findByMarketidAndSettlestallAndEntitystate(Long marketid,String settlestall,Integer EntityState);
	/**
	 * 获取对应商户的所有入驻对象
	 * @author Liangjing 2019年6月12日
	 * @param merchantid
	 * @return
	 */
	public List<Merchantsettles> findByMerchantid(Long merchantid);
	/**
	 * 获取对应商户的所有入驻对象
	 * @author Liangjing 2019年6月12日
	 * @param merchantid
	 * @param EntityState
	 * @return
	 */
	public List<Merchantsettles> findByMerchantidAndEntitystate(Long merchantid,Integer EntityState);
	/**
	 * 获取对应商户的所有入驻对象
	 * @author Liangjing 2019年7月30日
	 * @param marketid
	 * @param EntityState
	 * @return
	 */
	public List<Merchantsettles> findByMarketidAndEntitystate(Long marketid,Integer EntityState);
}
