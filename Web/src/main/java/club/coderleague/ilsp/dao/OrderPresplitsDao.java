/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderPresplitsDao.java
 * History:
 *         2019年7月14日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.OrderPresplitsDaoExtension;
import club.coderleague.ilsp.entities.Orderpresplits;

/**
 * 订单预拆分Dao
 * 
 * @author CJH
 */
public interface OrderPresplitsDao extends DataRepository<Orderpresplits, Long>, OrderPresplitsDaoExtension {

	/**
	 * 根据订单标识、商户标识和市场标识查询订单预拆分
	 * 
	 * @author CJH 2019年7月14日
	 * @param orderid 订单标识
	 * @param merchantid 商户标识
	 * @param marketid 市场标识
	 * @return 订单预拆分
	 */
	public Orderpresplits findByOrderidAndMerchantidAndMarketid(Long orderid, Long merchantid, Long marketid);

	/**
	 * 根据订单标识查询是否存在订单预拆分
	 * 
	 * @author CJH 2019年7月14日
	 * @param orderid 订单标识
	 * @return 是否存在
	 */
	public boolean existsByOrderid(Long orderid);

}
