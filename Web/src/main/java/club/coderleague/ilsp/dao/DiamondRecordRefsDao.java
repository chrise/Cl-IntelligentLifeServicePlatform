/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondRecordRefsDao.java
 * History:
 *         2019年8月25日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.DiamondRecordRefsDaoExtension;
import club.coderleague.ilsp.entities.Diamondrecordrefs;

/**
 * 钻石记录关系Dao
 * 
 * @author CJH
 */
public interface DiamondRecordRefsDao extends DataRepository<Diamondrecordrefs, Long>, DiamondRecordRefsDaoExtension {

	/**
	 * 查询钻石记录关系
	 * 
	 * @author CJH 2019年8月25日
	 * @param paymentrecordid 支出钻石记录标识
	 * @return 钻石记录关系
	 */
	public List<Diamondrecordrefs> findByPaymentrecordid(Long paymentrecordid);

}
