/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：GoodGroupsDaoExtension.java
 * History:
 *         2019年5月12日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.ilsp.entities.Goodgroups;

/**
 * 商品分类
 * @author Liangjing
 */
public interface GoodGroupsDaoExtension {
	/**
	 * 获取所有的商品分类
	 * @author Liangjing 2019年5月12日
	 * @param key
	 * @param istate
	 * @return
	 */
	public List<Goodgroups> getTheAllGoodGroup(String key,boolean isrecycle);
	/**
	 * 获取商品分类查看详情页面数据
	 * @author Liangjing 2019年5月12日
	 * @param entityid
	 * @return
	 */
	public Map<String,Object> getTheGoodGroupLookInfoPageData(String entityid);
	/**
	 * 获取商品分类编辑页面父级分类加载数据
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	public List<Map<String,Object>> getTheUpdatePageLoadParentGoods(String entityid);
	/**
	 * 获取手机加载商品分类的所有父级
	 * @author Liangjing 2019年6月17日
	 * @return
	 */
	public List<Map<String,Object>> getTheMobileParentGoods();
	/**
	 * 获取手机加载商品分类对应父级的所有二级分类
	 * @author Liangjing 2019年6月17日
	 * @param parentid
	 * @return
	 */
	public List<Map<String,Object>> getTheMobileSecondSonGoods(Long parentid);
	/**
	 * 获取手机加载商品分类对应父级的所有三级分类
	 * @author Liangjing 2019年6月17日
	 * @param parentid
	 * @return
	 */
	public List<Map<String,Object>> getTheMobileThreeSonGoods(Long parentid, String path);
	/**
	 * 根据实体id获取对应商品分类名称
	 * @author Liangjing 2019年6月24日
	 * @param entityid
	 * @return
	 */
	public String getTheGoodGroupNameByEntityid(Long entityid);
	/**
	 * 获取对应的商品分类
	 * @author Liangjing 2019年7月7日
	 * @param goodgroupid
	 * @return
	 */
	public List<Map<String,Object>> getTheAllGoodGroupFilterConditions(Long goodgroupid, String key);
	
}
