/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RoleAuthsDao.java
 * History:
 *         2019年5月18日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.RoleAuthsDaoExtension;
import club.coderleague.ilsp.entities.Roleauths;

/**
 * 角色授权Dao
 * 
 * @author CJH
 */
public interface RoleAuthsDao extends DataRepository<Roleauths, Long>, RoleAuthsDaoExtension {

	/**
	 * 根据角色主键和功能主键查询角色授权
	 * 
	 * @author CJH 2019年5月20日
	 * @param roleid 角色ID
	 * @param funcid 功能ID
	 * @return 角色授权
	 */
	public Roleauths findOneByRoleidAndFuncid(Long roleid, Long funcid);

}
