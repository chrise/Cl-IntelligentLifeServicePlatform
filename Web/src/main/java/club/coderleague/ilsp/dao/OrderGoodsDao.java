/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderGoodsDao.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.OrderGoodsDaoExtension;
import club.coderleague.ilsp.entities.Ordergoods;

/**
 * 订单商品Dao
 * 
 * @author CJH
 */
public interface OrderGoodsDao extends DataRepository<Ordergoods, Long>, OrderGoodsDaoExtension {

	/**
	 * 根据订单主键查询订单商品
	 * 
	 * @author CJH 2019年6月13日
	 * @param orderid 订单主键
	 * @return 订单商品
	 */
	public List<Ordergoods> findByOrderid(Long orderid);

	/**
	 * 根据订单标识查询订单商品个数
	 * 
	 * @author CJH 2019年8月8日
	 * @param ordersid 订单标识
	 * @return 订单商品个数
	 */
	public long countByOrderid(Long ordersid);

}
