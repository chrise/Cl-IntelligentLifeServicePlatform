/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrganizationsMgrDaoImpl.java
 * History:
 *         2019年5月11日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.OrganizationsMgrDaoExtension;
import club.coderleague.ilsp.entities.Organizations;

/**
 * 机构管理数据访问扩展对象实现。
 * @author wangjb
 */
public class OrganizationsMgrDaoImpl extends AbstractDataRepositoryExtension<Organizations, Long, SnowflakeV4> implements OrganizationsMgrDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.OrganizationsMgrDaoExtension#getOrgMgrListDao(java.util.Map)
	 */
	@Override
	public List<Map<String, Object>> getOrgMgrListDao(Map<String, Object> params) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select entityid, entitystate, orgname, parentorg, orgdesc from Organizations where entitystate = :entitystate ";
		boolean recycle = Boolean.valueOf((String) params.get("isrecycle"));
		int entitystate = recycle ? EntityState.INVALID.getValue() : EntityState.VALID.getValue();
		param.put("entitystate", entitystate);
		if (params.get("keyword") != null && StringUtils.isNotBlank(params.get("keyword").toString())) {
			sql += "and (orgname like :orgname) ";
            param.put("orgname", "%" + (String) params.get("keyword") + "%");
        }
		
		sql += "order by createtime desc ";
		return super.queryMapListBySql(sql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.OrganizationsMgrDaoExtension#getParentOrgSelectListDao()
	 */
	@Override
	public List<Map<String, Object>> getParentOrgSelectListDao() {
		String sql = "select entityid, parentorg, orgname from Organizations where entitystate = ?0";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.OrganizationsMgrDaoExtension#getRepeatDataByOrgNameOrParentOrgDao(java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public Boolean getRepeatDataByOrgNameOrParentOrgDao(String orgname, Long parentorg, Long entityid) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select count(entityid) from Organizations where orgname = :orgname and entitystate = :entitystate ";
		param.put("orgname", orgname);
		param.put("entitystate", EntityState.VALID.getValue());
		if (parentorg != null) {
			sql += "and parentorg = :parentorg ";
			param.put("parentorg", parentorg);
		} else sql += "and parentorg is null ";
		
		if (entityid != null ) {
			sql += "and entityid != :entityid ";
			param.put("entityid", entityid);
		}
		Long count = super.queryLong(sql, param);
		return (count > 0);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.OrganizationsMgrDaoExtension#getOrganizationsDetailDataByOrgidDao(java.lang.String)
	 */
	@Override
	public Map<String, Object> getOrganizationsDetailDataByOrgidDao(String entityid) {
		String sql = "select o1.entityid, o1.entitystate, o1.orgname, o2.orgname as parentorg, .1orgdesc from Organizations o1 left join Organizations o2 on o1.parentorg = o2.entityid and entitystate = ?0 where o1.entityid = ?1";
		return super.queryMapBySql(sql, EntityState.VALID.getValue(), entityid);
	}

}
