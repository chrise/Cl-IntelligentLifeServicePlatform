/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondRecordRefsDaoExtension.java
 * History:
 *         2019年8月25日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;

/**
 * 钻石记录关系DaoImpl
 * 
 * @author CJH
 */
public interface DiamondRecordRefsDaoExtension extends DataRepositoryExtension {

}
