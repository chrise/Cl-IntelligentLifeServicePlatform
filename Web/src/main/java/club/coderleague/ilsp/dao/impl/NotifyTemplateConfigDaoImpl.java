/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：NotifyTemplateConfigDaoImpl.java
 * History:
 *         2019年6月6日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.NotifyType;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.NotifyTemplateConfigDaoExtension;
import club.coderleague.ilsp.entities.Notifytemplateconfigs;

/**
 * 通知模板配置数据访问对象实现。
 * @author Chrise
 */
public class NotifyTemplateConfigDaoImpl extends AbstractDataRepositoryExtension<Notifytemplateconfigs, Long, SnowflakeV4> implements NotifyTemplateConfigDaoExtension {
	/**
	 * @see club.coderleague.ilsp.dao.extension.NotifyTemplateConfigDaoExtension#queryTemplate(club.coderleague.ilsp.common.domain.enums.NotifyType)
	 */
	@Override
	public Notifytemplateconfigs queryTemplate(NotifyType type) {
		String jpql = "from Notifytemplateconfigs where entitystate= ?0 and notifytype = ?1";
		return super.queryEntity(jpql, EntityState.VALID.getValue(), type.getValue());
	}

	@Override
	public Page<Notifytemplateconfigs> getTheAllTemplate(String key, boolean isrecycle, Integer pageIndex, Integer pageSize) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer qsql = new StringBuffer(" select EntityId as entityid,Creator as creator,CreateTime as createtime,Modifier as modifier,ModifyTime as modifytime,EntityState as entitystate,NotifyType as notifytype,TplCode as tplcode,TplParam as tplparam from NotifyTemplateConfigs where EntityState = :istate ");
		StringBuffer csql = new StringBuffer(" select count(EntityId) from NotifyTemplateConfigs where EntityState = :istate ");
		
		if(!isrecycle) {
			map.put("istate", EntityState.VALID.getValue());
		}else {
			map.put("istate", EntityState.INVALID.getValue());
		}
		
		if(!CommonUtil.isEmpty(key)) {
			qsql.append(" and (TplCode like :key or TplParam like :key)");
			csql.append(" and (TplCode like :key or TplParam like :key)");
			map.put("key", "%"+key+"%");
		}
		
		return super.queryEntityPageBySql(csql.toString(), qsql.toString(), pageIndex, pageSize, map);
	}

	@Override
	public Map<String, Object> getTheLookInfoPageData(Long entityid) {
		String sql = " select TplCode as tplcode,TplParam as tplparam,(case NotifyType when '"+NotifyType.PHONE_BIND.getValue()+"' then '"+NotifyType.PHONE_BIND.getText()+"'  when '"+NotifyType.IDENTITY_AUTH.getValue()+"' then '"+NotifyType.IDENTITY_AUTH.getText()+"'  when '"+NotifyType.ARRACC_REMIND.getValue()+"' then '"+NotifyType.ARRACC_REMIND.getText()+"'  when '"+NotifyType.REFUND_REMIND.getValue()+"' then '"+NotifyType.REFUND_REMIND.getText()+"'  when '"+NotifyType.SETACC_REMIND.getValue()+"' then '"+NotifyType.SETACC_REMIND.getText()+"'  else '' end) as notifytype from NotifyTemplateConfigs where EntityId = ?0 ";
		return super.queryMapBySql(sql, entityid);
	}
}
