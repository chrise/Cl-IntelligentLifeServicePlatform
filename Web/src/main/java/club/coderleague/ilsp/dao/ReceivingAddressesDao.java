/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：ReceivingAddressesDao.java
 * History:
 *         2019年6月3日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.ReceivingAddressesDaoExtension;
import club.coderleague.ilsp.entities.Receivingaddresses;

/**
 * 收货地址
 * @author Liangjing
 */
public interface ReceivingAddressesDao extends DataRepository<Receivingaddresses, Long>, ReceivingAddressesDaoExtension {
	
}
