/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomerAuditsDaoImpl.java
 * History:
 *         2019年6月1日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.impl;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.dao.extension.BigCustomerAuditsDaoExtension;
import club.coderleague.ilsp.entities.Bigcustomeraudits;

/**
 * 大客户审核数据访问扩展对象实现。
 * @author wangjb
 */
public class BigCustomerAuditsDaoImpl extends AbstractDataRepositoryExtension<Bigcustomeraudits, Long, SnowflakeV4> implements BigCustomerAuditsDaoExtension{

}
