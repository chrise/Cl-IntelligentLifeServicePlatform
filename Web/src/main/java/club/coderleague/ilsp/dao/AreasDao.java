/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AreasDao.java
 * History:
 *         2019年5月24日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.AreasDaoExtension;
import club.coderleague.ilsp.entities.Areas;

/**
 * 区域Dao
 * 
 * @author CJH
 */
public interface AreasDao extends DataRepository<Areas, Long>, AreasDaoExtension {

}
