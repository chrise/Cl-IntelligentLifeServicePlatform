/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RolesDaoImpl.java
 * History:
 *         2019年5月18日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.UserType;
import club.coderleague.ilsp.dao.extension.RolesDaoExtension;
import club.coderleague.ilsp.entities.Roles;
import club.coderleague.ilsp.util.SessionUtil;

/**
 * 角色DaoImpl
 * 
 * @author CJH
 */
public class RolesDaoImpl extends AbstractDataRepositoryExtension<Roles, Long, SnowflakeV4> implements RolesDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.RolesDaoExtension#findPageByParamsMap(club.coderleague.ilsp.common.domain.beans.PagingParameters, boolean, java.lang.String)
	 */
	@Override
	public Page<Roles> findPageByParamsMap(PagingParameters pagingparameters, boolean isrecycle, String keyword) {
		StringBuilder qjpqlBuilder = new StringBuilder("from Roles r where r.entitystate = :entitystate");
		StringBuilder cjpqlBuilder = new StringBuilder("select count(*) from Roles r where r.entitystate = :entitystate");
		Map<String, Object> params = new HashMap<>();
		params.put("entitystate", isrecycle ? EntityState.INVALID.getValue() : EntityState.VALID.getValue());
		
		// 关键字
		if (StringUtils.isNotBlank(keyword)) {
			qjpqlBuilder.append(" and (r.rolename like :keyword or r.roledesc like :keyword)");
			cjpqlBuilder.append(" and (r.rolename like :keyword or r.roledesc like :keyword)");
			params.put("keyword", "%" + keyword + "%");
		}
		return super.queryEntityPage(cjpqlBuilder.toString(), qjpqlBuilder.toString(), pagingparameters.getPageIndex(), pagingparameters.getPageSize(), params);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.RolesDaoExtension#existsByRolenameNotEntityid(java.lang.String, java.lang.Long)
	 */
	@Override
	public boolean existsByRolenameNotEntityid(String rolename, Long entityid) {
		StringBuilder sqlBuilder = new StringBuilder("select count(*) from roles r where r.entitystate = :validstate and r.rolename = :rolename");
		Map<String, Object> params = new HashMap<>();
		params.put("validstate", EntityState.VALID.getValue());
		params.put("rolename", rolename);
		if (entityid != null) {
			sqlBuilder.append(" and r.entityid != :entityid");
			params.put("entityid", entityid);
		}
		return super.queryLongBySql(sqlBuilder.toString(), params) > 0;
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.RolesDaoExtension#findMapByEntityid(java.lang.Long)
	 */
	@Override
	public Map<String, Object> findMapByEntityid(Long entityid) {
		String sql = "select r.entityid as entityid, r.rolename as rolename, r.roledesc as roledesc from roles r where r.entityid = ?0";
		return super.queryMapBySql(sql, entityid);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.RolesDaoExtension#findAuth()
	 */
	@Override
	public List<Roles> findAuth() {
		StringBuilder sqlBuilder = new StringBuilder("from Roles r where r.entitystate = :validstate");
		Map<String, Object> params = new HashMap<>();
		params.put("validstate", EntityState.VALID.getValue());
		// 获取用户信息
		UserSession us = SessionUtil.getUserSession();
		if (!UserType.ADMIN.equalsValue(us.getUsertype())) {
			sqlBuilder.append(" and exists(select 1 from Userauths ua where ua.userid = :userid and ua.roleid = r.entityid and ua.entitystate = :validstate)");
			params.put("userid", us.getUserid());
		}
		return super.queryEntityList(sqlBuilder.toString(), params);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.RolesDaoExtension#findAuthFunctionsByRoleid(java.lang.Long)
	 */
	@Override
	public List<Map<String, Object>> findAuthFunctionsByRoleid(Long roleid) {
		String sql = "select f.entityid as entityid, f.parentfunc as parentfunc, f.funcname as funcname from roleauths ra"
			+ " inner join roles r on r.entityid = ra.roleid and r.entitystate = ?1"
			+ " inner join functions f on f.entityid = ra.funcid and f.entitystate = ?1"
			+ " where ra.roleid = ?0 and ra.entitystate = ?1";
		return super.queryMapListBySql(sql, roleid, EntityState.VALID.getValue());
	}

}
