/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RefundGoodsDaoExtension.java
 * History:
 *         2019年8月8日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

/**
 * 退款商品Dao扩展
 * 
 * @author CJH
 */
public interface RefundGoodsDaoExtension {

}
