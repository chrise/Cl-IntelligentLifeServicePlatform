/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderGoodsDaoExtension.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.entities.Ordergoods;

/**
 * 订单商品Dao扩展
 * 
 * @author CJH
 */
public interface OrderGoodsDaoExtension extends DataRepositoryExtension {
	/**
	 * 从订单查询商品。
	 * @author Chrise 2019年7月16日
	 * @param order 订单标识。
	 * @return 商品集合。
	 */
	List<Ordergoods> queryFromOrder(long order);
	
	/**
	 * 从购物车查询商品。
	 * @author Chrise 2019年7月16日
	 * @param member 会员标识。
	 * @param buynow 立即购买。
	 * @return 商品集合。
	 */
	List<Ordergoods> queryFromShoppingcart(long member, boolean buynow);
	
	/**
	 * 根据订单标识查询是否存在失效商品
	 * 
	 * @author CJH 2019年7月26日
	 * @param orderid 订单标识
	 * @return 是否存在失效商品
	 */
	public boolean existsInvalidByOrderid(Long orderid);
	
	/**
	 * -根据订单id获取退款商品。
	 * @author wangjb 2019年8月24日。
	 * @param refundrecordid 订单id。
	 * @return
	 */
	List<Map<String, Object>> getRefundGoodByRefundRecordIdDao(Long refundrecordid);
	/**
	 * 获取该订单的所有商品图片
	 * @author Liangjing 2019年8月29日
	 * @param orderid
	 * @return
	 */
	List<String> getAllGoodPictureByOrderId(String orderid, String path);
	/**
	 * 获取该订单的所有商品详情
	 * @author Liangjing 2019年8月29日
	 * @param orderid
	 * @param path
	 * @return
	 */
	List<Map<String,Object>> getAllGoodPictureInfoByOrderId(Long orderid, String path);
	
	/**
	 * 获取退款商品。
	 * @author Chrise 2019年10月11日
	 * @param recordid 退款记录标识。
	 * @return 退款商品集合。
	 */
	List<Ordergoods> getRefundGoods(Long recordid);
}
