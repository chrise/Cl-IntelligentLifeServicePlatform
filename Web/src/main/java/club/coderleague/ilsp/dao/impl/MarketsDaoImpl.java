/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MarketsDaoImpl.java
 * History:
 *         2019年5月11日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.MarketsDaoExtension;
import club.coderleague.ilsp.entities.Markets;

/**
 * 市场表
 * @author Liangjing
 */
public class MarketsDaoImpl extends AbstractDataRepositoryExtension<Markets, String, SnowflakeV4> implements MarketsDaoExtension {

	@Override
	public Page<Map<String,Object>> getTheAllMarkets(Integer pageIndex, Integer pageSize, boolean isrecycle, String key) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer qsql = new StringBuffer(" select m.EntityId as entityid,m.EntityState as entitystate,m.MarketName as marketname,m.MarketDesc as marketdesc from Markets m where 1=1 ");
		StringBuffer csql = new StringBuffer(" select count(m.EntityId) from Markets m where 1=1 ");
		if(!isrecycle) {
			qsql.append(" and m.entitystate = :istate ");
			csql.append(" and m.entitystate = :istate ");
			map.put("istate", EntityState.VALID.getValue());
		}else {
			qsql.append(" and m.entitystate = :istate ");
			csql.append(" and m.entitystate = :istate ");
			map.put("istate", EntityState.INVALID.getValue());
		}
		if(!CommonUtil.isEmpty(key)) {
			qsql.append(" and (m.MarketName like :key or m.MarketDesc like :key) ");
			csql.append(" and (m.MarketName like :key or m.MarketDesc like :key) ");
			map.put("key", "%"+key+"%");
		}
		return super.queryMapPageBySql(csql.toString(), qsql.toString(), pageIndex, pageSize, map);
	}

	@Override
	public int getTheMaxMarketNumberAddOne() {
		String sql = " select MarketNumber from Markets order by MarketNumber desc limit 0,1 ";
		Integer i = super.queryIntegerBySql(sql);
		return i != null ? i+1 : 1;
	}

	@Override
	public List<Map<String, Object>> getTheAllMarketFilterConditions(Long goodgroupid, String key) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer sql = new StringBuffer(" select concat(m.EntityId) as entityid,m.MarketName as name from Goods g,GoodSpecs gs,GoodSales gsale,MerchantSettles ms,Markets m where g.EntityState = :istate and g.EntityId = gs.GoodId and gs.EntityState = :istate and gs.EntityId = gsale.SpecId and gsale.EntityState = :pushstate and gsale.SettleId = ms.EntityId and ms.EntityState = :istate and ms.MarketId = m.EntityId and m.EntityState = :istate ");
		if(!CommonUtil.isEmpty(goodgroupid)) {
			sql.append(" and g.GroupId = :goodgroupid ");
			map.put("goodgroupid", goodgroupid);
		}
		if(!CommonUtil.isEmpty(key)) {
			sql.append(" and g.GoodName like :key ");
			map.put("key", "%"+key+"%");
		}
		sql.append(" group by m.EntityId,m.MarketName ");
		map.put("istate", EntityState.VALID.getValue());
		map.put("pushstate", EntityState.PUBLISHED.getValue());
		return super.queryMapListBySql(sql.toString(), map);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.MarketsDaoExtension#findAllMap()
	 */
	@Override
	public List<Map<String, Object>> findAllMap() {
		String sql = "select m.entityid as entityid, m.creator as creator, m.createtime as createtime, m.modifier as modifier,"
				+ " m.modifytime as modifytime, m.entitystate as entitystate, m.marketname as marketname, m.marketdesc as marketdesc"
				+ " from markets m where m.entitystate = ?0";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue());
	}


}
