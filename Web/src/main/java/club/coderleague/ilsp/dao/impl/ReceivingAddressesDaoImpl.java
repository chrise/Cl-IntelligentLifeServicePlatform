/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：ReceivingAddressesDaoImpl.java
 * History:
 *         2019年6月3日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.ReceivingAddressesExtension;
import club.coderleague.ilsp.common.domain.beans.ReceivingAddressesSelectBean;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.ReceivingAddressesDaoExtension;
import club.coderleague.ilsp.entities.Receivingaddresses;

/**
 * 收货地址
 * @author Liangjing
 */
public class ReceivingAddressesDaoImpl extends AbstractDataRepositoryExtension<Receivingaddresses, Long, SnowflakeV4> implements ReceivingAddressesDaoExtension{

	@Override
	public Page<ReceivingAddressesSelectBean> getTheAllReceivingAddresses(Long memberid, Integer pageIndex, Integer pageSize, String key) {
		Map<String,Object> map = new HashMap<String, Object>();
		String sql = "select rd.EntityId as entityid,rd.MemberId as memberid,rd.Receiver as receiver,rd.DetailAddress as detailaddress,rd.MobilePhone as mobilephone "
				+ " ,(select a.AreaName from Areas a where a.EntityId = rd.City) as cityname "
				+ " ,(select a.AreaName from Areas a where a.EntityId = rd.County) as countyname "
				+ " ,(select a.AreaName from Areas a where a.EntityId = rd.Street) as streetname "
				+ " from Receivingaddresses rd where rd.MemberId = :memberid and rd.EntityState = :istate ";
		map.put("memberid", memberid);
		map.put("istate", EntityState.VALID.getValue());
		StringBuffer qsql = new StringBuffer(" select xx.* from ("+ sql +") as xx where 1=1 ");
		StringBuffer csql = new StringBuffer(" select count(xx.entityid) from ("+ sql +") as xx where 1=1 ");
		if(!CommonUtil.isEmpty(key)) {
			qsql.append(" and (xx.cityname like :key or xx.countyname like :key or xx.streetname like :key)");
			csql.append(" and (xx.cityname like :key or xx.countyname like :key or xx.streetname like :key)");
			map.put("key", "%"+key+"%");
		}
		return super.queryCustomBeanPageBySql(ReceivingAddressesSelectBean.class, csql.toString(), qsql.toString(), pageIndex, pageSize, map);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.ReceivingAddressesDaoExtension#getReceivingAddressesByEntityidAndDefaultaddressDao(java.lang.Long, java.lang.Boolean, java.lang.Long)
	 */
	@Override
	public Receivingaddresses getReceivingAddressesByEntityidAndDefaultaddressDao(Long entityid, Boolean defaultaddress,
			Long memberid) {
		Map<String,Object> param = new HashMap<String, Object>();
		String sql = "from Receivingaddresses where entitystate = :istate and memberid = :memberid and defaultaddress = :defaultaddress ";
		param.put("istate", EntityState.VALID.getValue());
		param.put("memberid", memberid);
		param.put("defaultaddress", defaultaddress);
		if (entityid != null) {
			sql += "and entityid != :entityid ";
			param.put("entityid", entityid);
		}
		return super.queryEntity(sql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.ReceivingAddressesDaoExtension#findByMemberidAndEntitystate(Long, int, boolean)
	 */
	@Override
	public List<ReceivingAddressesExtension> findByMemberidAndEntitystate(Long memberid, int entitystate, boolean defaultaddress) {
		Map<String,Object> param = new HashMap<String, Object>();
		String sql = "select ra.entityid as entityid, ra.memberid as memberid, ra.receiver as receiver, ra.province as province, ra.city as city, ra.county as county, ra.street as street, ra.detailaddress as detailaddress, ra.mobilephone as mobilephone, ra.defaultaddress as defaultaddress, ra.deliverymode as deliverymode, ra.paymentmode as paymentmode, "
				+ "a.areaname as provincename, a1.areaname as cityname, a2.areaname as countyname, a3.areaname as streetname from Receivingaddresses ra left join Areas a on ra.province = a.entityid and a.entitystate = :istate left join Areas a1 on ra.city = a1.entityid and a1.entitystate = :istate "
				+ "left join Areas a2 on ra.county = a2.entityid and a2.entitystate = :istate left join Areas a3 on ra.street = a3.entityid and a3.entitystate = :istate where ra.memberid = :memberid and ra.entitystate = :istate and ra.defaultaddress = :defaultaddress order by ra.createtime desc";
		param.put("istate", entitystate);
		param.put("memberid", memberid);
		param.put("defaultaddress", defaultaddress);
		return super.queryCustomBeanListBySql(ReceivingAddressesExtension.class, sql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.ReceivingAddressesDaoExtension#findByMemberidAndDefaultaddressAndEntitystate(java.lang.Long, boolean, int)
	 */
	@Override
	public ReceivingAddressesExtension findByMemberidAndDefaultaddressAndEntitystate(Long id, boolean defaultaddress,
			int entitystate) {
		Map<String,Object> param = new HashMap<String, Object>();
		String sql = "select ra.entityid as entityid, ra.memberid as memberid, ra.receiver as receiver, ra.province as province, ra.city as city, ra.county as county, ra.street as street, ra.detailaddress as detailaddress, ra.mobilephone as mobilephone, ra.defaultaddress as defaultaddress, ra.deliverymode as deliverymode, ra.paymentmode as paymentmode, "
				+ "a.areaname as provincename, a1.areaname as cityname, a2.areaname as countyname, a3.areaname as streetname from Receivingaddresses ra left join Areas a on ra.province = a.entityid and a.entitystate = :istate left join Areas a1 on ra.city = a1.entityid and a1.entitystate = :istate "
				+ "left join Areas a2 on ra.county = a2.entityid and a2.entitystate = :istate left join Areas a3 on ra.street = a3.entityid and a3.entitystate = :istate where ra.entitystate = :istate ";
		param.put("istate", entitystate);
		if (defaultaddress) { // 根据会员标识查询默认的地址。
			sql += "and ra.memberid = :memberid and ra.defaultaddress = :defaultaddress ";
			param.put("memberid", id);
			param.put("defaultaddress", defaultaddress);
		} else {
			sql += "and ra.entityid = :entityid ";
			param.put("entityid", id);
		}
		return super.queryCustomBeanBySql(ReceivingAddressesExtension.class, sql, param);
	}
}
