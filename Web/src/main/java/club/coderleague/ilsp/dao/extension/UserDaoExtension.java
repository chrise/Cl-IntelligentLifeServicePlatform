/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UserDaoExtension.java
 * History:
 *         2019年5月8日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.common.domain.beans.UsersExtension;
import club.coderleague.ilsp.entities.Users;

/**
 * 用户数据访问对象扩展。
 * @author Chrise
 */
public interface UserDaoExtension extends DataRepositoryExtension {
	/**
	 * 查询管理员。
	 * @author Chrise 2019年7月2日
	 * @return 管理员对象。
	 */
	Users queryAdministrator();
	
	/**
	 * 检查管理员用户是否存在。
	 * @author Chrise 2019年5月8日
	 * @return 管理员用户存在时返回true，否则返回false。
	 */
	boolean isAdministratorExists();
	
	/**
	 * 查询用户会话。
	 * @author Chrise 2019年5月12日
	 * @param loginname 登录名。
	 * @param isphone 是否手机号。
	 * @return 用户会话对象。
	 */
	UserSession queryUserSession(String loginname, boolean isphone);
	
	/**
	 * 查询授权。
	 * @author Chrise 2019年5月16日
	 * @param userid 用户标识。
	 * @return 授权标识集合。
	 */
	List<String> queryAuthorities(Long userid);
	
	/**
	 * 分页查询用户
	 * 
	 * @author CJH 2019年7月2日
	 * @param pagingparameters 分页参数
	 * @param isrecycle 是否回收站
	 * @param keyword 关键字
	 * @param username 加密用户名
	 * @param userphone 加密用户电话
	 * @return 用户
	 */
	public Page<UsersExtension> findPageByParamsMap(PagingParameters pagingparameters, boolean isrecycle, String keyword, String username, String userphone);
	
	/**
	 * 根据电话哈希或登录名查询除指定主键外是否存在
	 * 
	 * @author CJH 2019年5月18日
	 * @param phonehash 电话哈希
	 * @param loginname 登录名
	 * @param entityid 用户主键
	 * @return true/false
	 */
	public boolean existsByPhonehashOrLoginnameNotEntityid(String phonehash, String loginname, Long entityid);
	
	/**
	 * 根据用户主键查询用户
	 * 
	 * @author CJH 2019年5月20日
	 * @param entityid 用户主键
	 * @return 用户
	 */
	public UsersExtension findExtensionByEntityid(Long entityid);
	
	/**
	 * 查询所有用户名和用户电话
	 * 
	 * @author CJH 2019年5月22日
	 * @param isrecycle 是否回收站
	 * @return 用户
	 */
	public List<UsersExtension> findAllUsernameAndUserphone(boolean isrecycle);
	
	/**
	 * 根据登录名查询除指定主键外是否存在
	 * 
	 * @author CJH 2019年8月7日
	 * @param loginname 登录名
	 * @param entityid 用户标识
	 * @return true/false
	 */
	public boolean existsByLoginnameNotEntityid(String loginname, Long entityid);
}
