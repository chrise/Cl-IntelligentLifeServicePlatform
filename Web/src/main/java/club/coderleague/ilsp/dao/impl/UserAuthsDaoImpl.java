/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UserAuthsDaoImpl.java
 * History:
 *         2019年5月19日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.UserAuthsDaoExtension;
import club.coderleague.ilsp.entities.Userauths;
import club.coderleague.ilsp.util.SessionUtil;

/**
 * 用户授权DaoImpl
 * 
 * @author CJH
 */
public class UserAuthsDaoImpl extends AbstractDataRepositoryExtension<Userauths, Long, SnowflakeV4> implements UserAuthsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.UserAuthsDaoExtension#updateEntitystateByUseridNotInEntityids(java.lang.Integer, java.lang.Long, java.util.List)
	 */
	@Override
	public Integer updateEntitystateByUseridNotInEntityids(Integer entitystate, Long userid, List<Long> validentityids) {
		StringBuilder sqlBuilder = new StringBuilder("update userauths set modifier = :modifier, modifytime = :modifytime, entitystate = :entitystate"
			+ " where userid = :userid and entitystate = :validstate");
		Map<String, Object> params = new HashMap<>();
		params.put("validstate", EntityState.VALID.getValue());
		// 获取用户信息
		UserSession us = SessionUtil.getUserSession();
		params.put("modifier", us.getUserid());
		params.put("modifytime", new Date());
		params.put("entitystate", entitystate);
		params.put("userid", userid);
		if (validentityids != null && !validentityids.isEmpty()) {
			sqlBuilder.append(" and entityid not in (:validentityids)");
			params.put("validentityids", validentityids);
		}
		return super.executeUpdateBySql(sqlBuilder.toString(), params);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.UserAuthsDaoExtension#findRoleidByUserid(java.lang.Long)
	 */
	@Override
	public List<String> findRoleidByUserid(Long userid) {
		String sql = "select concat(ua.roleid) from userauths ua where ua.userid = ?0 and ua.entitystate = ?1";
		return super.queryStringListBySql(sql, userid, EntityState.VALID.getValue());
	}

}
