/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantsDao.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.MerchantsDaoExtension;
import club.coderleague.ilsp.entities.Merchants;

/**
 * 商户
 * @author Liangjing
 */
public interface MerchantsDao extends DataRepository<Merchants, Long>, MerchantsDaoExtension{

	/**
	 * 获取商户对象
	 * @author Liangjing 2019年5月24日
	 * @param entityid
	 * @return
	 */
	public Merchants findByEntityid(Long entityid);
}
