/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ShoppingCartsDaoExtension.java
 * History:
 *         2019年6月27日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.ilsp.common.domain.beans.ShoppingCartSettlementMerchantExtension;
import club.coderleague.ilsp.entities.Shoppingcarts;

/**
 * - 购物车数据访问扩展对象。
 * @author wangjb
 */
public interface ShoppingCartsDaoExtension {

	/**
	 * - 查询会员购物车商品数据。
	 * @author wangjb 2019年6月27日。
	 * @param buynow 立即购买。
	 * @param memberid 会员id。
	 * @param selectstate 选中状态。
	 * @return
	 */
	List<Map<String, Object>> getShoppingCartListByMemberIdDao(Boolean buynow, Long memberid, Boolean selectstate);
	
	/**
	 * - 查询购物车里商品所属商户。
	 * @author wangjb 2019年7月4日。
	 * @param buynow 立即购买。
	 * @param memberid 会员id。
	 * @param selectstate 选中状态。
	 * @return
	 */
	List<ShoppingCartSettlementMerchantExtension> getShoppingCartSettlementMerchantByMemberidDao(Boolean buynow, Long memberid, Boolean selectstate);
	
	/**
	 * 获取当前会员购物车数据
	 * @author Liangjing 2019年7月4日
	 * @param memberid
	 * @return
	 */
	Long getTheUserGoodCarNum(Long memberid);
	
	/**
	 * 修改选中状态。
	 * @author wangjb 2019年7月18日。
	 * @param shoppingcartidList 购物车id。
	 * @param memberid 会员id。
	 * @param selectstate 选中状态。
	 */
	void executeAmendSelectStateDao(List<String> shoppingcartidList, Long memberid, Boolean selectstate);
	
	/**
	 * 修改购物车商品数量。
	 * @author wangjb 2019年7月18日。
	 * @param shoppingcartid 购物车id。
	 * @param goodnumber 商品数量。
	 * @param userid 操作用户id。
	 */
	void executeAmendGoodNumberDao(String shoppingcartid, Double goodnumber, Long userid);
	
	/**
	 * 删除购物车数据。
	 * @author wangjb 2019年7月20日。
	 * @param shoppingcartid 购物车id。
	 */
	void executeDelShoppingCartGoodDao(String shoppingcartid);
	/**
	 * 删除购物车立即购买的数据
	 * @author Liangjing 2019年7月22日
	 */
	void deleteShoppingCartsBuyNowData(Long memberid);
	/**
	 * 获取当前会员立即购买的购物车对象
	 * @author Liangjing 2019年7月22日
	 * @param memberid
	 * @return
	 */
	Shoppingcarts getTheShoppingCartsBuyNowIsTrue(Long memberid);
	
	/**
	 * 根据会员标识、选中状态和立即购买删除购物车
	 * 
	 * @author CJH 2019年7月25日
	 * @param memberid 会员标识
	 * @param selectstate 选中状态
	 * @param buynow 立即购买
	 */
	public void deleteByMemberidAndSelectstateAndBuynow(Long memberid, boolean selectstate, boolean buynow);
}