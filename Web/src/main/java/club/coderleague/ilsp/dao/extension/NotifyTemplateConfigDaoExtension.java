/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：NotifyTemplateConfigDaoExtension.java
 * History:
 *         2019年6月6日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.NotifyType;
import club.coderleague.ilsp.entities.Notifytemplateconfigs;

/**
 * 通知模板配置数据访问对象扩展。
 * @author Chrise
 */
public interface NotifyTemplateConfigDaoExtension extends DataRepositoryExtension {
	/**
	 * 查询模板配置。
	 * @author Chrise 2019年6月7日
	 * @param type 通知类型。
	 * @return 模板配置对象。
	 */
	Notifytemplateconfigs queryTemplate(NotifyType type);
	/**
	 * 获取所有的通知模板
	 * @author Liangjing 2019年6月10日
	 * @param key
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public Page<Notifytemplateconfigs> getTheAllTemplate(String key,boolean isrecycle,Integer pageIndex,Integer pageSize);
	/**
	 * 获取通知模板查看详情页面数据
	 * @author Liangjing 2019年6月10日
	 * @param entityid
	 * @return
	 */
	public Map<String,Object> getTheLookInfoPageData(Long entityid);
}
