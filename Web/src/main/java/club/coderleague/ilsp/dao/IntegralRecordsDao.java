/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IntegralRecordsDao.java
 * History:
 *         2019年5月30日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.IntegralRecordsDaoExtension;
import club.coderleague.ilsp.entities.Integralrecords;

/**
 * 积分记录Dao
 * 
 * @author CJH
 */
public interface IntegralRecordsDao extends DataRepository<Integralrecords, Long>, IntegralRecordsDaoExtension {

	/**
	 * 查询积分记录
	 * 
	 * @author CJH 2019年8月25日
	 * @param orderid 订单标识
	 * @param recordtype 记录类型
	 * @return 积分记录
	 */
	public Integralrecords findByOrderidAndRecordtype(Long orderid, Integer recordtype);

}
