/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：BanksDaoImpl.java
 * History:
 *         2019年7月26日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.BanksDaoExtension;
import club.coderleague.ilsp.entities.Banks;

/**
 * 银行管理
 * @author Liangjing
 */
public class BanksDaoImpl extends AbstractDataRepositoryExtension<Banks, Long, SnowflakeV4> implements BanksDaoExtension{

	@Override
	public List<Map<String,Object>> getTheAllBanks(String key, boolean isrecycle) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer sql = new StringBuffer(" select EntityId as entityid,EntityState as entitystate,ParentBank as parentbank,BankName as bankname from Banks where EntityState = :istate ");
		if(!isrecycle) {
			map.put("istate", EntityState.VALID.getValue());
		}else {
			map.put("istate", EntityState.INVALID.getValue());
		}
		
		if(!CommonUtil.isEmpty(key)) {
			sql.append(" and BankName like :key ");
			map.put("key", "%"+key+"%");
		}
		
		return super.queryMapListBySql(sql.toString(), map);
	}

	@Override
	public Map<String, Object> getTheBankLookInfoPageData(Long entityid) {
		String sql = " select bk.BankName as bankname,(select b.BankName from Banks b where b.EntityId = bk.ParentBank) as parentbank from Banks bk where bk.EntityId = ?0 ";
		return super.queryMapBySql(sql, entityid);
	}

	@Override
	public List<Map<String,Object>> getTheAllParentBanks() {
		String sql = " select EntityId as entityid,BankName as bankname from Banks where ParentBank is null and EntityState = ?0 ";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue());
	}

}
