/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IntegralRecordsDaoExtension.java
 * History:
 *         2019年5月30日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.entities.Integralrecords;

/**
 * 积分记录Dao扩展
 * 
 * @author CJH
 */
public interface IntegralRecordsDaoExtension extends DataRepositoryExtension {
	
	/**
	 * 根据会员主键查询已过期积分记录
	 * 
	 * @author CJH 2019年5月30日
	 * @param memberid 会员主键
	 * @return 积分记录
	 */
	public List<Integralrecords> findExpireByMemberid(Long memberid);
	
	/**
	 * 根据会员主键查询有效积分记录
	 * 
	 * @author CJH 2019年6月3日
	 * @param memberid 会员主键
	 * @return 积分记录
	 */
	public List<Integralrecords> findValidByMemberid(Long memberid);
	
	/**
	 * 根据会员主键查询已锁定积分记录
	 * 
	 * @author CJH 2019年6月3日
	 * @param memberid 会员主键
	 * @return 积分记录
	 */
	public List<Integralrecords> findLockedByMemberid(Long memberid);

	/**
	 * 根据积分记录主键检查积分记录是否过期
	 * 
	 * @author CJH 2019年6月3日
	 * @param entityid 积分记录主键
	 * @return 是否过期
	 */
	public boolean isExpireByEntityid(Long entityid);
	/**
	 * 获取对应会员积分记录
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<Integralrecords> getTheAllIntegralRecords(Long memberid,Integer pageIndex,Integer pageSize,String key);
}
