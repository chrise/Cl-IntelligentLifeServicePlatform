/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSalesDao.java
 * History:
 *         2019年6月7日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.GoodSalesDaoExtension;
import club.coderleague.ilsp.entities.Goodsales;

/**
 * 商品销售表数据访问对象。
 * @author wangjb
 */
public interface GoodSalesDao extends DataRepository<Goodsales, Long>, GoodSalesDaoExtension{

	/**
	 * 获取对应商品销售表
	 * @author Liangjing 2019年7月30日
	 * @param Settleid
	 * @param istates
	 * @return
	 */
	List<Goodsales> findBySettleidAndEntitystateIn(Long Settleid, List<Integer> istates);
	/**
	 * 根据规格id查询商品销售。
	 * @author wangjb 2019年7月30日。
	 * @param specid 商品销售id。
	 * @return
	 */
	List<Goodsales> findBySpecid(Long specid);

}
