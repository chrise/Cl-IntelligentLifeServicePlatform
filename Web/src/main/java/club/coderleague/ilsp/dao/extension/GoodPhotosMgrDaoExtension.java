/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodPhotosMgrDaoExtension.java
 * History:
 *         2019年5月28日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;

import club.coderleague.ilsp.entities.Goodphotos;

/**
 * 商品相册管理数据访问扩展对象。
 * @author wangjb
 */
public interface GoodPhotosMgrDaoExtension {

	/**
	 * 根据商品id查询商品相册。
	 * @author wangjb 2019年5月28日。
	 * @param goodid 商品标识。
	 * @param flag 查询判断 (t、查询所有，f、只查封面)
	 * @return
	 */
	List<Goodphotos> getGoodPhotoListByGoodidDao(Long goodid, boolean flag);
	
	/**
	 * 根据商品主键查询封面相片
	 * 
	 * @author CJH 2019年6月10日
	 * @param goodid 商品主键
	 * @return 商品相册
	 */
	public Goodphotos getCoverphotoByGoodid(Long goodid);
}
