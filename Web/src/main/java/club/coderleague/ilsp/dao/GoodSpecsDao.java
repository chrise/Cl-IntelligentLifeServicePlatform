/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSpecsDao.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.GoodSpecsDaoExtension;
import club.coderleague.ilsp.entities.Goodspecs;

/**
 * 商品规格Dao
 * 
 * @author CJH
 */
public interface GoodSpecsDao extends DataRepository<Goodspecs, Long>, GoodSpecsDaoExtension {

	/**
	 * 根据商品id查询规格。
	 * @author wangjb 2019年7月30日。
	 * @param goodid 商品id。
	 * @return
	 */
	List<Goodspecs> findByGoodid(Long goodid);

}
