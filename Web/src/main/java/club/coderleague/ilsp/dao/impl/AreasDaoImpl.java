/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AreasDaoImpl.java
 * History:
 *         2019年5月24日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.AreasDaoExtension;
import club.coderleague.ilsp.entities.Areas;

/**
 * 区域DaoImpl
 * 
 * @author CJH
 */
public class AreasDaoImpl extends AbstractDataRepositoryExtension<Areas, Long, SnowflakeV4> implements AreasDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.AreasDaoExtension#findAllByParamsMap(boolean, java.lang.String)
	 */
	@Override
	public List<Areas> findAllByParamsMap(boolean isrecycle, String keyword) {
		StringBuilder jpqlBuilder = new StringBuilder("from Areas a where a.entitystate = :entitystate");
		Map<String, Object> params = new HashMap<>();
		params.put("entitystate", isrecycle ? EntityState.INVALID.getValue() : EntityState.VALID.getValue());
		// 关键字
		if (StringUtils.isNotBlank(keyword)) {
			jpqlBuilder.append(" and (a.areaname like :keyword or a.parentarea like :keyword)");
			params.put("keyword", "%" + keyword + "%");
		}
		return super.queryEntityList(jpqlBuilder.toString(), params);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.AreasDaoExtension#findAllMap()
	 */
	@Override
	public List<Map<String, Object>> findAllMap() {
		String sql = "select a.entityid as entityid, a.areaname as areaname, a.parentarea as parentarea from areas a where a.entitystate = ?0";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.AreasDaoExtension#existsByAreanameAndParentareaNotEntityid(java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public boolean existsByAreanameAndParentareaNotEntityid(String areaname, Long parentarea, Long entityid) {
		StringBuilder sqlBuilder = new StringBuilder("select count(*) from areas a where a.entitystate = :validstate and a.areaname = :areaname");
		Map<String, Object> params = new HashMap<>();
		params.put("validstate", EntityState.VALID.getValue());
		params.put("areaname", areaname);
		if (parentarea != null) {
			sqlBuilder.append(" and a.parentarea = :parentarea");
			params.put("parentarea", parentarea);
		} else {
			sqlBuilder.append(" and a.parentarea is null");
		}
		if (entityid != null) {
			sqlBuilder.append(" and a.entityid != :entityid");
			params.put("entityid", entityid);
		}
		return super.queryLongBySql(sqlBuilder.toString(), params) > 0;
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.AreasDaoExtension#findMapByEntityid(java.lang.Long)
	 */
	@Override
	public Map<String, Object> findMapByEntityid(Long entityid) {
		String sql = "select a.entityid as entityid, a.areaname as areaname, a.areadesc as areadesc, aa.areaname as parentareaname"
				+ " from areas a left join areas aa on aa.entityid = a.parentarea and aa.entitystate = ?1 where a.entityid = ?0";
		return super.queryMapBySql(sql, entityid, EntityState.VALID.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.AreasDaoExtension#getWetChatReceiviAreasDao()
	 */
	@Override
	public List<Areas> getWetChatReceiviAreasDao(Long parentarea) {
		Map<String, Object> param = new HashMap<>();
		String sql = "from Areas where entitystate = :istate ";
		param.put("istate", EntityState.VALID.getValue());
		if (parentarea != null) {
			sql += "and parentarea = :parentarea ";
			param.put("parentarea", parentarea);
		} else sql += "and parentarea is null ";
		sql += "order by createtime desc";
		return super.queryEntityList(sql, param);
	}

}
