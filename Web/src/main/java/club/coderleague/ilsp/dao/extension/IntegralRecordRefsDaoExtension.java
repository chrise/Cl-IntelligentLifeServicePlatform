/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IntegralRecordRefsDaoExtension.java
 * History:
 *         2019年8月25日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;

/**
 * 积分记录关系Dao扩展
 * 
 * @author CJH
 */
public interface IntegralRecordRefsDaoExtension extends DataRepositoryExtension {

}
