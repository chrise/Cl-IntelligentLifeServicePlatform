/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondRecordsDao.java
 * History:
 *         2019年5月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.DiamondRecordsDaoExtension;
import club.coderleague.ilsp.entities.Diamondrecords;

/**
 * 钻石记录Dao
 * 
 * @author CJH
 */
public interface DiamondRecordsDao extends DataRepository<Diamondrecords, Long>, DiamondRecordsDaoExtension {

	/**
	 * 查询钻石记录
	 * 
	 * @author CJH 2019年8月25日
	 * @param orderid 订单标识
	 * @param recordtype 记录类型
	 * @return 钻石记录
	 */
	public Diamondrecords findByOrderidAndRecordtype(Long orderid, Integer recordtype);

}
