/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SystemConfigDao.java
 * History:
 *         2019年5月23日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.SystemConfigDaoExtension;
import club.coderleague.ilsp.entities.Systemconfigs;

/**
 * 系统配置数据访问对象。
 * @author Chrise
 */
public interface SystemConfigDao extends DataRepository<Systemconfigs, Long>, SystemConfigDaoExtension {
	
	/**
	 * 获取系统配置
	 * @author Liangjing 2019年6月1日
	 * @param entitystate
	 * @return
	 */
	public Systemconfigs findByEntitystate(Integer entitystate);
}
