/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondRecordRefsDaoImpl.java
 * History:
 *         2019年8月25日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.dao.extension.DiamondRecordRefsDaoExtension;
import club.coderleague.ilsp.entities.Diamondrecordrefs;

/**
 * 钻石记录关系DaoImpl
 * 
 * @author CJH
 */
public class DiamondRecordRefsDaoImpl extends AbstractDataRepositoryExtension<Diamondrecordrefs, Long, SnowflakeV4> implements DiamondRecordRefsDaoExtension {

}
