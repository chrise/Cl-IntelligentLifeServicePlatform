/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MemberBindDao.java
 * History:
 *         2019年6月2日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.MemberBindDaoExtension;
import club.coderleague.ilsp.entities.Memberbinds;

/**
 * 会员绑定数据访问对象。
 * @author Chrise
 */
public interface MemberBindDao extends DataRepository<Memberbinds, Long>, MemberBindDaoExtension {
	/**
	 * 查询指定会员、绑定类型和绑定标识的会员绑定。
	 * @author Chrise 2019年6月2日
	 * @param memberId 会员标识。
	 * @param bindType 绑定类型。
	 * @param bindId 绑定标识。
	 * @return 会员绑定对象。
	 */
	Memberbinds findByMemberidAndBindtypeAndBindid(long memberId, int bindType, String bindId);
}
