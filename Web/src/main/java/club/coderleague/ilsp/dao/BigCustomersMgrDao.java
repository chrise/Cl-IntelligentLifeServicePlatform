/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomersDao.java
 * History:
 *         2019年5月25日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.BigCustomersMgrDaoExtension;
import club.coderleague.ilsp.entities.Bigcustomers;

/**
 * 大客户管理数据访问对象。
 * @author wangjb
 */
public interface BigCustomersMgrDao extends DataRepository<Bigcustomers, Long>, BigCustomersMgrDaoExtension{

}
