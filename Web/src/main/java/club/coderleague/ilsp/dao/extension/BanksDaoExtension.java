/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：BanksDaoExtension.java
 * History:
 *         2019年7月26日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;


/**
 * 银行管理
 * @author Liangjing
 */
public interface BanksDaoExtension {
	
	/**
	 * 获取对应所有银行信息
	 * @author Liangjing 2019年7月26日
	 * @param key
	 * @param isrecycle
	 * @return
	 */
	public List<Map<String,Object>> getTheAllBanks(String key,boolean isrecycle);
	/**
	 * 获取银行管理查看详情页面数据
	 * @author Liangjing 2019年7月26日
	 * @param entityid
	 * @return
	 */
	public Map<String,Object> getTheBankLookInfoPageData(Long entityid);
	/**
	 * 获取所有银行父级信息
	 * @author Liangjing 2019年7月26日
	 * @return
	 */
	public List<Map<String,Object>> getTheAllParentBanks();

}
