/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OperateLogsDaoExtension.java
 * History:
 *         2019年5月11日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.OperateLogsExtension;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;

/**
 * 操作日志Dao扩展
 * 
 * @author CJH
 */
public interface OperateLogsDaoExtension extends DataRepositoryExtension {
	/**
	 * 分页查询操作日志
	 * 
	 * @author CJH 2019年7月2日
	 * @param pagingparameters 分页参数
	 * @param keyword 关键字
	 * @param starttime 开始时间
	 * @param endtime 结束时间
	 * @return 操作日志
	 */
	public Page<OperateLogsExtension> findPageByParamsMap(PagingParameters pagingparameters, String keyword, String starttime, String endtime);
	
	/**
	 * 根据操作日志主键查询操作日志
	 * 
	 * @author CJH 2019年5月21日
	 * @param entityid 操作日志主键
	 * @return 操作日志
	 */
	public OperateLogsExtension findExtensionByEntityid(Long entityid);
}
