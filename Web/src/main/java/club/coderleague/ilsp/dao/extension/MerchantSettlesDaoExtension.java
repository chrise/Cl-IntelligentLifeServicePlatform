/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesDaoExtension.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.MerchantSettlesPageBean;
import club.coderleague.ilsp.entities.Merchantsettles;

/**
 * 商户入驻
 * @author Liangjing
 */
public interface MerchantSettlesDaoExtension {
	
	/**
	 * 获取所有的商户入驻数据
	 * @author Liangjing 2019年5月24日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @param istate
	 * @return
	 */
	public Page<MerchantSettlesPageBean> getTheAllMerchantSettles(Integer pageIndex,Integer pageSize,String key,String istate);
	/**
	 * 获取商户入驻查看详情页面加载数据
	 * @author Liangjing 2019年5月24日
	 * @param entityid
	 * @return
	 */
	public MerchantSettlesPageBean getTheMerchantSettlesLookInfoPageData(Long entityid);
	/**
	 * 获取所有的市场
	 * @author Liangjing 2019年5月24日
	 * @return
	 */
	public List<Map<String,Object>> getTheAllMarkets();
	/**
	 * 获取所有的商户
	 * @author Liangjing 2019年5月24日
	 * @return
	 */
	public List<Map<String,Object>> getTheAllMerchants();
	/**
	 * 获取当前最大商户入驻编号
	 * @author Liangjing 2019年5月24日
	 * @return
	 */
	public Long getTheMaxMerchantsNumber();

	/**
	 * 根据商品销售主键查询商户入驻
	 * 
	 * @author CJH 2019年6月10日
	 * @param saleid 商品销售主键
	 * @return 商户入驻
	 */
	public Merchantsettles findByGoodsalesid(Long saleid);
	/**
	 * 获取当前查询详情页面该商户对应入驻信息
	 * @author Liangjing 2019年6月8日
	 * @param merchantid
	 * @return
	 */
	public List<Map<String,Object>> getTheLookInfoLoadData(Long merchantid);

	/**
	 * 根据商品销售标识查询商户入驻个数
	 * 
	 * @author CJH 2019年7月25日
	 * @param saleids 商品销售标识
	 * @return 商户入驻个数
	 */
	public Long findByGoodsalesid(List<Long> saleids);
	
}
