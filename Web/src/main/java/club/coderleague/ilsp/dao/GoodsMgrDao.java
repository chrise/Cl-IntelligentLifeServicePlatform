/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodsMgrDao.java
 * History:
 *         2019年5月18日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension;
import club.coderleague.ilsp.entities.Goods;

/**
 * 商品管理数据访问对象。
 * @author wangjb
 */
public interface GoodsMgrDao extends DataRepository<Goods, Long>, GoodsMgrDaoExtension{

}
