/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomerPersonDaoExtension.java
 * History:
 *         2019年6月3日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.entities.Bigcustomerpersons;

/**
 * 大客户人员数据访问对象扩展。
 * @author Chrise
 */
public interface BigCustomerPersonDaoExtension extends DataRepositoryExtension {
	/**
	 * 查询绑定信息。
	 * @author Chrise 2019年6月4日
	 * @param openId 开放标识。
	 * @return 绑定信息。
	 */
	Map<String, Object> queryBoundInfo(String openId);
	
	/**
	 * 查询存在的人员。
	 * @author Chrise 2019年6月3日
	 * @param phoneHash 电话哈希。
	 * @return 人员对象。
	 */
	Bigcustomerpersons queryExistsPersion(String phoneHash);
	
	/**
	 * 清除绑定。
	 * @author Chrise 2019年6月4日
	 * @param boundId 绑定标识。
	 */
	void clearBound(String boundId);
	
	/**
	 * 根据大客户标识查询人员。
	 * @author wangjb 2019年6月6日。
	 * @param customerid 大客户标识。
	 * @return
	 */
	List<Bigcustomerpersons> getPersonByBgcidDao(Long customerid);
	
	/**
	 * 根据电话和大客户id查询人员。
	 * @author wangjb 2019年6月19日。
	 * @param customerid 大客户id。 
	 * @param phonehash 电话哈希。
	 * @return
	 */
	Bigcustomerpersons getBigCustomerPersonsByCustomerIdAndPhoneHashDao(Long customerid, String phonehash);
}
