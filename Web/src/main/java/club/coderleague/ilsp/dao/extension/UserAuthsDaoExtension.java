/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UserAuthsDaoExtension.java
 * History:
 *         2019年5月19日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;

/**
 * 用户授权Dao扩展
 * 
 * @author CJH
 */
public interface UserAuthsDaoExtension extends DataRepositoryExtension {

	/**
	 * 根据用户主键更新除指定主键外用户授权状态
	 * 
	 * @author CJH 2019年5月20日
	 * @param entitystate 状态
	 * @param userid 用户主键
	 * @param validentityids 用户授权主键
	 * @return 执行SQL受影响行数
	 */
	public Integer updateEntitystateByUseridNotInEntityids(Integer entitystate, Long userid, List<Long> validentityids);
	
	/**
	 * 根据用户主键查询授权角色主键
	 * 
	 * @author CJH 2019年5月20日
	 * @param userid 用户主键
	 * @return 角色主键
	 */
	public List<String> findRoleidByUserid(Long userid);
}
