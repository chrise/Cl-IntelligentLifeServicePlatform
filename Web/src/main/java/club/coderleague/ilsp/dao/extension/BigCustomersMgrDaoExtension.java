/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomersDaoExtension.java
 * History:
 *         2019年5月25日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.SettlementBigCustomerOrdersExtension;
import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.entities.Bigcustomers;

/**
 * 大客户管理数据访问扩展对象。
 * @author wangjb
 */
public interface BigCustomersMgrDaoExtension {
	
	/**
	 * 获取大客户列表数据。
	 * @author wangjb 2019年5月26日。
	 * @param params 关键字查询参数。
	 * @return
	 */
	Page<Bigcustomers> getBigCustomersMgrDao(Map<String, Object> params);
	
	/**
	 * 查询用户会话。
	 * @author Chrise 2019年5月31日
	 * @param openId 开放标识。
	 * @return 用户会话对象。
	 */
	UserSession queryUserSession(String openId);
	
	/**
	 * 修改登录时间。
	 * @author Chrise 2019年6月3日
	 * @param person 人员标识。
	 */
	void updateLoginTime(long person);
	
	/**
	 * 根据大客户名称查询重复数据。
	 * @author wangjb 2019年6月1日。
	 * @param customerhash 名称哈希。
	 * @param entityid 大客户id。
	 * @return
	 */
	Boolean getRepeatByEntityidAndCustomerNameDao(String customerhash, Long entityid);
	
	/**
	 * 查询大客户下所有 为结算的订单。
	 * @author wangjb 2019年6月8日。
	 * @param params 页面查询参数。
	 * @return
	 */
	Page<SettlementBigCustomerOrdersExtension> getBigCustomerOrdersMgrDao(Map<String, Object> params);
	
	/**
	 * -根据订单id查询商户。
	 * @author wangjb 2019年8月29日。
	 * @param orderIdList 订单标识。
	 * @return
	 */
	List<Map<String, Object>> getMerchantListByOrderIdDao(List<Long> orderIdList);
	
	/**
	 * 获取大客户数据。
	 * @author wangjb 2019年8月30日。
	 * @return
	 */
	List<Bigcustomers> getBigCustomerListDao();
}
