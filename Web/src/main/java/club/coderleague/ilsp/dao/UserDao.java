/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：UserDao.java
 * History:
 *         2019年5月8日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.UserDaoExtension;
import club.coderleague.ilsp.entities.Users;

/**
 * 用户数据访问对象。
 * @author Chrise
 */
public interface UserDao extends DataRepository<Users, Long>, UserDaoExtension {

}
