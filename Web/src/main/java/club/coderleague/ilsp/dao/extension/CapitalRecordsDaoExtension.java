/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：CapitalRecordsDaoExtension.java
 * History:
 *         2019年6月17日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;

/**
 * 资金记录Dao扩展
 * 
 * @author CJH
 */
public interface CapitalRecordsDaoExtension extends DataRepositoryExtension {
	
	/**
	 * -WeChat 根据商户id获取资金记录数据。
	 * @author wangjb 2019年8月3日。
	 * @param merchantid 商户id。
	 * @param params 页面查询参数。
	 * @return
	 */
	List<Map<String, Object>> getCapitalRecordListByMerchantidDao(Long merchantid, Map<String, Object> params);
	
	/**
	 * -根据记录id获取记录详情。
	 * @author wangjb 2019年8月3日。
	 * @param recordid 记录id。
	 * @return
	 */
	Map<String, Object> getRecordDetailByRecordidDao(Long recordid);

}
