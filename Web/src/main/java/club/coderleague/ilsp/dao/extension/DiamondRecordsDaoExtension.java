/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondRecordsDaoExtension.java
 * History:
 *         2019年5月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.entities.Diamondrecords;

/**
 * 钻石记录Dao扩展
 * 
 * @author CJH
 */
public interface DiamondRecordsDaoExtension extends DataRepositoryExtension {
	/**
	 * 根据会员主键查询已过期钻石记录
	 * 
	 * @author CJH 2019年5月28日
	 * @param memberid 会员主键
	 * @return 钻石记录
	 */
	public List<Diamondrecords> findExpireByMemberid(Long memberid);
	
	/**
	 * 根据会员主键查询有效钻石记录
	 * 
	 * @author CJH 2019年5月31日
	 * @param memberid 会员主键
	 * @return 钻石记录
	 */
	public List<Diamondrecords> findValidByMemberid(Long memberid);
	
	/**
	 * 根据会员主键查询已锁定钻石记录
	 * 
	 * @author CJH 2019年5月31日
	 * @param memberid 会员主键
	 * @return 钻石记录
	 */
	public List<Diamondrecords> findLockedByMemberid(Long memberid);
	
	/**
	 * 根据钻石记录主键检查钻石记录是否过期
	 * 
	 * @author CJH 2019年5月31日
	 * @param entityid 钻石记录主键
	 * @return 是否过期
	 */
	public boolean isExpireByEntityid(Long entityid);
	/**
	 * 获取对应会员钻石记录
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<Diamondrecords> getTheAllDiamondRecords(Long memberid,Integer pageIndex,Integer pageSize,String key);
}
