/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：NotifyTemplateConfigDao.java
 * History:
 *         2019年6月6日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.NotifyTemplateConfigDaoExtension;
import club.coderleague.ilsp.entities.Notifytemplateconfigs;

/**
 * 通知模板配置数据访问对象。
 * @author Chrise
 */
public interface NotifyTemplateConfigDao extends DataRepository<Notifytemplateconfigs, Long>, NotifyTemplateConfigDaoExtension {

	/**
	 * 通过实体id获取通知模板对象
	 * @author Liangjing 2019年6月10日
	 * @param entityid
	 * @return
	 */
	public Notifytemplateconfigs findByEntityid(Long entityid);
	/**
	 * 根据通知类型和状态获取通知模板对象
	 * @author Liangjing 2019年6月10日
	 * @param notifytype
	 * @param entitystate
	 * @return
	 */
	public Notifytemplateconfigs findByNotifytypeAndEntitystate(Integer notifytype,Integer entitystate);
}
