/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MemberBindDaoImpl.java
 * History:
 *         2019年6月2日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.MemberBindDaoExtension;
import club.coderleague.ilsp.entities.Memberbinds;

/**
 * 会员绑定数据访问对象实现。
 * @author Chrise
 */
public class MemberBindDaoImpl extends AbstractDataRepositoryExtension<Memberbinds, Long, SnowflakeV4> implements MemberBindDaoExtension {
	/**
	 * @see club.coderleague.ilsp.dao.extension.MemberBindDaoExtension#queryOtherBinds(long, int)
	 */
	@Override
	public List<Memberbinds> queryOtherBinds(long memberId, int bindType) {
		String jpql = "from Memberbinds where memberid = ?0 and bindtype = ?1 and entitystate = ?2";
		return super.queryEntityList(jpql, memberId, bindType, EntityState.VALID.getValue());
	}
	
	@Override
	public Page<Memberbinds> getTheAllMemberBinds(Long memberid, Integer pageIndex, Integer pageSize, String key) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer qsql = new StringBuffer(" select mb.* from MemberBinds mb where mb.MemberId = :memberid and mb.EntityState = :istate ");
		StringBuffer csql = new StringBuffer(" select count(mb.entityid) from MemberBinds mb where mb.MemberId = :memberid and mb.EntityState = :istate ");
		map.put("memberid", memberid);
		map.put("istate", EntityState.VALID.getValue());
		if(!CommonUtil.isEmpty(key)) {
			qsql.append("");
			csql.append("");
			map.put("key", "%"+key+"%");
		}
		return super.queryEntityPageBySql(csql.toString(), qsql.toString(), pageIndex, pageSize, map);
	}

	@Override
	public List<Map<String, Object>> getTheMemberBindInfos(Long memberid) {
		String sql = " select concat(EntityId) as entityid,BindType as bindtype,'1' as isreal from MemberBinds where MemberId = ?0 and EntityState = ?1 order by bindtype asc ";
		return super.queryMapListBySql(sql, memberid, EntityState.VALID.getValue());
	}
}
