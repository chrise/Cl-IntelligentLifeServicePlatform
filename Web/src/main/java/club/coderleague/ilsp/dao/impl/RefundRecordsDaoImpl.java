/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RefundRecordsDaoImpl.java
 * History:
 *         2019年8月8日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.RefundRecordsDaoExtension;
import club.coderleague.ilsp.entities.Refundrecords;

/**
 * 退款记录DaoImpl
 * 
 * @author CJH
 */
public class RefundRecordsDaoImpl extends AbstractDataRepositoryExtension<Refundrecords, Long, SnowflakeV4> implements RefundRecordsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.RefundRecordsDaoExtension#existsValidByOrderid(java.lang.Long)
	 */
	@Override
	public boolean existsValidByOrderid(Long ordersid) {
		String sql = "select count(*) from refundrecords rr where rr.orderid = ?0 and rr.entitystate != ?1";
		return super.queryIntegerBySql(sql, ordersid, EntityState.UNDO.getValue()) > 0;
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.RefundRecordsDaoExtension#findRefundingByOrderid(java.lang.Long)
	 */
	@Override
	public Refundrecords findRefundingByOrderid(Long orderid) {
		String jpql = "from Refundrecords rr where rr.orderid = ?0 and rr.entitystate = ?1";
		return super.queryEntity(jpql, orderid, EntityState.REFUNDING.getValue());
	}

}
