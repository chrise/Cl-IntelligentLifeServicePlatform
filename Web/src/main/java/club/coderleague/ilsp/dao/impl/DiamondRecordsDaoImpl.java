/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：DiamondRecordsDaoImpl.java
 * History:
 *         2019年5月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.DiamondRecordType;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.DiamondRecordsDaoExtension;
import club.coderleague.ilsp.entities.Diamondrecords;

/**
 * 钻石记录DaoImpl
 * 
 * @author CJH
 */
public class DiamondRecordsDaoImpl extends AbstractDataRepositoryExtension<Diamondrecords, Long, SnowflakeV4> implements DiamondRecordsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.DiamondRecordsDaoExtension#findExpireByMemberid(java.lang.Long)
	 */
	@Override
	public List<Diamondrecords> findExpireByMemberid(Long memberid) {
		String jpql = "from Diamondrecords dr where dr.memberid = ?0 and dr.entitystate = ?1 and dr.recordtype = ?2"
			+ " and (dr.remaineddiamond - dr.lockeddiamond) > 0"
			+ " and datediff(now(), dr.recordtime) > (select sc.diamondtimeout from Systemconfigs sc where sc.entitystate = ?1)";
		return super.queryEntityList(jpql, memberid, EntityState.VALID.getValue(), DiamondRecordType.INCOME.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.DiamondRecordsDaoExtension#findValidByMemberid(java.lang.Long)
	 */
	@Override
	public List<Diamondrecords> findValidByMemberid(Long memberid) {
		String jpql = "from Diamondrecords dr where dr.memberid = ?0 and dr.entitystate = ?1 and dr.recordtype = ?2"
			+ " and (dr.remaineddiamond - dr.lockeddiamond) > 0"
			+ " and datediff(now(), dr.recordtime) <= (select sc.diamondtimeout from Systemconfigs sc where sc.entitystate = ?1)"
			+ " order by dr.recordtime asc";
		return super.queryEntityList(jpql, memberid, EntityState.VALID.getValue(), DiamondRecordType.INCOME.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.DiamondRecordsDaoExtension#findLockedByMemberid(java.lang.Long)
	 */
	@Override
	public List<Diamondrecords> findLockedByMemberid(Long memberid) {
		String jpql = "from Diamondrecords dr where dr.memberid = ?0 and dr.entitystate = ?1 and dr.recordtype = ?2"
			+ " and dr.lockeddiamond > 0 order by dr.recordtime asc";
		return super.queryEntityList(jpql, memberid, EntityState.VALID.getValue(), DiamondRecordType.INCOME.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.DiamondRecordsDaoExtension#isExpireByEntityid(java.lang.Long)
	 */
	@Override
	public boolean isExpireByEntityid(Long entityid) {
		String sql = "select exists(select 1 from Diamondrecords dr where dr.entityid = ?0 and"
			+ " datediff(now(), dr.recordtime) > (select sc.diamondtimeout from Systemconfigs sc where sc.entitystate = ?1))";
		return super.queryLongBySql(sql, entityid, EntityState.VALID.getValue()) > 0;
	}
	
	@Override
	public Page<Diamondrecords> getTheAllDiamondRecords(Long memberid, Integer pageIndex, Integer pageSize, String key) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer qsql = new StringBuffer(" select * from Diamondrecords dr where dr.MemberId = :memberid and dr.EntityState = :istate ");
		StringBuffer csql = new StringBuffer(" select count(*) from Diamondrecords dr where dr.MemberId = :memberid and dr.EntityState = :istate ");
		map.put("memberid", memberid);
		map.put("istate", EntityState.VALID.getValue());
		if(!CommonUtil.isEmpty(key)) {
			qsql.append("");
			csql.append("");
			map.put("key", "%"+key+"%");
		}
		return super.queryEntityPageBySql(csql.toString(), qsql.toString(), pageIndex, pageSize, map);
	}

}
