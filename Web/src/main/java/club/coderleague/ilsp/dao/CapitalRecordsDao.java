/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：CapitalRecordsDao.java
 * History:
 *         2019年6月17日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.CapitalRecordsDaoExtension;
import club.coderleague.ilsp.entities.Capitalrecords;

/**
 * 资金记录Dao
 * 
 * @author CJH
 */
public interface CapitalRecordsDao extends DataRepository<Capitalrecords, Long>, CapitalRecordsDaoExtension {

}
