/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderPresplitsDaoImpl.java
 * History:
 *         2019年7月14日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.dao.extension.OrderPresplitsDaoExtension;
import club.coderleague.ilsp.entities.Orderpresplits;

/**
 * 订单预拆分DaoImpl
 * 
 * @author CJH
 */
public class OrderPresplitsDaoImpl extends AbstractDataRepositoryExtension<Orderpresplits, Long, SnowflakeV4> implements OrderPresplitsDaoExtension {

}
