/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantsDaoExtension.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.MerchantRemind;
import club.coderleague.ilsp.common.domain.beans.MerchantsPageBean;
import club.coderleague.ilsp.common.domain.beans.MerchantsSearchName;
import club.coderleague.ilsp.common.domain.beans.MerchantsSearchPhone;
import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.common.domain.beans.mobile.MerchantsFilterConditions;
import club.coderleague.ilsp.common.domain.beans.mobile.MoblieMerchantInfo;
import club.coderleague.ilsp.entities.Merchants;

/**
 * 商户
 * @author Liangjing
 */
public interface MerchantsDaoExtension {

	/**
	 * 获取所有的商户数据
	 * @author Liangjing 2019年5月24日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @param istate
	 * @return
	 */
	public Page<MerchantsPageBean> getTheAllMerchants(Integer pageIndex,Integer pageSize,Long searchnameentityid,String searchphonehash,boolean isrecycle);
	/**
	 * 根据商户名称获取对象
	 * @author Liangjing 2019年5月24日
	 * @param merchantname
	 * @param istate
	 * @return
	 */
	public Merchants findByMerchantnameAndEntityState(String merchantname,List<Integer> istate);
	
	/**
	 * 查询用户会话。
	 * @author Chrise 2019年5月31日
	 * @param openId 开放标识。
	 * @return 用户会话对象。
	 */
	UserSession queryUserSession(String openId);
	
	/**
	 * 修改登录时间。
	 * @author Chrise 2019年6月3日
	 * @param merchant 商户标识。
	 */
	void updateLoginTime(long merchant);
	
	/**
	 * 查询绑定信息。
	 * @author Chrise 2019年6月4日
	 * @param openId 开放标识。
	 * @return 绑定信息。
	 */
	Map<String, Object> queryBoundInfo(String openId);
	
	/**
	 * 查询存在的商户。
	 * @author Chrise 2019年6月4日
	 * @param phoneHash 电话哈希。
	 * @return 商户对象。
	 */
	Merchants queryExistsMerchant(String phoneHash);
	
	/**
	 * 清除绑定。
	 * @author Chrise 2019年6月4日
	 * @param boundId 绑定标识。
	 */
	void clearBound(String boundId);
	
	/**
	 * 查询商户提醒。
	 * @author Chrise 2019年6月9日
	 * @param merchant 商户标识。
	 * @return 商户提醒对象。
	 */
	MerchantRemind queryMerchantRemind(long merchant);
	/**
	 * 获取所有有效的市场
	 * @author Liangjing 2019年6月8日
	 * @return
	 */
	public List<Map<String,Object>> getTheAllMarkets();
	/**
	 * 该市场是否存在该摊位
	 * @author Liangjing 2019年6月8日
	 * @param marketid
	 * @param settlestall
	 * @return
	 */
	public boolean getIsHaveTheSettleStall(Long marketid,String settlestall,Long entityid);
	/**
	 * 获取当前市场最大入驻序号+1(起始1001)
	 * @author Liangjing 2019年6月8日
	 * @param marketid
	 * @return
	 */
	public Long getTheMaxAddOneSettleSerials(Long marketid);
	/**
	 * 根据商品分类id获取对应所有商户信息
	 * @author Liangjing 2019年7月7日
	 * @param goodgroupid
	 * @return
	 */
	public List<MerchantsFilterConditions> getTheAllMerchantFilterConditions(Long goodgroupid, String key);
	/**
	 * 获取对应展开查询商户列表
	 * @author Liangjing 2019年7月25日
	 * @return
	 */
	public List<MerchantsSearchName> getTheAllSearchMerchant();
	/**
	 * 获取对应展开查询商户电话
	 * @author Liangjing 2019年7月25日
	 * @return
	 */
	public List<MerchantsSearchPhone> getTheAllSearchMerchantPhoneHash();
	
	/**
	 * 获取自动结算的商户数据。
	 * @author wangjb 2019年6月15日。
	 * @param params 页面查询参数。
	 * @return
	 */
	Page<Merchants> getMerchanstListDao(Map<String, Object> params);
	/**
	 * 获取所有开户行父级银行
	 * @author Liangjing 2019年7月26日
	 * @return
	 */
	public List<Map<String,Object>> getTheAllParentBanks();
	/**
	 * 获取所有开户行子级银行
	 * @author Liangjing 2019年7月26日
	 * @param entityid
	 * @return
	 */
	public List<Map<String,Object>> getTheAllSonBanks(Long entityid);
	/**
	 * 获取对应开户网点和开户银行
	 * @author Liangjing 2019年7月26日
	 * @param merchantid
	 * @return
	 */
	public Map<String,Object> getTheBankNameAndParentName(Long merchantid);
	/**
	 * 获取对应商户的个人信息
	 * @author Liangjing 2019年7月30日
	 * @param merchantid
	 * @return
	 */
	public MoblieMerchantInfo getTheMerchantInfo(Long merchantid);
	
	/**
	 * 根据市场标识查询商户
	 * 
	 * @author CJH 2019年8月21日
	 * @param marketid 市场标识
	 * @return 商户
	 */
	public List<Map<String, Object>> findByMarketid(Long marketid);
}
