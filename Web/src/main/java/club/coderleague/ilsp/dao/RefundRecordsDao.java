/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RefundRecordsDao.java
 * History:
 *         2019年8月8日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.RefundRecordsDaoExtension;
import club.coderleague.ilsp.entities.Refundrecords;

/**
 * 退款记录Dao
 * 
 * @author CJH
 */
public interface RefundRecordsDao extends DataRepository<Refundrecords, Long>, RefundRecordsDaoExtension {

	/**
	 * -根据订单id和状态获取记录。
	 * @author wangjb 2019年8月30日。
	 * @param orderid 订单id。
	 * @param entitystate 数据状态。
	 * @return
	 */
	Refundrecords findByOrderidAndEntitystate(Long orderid, int entitystate);

	/**
	 * -根据订单id获取退款记录。
	 * @author wangjb 2019年9月3日。
	 * @param orderid 订单id。
	 * @return
	 */
	List<Refundrecords> findByOrderid(Long orderid);
	
}
