/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderGoodsDaoImpl.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.OrderGoodsDaoExtension;
import club.coderleague.ilsp.entities.Ordergoods;

/**
 * 订单商品DaoImpl
 * 
 * @author CJH
 */
public class OrderGoodsDaoImpl extends AbstractDataRepositoryExtension<Ordergoods, Long, SnowflakeV4> implements OrderGoodsDaoExtension {
	/**
	 * @see club.coderleague.ilsp.dao.extension.OrderGoodsDaoExtension#queryFromOrder(long)
	 */
	@Override
	public List<Ordergoods> queryFromOrder(long order) {
		String sql = "select entityid, goodtotal from Ordergoods where orderid = ?0";
		return super.queryCustomBeanListBySql(Ordergoods.class, sql, order);
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.OrderGoodsDaoExtension#queryFromShoppingcart(long, boolean)
	 */
	@Override
	public List<Ordergoods> queryFromShoppingcart(long member, boolean buynow) {
		String sql = "select sc.entityid, (sc.goodnumber * gs.goodprice) as goodtotal from Shoppingcarts sc, Goodsales gs " 
			+ "where sc.saleid = gs.entityid and sc.memberid = ?0 and sc.selectstate = ?1 and sc.buynow = ?2 and gs.entitystate = ?3";
		return super.queryCustomBeanListBySql(Ordergoods.class, sql, member, true, buynow, EntityState.PUBLISHED.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.OrderGoodsDaoExtension#existsInvalidByOrderid(java.lang.Long)
	 */
	@Override
	public boolean existsInvalidByOrderid(Long orderid) {
		String sql = "select count(*) from ordergoods sc where sc.orderid = ?0 and"
				+ " not exists(select 1 from goodsales gs where gs.entityid = sc.saleid and gs.entitystate = ?1)";
		return super.queryLongBySql(sql, orderid, EntityState.PUBLISHED.getValue()) > 0;
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.OrderGoodsDaoExtension#getRefundGoodByOrderIdDao(java.lang.Long)
	 */
	@Override
	public List<Map<String, Object>> getRefundGoodByRefundRecordIdDao(Long refundrecordid) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select o.entityid, o.goodname, o.goodprice, o.goodnumber, o.goodpicture, o.orderid from RefundGoods rg, Ordergoods o where rg.goodid = o.entityid and rg.entitystate = :istate and rg.recordid = :recordid ";
		param.put("istate", EntityState.VALID.getValue());
		param.put("recordid", refundrecordid);
		return super.queryMapListBySql(sql, param);
	}

	@Override
	public List<String> getAllGoodPictureByOrderId(String orderid, String path) {
		Map<String, Object> map = new HashMap<String, Object>();
		String sql = " select CONCAT(:path,GoodPicture) from OrderGoods where OrderId = :orderid ";
		map.put("path",path);
		map.put("orderid",orderid);
		return super.queryStringListBySql(sql, map);
	}

	@Override
	public List<Map<String, Object>> getAllGoodPictureInfoByOrderId(Long orderid, String path) {
		Map<String, Object> map = new HashMap<String, Object>();
		String sql = " select GoodName as goodname,GoodNumber as goodnumber,GoodTotal as goodtotal,CONCAT(:path,GoodPicture) as path from OrderGoods where OrderId = :orderid ";
		map.put("path",path);
		map.put("orderid",orderid);
		return super.queryMapListBySql(sql, map);
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.OrderGoodsDaoExtension#getRefundGoods(java.lang.Long)
	 */
	@Override
	public List<Ordergoods> getRefundGoods(Long recordid) {
		String jpql = "from Ordergoods where entityid in (select goodid from Refundgoods where recordid = ?0)";
		return super.queryEntityList(jpql, recordid);
	}
}
