/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：BigCustomerAuditsDao.java
 * History:
 *         2019年6月1日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.BigCustomerAuditsDaoExtension;
import club.coderleague.ilsp.entities.Bigcustomeraudits;

/**
 * 大客户审核数据访问对象。
 * @author wangjb
 */
public interface BigCustomerAuditsDao extends DataRepository<Bigcustomeraudits, Long>, BigCustomerAuditsDaoExtension{

}
