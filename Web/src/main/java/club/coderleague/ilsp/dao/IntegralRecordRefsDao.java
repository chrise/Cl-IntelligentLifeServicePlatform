/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IntegralRecordRefsDao.java
 * History:
 *         2019年8月25日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.IntegralRecordRefsDaoExtension;
import club.coderleague.ilsp.entities.Integralrecordrefs;

/**
 * 积分记录关系Dao
 * 
 * @author CJH
 */
public interface IntegralRecordRefsDao extends DataRepository<Integralrecordrefs, Long>, IntegralRecordRefsDaoExtension {

	/**
	 * 查询积分记录关系
	 * 
	 * @author CJH 2019年8月25日
	 * @param paymentrecordid 支出积分记录标识
	 * @return 积分记录关系
	 */
	public List<Integralrecordrefs> findByPaymentrecordid(Long paymentrecordid);

}
