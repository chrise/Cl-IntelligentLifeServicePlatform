/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RoleAuthsDaoImpl.java
 * History:
 *         2019年5月18日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.RoleAuthsDaoExtension;
import club.coderleague.ilsp.entities.Roleauths;
import club.coderleague.ilsp.util.SessionUtil;

/**
 * 角色授权DaoImpl
 * 
 * @author CJH
 */
public class RoleAuthsDaoImpl extends AbstractDataRepositoryExtension<Roleauths, Long, SnowflakeV4> implements RoleAuthsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.RoleAuthsDaoExtension#findFuncidByRoleid(java.lang.Long)
	 */
	@Override
	public List<String> findFuncidByRoleid(Long roleid) {
		String sql = "select concat(ra.funcid) from roleauths ra where ra.roleid = ?0 and ra.entitystate = ?1";
		return super.queryStringListBySql(sql, roleid, EntityState.VALID.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.RoleAuthsDaoExtension#updateEntitystateByRoleidNotInEntityids(java.lang.Integer, java.lang.Long, java.util.List)
	 */
	@Override
	public Integer updateEntitystateByRoleidNotInEntityids(Integer entitystate, Long roleid, List<Long> validentityids) {
		StringBuilder sqlBuilder = new StringBuilder("update roleauths set modifier = :modifier, modifytime = :modifytime, entitystate = :entitystate"
			+ " where roleid = :roleid and entitystate = :validstate");
		Map<String, Object> params = new HashMap<>();
		params.put("validstate", EntityState.VALID.getValue());
		// 获取用户信息
		UserSession us = SessionUtil.getUserSession();
		params.put("modifier", us.getUserid());
		params.put("modifytime", new Date());
		params.put("entitystate", entitystate);
		params.put("roleid", roleid);
		if (validentityids != null && !validentityids.isEmpty()) {
			sqlBuilder.append(" and entityid not in (:validentityids)");
			params.put("validentityids", validentityids);
		}
		return super.executeUpdateBySql(sqlBuilder.toString(), params);
	}

}
