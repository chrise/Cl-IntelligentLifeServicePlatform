/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrganizationsMgrDaoExtension.java
 * History:
 *         2019年5月11日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

/**
 * 机构管理数据访问扩展对象。
 * @author wangjb
 */
public interface OrganizationsMgrDaoExtension {

	/**
	 * 获取机构管理数据。
	 * @author wangjb 2019年5月12日。
	 * @param params 关键字查询。
	 * @return
	 */
	List<Map<String, Object>> getOrgMgrListDao(Map<String, Object> params);
	
	/**
	 * 获取父级机构数据。
	 * @author wangjb 2019年5月12日。
	 * @return
	 */
	List<Map<String, Object>> getParentOrgSelectListDao();
	
	/**
	 * 根据机构名称、父级机构和机构id查询重复数据。
	 * @author wangjb 2019年5月12日。
	 * @param orgname 机构名称。
	 * @param parentorg 父级机构。
	 * @param entityid 机构id。
	 * @return
	 */
	Boolean getRepeatDataByOrgNameOrParentOrgDao(String orgname, Long parentorg, Long entityid);
	
	/**
	 * 根据机构id查询机构详情。
	 * @author wangjb 2019年5月12日。
	 * @param entityid 机构id。
	 * @return
	 */
	Map<String, Object> getOrganizationsDetailDataByOrgidDao(String entityid);
	
}
