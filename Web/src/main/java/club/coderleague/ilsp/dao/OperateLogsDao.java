/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OperateLogsDao.java
 * History:
 *         2019年5月11日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.OperateLogsDaoExtension;
import club.coderleague.ilsp.entities.Operatelogs;

/**
 * 操作日志Dao
 * 
 * @author CJH
 */
public interface OperateLogsDao extends DataRepository<Operatelogs, Long>, OperateLogsDaoExtension {

}
