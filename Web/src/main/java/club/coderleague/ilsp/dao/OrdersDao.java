/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrdersDao.java
 * History:
 *         2019年6月6日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.OrdersDaoExtension;
import club.coderleague.ilsp.entities.Orders;

/**
 * 订单Dao
 * 
 * @author CJH
 */
public interface OrdersDao extends DataRepository<Orders, Long>, OrdersDaoExtension {

	/**
	 * 根据原始订单查询订单
	 * 
	 * @author CJH 2019年6月13日
	 * @param ordersid 原始订单
	 * @return 订单
	 */
	public List<Orders> findByOriginalorder(Long ordersid);

	/**
	 * 根据原始订单标识、商户标识和市场标识查询订单
	 * 
	 * @author CJH 2019年7月14日
	 * @param originalorder 原始订单标识
	 * @param merchantid 商户标识
	 * @param marketid 市场标识
	 * @return 订单
	 */
	public Orders findByOriginalorderAndMerchantidAndMarketid(Long originalorder, Long merchantid, Long marketid);

	/**
	 * 根据订单标识和状态查询是否存在订单
	 * 
	 * @author CJH 2019年7月31日
	 * @param originalorder 原始订单
	 * @param entitystate 状态
	 * @return 是否存在订单
	 */
	public boolean existsByOriginalorderAndEntitystate(Long originalorder, int entitystate);

}
