/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSalesDaoExtension.java
 * History:
 *         2019年6月7日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.GoodSaleSettleIdExtension;
import club.coderleague.ilsp.common.domain.beans.GoodSalesExtension;
import club.coderleague.ilsp.common.domain.beans.mobile.SingleGoodsExtension;
import club.coderleague.ilsp.entities.Goodsales;

/**
 * 商品销售表数据访问扩展对象。
 * @author wangjb
 */
public interface GoodSalesDaoExtension {
	
	/**
	 * 商品销售列表查询。
	 * @author wangjb 2019年6月11日。
	 * @param params 查询参数。
	 * @return
	 */
	Page<GoodSalesExtension> getGoodSalesMgrDao(Map<String, Object> params) throws Exception;
	
	/**
	 * 根据入驻id查询所有入驻的商品规格。
	 * @author wangjb 2019年6月14日。
	 * @param settleid 入驻id。
	 * @return
	 */
	List<Map<String, Object>> getGoodSalesBySettleidDao(Long settleid);
	
	/**
	 * 获取商户入驻标识。
	 * @author wangjb 2019年6月12日。
	 * @return
	 */
	List<GoodSaleSettleIdExtension> getSettleIdDao();
	
	/**
	 * 根据商户id查询商品。
	 * @author wangjb 2019年6月12日。
	 * @param mid 商户id。
	 * @param settleid 市场id。
	 * @return
	 */
	List<Map<String, Object>> getGoodByMerchantIdDao(Long mid, Long settleid);
	
	/**
	 * 根据入驻id获取商品详情。
	 * @author wangjb 2019年6月14日。
	 * @param param 页面查询参数。
	 * @return
	 */
	List<Map<String, Object>> getGoodSalesDetailBySettleidDao(Map<String, Object> param);
	
	/**
	 * 根据入驻id获取商品序号最大值。
	 * @author wangjb 2019年6月17日。
	 * @param settleid 入驻id。
	 * @return
	 */
	Integer getGoodSerialsMaxValBySettleidDao(Long settleid);
	
	/**
	 * 根据入驻id和规格id查询商品销售情况。
	 * @author wangjb 2019年6月17日。
	 * @param settleid 入驻id。
	 * @param specid 规格id。
	 * @return
	 */
	Goodsales getGoodSalesObjBySettleIdAndSpecIdDao(Long settleid, Long specid);
	
	/**
	 * 获取对应商品list
	 * @author Liangjing 2019年6月18日
	 * @param limitMin
	 * @param limitMax
	 * @param key
	 * @param goodgroupid
	 * @return
	 */
	List<SingleGoodsExtension> getTheSingleGoodsExtension(Integer limitMin,Integer limitMax,String key,Long goodgroupid, String path, List<Long> classSearch, List<Long> merchatSearch, List<Long> marketSearch);
	/**
	 * 通过商品销售id获取对应商品的所有相片
	 * @author Liangjing 2019年7月2日
	 * @param goodentityid
	 * @return
	 */
	List<Map<String,Object>> getTheGoodsAllPhotos(Long goodentityid, String path);
	/**
	 * 获取对应商品详情数据
	 * @author Liangjing 2019年7月2日
	 * @param goodentityid
	 * @param merchantsettleid
	 * @return
	 */
	public Map<String,Object> getTheGoodInfos(Long goodentityid,Long merchantsettleid);
	/**
	 * 获取对应所有商品规格
	 * @author Liangjing 2019年7月19日
	 * @param goodid
	 * @param merchantSettleid
	 * @param path
	 * @return
	 */
	public List<Map<String,Object>> geTheAllGoodSpecs(Long goodid,Long merchantsettleid,String path);
	
	/**
	 * 根据入驻标识查询商品
	 * 
	 * @author CJH 2019年8月21日
	 * @param settleid 入驻标识
	 * @return 商品
	 */
	public List<Map<String, Object>> findBySettleid(Long settleid);
}
