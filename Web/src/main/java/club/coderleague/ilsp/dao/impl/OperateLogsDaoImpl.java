/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OperateLogsDaoImpl.java
 * History:
 *         2019年5月11日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.OperateLogsExtension;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.OperateLogsDaoExtension;
import club.coderleague.ilsp.entities.Operatelogs;

/**
 * 操作日志DaoImpl
 * 
 * @author CJH
 */
public class OperateLogsDaoImpl extends AbstractDataRepositoryExtension<Operatelogs, Long, SnowflakeV4> implements OperateLogsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.OperateLogsDaoExtension#findPageByParamsMap(club.coderleague.ilsp.common.domain.beans.PagingParameters, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Page<OperateLogsExtension> findPageByParamsMap(PagingParameters pagingparameters, String keyword, String starttime, String endtime) {
		StringBuilder qsqlBuilder = new StringBuilder("select ol.entityid as entityid, ol.creator as creator, ol.createtime as createtime,"
			+ " ol.modifier as modifier, ol.modifytime as modifytime, ol.entitystate as entitystate, ol.operator as operator, ol.operatortype as operatortype,"
			+ " ol.operatetime as operatetime, ol.operateaddr as operateaddr, ol.operatedesc as operatedesc, ol.requesttype as requesttype,"
			+ " ol.returnresult as returnresult,"
			+ " (case ol.operatortype when 1 then u.entityid when 3 then bcp.entityid when 4 then mb.entityid else null end) as operatorid,"
			+ " (case ol.operatortype when 1 then u.username when 2 then ma.nickname when 3 then bcp.personname when 4 then mb.merchantname else null end) as operatorusername"
			+ " from operatelogs ol"
			+ " left join users u on u.entityid = ol.operator"
			+ " left join members ma on ma.entityid = ol.operator"
			+ " left join bigcustomerpersons bcp on bcp.entityid = ol.operator"
			+ " left join merchants mb on mb.entityid = ol.operator"
			+ " where ol.entitystate = :validstate");
		StringBuilder csqlBuidler = new StringBuilder("select count(*) from operatelogs ol where ol.entitystate = :validstate");
		Map<String, Object> params = new HashMap<>();
		params.put("validstate", EntityState.VALID.getValue());
		// 开始时间
		if (StringUtils.isNotBlank(starttime)) {
			qsqlBuilder.append(" and DATEDIFF(ol.operatetime, :starttime) >= 0");
			csqlBuidler.append(" and DATEDIFF(ol.operatetime, :starttime) >= 0");
			params.put("starttime", starttime);
		}
		// 结束时间
		if (StringUtils.isNotBlank(endtime)) {
			qsqlBuilder.append(" and DATEDIFF(ol.operatetime, :endtime) <= 0");
			csqlBuidler.append(" and DATEDIFF(ol.operatetime, :endtime) <= 0");
			params.put("endtime", endtime);
		}
		// 关键字
		if (StringUtils.isNotBlank(keyword)) {
			qsqlBuilder.append(" and (ol.operatedesc like :keyword or ol.operateaddr like :keyword)");
			csqlBuidler.append(" and (ol.operatedesc like :keyword or ol.operateaddr like :keyword)");
			params.put("keyword", "%" + keyword + "%");
		}
		// 排序
		qsqlBuilder.append(" order by ");
		if (StringUtils.isNoneBlank(pagingparameters.getSortField(), pagingparameters.getSortOrder())) {
			qsqlBuilder.append(pagingparameters.getSortField()).append(" ").append(pagingparameters.getSortOrder());
		} else {
			qsqlBuilder.append("createtime desc");
		}
		return super.queryCustomBeanPageBySql(OperateLogsExtension.class, csqlBuidler.toString(), qsqlBuilder.toString(), pagingparameters.getPageIndex(), pagingparameters.getPageSize(), params);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.OperateLogsDaoExtension#findExtensionByEntityid(java.lang.Long)
	 */
	@Override
	public OperateLogsExtension findExtensionByEntityid(Long entityid) {
		String sql = "select ol.entityid as entityid, ol.creator as creator, ol.createtime as createtime,"
			+ " ol.modifier as modifier, ol.modifytime as modifytime, ol.entitystate as entitystate, ol.operator as operator, ol.operatortype as operatortype,"
			+ " ol.operatetime as operatetime, ol.operateaddr as operateaddr, ol.operatedesc as operatedesc, ol.requesttype as requesttype,"
			+ " ol.returnresult as returnresult, ol.invokemethod as invokemethod, ol.requestdata as requestdata, ol.responsedata as responsedata,"
			+ " (case ol.operatortype when 1 then u.entityid when 3 then bcp.entityid when 4 then mb.entityid else null end) as operatorid,"
			+ " (case ol.operatortype when 1 then u.username when 2 then ma.nickname when 3 then bcp.personname when 4 then mb.merchantname else null end) as operatorusername"
			+ " from operatelogs ol"
			+ " left join users u on u.entityid = ol.operator"
			+ " left join members ma on ma.entityid = ol.operator"
			+ " left join bigcustomerpersons bcp on bcp.entityid = ol.operator"
			+ " left join merchants mb on mb.entityid = ol.operator"
			+ " where ol.entityid = ?0 and ol.entitystate = ?1";
		return super.queryCustomBeanBySql(OperateLogsExtension.class, sql, entityid, EntityState.VALID.getValue());
	}

}
