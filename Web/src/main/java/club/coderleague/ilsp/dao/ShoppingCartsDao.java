/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ShoppingCartsDao.java
 * History:
 *         2019年6月27日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension;
import club.coderleague.ilsp.entities.Shoppingcarts;

/**
 * 购物车数据访问对象。
 * @author wangjb
 */
public interface ShoppingCartsDao extends DataRepository<Shoppingcarts, Long>, ShoppingCartsDaoExtension{
	
	/**
	 * 通过会员id和商品销售id获取对应对象
	 * @author Liangjing 2019年7月2日
	 * @param memberid
	 * @param saleid
	 * @return
	 */
	Shoppingcarts findByMemberidAndSaleid(Long memberid,Long saleid);

	/**
	 * 根据会员标识、选中状态和立即购买查询购物车
	 * 
	 * @author CJH 2019年7月21日
	 * @param memberid 会员标识
	 * @param selectstate 选中状态
	 * @param buynow 立即购买
	 * @return 购物车
	 */
	public List<Shoppingcarts> findByMemberidAndSelectstateAndBuynow(Long memberid, boolean selectstate, Boolean buynow);
}
