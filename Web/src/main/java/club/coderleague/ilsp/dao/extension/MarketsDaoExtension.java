/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MarketsDaoExtension.java
 * History:
 *         2019年5月11日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;

/**
 * 市场表
 * @author Liangjing
 */
public interface MarketsDaoExtension {
	/**
	 *  获取所有的市场数据
	 * @author Liangjing 2019年5月11日
	 * @param pageIndex
	 * @param pageSize
	 * @param istate
	 * @param key
	 * @return
	 */
	public Page<Map<String,Object>> getTheAllMarkets(Integer pageIndex, Integer pageSize, boolean isrecycle, String key);
	/**
	 * 获取当前最大的市场编号+1
	 * @author Liangjing 2019年5月17日
	 * @return
	 */
	public int getTheMaxMarketNumberAddOne();
	/**
	 * 获取对应商品分类的所有市场信息
	 * @author Liangjing 2019年7月7日
	 * @param goodgroupid
	 * @return
	 */
	public List<Map<String,Object>> getTheAllMarketFilterConditions(Long goodgroupid, String key);
	
	/**
	 * 查询市场
	 * 
	 * @author CJH 2019年8月21日
	 * @return 市场
	 */
	public List<Map<String, Object>> findAllMap();
}
