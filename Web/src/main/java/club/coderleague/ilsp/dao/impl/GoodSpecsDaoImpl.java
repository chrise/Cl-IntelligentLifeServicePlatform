/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSpecsDaoImpl.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.GoodSpecsDaoExtension;
import club.coderleague.ilsp.entities.Goodspecs;

/**
 * 商品规格DaoImpl
 * 
 * @author CJH
 */
public class GoodSpecsDaoImpl extends AbstractDataRepositoryExtension<Goodspecs, Long, SnowflakeV4> implements GoodSpecsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodSpecsDaoExtension#getGoodSpecsByGoodIdDao(java.lang.Long)
	 */
	@Override
	public List<Map<String, Object>> getGoodSpecsByGoodIdDao(Long goodid) {
		String sql = "select entityid, specdefine, meterunit from Goodspecs where GoodId = ?0 and entitystate = ?1 ";
		return super.queryMapListBySql(sql, goodid, EntityState.VALID.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodSpecsDaoExtension#getSpecRepeatByGoodIdAndSpecDefineDao(java.lang.Long, java.lang.String)
	 */
	@Override
	public Boolean getSpecRepeatByGoodIdAndSpecDefineDao(Long goodid, String specdefine, Long entityid) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select count(entityid) from Goodspecs where goodid = :goodid and specdefine = :specdefine ";
		param.put("goodid", goodid);
		param.put("specdefine", specdefine);
		if (entityid != null) {
			sql += "and entityid != :entityid ";
			param.put("entityid", entityid);
		}
		Long count = super.queryLongBySql(sql, param);
	    if (count > 0) {
	    	sql += "and entitystate = :entitystate ";
	    	param.put("entitystate", EntityState.VALID.getValue());
			count = super.queryLongBySql(sql, param);
			return (count > 0);
	    } else return false;
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodSpecsDaoExtension#getGoodSpecsByGoodIdAndSpecDefineDao(java.lang.Long, java.lang.String)
	 */
	@Override
	public Goodspecs getGoodSpecsByGoodIdAndSpecDefineDao(Long goodid, String specdefine) {
		String sql = "from Goodspecs where goodid = ?0 and specdefine = ?1 and entitystate = ?2 ";
		return super.queryEntity(sql, goodid, specdefine, EntityState.INVALID.getValue());
	}

}
