/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：AreasDaoExtension.java
 * History:
 *         2019年5月24日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.entities.Areas;

/**
 * 区域Dao扩展
 * 
 * @author CJH
 */
public interface AreasDaoExtension extends DataRepositoryExtension {
	/**
	 * 分页查询区域
	 * 
	 * @author CJH 2019年7月2日
	 * @param isrecycle 是否回收站
	 * @param keyword 关键字
	 * @return 区域
	 */
	public List<Areas> findAllByParamsMap(boolean isrecycle, String keyword);
	
	/**
	 * 查询所有区域
	 * 
	 * @author CJH 2019年5月24日
	 * @return 区域
	 */
	public List<Map<String, Object>> findAllMap();
	
	/**
	 * 根据区域名称和父级区域查询除指定主键外是否存在
	 * 
	 * @author CJH 2019年5月24日
	 * @param areaname 区域名称
	 * @param parentarea 父级区域
	 * @param entityid 区域主键
	 * @return 是否存在true/false
	 */
	public boolean existsByAreanameAndParentareaNotEntityid(String areaname, Long parentarea, Long entityid);
	
	/**
	 * 根据区域主键查询区域
	 * 
	 * @author CJH 2019年5月24日
	 * @param entityid 区域主键
	 * @return 区域
	 */
	public Map<String, Object> findMapByEntityid(Long entityid);
	
	/**
	 * WeChat 个人收货地址查询区域。
	 * @author wangjb 2019年7月15日。
	 * @param parentarea 父级区域。
	 * @return
	 */
	List<Areas> getWetChatReceiviAreasDao(Long parentarea);
}
