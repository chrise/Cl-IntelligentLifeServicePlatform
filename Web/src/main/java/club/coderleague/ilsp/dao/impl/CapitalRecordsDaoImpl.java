/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：CapitalRecordsDaoImpl.java
 * History:
 *         2019年6月17日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.CapitalRecordsDaoExtension;
import club.coderleague.ilsp.entities.Capitalrecords;

/**
 * 资金记录DaoImpl
 * 
 * @author CJH
 */
public class CapitalRecordsDaoImpl extends AbstractDataRepositoryExtension<Capitalrecords, Long, SnowflakeV4> implements CapitalRecordsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.CapitalRecordsDaoExtension#getCapitalRecordListByMerchantidDao(Long, Map)
	 */
	@Override
	public List<Map<String, Object>> getCapitalRecordListByMerchantidDao(Long merchantid, Map<String, Object> params) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select entityid, merchantid, recordtype, changedreason, reasondesc, totalamount, changedamount, royaltyrate, "
				+ "royaltyamount, recordtime, entitystate from Capitalrecords where merchantid = :merchantid and entitystate = :entitystate ";
		param.put("merchantid", merchantid);
		param.put("entitystate", EntityState.VALID.getValue());
		if (params.get("recordtype") != null && StringUtils.isNotBlank(params.get("recordtype").toString())) {
			sql += "and recordtype in (:recordtype) ";
			String recordtype = params.get("recordtype").toString();
			List<Integer> recordTypeList = new ArrayList<Integer>();
			for (String type : recordtype.split(",")) {
				recordTypeList.add(Integer.parseInt(type));
			}
            param.put("recordtype", recordTypeList);
        }
		sql += "order by recordtime desc ";
		return super.queryMapListBySql(sql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.CapitalRecordsDaoExtension#getRecordDetailByRecordId(java.lang.Long)
	 */
	@Override
	public Map<String, Object> getRecordDetailByRecordidDao(Long recordid) {
		String sql = "select entityid, merchantid, recordtype, changedreason, reasondesc, totalamount, changedamount, royaltyrate, "
				+ "royaltyamount, recordtime, entitystate from Capitalrecords where entityid = ?0 ";
		return super.queryMapBySql(sql, recordid);
	}

}
