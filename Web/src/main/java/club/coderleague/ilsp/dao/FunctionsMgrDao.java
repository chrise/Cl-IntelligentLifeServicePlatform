/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：FunctionsDao.java
 * History:
 *         2019年5月10日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.FunctionsMgrDaoExtension;
import club.coderleague.ilsp.entities.Functions;

/**
  * 功能管理业务处理。
 * @author wangjb
 */
public interface FunctionsMgrDao extends DataRepository<Functions, Long>, FunctionsMgrDaoExtension{

	/**
	 * 查询子级。
	 * @author wangjb 2019年5月12日。
	 * @param dataid 机构id。
	 * @return
	 */
	List<Functions> findByParentfunc(Long dataid);

}
