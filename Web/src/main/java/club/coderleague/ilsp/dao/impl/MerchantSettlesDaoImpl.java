/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MerchantSettlesDaoImpl.java
 * History:
 *         2019年5月24日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.MerchantSettlesPageBean;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.MerchantSettlesDaoExtension;
import club.coderleague.ilsp.entities.Merchantsettles;

/**
 * 商户入驻
 * @author Liangjing
 */
public class MerchantSettlesDaoImpl extends AbstractDataRepositoryExtension<Merchantsettles, Long, SnowflakeV4> implements MerchantSettlesDaoExtension {

	@Override
	public Page<MerchantSettlesPageBean> getTheAllMerchantSettles(Integer pageIndex, Integer pageSize, String key, String istate) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer qsql = new StringBuffer(" select ms.EntityId as entityid,ms.MerchantId as merchantid,ms.SettleStall as settlestall,mk.MarketName as marketname,mc.MerchantName as merchantname from MerchantSettles ms,Markets mk,Merchants mc where ms.MarketId = mk.EntityId and ms.MerchantId = mc.EntityId and mk.EntityState = :istate and mc.EntityState = :istate ");
		StringBuffer csql = new StringBuffer(" select count(ms.EntityId) from MerchantSettles ms,Markets mk,Merchants mc where ms.MarketId = mk.EntityId and ms.MerchantId = mc.EntityId and mk.EntityState = :istate and mc.EntityState = :istate ");
		map.put("istate", EntityState.VALID.getValue());
		if("1".equals(istate)) {
			qsql.append(" and ms.EntityState = :msstate ");
			csql.append(" and ms.EntityState = :msstate ");
			map.put("msstate", EntityState.VALID.getValue());
		}else {
			qsql.append(" and ms.EntityState = :msstate ");
			csql.append(" and ms.EntityState = :msstate ");
			map.put("msstate", EntityState.INVALID.getValue());
		}
		if(!CommonUtil.isEmpty(key)) {
			qsql.append(" and ( ms.SettleStall like :key) ");
			csql.append(" and ( ms.SettleStall like :key) ");
			map.put("key", "%"+key+"%");
		}
		return super.queryCustomBeanPageBySql(MerchantSettlesPageBean.class, csql.toString(), qsql.toString(), pageIndex, pageSize, map);
	}

	@Override
	public MerchantSettlesPageBean getTheMerchantSettlesLookInfoPageData(Long entityid) {
		String sql = " select ms.EntityId as entityid,ms.MerchantId as merchantid,ms.SettleStall as settlestall,mk.MarketName as marketname,mc.MerchantName as merchantname from MerchantSettles ms,Markets mk,Merchants mc where ms.MarketId = mk.EntityId and ms.MerchantId = mc.EntityId and ms.EntityId = ?0 ";
		return super.queryCustomBeanBySql(MerchantSettlesPageBean.class, sql, entityid);
	}

	@Override
	public List<Map<String, Object>> getTheAllMarkets() {
		String sql = " select EntityId as id,MarketName as name from Markets where EntityState = ?0 ";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue());
	}

	@Override
	public List<Map<String, Object>> getTheAllMerchants() {
		String sql = " select EntityId as id,MerchantName as name from Merchants where EntityState = ?0 ";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue());
	}

	@Override
	public Long getTheMaxMerchantsNumber() {
		String sql = " select SettleSerials from MerchantSettles order by SettleSerials desc limit 0,1 ";
		Long i = super.queryLong(sql);
		return i != null ? i + 1 : 1001;
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.MerchantSettlesDaoExtension#findByGoodsalesid(java.lang.Long)
	 */
	@Override
	public Merchantsettles findByGoodsalesid(Long saleid) {
		String jpql = "from Merchantsettles ms where exists(select 1 from Goodsales gs where gs.entityid = ?0 and gs.settleid = ms.entityid)";
		return super.queryEntity(jpql, saleid);
	}

	@Override
	public List<Map<String, Object>> getTheLookInfoLoadData(Long merchantid) {
		String sql = " select m.MarketName as marketname,ms.SettleStall as settlestall from MerchantSettles ms,Markets m where ms.MarketId = m.EntityId and ms.EntityState = ?0 and ms.MerchantId = ?1 ";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue(), merchantid);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.MerchantSettlesDaoExtension#findByGoodsalesid(java.util.List)
	 */
	@Override
	public Long findByGoodsalesid(List<Long> saleids) {
		String sql = "select count(*) from merchantsettles ms"
				+ " where exists(select 1 from goodsales gs where gs.settleid = ms.entityid and gs.entityid in (:saleids))";
		Map<String, Object> params = new HashMap<>();
		params.put("saleids", saleids);
		return super.queryLongBySql(sql, params);
	}

}
