/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodSpecsDaoExtension.java
 * History:
 *         2019年6月10日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.entities.Goodspecs;

/**
 * 商品规格Dao扩展
 * 
 * @author CJH
 */
public interface GoodSpecsDaoExtension extends DataRepositoryExtension {
	
	/**
	 * 根据商品id查询商品规格数据。
	 * @author wangjb 2019年6月10日。
	 * @param goodid 商品ID。
	 * @return
	 */
	List<Map<String, Object>> getGoodSpecsByGoodIdDao(Long goodid);
	
	/**
	 * 根据商品id和规格查询规格重复。
	 * @author wangjb 2019年6月17日。
	 * @param goodid 商品id。
	 * @param specdefine 商品规格。
	 * @param entityid 规格id。
	 * @return
	 */
	Boolean getSpecRepeatByGoodIdAndSpecDefineDao(Long goodid, String specdefine, Long entityid);
	
	/**
	 * 根据商品id和规格名称查询规格对象。
	 * @author wangjb 2019年6月17日。
	 * @param goodid 商品id。
	 * @param specdefine 商品规格。
	 * @return
	 */
	Goodspecs getGoodSpecsByGoodIdAndSpecDefineDao(Long goodid, String specdefine);

}
