/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ShoppingCartsDaoImpl.java
 * History:
 *         2019年6月27日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.ShoppingCartSettlementMerchantExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension;
import club.coderleague.ilsp.entities.Shoppingcarts;

/**
 * -购物车数据访问扩展对象。
 * @author wangjb
 */
public class ShoppingCartsDaoImpl extends AbstractDataRepositoryExtension<Shoppingcarts, Long, SnowflakeV4> implements ShoppingCartsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension#getShoppingCartListByMemberIdDao(Boolean, Long, Boolean)
	 */
	@Override
	public List<Map<String, Object>> getShoppingCartListByMemberIdDao(Boolean buynow, Long memberid, Boolean selectstate) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select ms.entityid as merchantsettleid, g.entityid as goodid, ms.merchantid, gs.goodstock, gs.entitystate as salestate, sc.entityid as shoppingcartid, sc.saleid, sc.selectstate, sc.goodnumber, g.goodname, g.goodorigin, gsp.specdefine, gsp.meterunit, gs.goodprice, (select photopath from Goodphotos where goodid = g.entityid " + 
				" and coverphoto = 1 and entitystate = :istate) as photopath from Shoppingcarts sc, Goodsales gs, Goodspecs gsp, Goods g, MerchantSettles ms " + 
				"where gs.SettleId = ms.entityid and sc.saleid = gs.entityid and gs.specid = gsp.entityid and gsp.goodid = g.entityid and gs.entitystate != :salestate and sc.buynow = :buynow and sc.memberid = :memberid and sc.entitystate = :istate ";
		param.put("salestate", EntityState.INVALID.getValue());
		param.put("buynow", buynow);
		param.put("memberid", memberid);
		param.put("istate", EntityState.VALID.getValue());
		if (selectstate) {
			sql += "and sc.selectstate = :selectstate ";
			param.put("selectstate", selectstate);
		}
		sql += "order by sc.createtime desc";
		return super.queryMapListBySql(sql, param);
	}
	
	/**
	 * @see club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension#getShoppingCartSettlementMerchantByMemberidDao(Boolean, Long, Boolean)
	 */
	@Override
	public List<ShoppingCartSettlementMerchantExtension> getShoppingCartSettlementMerchantByMemberidDao(Boolean buynow, Long memberid, Boolean selectstate) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select entityid as entityid, merchantname as merchantname from Merchants where entityid in (select ms.merchantid from Goodsales gs, Merchantsettles ms, Shoppingcarts sc "
				+ "where sc.saleid = gs.entityid and gs.settleid = ms.entityid and ms.entitystate = :entitystate and sc.buynow = :buynow and sc.memberid = :memberid "; 
		param.put("entitystate", EntityState.VALID.getValue());
		param.put("buynow", buynow);
		param.put("memberid", memberid);
		if (selectstate) {
			sql += "and sc.selectstate = :selectstate ";
			param.put("selectstate", selectstate);
		}
		sql += ")";
		return super.queryCustomBeanListBySql(ShoppingCartSettlementMerchantExtension.class, sql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension#getTheUserGoodCarNum(java.lang.Long)
	 */
	@Override
	public Long getTheUserGoodCarNum(Long memberid) {
		String sql = " select count(SaleId) from ShoppingCarts where MemberId = ?0 and buynow = false";
		return super.queryLongBySql(sql, memberid);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension#executeAmendSelectStateDao(java.util.List, java.lang.Long, java.lang.Boolean)
	 */
	@Override
	public void executeAmendSelectStateDao(List<String> shoppingcartidList, Long userid, Boolean selectstate) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "update Shoppingcarts set selectstate = :selectstate, modifier = :modifier, modifytime = :modifytime where entityid in (:entityid) ";
		param.put("selectstate", selectstate);
		param.put("modifier", userid);
		param.put("modifytime", new Date());
		param.put("entityid", shoppingcartidList);
		super.executeUpdateBySql(sql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension#executeAmendGoodNumberDao(java.lang.String, java.lang.Double)
	 */
	@Override
	public void executeAmendGoodNumberDao(String shoppingcartid, Double goodnumber, Long userid) {
		String sql = "update Shoppingcarts set goodnumber = ?0, modifier = ?1, modifytime = ?2 where entityid = ?3 ";
		super.executeUpdateBySql(sql, goodnumber, userid, new Date(), shoppingcartid);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension#executeDelShoppingCartGoodDao(java.lang.String)
	 */
	@Override
	public void executeDelShoppingCartGoodDao(String shoppingcartid) {
		String sql = "delete from Shoppingcarts where entityid = ?0 ";
		super.executeUpdateBySql(sql, Long.valueOf(shoppingcartid));
	}

	@Override
	public void deleteShoppingCartsBuyNowData(Long memberid) {
		String sql = " delete from ShoppingCarts where MemberId = ?0 and BuyNow = ?1 ";
		super.executeUpdateBySql(sql, memberid, true);
	}

	@Override
	public Shoppingcarts getTheShoppingCartsBuyNowIsTrue(Long memberid) {
		String hql = " from Shoppingcarts where MemberId = ?0 and BuyNow = ?1 ";
		return super.queryEntity(hql, memberid, true);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.ShoppingCartsDaoExtension#deleteByMemberidAndSelectstateAndBuynow(java.lang.Long, boolean, boolean)
	 */
	@Override
	public void deleteByMemberidAndSelectstateAndBuynow(Long memberid, boolean selectstate, boolean buynow) {
		String sql = "delete from shoppingcarts where memberid = ?0 and selectstate = ?1 and buynow = ?2";
		super.executeUpdateBySql(sql, memberid, selectstate, buynow);
	}

}
