/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RolesDaoExtension.java
 * History:
 *         2019年5月18日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.PagingParameters;
import club.coderleague.ilsp.entities.Roles;

/**
 * 角色Dao扩展
 * 
 * @author CJH
 */
public interface RolesDaoExtension extends DataRepositoryExtension {
	/**
	 * 分页查询角色
	 * 
	 * @author CJH 2019年7月2日
	 * @param pagingparameters 分页参数
	 * @param isrecycle 是否回收站
	 * @param keyword 关键字
	 * @return 角色
	 */
	public Page<Roles> findPageByParamsMap(PagingParameters pagingparameters, boolean isrecycle, String keyword);
	
	/**
	 * 根据角色名称查询除指定主键外是否存在
	 * 
	 * @author CJH 2019年5月18日
	 * @param rolename 角色名称
	 * @param entityid 角色主键
	 * @return 是否存在true/false
	 */
	public boolean existsByRolenameNotEntityid(String rolename, Long entityid);
	
	/**
	 * 根据角色主键查询角色
	 * 
	 * @author CJH 2019年5月18日
	 * @param entityid 角色主键
	 * @return 角色
	 */
	public Map<String, Object> findMapByEntityid(Long entityid);
	
	/**
	 * 查询授权角色
	 * 
	 * @author CJH 2019年5月20日
	 * @return 角色
	 */
	public List<Roles> findAuth();
	
	/**
	 * 根据角色主键查询授权功能
	 * 
	 * @author CJH 2019年5月21日
	 * @param roleid 角色主键
	 * @return 功能
	 */
	public List<Map<String, Object>> findAuthFunctionsByRoleid(Long roleid);
}
