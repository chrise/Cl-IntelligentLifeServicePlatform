/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodsMgrDaoExtension.java
 * History:
 *         2019年5月18日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.GoodsExtension;
import club.coderleague.ilsp.common.domain.beans.GoodsSettleExtension;

/**
 * 商品管理数据访问扩展对象。
 * @author wangjb
 */
public interface GoodsMgrDaoExtension {
	
	/**
	 *  获取商品管理分页数据。
	 * @author wangjb 2019年5月18日。
	 * @param params 关键字参数。
	 * @return
	 */
	Page<GoodsExtension> getGoodsMgrDao(Map<String, Object> params) throws Exception;
	
	/**
	 * 获取商户入驻标识。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	List<GoodsSettleExtension> getMerchantIdListDao();
	
	/**
	 * 获取商品分类标识。
	 * @author wangjb 2019年5月18日。
	 * @return
	 */
	List<Map<String, Object>> getGroupIdListDao();
	
	/**
	 * 查询重复。
	 * @author wangjb 2019年5月18日。
	 * @param merchantid 商户id。
	 * @param groupid 分类id。 
	 * @param goodname 商品名称。
	 * @param entityid 商品id。
	 * @return
	 */
	Boolean getRepeatBySettleIdAndGroupIdAndGoodNameDao(Long merchantid, Long groupid, String goodName, Long entityid);
	
	/**
	 * 根据商品id获取商品详情。
	 * @author wangjb 2019年5月19日。
	 * @param entityid 商品id。
	 * @return
	 */
	Map<String, Object> getGoodsDetailByEntityidDao(Long entityid);
	
	/**
	 * 根据商品id查询是否有上架商品。
	 * @author wangjb 2019年6月8日。
	 * @param goodid 商品id。
	 * @return
	 */
	Boolean getPutawayGoodBygoodidDao(Long goodid);
	
	/**
	 * -WeChat 根据商户id查询商品数据。
	 * @author wangjb 2019年7月31日。
	 * @param merchantid 商户id。
	 * @return
	 */
	List<Map<String, Object>> getGoodMgrMerchantidDao(Long merchantid, Map<String, Object> params);
	
	/**
	 * -WeChat 获取商品分类。
	 * @author wangjb 2019年8月3日。
	 * @param merchantid 商户id。
	 * @return
	 */
	List<Map<String, Object>> getGoodGroupListByMerchantidDao(Long merchantid);
	
	/**
	 * -WeChat 获取入驻市场。
	 * @author wangjb 2019年8月3日。
	 * @param merchantid 商户id。
	 * @return
	 */
	List<Map<String, Object>> getMarketListByMerchantidDao(Long merchantid);
}
