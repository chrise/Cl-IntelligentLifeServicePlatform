/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodPhotosMgrDao.java
 * History:
 *         2019年5月28日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.GoodPhotosMgrDaoExtension;
import club.coderleague.ilsp.entities.Goodphotos;

/**
 * 商品上册数据访问对象。
 * @author wangjb
 */
public interface GoodPhotosMgrDao extends DataRepository<Goodphotos, Long>, GoodPhotosMgrDaoExtension{

}
