/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：MarketsDao.java
 * History:
 *         2019年5月11日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.MarketsDaoExtension;
import club.coderleague.ilsp.entities.Markets;

/**
 * 市场表
 * @author Liangjing
 */
public interface MarketsDao extends DataRepository<Markets, String>, MarketsDaoExtension{
	/**
	 * 通过名称，状态 得到对应的市场对象
	 * @author Liangjing 2019年5月12日
	 * @param name
	 * @param number
	 * @param istate
	 * @return
	 */
	public Markets findByMarketnameAndEntitystate(String name,Integer istate);
	/**
	 * 通过entityid得到对应市场对象
	 * @author Liangjing 2019年5月17日
	 * @param entityid
	 * @return
	 */
	public Markets findByEntityid(Long entityid);
}
