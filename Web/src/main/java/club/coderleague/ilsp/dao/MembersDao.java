/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MembersDao.java
 * History:
 *         2019年5月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.MembersDaoExtension;
import club.coderleague.ilsp.entities.Members;

/**
 * 会员Dao
 * 
 * @author CJH
 */
public interface MembersDao extends DataRepository<Members, Long>, MembersDaoExtension {
	/**
	 * 查询指定手机号的会员。
	 * @author Chrise 2019年6月2日
	 * @param phoneHash 手机号哈希。
	 * @return 会员对象。
	 */
	Members findByPhonehash(String phoneHash);
}
