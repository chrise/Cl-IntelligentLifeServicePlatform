/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrderPresplitsDaoExtension.java
 * History:
 *         2019年7月14日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;

/**
 * 订单预拆分Dao扩展
 * 
 * @author CJH
 */
public interface OrderPresplitsDaoExtension extends DataRepositoryExtension {

}
