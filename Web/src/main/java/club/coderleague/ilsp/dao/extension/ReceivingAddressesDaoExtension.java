/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：ReceivingAddressesDaoExtension.java
 * History:
 *         2019年6月3日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.ReceivingAddressesExtension;
import club.coderleague.ilsp.common.domain.beans.ReceivingAddressesSelectBean;
import club.coderleague.ilsp.entities.Receivingaddresses;

/**
 * 收货地址
 * @author Liangjing
 */
public interface ReceivingAddressesDaoExtension {
	
	/**
	 * 获取会员对应收货地址
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<ReceivingAddressesSelectBean> getTheAllReceivingAddresses(Long memberid,Integer pageIndex,Integer pageSize,String key);
	
	/**
	 * 获取默认地址。 (新增编辑查询)
	 * @author wangjb 2019年7月13日。
	 * @param entityid 实体id。
	 * @param defaultaddress 是否默认地址。
	 * @param memberid 会员标识。
	 * @return
	 */
	Receivingaddresses getReceivingAddressesByEntityidAndDefaultaddressDao(Long entityid, Boolean defaultaddress,
			Long memberid);
	
	/**
	 * -根据会员标识id查询收货地址。 (页面 使用)
	 * @author wangjb 2019年7月6日。
	 * @param memberid 会员id。
	 * @param defaultaddress 默认地址。
	 * @param entitystate 有效状态。
	 * @return
	 */
	ReceivingAddressesExtension findByMemberidAndDefaultaddressAndEntitystate(Long memberid, boolean defaultaddress, int entitystate);
	
	/**
	 * -查询有效收货地址。
	 * @author wangjb 2019年7月7日。
	 * @param memberid 会员标识。
	 * @param entitystate 状态。
	 * @param defaultaddress 默认地址。
	 * @return
	 */
	List<ReceivingAddressesExtension> findByMemberidAndEntitystate(Long memberid, int entitystate, boolean defaultaddress);
}
