/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SystemConfigDaoExtension.java
 * History:
 *         2019年5月23日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.extension;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.SystemConfig;

/**
 * 系统配置数据访问对象扩展。
 * @author Chrise
 */
public interface SystemConfigDaoExtension extends DataRepositoryExtension {
	/**
	 * 查询缓存封装类。
	 * @author Chrise 2019年5月23日
	 * @return 缓存封装类对象。
	 */
	SystemConfig queryCacheBean();
}
