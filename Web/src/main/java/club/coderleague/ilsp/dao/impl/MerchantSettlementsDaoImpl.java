/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MerchantSettlementsDaoImpl.java
 * History:
 *         2019年6月15日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.ExportMerchantSettlementExtension;
import club.coderleague.ilsp.common.domain.beans.MerchantSettlementsExtension;
import club.coderleague.ilsp.common.domain.beans.mobile.MoblieMerchantSettlementsInfo;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.PaymentTypeEnums;
import club.coderleague.ilsp.common.domain.enums.SettlementTypeEnums;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.MerchantSettlementsDaoExtension;
import club.coderleague.ilsp.entities.Merchantsettlements;

/**
 * 商户结算数据访问扩展对象实现。
 * @author wangjb
 */
public class MerchantSettlementsDaoImpl extends AbstractDataRepositoryExtension<Merchantsettlements, Long, SnowflakeV4> implements MerchantSettlementsDaoExtension{

	/**
	 * @see club.coderleague.ilsp.dao.extension.MerchantSettlementsDaoExtension#getMerchantSettlementsMgrDao(java.util.Map)
	 */
	@Override
	public Page<MerchantSettlementsExtension> getMerchantSettlementsMgrDao(Map<String, Object> params) {
		Map<String, Object> param = new HashMap<String, Object>();
		String qsql = "select u.entityid as userid, u.username as username, m.entityid as mid, m.merchantname as merchantname, ms.entityid as entityid,m.merchantphone as merchantphone, m.idnumber as idnumber, m.accountname as accountname, m.bcnumber as bcnumber " + 
				", ms.settlementtype as settlementtype, ms.settlementamount as settlementamount, ms.applytime as applytime, ms.revoketime as revoketime, ms.paymenttype as paymenttype, ms.paytime as paytime, ms.entitystate as entitystate, b.bankname as bankname, bs.bankname as bankcard " + 
				"from Merchantsettlements ms left join Merchants m on ms.merchantid = m.entityid left join Users u on ms.handler = u.entityid left join Banks b on m.bankid = b.entityid left join Banks bs on b.parentbank = bs.entityid where 1 = 1 ";
		String csql = "select count(ms.entityid) from Merchantsettlements ms left join Merchants m on ms.merchantid = m.entityid left join Users u on ms.handler = u.entityid left join Banks b on m.bankid = b.entityid left join Banks bs on b.parentbank = bs.entityid where 1 = 1 ";
		
		if (params.get("keyword") != null && StringUtils.isNotBlank(params.get("keyword").toString())) {
			qsql += "and m.entityid = :entityid ";
			csql += "and m.entityid = :entityid ";
            param.put("entityid", Long.valueOf(params.get("keyword").toString()));
        }
		qsql += "order by ms.entityid desc ";
		return super.queryCustomBeanPageBySql(MerchantSettlementsExtension.class, csql, qsql, Integer.parseInt(params.get("pageIndex").toString()), Integer.parseInt(params.get("pageSize").toString()), param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.MerchantSettlementsDaoExtension#getExportMerchantSettlementExtensionByMidDao(java.util.List)
	 */
	@Override
	public List<ExportMerchantSettlementExtension> getExportMerchantSettlementExtensionByMidDao(List<Long> midList) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select m.entityid as entityid, m.merchantname as merchantname, m.merchantphone as merchantphone, m.idnumber as idnumber, m.accountname as accountname, "
				+ "m.bcnumber as bcnumber, m.totalamount as totalamount, b.bankname as bankname, bs.bankname as bankcard from Merchants m left join Banks b on m.bankid = b.entityid left join Banks bs on b.parentbank = bs.entityid where m.entityid in (:ids) ";
		param.put("ids", midList);
		return super.queryCustomBeanListBySql(ExportMerchantSettlementExtension.class, sql, param);
	}

	@Override
	public List<Map<String, Object>> getTheAllMerchantSettlements(Long merchantid, List<String> istate) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer sql = new StringBuffer(" select entityid "
				+ " ,(case EntityState when "+EntityState.OBLIGATION.getValue()+" then '待处理'  when "+EntityState.PAYMENT.getValue()+" then '处理中'  when "+EntityState.UNDO.getValue()+" then '已撤销'  when "+EntityState.SETTLEOVER.getValue()+" then '结算完成'  when "+EntityState.SETTLECANCEL.getValue()+" then '结算取消'  else '' end) as entitystate "
				+ " ,settlementamount,DATE_FORMAT(ApplyTime,'%Y-%m-%H') as applytime from MerchantSettlements where MerchantId = :merchantid ");
		map.put("merchantid", merchantid);
		if(!CommonUtil.isEmpty(istate)) {
			sql.append(" and EntityState IN (:istate)");
			map.put("istate", istate);
		}
		return super.queryMapListBySql(sql.toString(), map);
	}

	@Override
	public MoblieMerchantSettlementsInfo getTheMerchantSettlementInfo(Long entityid) {
		String sql = " select ms.entityid as entityid "
				+ " ,(case ms.EntityState when "+EntityState.OBLIGATION.getValue()+" then '待处理'  when "+EntityState.PAYMENT.getValue()+" then '处理中'  when "+EntityState.UNDO.getValue()+" then '已撤销'  when "+EntityState.SETTLEOVER.getValue()+" then '结算完成'  when "+EntityState.SETTLECANCEL.getValue()+" then '结算取消'  else '' end) as entitystate "
				+ " ,(case ms.SettlementType when "+SettlementTypeEnums.AUTOMATIC.getValue()+" then '自动结算'  when "+SettlementTypeEnums.MANUAL.getValue()+" then '手动结算'  else '' end) as settlementtype "
				+ " ,ms.settlementamount,DATE_FORMAT(ms.ApplyTime,'%Y-%m-%H') as applytime,DATE_FORMAT(ms.RevokeTime,'%Y-%m-%H') as revoketime "
				+ " ,(case ms.PaymentType when "+PaymentTypeEnums.ARTIFICIAL.getValue()+" then '人工打款'  when "+PaymentTypeEnums.AUTOMATIC.getValue()+" then '自动转账'  else '' end) as paymenttype "
				+ " ,DATE_FORMAT(ms.PayTime,'%Y-%m-%H') as paytime,u.entityid as userentityid,u.UserName as username "
				+ " from MerchantSettlements ms left join Users u on ms.Handler = u.entityid where ms.EntityId = ?0 ";
		return super.queryCustomBeanBySql(MoblieMerchantSettlementsInfo.class, sql, entityid);
	}

}
