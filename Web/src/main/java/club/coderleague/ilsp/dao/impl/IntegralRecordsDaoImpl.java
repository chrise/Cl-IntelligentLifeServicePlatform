/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IntegralRecordsDaoImpl.java
 * History:
 *         2019年5月30日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.domain.enums.IntegralRecordType;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.IntegralRecordsDaoExtension;
import club.coderleague.ilsp.entities.Integralrecords;

/**
 * 积分记录DaoImpl
 * 
 * @author CJH
 */
public class IntegralRecordsDaoImpl extends AbstractDataRepositoryExtension<Integralrecords, Long, SnowflakeV4> implements IntegralRecordsDaoExtension {

	/**
	 * @see club.coderleague.ilsp.dao.extension.IntegralRecordsDaoExtension#findExpireByMemberid(java.lang.Long)
	 */
	@Override
	public List<Integralrecords> findExpireByMemberid(Long memberid) {
		String jpql = "from Integralrecords ir where ir.memberid = ?0 and ir.entitystate = ?1"
			+ " and ir.recordtype = ?2 and (ir.remainedintegral - ir.lockedintegral) > 0 and"
			+ " (year(now()) - year(ir.recordtime)) > (select sc.integraltimeout from Systemconfigs sc where sc.entitystate = ?1)";
		return super.queryEntityList(jpql, memberid, EntityState.VALID.getValue(), IntegralRecordType.INCOME.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.IntegralRecordsDaoExtension#findValidByMemberid(java.lang.Long)
	 */
	@Override
	public List<Integralrecords> findValidByMemberid(Long memberid) {
		String jpql = "from Integralrecords ir where ir.memberid = ?0 and ir.entitystate = ?1"
			+ " and ir.recordtype = ?2 and (ir.remainedintegral - ir.lockedintegral) > 0 and"
			+ " (year(now()) - year(ir.recordtime)) <= (select sc.integraltimeout from Systemconfigs sc where sc.entitystate = ?1)"
			+ " order by ir.recordtime asc";
		return super.queryEntityList(jpql, memberid, EntityState.VALID.getValue(), IntegralRecordType.INCOME.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.IntegralRecordsDaoExtension#findLockedByMemberid(java.lang.Long)
	 */
	@Override
	public List<Integralrecords> findLockedByMemberid(Long memberid) {
		String jpql = "from Integralrecords ir where ir.memberid = ?0 and ir.entitystate = ?1 and ir.recordtype = ?2"
			+ " and ir.lockedintegral > 0 order by ir.recordtime asc";
		return super.queryEntityList(jpql, memberid, EntityState.VALID.getValue(), IntegralRecordType.INCOME.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.IntegralRecordsDaoExtension#isExpireByEntityid(java.lang.Long)
	 */
	@Override
	public boolean isExpireByEntityid(Long entityid) {
		String sql = "select exists(select 1 from Integralrecords ir where ir.entityid = ?0 and"
			+ " (year(now()) - year(ir.recordtime)) > (select sc.integraltimeout from Systemconfigs sc where sc.entitystate = ?1))";
		return super.queryLongBySql(sql, entityid, EntityState.VALID.getValue()) > 0;
	}
	
	@Override
	public Page<Integralrecords> getTheAllIntegralRecords(Long memberid, Integer pageIndex, Integer pageSize, String key) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer qsql = new StringBuffer(" select ir.* from IntegralRecords ir where ir.MemberId = :memberid and ir.EntityState = :istate");
		StringBuffer csql = new StringBuffer(" select count(ir.entityid) from IntegralRecords ir where ir.MemberId = :memberid and ir.EntityState = :istate ");
		map.put("memberid", memberid);
		map.put("istate", EntityState.VALID.getValue());
		if(!CommonUtil.isEmpty(key)) {
			qsql.append("");
			csql.append("");
			map.put("key", "%"+key+"%");
		}
		return super.queryEntityPageBySql(csql.toString(), qsql.toString(), pageIndex, pageSize, map);
	}

}
