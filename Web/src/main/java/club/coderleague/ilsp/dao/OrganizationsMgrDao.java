/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrganizationsMgrDao.java
 * History:
 *         2019年5月11日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.OrganizationsMgrDaoExtension;
import club.coderleague.ilsp.entities.Organizations;

/**
 * 机构管理数据访问对象。
 * @author wangjb
 */
public interface OrganizationsMgrDao extends DataRepository<Organizations, Long>, OrganizationsMgrDaoExtension{

	/**
	 * 根据数据id查询子级。
	 * @author wangjb 2019年5月12日。
	 * @param dataid 数据id。
	 * @return
	 */
	List<Organizations> findByParentorg(Long dataid);

	/**
	 * 根据状态查询所有机构
	 * 
	 * @author CJH 2019年5月20日
	 * @param entitystate 状态
	 * @return 机构
	 */
	public List<Organizations> findAllByEntitystate(Integer entitystate);

}
