/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：GoodGroupsDaoImpl.java
 * History:
 *         2019年5月12日: Initially created, Liangjing.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.common.util.CommonUtil;
import club.coderleague.ilsp.dao.extension.GoodGroupsDaoExtension;
import club.coderleague.ilsp.entities.Goodgroups;

/**
 * 商品分类
 * @author Liangjing
 */
public class GoodGroupsDaoImpl extends AbstractDataRepositoryExtension<Goodgroups, String, SnowflakeV4> implements GoodGroupsDaoExtension {

	@Override
	public List<Goodgroups> getTheAllGoodGroup(String key, boolean isrecycle) {
		Map<String, Object> map = new HashMap<String, Object>();
		StringBuffer hql = new StringBuffer(" from Goodgroups where EntityState = :istate ");
		if(!isrecycle) {
			map.put("istate", EntityState.VALID.getValue());
		}else {
			map.put("istate", EntityState.INVALID.getValue());
		}
		if(!CommonUtil.isEmpty(key)) {
			hql.append(" and GroupName like :key ");
			map.put("key", "%"+key+"%");
		}
		return super.queryEntityList(hql.toString(), map);
	}

	@Override
	public Map<String, Object> getTheGoodGroupLookInfoPageData(String entityid) {
		String sql = " select g.GroupName as groupname,(select gs.GroupName from Goodgroups gs where gs.EntityId = g.ParentGroup) as parentgroupname, g.GroupDesc as groupdesc, g.GroupPicture as grouppicture from Goodgroups g where g.EntityId = ?0 ";
		return super.queryMapBySql(sql, entityid);
	}

	@Override
	public List<Map<String, Object>> getTheUpdatePageLoadParentGoods(String entityid) {
		Map<String,Object> map = new HashMap<String, Object>();
		if(CommonUtil.isEmpty(entityid)) {
			String sql = " select EntityId as id,ParentGroup as pid,GroupName as name from GoodGroups where EntityState = :istate and ParentGroup is null "
					+ " union select gp.EntityId as id,gp.ParentGroup as pid,gp.GroupName as name from GoodGroups gp where gp.EntityState = :istate and gp.ParentGroup IN (select gp1.EntityId from GoodGroups gp1 where gp1.EntityState = :istate and gp1.ParentGroup is null)";
			map.put("istate", EntityState.VALID.getValue());
			return super.queryMapListBySql(sql, map);
		}else {
			String sql = " select EntityId as id,ParentGroup as pid,GroupName as name from GoodGroups where EntityState = :istate and ParentGroup is null and entityid != :entityid "
					+ " union select gp.EntityId as id,gp.ParentGroup as pid,gp.GroupName as name from GoodGroups gp where gp.EntityState = :istate and gp.ParentGroup IN (select gp1.EntityId from GoodGroups gp1 where gp1.EntityState = :istate and gp1.ParentGroup is null and gp1.entityid != :entityid ) and gp.entityid != :entityid ";
			map.put("istate", EntityState.VALID.getValue());
			map.put("entityid", entityid);
			return super.queryMapListBySql(sql, map);
		}
	}

	@Override
	public List<Map<String, Object>> getTheMobileParentGoods() {
		String sql = " select concat(EntityId) as entityid,GroupName as groupname from GoodGroups where EntityState = ?0 and ParentGroup is null ";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue());
	}

	@Override
	public List<Map<String, Object>> getTheMobileSecondSonGoods(Long parentid) {
		String sql = " select concat(EntityId) as entityid,GroupName as groupname from GoodGroups where EntityState = ?0 and ParentGroup = ?1 ";
		return super.queryMapListBySql(sql, EntityState.VALID.getValue(), parentid);
	}

	@Override
	public List<Map<String, Object>> getTheMobileThreeSonGoods(Long parentid, String path) {
		String sql = " select concat(EntityId) as entityid,GroupName as groupname,CONCAT(?0,GroupPicture) as grouppicture from GoodGroups where EntityState = ?1 and ParentGroup = ?2 ";
		return super.queryMapListBySql(sql, path, EntityState.VALID.getValue(), parentid);
	}

	@Override
	public String getTheGoodGroupNameByEntityid(Long entityid) {
		String sql = " select GroupName from GoodGroups where EntityId = ?0 ";
		return super.queryStringBySql(sql, entityid);
	}

	@Override
	public List<Map<String, Object>> getTheAllGoodGroupFilterConditions(Long goodgroupid, String key) {
		Map<String,Object> map = new HashMap<String, Object>();
		StringBuffer sql = new StringBuffer(" select concat(gg.entityid) as entityid, gg.GroupName as name from GoodGroups gg,Goods g where gg.EntityId = g.GroupId and g.EntityState = :istate and gg.EntityState = :istate ");
		if(!CommonUtil.isEmpty(goodgroupid)) {
			sql.append(" and g.GroupId = :goodgroupid ");
			map.put("goodgroupid", goodgroupid);
		}
		if(!CommonUtil.isEmpty(key)) {
			sql.append(" and g.GoodName like :key ");
			map.put("key", "%"+key+"%");
		}
		map.put("istate", EntityState.VALID.getValue());
		sql.append(" group by gg.entityid, gg.GroupName ");
		return super.queryMapListBySql(sql.toString(), map);
	}
	

}
