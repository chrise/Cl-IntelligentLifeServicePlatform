/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MembersDaoExtension.java
 * History:
 *         2019年5月27日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.MobileUser;
import club.coderleague.ilsp.common.domain.beans.UserSession;
import club.coderleague.ilsp.common.domain.enums.MemberBindType;
import club.coderleague.ilsp.entities.Members;

/**
 * 会员Dao扩展
 * 
 * @author CJH
 */
public interface MembersDaoExtension extends DataRepositoryExtension {
	/**
	 * 查询存在过期钻石的会员
	 * 
	 * @author CJH 2019年5月28日
	 * @return 会员
	 */
	public List<Members> findExistsExpireDiamond();
	
	/**
	 * 查询存在过期积分的会员
	 * 
	 * @author CJH 2019年5月30日
	 * @return 会员
	 */
	public List<Members> findExistsExpireIntegral();
	
	/**
	 * 查询用户会话。
	 * @author Chrise 2019年5月31日
	 * @param bindType 绑定类型。
	 * @param bindId 绑定标识。
	 * @return 用户会话对象。
	 */
	UserSession queryUserSession(MemberBindType bindType, String bindId);
	
	/**
	 * 修改登录时间。
	 * @author Chrise 2019年6月3日
	 * @param member 会员标识。
	 */
	void updateLoginTime(long member);
	
	/**
	 * 查询绑定的手机号。
	 * @author Chrise 2019年6月2日
	 * @param bindType 绑定类型。
	 * @param bindId 绑定标识。
	 * @return 手机号。
	 */
	String queryBoundPhone(MemberBindType bindType, String bindId);
	
	/**
	 * 获取所有的会员
	 * @author Liangjing 2019年6月1日
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<Members> getTheAllMembers(Integer pageIndex,Integer pageSize,String key);
	
	/**
	 * 查询可用钻石数。
	 * @author Chrise 2019年6月19日
	 * @param member 会员标识。
	 * @return 可用钻石数。
	 */
	Integer queryAvailableDiamond(long member);
	/**
	 * 获取手机用户个人信息
	 * @author Liangjing 2019年8月19日
	 * @param entityid
	 * @return
	 */
	public MobileUser getTheMobileUserInfo(Long entityid);
	
	/**
	 * -根据会员id获取会员可使用得钻石数量。
	 * @author wangjb 2019年8月19日。
	 * @param memberid 会员Id。
	 * @return
	 */
	Long getMemberDiamondByMemberIdDao(Long memberid);
	
	/**
	 * -根据会员id获取会员可使用得积分数量。
	 * @author wangjb 2019年8月19日。
	 * @param memberid 会员Id。
	 * @return
	 */
	Long getMemberIntegralByMemberIdDao(Long memberid); 
	
	/**
	 * -根据会员id获取会员待付款数量。
	 * @author wangjb 2019年8月20日。
	 * @param memberid 会员Id。
	 * @return
	 */
	Long getObligationByMemberIdDao(Long memberid);
	
	/**
	 * -根据会员id获取会员待收货数量。
	 * @author wangjb 2019年8月20日。
	 * @param memberid 会员Id。
	 * @return
	 */
	Long getWaitReceivByMemberIdDao(Long memberid);
}
