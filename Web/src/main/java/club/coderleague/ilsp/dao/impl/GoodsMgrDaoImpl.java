/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：GoodsMgrDaoImpl.java
 * History:
 *         2019年5月18日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.GoodsExtension;
import club.coderleague.ilsp.common.domain.beans.GoodsSettleExtension;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension;
import club.coderleague.ilsp.entities.Goods;

/**
 * 商品管理数据访问对象实现。
 * @author wangjb
 */
public class GoodsMgrDaoImpl extends AbstractDataRepositoryExtension<Goods, Long, SnowflakeV4> implements GoodsMgrDaoExtension{

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getGoodsMgrDao()
	 */
	@Override
	public Page<GoodsExtension> getGoodsMgrDao(Map<String, Object> params) throws Exception{
		Map<String, Object> param = new HashMap<String, Object>();
		String qsql = "select g.entityid as entityid, g.goodname as goodname, g.goodorigin as goodorigin, g.gooddesc as gooddesc, m.merchantname as merchantname, m.entityid as mid, gg.groupname as groupname, g.entitystate as entitystate " 
				+ "from Goods g, Merchants m, Goodgroups gg where g.GroupId = gg.EntityId and g.merchantid = m.entityid and "
				+ "m.entitystate = :istate and gg.entitystate = :istate and g.entitystate = :entitystate ";
		String csql = "select count(g.entityid) from Goods g, Merchants m, Goodgroups gg where g.groupid = gg.entityid and g.merchantid = m.entityid "
				+ "and m.entitystate = :istate and gg.entitystate = :istate and g.entitystate = :entitystate ";
		param.put("istate", EntityState.VALID.getValue());
		boolean isrecycle = Boolean.valueOf((String) params.get("isrecycle"));
		int entitystate = isrecycle ? EntityState.INVALID.getValue() : EntityState.VALID.getValue();
		param.put("entitystate", entitystate);
		if (params.get("keyword") != null && StringUtils.isNotBlank(params.get("keyword").toString())) {
			qsql += "and (g.goodname like :keyword or gg.groupname like :keyword or g.goodorigin like :keyword) ";
			csql += "and (g.goodname like :keyword or gg.groupname like :keyword or g.goodorigin like :keyword) ";
            param.put("keyword", "%" + (String) params.get("keyword") + "%");
        }
		
		qsql += "order by m.entityid, g.createtime desc ";
		return super.queryCustomBeanPageBySql(GoodsExtension.class, csql, qsql, Integer.parseInt(params.get("pageIndex").toString()), Integer.parseInt(params.get("pageSize").toString()), param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getSettleIdListDao()
	 */
	@Override
	public List<GoodsSettleExtension> getMerchantIdListDao() {
		String sql = "select m.entityid as entityid, m.merchantname as merchantname from Merchants m where m.entitystate = ?0 ";
		return super.queryCustomBeanListBySql(GoodsSettleExtension.class, sql, EntityState.VALID.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getGroupIdListDao()
	 */
	@Override
	public List<Map<String, Object>> getGroupIdListDao() {
		String sql = "select entityid, parentgroup as pid, groupname from Goodgroups where entitystate = ?0";
		return super.queryMapListBySql(sql,  EntityState.VALID.getValue());
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getRepeatBySettleIdAndGroupIdAndGoodNameDao(java.lang.Long, java.lang.Long, java.lang.String)
	 */
	@Override
	public Boolean getRepeatBySettleIdAndGroupIdAndGoodNameDao(Long merchantid, Long groupid, String goodName, Long entityid) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select count(entityid) from Goods where merchantid = :merchantid and groupid = :groupid and goodname = :goodname ";
		param.put("merchantid", merchantid);
		param.put("groupid", groupid);
		param.put("goodname", goodName);
		if (entityid != null) {
			sql += "and entityid != :entityid ";
			param.put("entityid", entityid);
		}
		Long count = super.queryLongBySql(sql, param);
		return (count > 0);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getGoodsDetailByEntityidDao(java.lang.Long)
	 */
	@Override
	public Map<String, Object> getGoodsDetailByEntityidDao(Long entityid) {
		String sql = "select g.entityid, gp.groupname, g.goodname, g.goodorigin, g.gooddesc, g.merchantid "
				+ "from Goods g, Goodgroups gp where g.groupid = gp.entityid and g.entityid = ?0 ";
		return super.queryMapBySql(sql, entityid);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getPutawayGoodBygoodidDao(java.lang.Long)
	 */
	@Override
	public Boolean getPutawayGoodBygoodidDao(Long goodid) {
		String sql = "select count(gs.entityid) from GoodSpecs g, Goodsales gs where g.entityid = gs.specid and g.goodid = ?0 and gs.entitystate != ?1 ";
		Long count = super.queryLongBySql(sql, goodid, EntityState.INVALID.getValue());
		return (count > 0);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getGoodMgrMerchantidDao(java.lang.Long)
	 */
	@Override
	public List<Map<String, Object>> getGoodMgrMerchantidDao(Long merchantid, Map<String, Object> params) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select g.goodname as goodname, g.goodorigin as goodorigin, g.gooddesc as gooddesc, gg.groupname as groupname, gs.specdefine as specdefine, gs.meterunit as meterunit, gsa.goodprice as goodprice, gsa.entitystate as salestate, gsa.goodstock as goodstock, gsa.entityid as saleid," + 
				"m.marketname as marketname, (select photopath from Goodphotos where goodid = g.entityid and coverphoto = 1 and entitystate = :istate) as photopath " + 
				"from Goods g, Goodspecs gs, Goodsales gsa, Merchantsettles ms, Goodgroups gg, Markets m where g.entityid = gs.goodid and g.groupid = gg.entityid " + 
				"and gs.entityid = gsa.specid and ms.entityid = gsa.settleid and ms.marketid = m.entityid and g.merchantid = :mid and ms.merchantid = :mid " + 
				"and g.entitystate = :istate and gs.entitystate = :istate and ms.entitystate = :istate and m.entitystate = :istate ";
		param.put("mid", merchantid);
		param.put("istate", EntityState.VALID.getValue());
		// 分类。
		if (params.get("groupid") != null && StringUtils.isNotBlank(params.get("groupid").toString())) {
			sql += "and g.groupid = :groupid ";
            param.put("groupid", params.get("groupid"));
        }
		// 市场。
		if (params.get("marketid") != null && StringUtils.isNotBlank(params.get("marketid").toString())) {
			sql += "and ms.marketid = :marketid ";
            param.put("marketid", params.get("marketid"));
        }
		// 状态。
		if (params.get("salestate") != null && StringUtils.isNotBlank(params.get("salestate").toString())) {
			sql += "and gsa.entitystate = :salestate ";
			param.put("salestate", params.get("salestate"));
        } else {
        	sql += "and gsa.entitystate in (:salestate) ";
        	param.put("salestate", Arrays.asList(new Integer[]{EntityState.VALID.getValue(), EntityState.PUBLISHED.getValue()}));
        }
		sql += "order by gsa.entitystate desc";
		return super.queryMapListBySql(sql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getGoodGroupListByMerchantidDao(java.lang.Long)
	 */
	@Override
	public List<Map<String, Object>> getGoodGroupListByMerchantidDao(Long merchantid) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select g.groupid, gg.groupname from Goods g, Goodgroups gg where g.groupid = gg.entityid and g.entitystate = :istate and gg.entitystate = :istate and g.merchantid = :mid group by g.groupid, gg.groupname, g.createtime desc";
		param.put("mid", merchantid);
		param.put("istate", EntityState.VALID.getValue());
		return super.queryMapListBySql(sql, param);
	}

	/**
	 * @see club.coderleague.ilsp.dao.extension.GoodsMgrDaoExtension#getMarketListByMerchantidDao(java.lang.Long)
	 */
	@Override
	public List<Map<String, Object>> getMarketListByMerchantidDao(Long merchantid) {
		Map<String, Object> param = new HashMap<String, Object>();
		String sql = "select ms.marketid, m.marketname from Markets m, Merchantsettles ms where m.entityid = ms.marketid and m.entitystate = :istate and ms.entitystate = :istate and ms.merchantid = :mid group by ms.marketid, m.marketname ";
		param.put("mid", merchantid);
		param.put("istate", EntityState.VALID.getValue());
		return super.queryMapListBySql(sql, param);
	}

}
