/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：SystemConfigDaoImpl.java
 * History:
 *         2019年5月23日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.impl;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.SystemConfig;
import club.coderleague.ilsp.common.domain.enums.EntityState;
import club.coderleague.ilsp.dao.extension.SystemConfigDaoExtension;
import club.coderleague.ilsp.entities.Systemconfigs;

/**
 * 系统配置数据访问对象实现。
 * @author Chrise
 */
public class SystemConfigDaoImpl extends AbstractDataRepositoryExtension<Systemconfigs, Long, SnowflakeV4> implements SystemConfigDaoExtension {
	/**
	 * @see club.coderleague.ilsp.dao.extension.SystemConfigDaoExtension#queryCacheBean()
	 */
	@Override
	public SystemConfig queryCacheBean() {
		String sql = "select entityid as secretkey, confirmtimeout, paytimeout, receivetimeout, refundtimeout, vercodetimeout, " 
			+ "wxswitch, wxappid, wxappkey, wxmerid, wxapikey, wxcerfile, wxvertoken, wxwelcome, " 
			+ "apswitch, apappid, appubkey, apmerprikey, aynotify, ayappid, ayappkey, aysign, diadeductratio from Systemconfigs where entitystate = ?0";
		return super.queryCustomBeanBySql(SystemConfig.class, sql, EntityState.VALID.getValue());
	}
}
