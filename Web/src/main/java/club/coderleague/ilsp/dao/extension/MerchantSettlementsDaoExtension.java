/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MerchantSettlementsDao.java
 * History:
 *         2019年6月15日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.ilsp.common.domain.beans.ExportMerchantSettlementExtension;
import club.coderleague.ilsp.common.domain.beans.MerchantSettlementsExtension;
import club.coderleague.ilsp.common.domain.beans.mobile.MoblieMerchantSettlementsInfo;

/**
 * 商户结算数据访问扩展对象。
 * @author wangjb
 */
public interface MerchantSettlementsDaoExtension {
	
	/**
	 * 获取商户结算分页数据。
	 * @author wangjb 2019年6月15日。
	 * @param params 页面查询参数。
	 * @return
	 */
	Page<MerchantSettlementsExtension> getMerchantSettlementsMgrDao(Map<String, Object> params);
	
	/**
	 * 导出商户结算查询。
	 * @author wangjb 2019年7月26日。
	 * @param midList 商户id。
	 * @return
	 */
	List<ExportMerchantSettlementExtension> getExportMerchantSettlementExtensionByMidDao(List<Long> midList);
	/**
	 * 获取对应商户所有结算数据
	 * @author Liangjing 2019年7月30日
	 * @param merchantid
	 * @return
	 */
	public List<Map<String,Object>> getTheAllMerchantSettlements(Long merchantid, List<String> istate);
	/**
	 * 获取对应结算数据详情
	 * @author Liangjing 2019年7月30日
	 * @param entityid
	 * @return
	 */
	public MoblieMerchantSettlementsInfo getTheMerchantSettlementInfo(Long entityid);
	
}
