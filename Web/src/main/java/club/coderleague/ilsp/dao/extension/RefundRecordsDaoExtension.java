/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RefundRecordsDaoExtension.java
 * History:
 *         2019年8月8日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import club.coderleague.ilsp.entities.Refundrecords;

/**
 * 退款记录Dao扩展
 * 
 * @author CJH
 */
public interface RefundRecordsDaoExtension {
	/**
	 * 根据订单标识查询有效退款记录是否存在
	 * 
	 * @author CJH 2019年8月8日
	 * @param ordersid 订单标识
	 * @return true/false
	 */
	public boolean existsValidByOrderid(Long ordersid);
	
	/**
	 * 根据订单标识查询待退款的退款记录
	 * 
	 * @author CJH 2019年8月19日
	 * @param orderid 订单标识
	 * @return 退款记录
	 */
	public Refundrecords findRefundingByOrderid(Long orderid);
}
