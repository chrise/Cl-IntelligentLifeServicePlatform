/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MemberBindDaoExtension.java
 * History:
 *         2019年6月2日: Initially created, Chrise.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.entities.Memberbinds;

/**
 * 会员绑定数据访问对象扩展。
 * @author Chrise
 */
public interface MemberBindDaoExtension extends DataRepositoryExtension {
	/**
	 * 查询其他绑定。
	 * @author Chrise 2019年8月19日
	 * @param memberId 会员标识。
	 * @param bindType 绑定类型。
	 * @return 会员绑定集合对象。
	 */
	List<Memberbinds> queryOtherBinds(long memberId, int bindType);
	
	/**
	 * 获取对应会员绑定数据
	 * @author Liangjing 2019年6月1日
	 * @param memberid
	 * @param pageIndex
	 * @param pageSize
	 * @param key
	 * @return
	 */
	public Page<Memberbinds> getTheAllMemberBinds(Long memberid,Integer pageIndex,Integer pageSize,String key);
	/**
	 * 获取会员绑定的信息
	 * @author Liangjing 2019年8月19日
	 * @param memberid
	 * @return
	 */
	public List<Map<String,Object>> getTheMemberBindInfos(Long memberid);
}
