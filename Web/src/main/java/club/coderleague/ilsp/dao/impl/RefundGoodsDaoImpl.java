/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RefundGoodsDaoImpl.java
 * History:
 *         2019年8月8日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.impl;

import club.coderleague.data.jpa.id.support.SnowflakeV4;
import club.coderleague.data.jpa.repository.support.AbstractDataRepositoryExtension;
import club.coderleague.ilsp.dao.extension.RefundGoodsDaoExtension;
import club.coderleague.ilsp.entities.Refundgoods;

/**
 * 退款商品DaoImpl
 * 
 * @author CJH
 */
public class RefundGoodsDaoImpl extends AbstractDataRepositoryExtension<Refundgoods, Long, SnowflakeV4> implements RefundGoodsDaoExtension {

}
