/**
 * Copyright (c) 2019 Coder_League(码农联盟)
 * All rights reserved.
 *
 * File：BanksDao.java
 * History:
 *         2019年7月26日: Initially created, Liangj.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.BanksDaoExtension;
import club.coderleague.ilsp.entities.Banks;

/**
 * 银行管理
 * @author Liangjing
 */
public interface BanksDao extends DataRepository<Banks, Long>, BanksDaoExtension {
	/**
	 * 获取对应银行数据
	 * @author Liangjing 2019年7月26日
	 * @param entityid
	 * @return
	 */
	Banks findByEntityid(Long entityid);
	/**
	 * 获取对应银行数据
	 * @author Liangjing 2019年7月26日
	 * @param parentid
	 * @param bankname
	 * @param istate
	 * @return
	 */
	Banks findByParentbankAndBanknameAndEntitystate(Long parentid, String bankname, Integer istate);
}
