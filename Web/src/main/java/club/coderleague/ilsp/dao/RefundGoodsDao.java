/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RefundGoodsDao.java
 * History:
 *         2019年8月8日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.RefundGoodsDaoExtension;
import club.coderleague.ilsp.entities.Refundgoods;

/**
 * 退款商品Dao
 * 
 * @author CJH
 */
public interface RefundGoodsDao extends DataRepository<Refundgoods, Long>, RefundGoodsDaoExtension {

}
