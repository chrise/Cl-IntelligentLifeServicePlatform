/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：OrdersDaoExtension.java
 * History:
 *         2019年6月6日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;
import java.util.Map;

import club.coderleague.data.jpa.domain.Page;
import club.coderleague.data.jpa.repository.DataRepositoryExtension;
import club.coderleague.ilsp.common.domain.beans.GoodsInfo;
import club.coderleague.ilsp.common.domain.beans.OrderStatisticsExtension;
import club.coderleague.ilsp.common.domain.beans.PayOrder;
import club.coderleague.ilsp.common.domain.beans.RefundOrder;
import club.coderleague.ilsp.common.domain.beans.mobile.MobileOrderDetails;
import club.coderleague.ilsp.common.domain.beans.mobile.MobileOrders;
import club.coderleague.ilsp.common.domain.beans.mobile.MoblieOrderMerchantExtension;
import club.coderleague.ilsp.common.domain.enums.OrderPaymentMode;
import club.coderleague.ilsp.entities.Orders;

/**
 * 订单Dao扩展
 * 
 * @author CJH
 */
public interface OrdersDaoExtension extends DataRepositoryExtension {

	/**
	 * 查询订单统计。
	 * @author wangjb 2019年6月8日。
	 * @param params 页面查询参数。
	 * @return
	 */
	Page<OrderStatisticsExtension> getOrderStatisticsMgrDao(Map<String, Object> params);
	
	/**
	 * 根据订单id查询退款记录数据。
	 * @author wangjb 2019年10月10日。
	 * @param orderid 订单id。
	 * @return
	 */
	Map<String, Object> getRefundRecordByOrderidDao(Long orderid);
	
	/**
	 * 根据订单id获取退款商品。
	 * @author wangjb 2019年10月10日。
	 * @param orderid 订单id。
	 * @param recordid 退款记录id。
	 * @return
	 */
	List<Map<String, Object>> getRefundGoodByOrderidDao(Long orderid, String recordid);
	
	/**
	 * 根据订单id查询订单商品数据。
	 * @author wangjb 2019年6月15日。
	 * @param params 业务查询参数。
	 * @return
	 */
	List<Map<String, Object>> getOrderGoodsByOrderIdDao(Map<String, Object> params);
	
	/**
	 * 查询支付订单。
	 * @author Chrise 2019年6月10日
	 * @param order 订单标识。
	 * @return 支付订单对象。
	 */
	PayOrder queryPayOrder(long order);
	
	/**
	 * 保存支付交易。
	 * @author Chrise 2019年6月20日
	 * @param order 订单标识。
	 * @param mode 支付方式。
	 * @param trade 交易号。
	 */
	void savePayTrade(long order, OrderPaymentMode mode, String trade);
	
	/**
	 * 查询退款订单。
	 * @author Chrise 2019年8月1日
	 * @param order 订单标识。
	 * @return 退款订单对象。
	 */
	RefundOrder queryRefundOrder(long order);
	
	/**
	 * 查询订单。
	 * @author Chrise 2019年6月25日
	 * @param order 订单标识。
	 * @param refund 退款查询。
	 * @return 订单对象。
	 */
	Orders queryPartialOrder(long order, boolean refund);
	
	/**
	 * 根据商品销售标识查询商品信息
	 * 
	 * @author CJH 2019年7月25日
	 * @param saleid 商品销售标识
	 * @return 商品信息
	 */
	public GoodsInfo findGoodsinfoBySaleid(Long saleid);
	
	/**
	 * 查询订单详情
	 * 
	 * @author CJH 2019年8月22日
	 * @param ordersid 订单标识
	 * @return 订单详情
	 */
	public Map<String, Object> findOrdersInfo(Long ordersid);
	
	/**
	 * -根据会员标识获取已完成订单。
	 * @author wangjb 2019年8月23日。
	 * @param memberid 会员标识。
	 * @param keyword 关键字查询。
	 * @return
	 */
	List<MoblieOrderMerchantExtension> getMobileOrderByMemberIdDao(Long memberid, String keyword);
	
	/**
	 * -根据订单id获取订单商户。
	 * @author wangjb 2019年8月24日。
	 * @param orderid 订单id。
	 * @return
	 */
	MoblieOrderMerchantExtension getOrderMerchantByOrderidDao(Long orderid);
	
	/**
	 * -根据会员标识获取退款处理中得订单。
	 * @author wangjb 2019年8月24日。
	 * @param memberid 会员标识。
	 * @param refundstate 退款状态。
	 * @param keyword 关键字查询。
	 * @return
	 */
	List<MoblieOrderMerchantExtension> getRefundHandleByMemberIdDao(Long memberid, List<Integer> refundstate, String keyword);
	
	/**
	 * -退款处理中查询详情。
	 * @author wangjb 2019年8月24日。
	 * @param orderid 订单id。
	 * @return
	 */
	MoblieOrderMerchantExtension getRefundHandleByOrderIdDao(Long orderid);
	
	/**
	 * -退款记录查询详情。
	 * @author wangjb 2019年8月24日。
	 * @param orderid 订单id。
	 * @return
	 */
	MoblieOrderMerchantExtension getRefundRecordByOrderIdDao(Long orderid);
	/**
	 *  获取会员对应待付款订单数量
	 * @author Liangjing 2019年8月28日
	 * @param memberid
	 * @return
	 */
	public Long getTheMemberWaitPayOrders(Long memberid);
	/**
	 * 获取会员对应待收货单数量
	 * @author Liangjing 2019年9月4日
	 * @param memberid
	 * @return
	 */
	public Long getTheMemberWaitTakeOrders(Long memberid);
	/**
	 * 查询会员对应参数的订单详情
	 * @author Liangjing 2019年8月29日
	 * @param memberid
	 * @param entityidState
	 * @param deliveryState
	 * @param paymentState
	 * @return
	 */
	public List<MobileOrders> getTheOrdersByMemberidAndTabIndex(Long memberid, Integer entityidState, Integer deliveryState, Integer paymentState);
	/**
	 * 获取对应订单详情
	 * @author Liangjing 2019年8月29日
	 * @param orderid
	 * @param tabIndex
	 * @return
	 */
	public MobileOrderDetails getTheMobileOrderDetailsByOrderidAndTabIndex(Long orderid, Integer tabIndex);
	/**
	 * 获取订单不可退款类型（1.超时不可退   2.订单内所有商品不可退   3.已经申请退款）
	 * @author Liangjing 2019年9月5日
	 * @param orderid
	 * @return
	 */
	public Long getTheOrderIsNoRefundType(String orderid);
}
