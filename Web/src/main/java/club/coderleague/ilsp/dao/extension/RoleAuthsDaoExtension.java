/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：RoleAuthsDaoExtension.java
 * History:
 *         2019年5月18日: Initially created, CJH.
 */
package club.coderleague.ilsp.dao.extension;

import java.util.List;

import club.coderleague.data.jpa.repository.DataRepositoryExtension;

/**
 * 角色授权Dao扩展
 * 
 * @author CJH
 */
public interface RoleAuthsDaoExtension extends DataRepositoryExtension {
	/**
	 * 根据角色主键查询授权功能主键
	 * 
	 * @author CJH 2019年5月20日
	 * @param roleid 角色主键
	 * @return 功能主键
	 */
	public List<String> findFuncidByRoleid(Long roleid);
	
	/**
	 * 根据角色主键更新除指定主键外角色授权状态
	 * 
	 * @author CJH 2019年5月20日
	 * @param entitystate 更新状态
	 * @param roleid 角色主键
	 * @param validentityids 角色授权主键
	 * @return 执行SQL受影响行数
	 */
	public Integer updateEntitystateByRoleidNotInEntityids(Integer entitystate, Long roleid, List<Long> validentityids);
}
