/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：MerchantSettlementsDao.java
 * History:
 *         2019年6月15日: Initially created, wangjb.
 */
package club.coderleague.ilsp.dao;

import club.coderleague.data.jpa.repository.DataRepository;
import club.coderleague.ilsp.dao.extension.MerchantSettlementsDaoExtension;
import club.coderleague.ilsp.entities.Merchantsettlements;

/**
 * 商户结算数据访问对象。
 * @author wangjb
 */
public interface MerchantSettlementsDao extends DataRepository<Merchantsettlements, Long>, MerchantSettlementsDaoExtension{

}
