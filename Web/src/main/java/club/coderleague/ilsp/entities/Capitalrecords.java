/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 资金记录表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Capitalrecords extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 商户标识。
	 */
	private java.lang.Long merchantid;
	/**
	 * 记录类型（1、收入，2、支出）。
	 */
	private java.lang.Integer recordtype;
	/**
	 * 变化原因（1、订单收入，2、订单退款，3、结算支出，4、支付货款）。
	 */
	private java.lang.Integer changedreason;
	/**
	 * 订单标识。
	 */
	private java.lang.Long orderid;
	/**
	 * 原因描述。
	 */
	private java.lang.String reasondesc;
	/**
	 * 记录总额。
	 */
	private java.lang.Double totalamount;
	/**
	 * 变化金额。
	 */
	private java.lang.Double changedamount;
	/**
	 * 提成费率。
	 */
	private java.lang.Double royaltyrate;
	/**
	 * 提成金额。
	 */
	private java.lang.Double royaltyamount;
	/**
	 * 记录时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date recordtime;

	/**
	 * 资金记录表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param merchantid 商户标识。
	 * @param recordtype 记录类型（1、收入，2、支出）。
	 * @param changedreason 变化原因（1、订单收入，2、订单退款，3、结算支出，4、支付货款）。
	 * @param orderid 订单标识。
	 * @param reasondesc 原因描述。
	 * @param totalamount 记录总额。
	 * @param changedamount 变化金额。
	 * @param royaltyrate 提成费率。
	 * @param royaltyamount 提成金额。
	 * @param recordtime 记录时间。
	 */
	public Capitalrecords(
		  java.lang.Integer entitystate
		, java.lang.Long merchantid
		, java.lang.Integer recordtype
		, java.lang.Integer changedreason
		, java.lang.Long orderid
		, java.lang.String reasondesc
		, java.lang.Double totalamount
		, java.lang.Double changedamount
		, java.lang.Double royaltyrate
		, java.lang.Double royaltyamount
		, java.util.Date recordtime
		) {
		super(entitystate);
		this.merchantid = merchantid;
		this.recordtype = recordtype;
		this.changedreason = changedreason;
		this.orderid = orderid;
		this.reasondesc = reasondesc;
		this.totalamount = totalamount;
		this.changedamount = changedamount;
		this.royaltyrate = royaltyrate;
		this.royaltyamount = royaltyamount;
		this.recordtime = recordtime;
	}
}