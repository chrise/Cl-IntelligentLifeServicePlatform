/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品规格表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Goodspecs extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 商品标识。
	 */
	private java.lang.Long goodid;
	/**
	 * 规格定义。
	 */
	private java.lang.String specdefine;
	/**
	 * 计量单位。
	 */
	private java.lang.String meterunit;

	/**
	 * 商品规格表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，98、级联删除）。
	 * @param goodid 商品标识。
	 * @param specdefine 规格定义。
	 * @param meterunit 计量单位。
	 */
	public Goodspecs(
		  java.lang.Integer entitystate
		, java.lang.Long goodid
		, java.lang.String specdefine
		, java.lang.String meterunit
		) {
		super(entitystate);
		this.goodid = goodid;
		this.specdefine = specdefine;
		this.meterunit = meterunit;
	}
}