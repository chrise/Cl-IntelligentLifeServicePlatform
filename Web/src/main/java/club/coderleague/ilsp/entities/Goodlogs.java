/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品日志表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Goodlogs extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 商品销售标识。
	 */
	private java.lang.Long saleid;
	/**
	 * 变化类型（1、商户修改，2、订单扣减，3、订单返还）。
	 */
	private java.lang.Integer changetype;
	/**
	 * 订单标识。
	 */
	private java.lang.Long orderid;
	/**
	 * 原始状态。
	 */
	private java.lang.Integer originalstate;
	/**
	 * 当前状态。
	 */
	private java.lang.Integer currentstate;
	/**
	 * 原始库存。
	 */
	private java.lang.Double originalstock;
	/**
	 * 当前库存。
	 */
	private java.lang.Double currentstock;
	/**
	 * 原始价格。
	 */
	private java.lang.Double originalprice;
	/**
	 * 当前价格。
	 */
	private java.lang.Double currentprice;

	/**
	 * 商品日志表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param saleid 商品销售标识。
	 * @param changetype 变化类型（1、商户修改，2、订单扣减，3、订单返还）。
	 * @param orderid 订单标识。
	 * @param originalstate 原始状态。
	 * @param currentstate 当前状态。
	 * @param originalstock 原始库存。
	 * @param currentstock 当前库存。
	 * @param originalprice 原始价格。
	 * @param currentprice 当前价格。
	 */
	public Goodlogs(
		  java.lang.Integer entitystate
		, java.lang.Long saleid
		, java.lang.Integer changetype
		, java.lang.Long orderid
		, java.lang.Integer originalstate
		, java.lang.Integer currentstate
		, java.lang.Double originalstock
		, java.lang.Double currentstock
		, java.lang.Double originalprice
		, java.lang.Double currentprice
		) {
		super(entitystate);
		this.saleid = saleid;
		this.changetype = changetype;
		this.orderid = orderid;
		this.originalstate = originalstate;
		this.currentstate = currentstate;
		this.originalstock = originalstock;
		this.currentstock = currentstock;
		this.originalprice = originalprice;
		this.currentprice = currentprice;
	}
}