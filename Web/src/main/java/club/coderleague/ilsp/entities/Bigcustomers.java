/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 大客户表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@club.coderleague.data.jpa.annotation.EnableHibernateEncryption
public class Bigcustomers extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 客户名称。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String customername;
	/**
	 * 名称哈希。
	 */
	private java.lang.String customerhash;
	/**
	 * 客户地址。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String customeraddress;
	/**
	 * 营业执照。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String businesslicense;
	/**
	 * 联系人电话。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String contactsphone;

	/**
	 * 大客户表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，3、冻结，10、待审核，11、未通过，99、删除）。
	 * @param customername 客户名称。
	 * @param customerhash 名称哈希。
	 * @param customeraddress 客户地址。
	 * @param businesslicense 营业执照。
	 * @param contactsphone 联系人电话。
	 */
	public Bigcustomers(
		  java.lang.Integer entitystate
		, java.lang.String customername
		, java.lang.String customerhash
		, java.lang.String customeraddress
		, java.lang.String businesslicense
		, java.lang.String contactsphone
		) {
		super(entitystate);
		this.customername = customername;
		this.customerhash = customerhash;
		this.customeraddress = customeraddress;
		this.businesslicense = businesslicense;
		this.contactsphone = contactsphone;
	}
}