/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 系统配置表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@club.coderleague.data.jpa.annotation.EnableHibernateEncryption
public class Systemconfigs extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 确认超时（分钟）。
	 */
	private java.lang.Integer confirmtimeout;
	/**
	 * 支付超时（分钟）。
	 */
	private java.lang.Integer paytimeout;
	/**
	 * 收货超时（天）。
	 */
	private java.lang.Integer receivetimeout;
	/**
	 * 退款超时（天）。
	 */
	private java.lang.Integer refundtimeout;
	/**
	 * 积分超时（年）。
	 */
	private java.lang.Integer integraltimeout;
	/**
	 * 钻石超时（天）。
	 */
	private java.lang.Integer diamondtimeout;
	/**
	 * 验证码超时（分钟）。
	 */
	private java.lang.Integer vercodetimeout;
	/**
	 * 微信开关。
	 */
	private java.lang.Boolean wxswitch;
	/**
	 * 微信应用标识。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String wxappid;
	/**
	 * 微信应用密钥。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String wxappkey;
	/**
	 * 微信商户标识。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String wxmerid;
	/**
	 * 微信接口密钥。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String wxapikey;
	/**
	 * 微信证书文件。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String wxcerfile;
	/**
	 * 微信验证令牌。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String wxvertoken;
	/**
	 * 微信验证文件。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String wxverfile;
	/**
	 * 微信关注欢迎语。
	 */
	private java.lang.String wxwelcome;
	/**
	 * 支付宝开关。
	 */
	private java.lang.Boolean apswitch;
	/**
	 * 支付宝应用标识。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String apappid;
	/**
	 * 支付宝公钥。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String appubkey;
	/**
	 * 支付宝商户私钥。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String apmerprikey;
	/**
	 * 阿里云通知。
	 */
	private java.lang.Boolean aynotify;
	/**
	 * 阿里云应用标识。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String ayappid;
	/**
	 * 阿里云应用密钥。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String ayappkey;
	/**
	 * 阿里云签名。
	 */
	private java.lang.String aysign;
	/**
	 * 钻石抵扣比例（整百分数）。
	 */
	private java.lang.Integer diadeductratio;

	/**
	 * 系统配置表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param confirmtimeout 确认超时（分钟）。
	 * @param paytimeout 支付超时（分钟）。
	 * @param receivetimeout 收货超时（天）。
	 * @param refundtimeout 退款超时（天）。
	 * @param integraltimeout 积分超时（年）。
	 * @param diamondtimeout 钻石超时（天）。
	 * @param vercodetimeout 验证码超时（分钟）。
	 * @param wxswitch 微信开关。
	 * @param wxappid 微信应用标识。
	 * @param wxappkey 微信应用密钥。
	 * @param wxmerid 微信商户标识。
	 * @param wxapikey 微信接口密钥。
	 * @param wxcerfile 微信证书文件。
	 * @param wxvertoken 微信验证令牌。
	 * @param wxverfile 微信验证文件。
	 * @param wxwelcome 微信关注欢迎语。
	 * @param apswitch 支付宝开关。
	 * @param apappid 支付宝应用标识。
	 * @param appubkey 支付宝公钥。
	 * @param apmerprikey 支付宝商户私钥。
	 * @param aynotify 阿里云通知。
	 * @param ayappid 阿里云应用标识。
	 * @param ayappkey 阿里云应用密钥。
	 * @param aysign 阿里云签名。
	 * @param diadeductratio 钻石抵扣比例（整百分数）。
	 */
	public Systemconfigs(
		  java.lang.Integer entitystate
		, java.lang.Integer confirmtimeout
		, java.lang.Integer paytimeout
		, java.lang.Integer receivetimeout
		, java.lang.Integer refundtimeout
		, java.lang.Integer integraltimeout
		, java.lang.Integer diamondtimeout
		, java.lang.Integer vercodetimeout
		, java.lang.Boolean wxswitch
		, java.lang.String wxappid
		, java.lang.String wxappkey
		, java.lang.String wxmerid
		, java.lang.String wxapikey
		, java.lang.String wxcerfile
		, java.lang.String wxvertoken
		, java.lang.String wxverfile
		, java.lang.String wxwelcome
		, java.lang.Boolean apswitch
		, java.lang.String apappid
		, java.lang.String appubkey
		, java.lang.String apmerprikey
		, java.lang.Boolean aynotify
		, java.lang.String ayappid
		, java.lang.String ayappkey
		, java.lang.String aysign
		, java.lang.Integer diadeductratio
		) {
		super(entitystate);
		this.confirmtimeout = confirmtimeout;
		this.paytimeout = paytimeout;
		this.receivetimeout = receivetimeout;
		this.refundtimeout = refundtimeout;
		this.integraltimeout = integraltimeout;
		this.diamondtimeout = diamondtimeout;
		this.vercodetimeout = vercodetimeout;
		this.wxswitch = wxswitch;
		this.wxappid = wxappid;
		this.wxappkey = wxappkey;
		this.wxmerid = wxmerid;
		this.wxapikey = wxapikey;
		this.wxcerfile = wxcerfile;
		this.wxvertoken = wxvertoken;
		this.wxverfile = wxverfile;
		this.wxwelcome = wxwelcome;
		this.apswitch = apswitch;
		this.apappid = apappid;
		this.appubkey = appubkey;
		this.apmerprikey = apmerprikey;
		this.aynotify = aynotify;
		this.ayappid = ayappid;
		this.ayappkey = ayappkey;
		this.aysign = aysign;
		this.diadeductratio = diadeductratio;
	}
}