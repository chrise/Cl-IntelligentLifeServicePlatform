/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 大客户审核表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Bigcustomeraudits extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 大客户标识。
	 */
	private java.lang.Long customerid;
	/**
	 * 审核人。
	 */
	private java.lang.Long auditor;
	/**
	 * 审核时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date audittime;
	/**
	 * 审核记录。
	 */
	private java.lang.String auditrecord;

	/**
	 * 大客户审核表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param customerid 大客户标识。
	 * @param auditor 审核人。
	 * @param audittime 审核时间。
	 * @param auditrecord 审核记录。
	 */
	public Bigcustomeraudits(
		  java.lang.Integer entitystate
		, java.lang.Long customerid
		, java.lang.Long auditor
		, java.util.Date audittime
		, java.lang.String auditrecord
		) {
		super(entitystate);
		this.customerid = customerid;
		this.auditor = auditor;
		this.audittime = audittime;
		this.auditrecord = auditrecord;
	}
}