/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商户入驻表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Merchantsettles extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 市场标识。
	 */
	private java.lang.Long marketid;
	/**
	 * 商户标识。
	 */
	private java.lang.Long merchantid;
	/**
	 * 入驻序号（起始值1001）。
	 */
	private java.lang.Long settleserials;
	/**
	 * 入驻摊位。
	 */
	private java.lang.String settlestall;

	/**
	 * 商户入驻表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，98、级联删除）。
	 * @param marketid 市场标识。
	 * @param merchantid 商户标识。
	 * @param settleserials 入驻序号（起始值1001）。
	 * @param settlestall 入驻摊位。
	 */
	public Merchantsettles(
		  java.lang.Integer entitystate
		, java.lang.Long marketid
		, java.lang.Long merchantid
		, java.lang.Long settleserials
		, java.lang.String settlestall
		) {
		super(entitystate);
		this.marketid = marketid;
		this.merchantid = merchantid;
		this.settleserials = settleserials;
		this.settlestall = settlestall;
	}
}