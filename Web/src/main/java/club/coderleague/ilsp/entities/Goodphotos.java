/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品相册表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Goodphotos extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 商品标识。
	 */
	private java.lang.Long goodid;
	/**
	 * 相片路径。
	 */
	private java.lang.String photopath;
	/**
	 * 封面相片。
	 */
	private java.lang.Boolean coverphoto;

	/**
	 * 商品相册表构造方法。
	 * @param entitystate 实体状态（1、有效，99、删除）。
	 * @param goodid 商品标识。
	 * @param photopath 相片路径。
	 * @param coverphoto 封面相片。
	 */
	public Goodphotos(
		  java.lang.Integer entitystate
		, java.lang.Long goodid
		, java.lang.String photopath
		, java.lang.Boolean coverphoto
		) {
		super(entitystate);
		this.goodid = goodid;
		this.photopath = photopath;
		this.coverphoto = coverphoto;
	}
}