/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 角色表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Roles extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 角色名称。
	 */
	private java.lang.String rolename;
	/**
	 * 角色描述。
	 */
	private java.lang.String roledesc;

	/**
	 * 角色表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param rolename 角色名称。
	 * @param roledesc 角色描述。
	 */
	public Roles(
		  java.lang.Integer entitystate
		, java.lang.String rolename
		, java.lang.String roledesc
		) {
		super(entitystate);
		this.rolename = rolename;
		this.roledesc = roledesc;
	}
}