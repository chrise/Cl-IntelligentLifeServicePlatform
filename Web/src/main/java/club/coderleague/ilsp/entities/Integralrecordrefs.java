/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 积分记录关系表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Integralrecordrefs extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 支出记录标识。
	 */
	private java.lang.Long paymentrecordid;
	/**
	 * 收入记录标识。
	 */
	private java.lang.Long incomerecordid;
	/**
	 * 支出数量。
	 */
	private java.lang.Integer paymentamount;
	/**
	 * 退款数量。
	 */
	private java.lang.Integer refundamount;

	/**
	 * 积分记录关系表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param paymentrecordid 支出记录标识。
	 * @param incomerecordid 收入记录标识。
	 * @param paymentamount 支出数量。
	 * @param refundamount 退款数量。
	 */
	public Integralrecordrefs(
		  java.lang.Integer entitystate
		, java.lang.Long paymentrecordid
		, java.lang.Long incomerecordid
		, java.lang.Integer paymentamount
		, java.lang.Integer refundamount
		) {
		super(entitystate);
		this.paymentrecordid = paymentrecordid;
		this.incomerecordid = incomerecordid;
		this.paymentamount = paymentamount;
		this.refundamount = refundamount;
	}
}