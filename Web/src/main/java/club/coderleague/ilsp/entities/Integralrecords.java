/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 积分记录表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Integralrecords extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员标识。
	 */
	private java.lang.Long memberid;
	/**
	 * 记录类型（1、收入，2、支出）。
	 */
	private java.lang.Integer recordtype;
	/**
	 * 变化原因（1、自动清零，2、订单收入，3、订单支出）。
	 */
	private java.lang.Integer changedreason;
	/**
	 * 订单标识。
	 */
	private java.lang.Long orderid;
	/**
	 * 原因描述。
	 */
	private java.lang.String reasondesc;
	/**
	 * 变化积分。
	 */
	private java.lang.Integer changedintegral;
	/**
	 * 剩余积分。
	 */
	private java.lang.Integer remainedintegral;
	/**
	 * 锁定积分。
	 */
	private java.lang.Integer lockedintegral;
	/**
	 * 记录时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date recordtime;

	/**
	 * 积分记录表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param memberid 会员标识。
	 * @param recordtype 记录类型（1、收入，2、支出）。
	 * @param changedreason 变化原因（1、自动清零，2、订单收入，3、订单支出）。
	 * @param orderid 订单标识。
	 * @param reasondesc 原因描述。
	 * @param changedintegral 变化积分。
	 * @param remainedintegral 剩余积分。
	 * @param lockedintegral 锁定积分。
	 * @param recordtime 记录时间。
	 */
	public Integralrecords(
		  java.lang.Integer entitystate
		, java.lang.Long memberid
		, java.lang.Integer recordtype
		, java.lang.Integer changedreason
		, java.lang.Long orderid
		, java.lang.String reasondesc
		, java.lang.Integer changedintegral
		, java.lang.Integer remainedintegral
		, java.lang.Integer lockedintegral
		, java.util.Date recordtime
		) {
		super(entitystate);
		this.memberid = memberid;
		this.recordtype = recordtype;
		this.changedreason = changedreason;
		this.orderid = orderid;
		this.reasondesc = reasondesc;
		this.changedintegral = changedintegral;
		this.remainedintegral = remainedintegral;
		this.lockedintegral = lockedintegral;
		this.recordtime = recordtime;
	}
}