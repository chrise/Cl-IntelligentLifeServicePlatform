/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 会员表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@club.coderleague.data.jpa.annotation.EnableHibernateEncryption
public class Members extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员来源（1、微信关注，2、微信扫码，3、支付宝扫码，4、银联扫码）。
	 */
	private java.lang.Integer memberorigin;
	/**
	 * 会员电话。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String memberphone;
	/**
	 * 电话哈希。
	 */
	private java.lang.String phonehash;
	/**
	 * 会员类型（1、普通会员，2、超级会员）。
	 */
	private java.lang.Integer membertype;
	/**
	 * 昵称。
	 */
	private java.lang.String nickname;
	/**
	 * 最近登录。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date lastlogin;

	/**
	 * 会员表构造方法。
	 * @param entitystate 实体状态（1、有效，3、冻结）。
	 * @param memberorigin 会员来源（1、微信关注，2、微信扫码，3、支付宝扫码，4、银联扫码）。
	 * @param memberphone 会员电话。
	 * @param phonehash 电话哈希。
	 * @param membertype 会员类型（1、普通会员，2、超级会员）。
	 * @param nickname 昵称。
	 * @param lastlogin 最近登录。
	 */
	public Members(
		  java.lang.Integer entitystate
		, java.lang.Integer memberorigin
		, java.lang.String memberphone
		, java.lang.String phonehash
		, java.lang.Integer membertype
		, java.lang.String nickname
		, java.util.Date lastlogin
		) {
		super(entitystate);
		this.memberorigin = memberorigin;
		this.memberphone = memberphone;
		this.phonehash = phonehash;
		this.membertype = membertype;
		this.nickname = nickname;
		this.lastlogin = lastlogin;
	}
}