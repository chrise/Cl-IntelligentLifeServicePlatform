/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 用户表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@club.coderleague.data.jpa.annotation.EnableHibernateEncryption
public class Users extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 机构标识。
	 */
	private java.lang.Long orgid;
	/**
	 * 用户名。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String username;
	/**
	 * 用户电话。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String userphone;
	/**
	 * 电话哈希。
	 */
	private java.lang.String phonehash;
	/**
	 * 登录名。
	 */
	private java.lang.String loginname;
	/**
	 * 登录密码。
	 */
	private java.lang.String loginpassword;
	/**
	 * 用户类型（1、管理员，2、普通用户）。
	 */
	private java.lang.Integer usertype;

	/**
	 * 用户表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，3、冻结，99、删除）。
	 * @param orgid 机构标识。
	 * @param username 用户名。
	 * @param userphone 用户电话。
	 * @param phonehash 电话哈希。
	 * @param loginname 登录名。
	 * @param loginpassword 登录密码。
	 * @param usertype 用户类型（1、管理员，2、普通用户）。
	 */
	public Users(
		  java.lang.Integer entitystate
		, java.lang.Long orgid
		, java.lang.String username
		, java.lang.String userphone
		, java.lang.String phonehash
		, java.lang.String loginname
		, java.lang.String loginpassword
		, java.lang.Integer usertype
		) {
		super(entitystate);
		this.orgid = orgid;
		this.username = username;
		this.userphone = userphone;
		this.phonehash = phonehash;
		this.loginname = loginname;
		this.loginpassword = loginpassword;
		this.usertype = usertype;
	}
}