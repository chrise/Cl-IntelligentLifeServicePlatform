/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 角色授权表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Roleauths extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 角色标识。
	 */
	private java.lang.Long roleid;
	/**
	 * 功能标识。
	 */
	private java.lang.Long funcid;

	/**
	 * 角色授权表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效）。
	 * @param roleid 角色标识。
	 * @param funcid 功能标识。
	 */
	public Roleauths(
		  java.lang.Integer entitystate
		, java.lang.Long roleid
		, java.lang.Long funcid
		) {
		super(entitystate);
		this.roleid = roleid;
		this.funcid = funcid;
	}
}