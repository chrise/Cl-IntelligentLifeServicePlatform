/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品销售表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Goodsales extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 原始状态。
	 */
	private java.lang.Integer originalstate;
	/**
	 * 入驻标识。
	 */
	private java.lang.Long settleid;
	/**
	 * 规格标识。
	 */
	private java.lang.Long specid;
	/**
	 * 商品序号（起始值1）。
	 */
	private java.lang.Integer goodserials;
	/**
	 * 商品编号（36进制大写字符串）。
	 */
	private java.lang.String goodnumber;
	/**
	 * 商品价格。
	 */
	private java.lang.Double goodprice;
	/**
	 * 商品库存。
	 */
	private java.lang.Double goodstock;

	/**
	 * 商品销售表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，4、上架，98、级联删除）。
	 * @param originalstate 原始状态。
	 * @param settleid 入驻标识。
	 * @param specid 规格标识。
	 * @param goodserials 商品序号（起始值1）。
	 * @param goodnumber 商品编号（36进制大写字符串）。
	 * @param goodprice 商品价格。
	 * @param goodstock 商品库存。
	 */
	public Goodsales(
		  java.lang.Integer entitystate
		, java.lang.Integer originalstate
		, java.lang.Long settleid
		, java.lang.Long specid
		, java.lang.Integer goodserials
		, java.lang.String goodnumber
		, java.lang.Double goodprice
		, java.lang.Double goodstock
		) {
		super(entitystate);
		this.originalstate = originalstate;
		this.settleid = settleid;
		this.specid = specid;
		this.goodserials = goodserials;
		this.goodnumber = goodnumber;
		this.goodprice = goodprice;
		this.goodstock = goodstock;
	}
}