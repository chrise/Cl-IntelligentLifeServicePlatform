/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 银行表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Banks extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 父级银行。
	 */
	private java.lang.Long parentbank;
	/**
	 * 银行名称。
	 */
	private java.lang.String bankname;

	/**
	 * 银行表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param parentbank 父级银行。
	 * @param bankname 银行名称。
	 */
	public Banks(
		  java.lang.Integer entitystate
		, java.lang.Long parentbank
		, java.lang.String bankname
		) {
		super(entitystate);
		this.parentbank = parentbank;
		this.bankname = bankname;
	}
}