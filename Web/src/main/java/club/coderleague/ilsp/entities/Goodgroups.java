/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品分类表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Goodgroups extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 分类名称。
	 */
	private java.lang.String groupname;
	/**
	 * 父级分类。
	 */
	private java.lang.Long parentgroup;
	/**
	 * 分类图片。
	 */
	private java.lang.String grouppicture;
	/**
	 * 分类描述。
	 */
	private java.lang.String groupdesc;

	/**
	 * 商品分类表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param groupname 分类名称。
	 * @param parentgroup 父级分类。
	 * @param grouppicture 分类图片。
	 * @param groupdesc 分类描述。
	 */
	public Goodgroups(
		  java.lang.Integer entitystate
		, java.lang.String groupname
		, java.lang.Long parentgroup
		, java.lang.String grouppicture
		, java.lang.String groupdesc
		) {
		super(entitystate);
		this.groupname = groupname;
		this.parentgroup = parentgroup;
		this.grouppicture = grouppicture;
		this.groupdesc = groupdesc;
	}
}