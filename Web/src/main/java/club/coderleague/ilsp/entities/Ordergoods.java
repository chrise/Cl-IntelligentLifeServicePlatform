/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 订单商品表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Ordergoods extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单标识。
	 */
	private java.lang.Long orderid;
	/**
	 * 商品销售标识。
	 */
	private java.lang.Long saleid;
	/**
	 * 预拆分标识。
	 */
	private java.lang.Long presplitid;
	/**
	 * 商品名称。
	 */
	private java.lang.String goodname;
	/**
	 * 商品产地。
	 */
	private java.lang.String goodorigin;
	/**
	 * 商品图片。
	 */
	private java.lang.String goodpicture;
	/**
	 * 商品规格。
	 */
	private java.lang.String goodspec;
	/**
	 * 商品价格。
	 */
	private java.lang.Double goodprice;
	/**
	 * 商品数量。
	 */
	private java.lang.Double goodnumber;
	/**
	 * 商品总额。
	 */
	private java.lang.Double goodtotal;
	/**
	 * 支付总额。
	 */
	private java.lang.Double paymenttotal;
	/**
	 * 积分支付。
	 */
	private java.lang.Integer integralpay;
	/**
	 * 钻石支付。
	 */
	private java.lang.Integer diamondpay;
	/**
	 * 支持退款。
	 */
	private java.lang.Boolean supportrefund;

	/**
	 * 订单商品表构造方法。
	 * @param entitystate 实体状态（1、有效，18、已退款）。
	 * @param orderid 订单标识。
	 * @param saleid 商品销售标识。
	 * @param presplitid 预拆分标识。
	 * @param goodname 商品名称。
	 * @param goodorigin 商品产地。
	 * @param goodpicture 商品图片。
	 * @param goodspec 商品规格。
	 * @param goodprice 商品价格。
	 * @param goodnumber 商品数量。
	 * @param goodtotal 商品总额。
	 * @param paymenttotal 支付总额。
	 * @param integralpay 积分支付。
	 * @param diamondpay 钻石支付。
	 * @param supportrefund 支持退款。
	 */
	public Ordergoods(
		  java.lang.Integer entitystate
		, java.lang.Long orderid
		, java.lang.Long saleid
		, java.lang.Long presplitid
		, java.lang.String goodname
		, java.lang.String goodorigin
		, java.lang.String goodpicture
		, java.lang.String goodspec
		, java.lang.Double goodprice
		, java.lang.Double goodnumber
		, java.lang.Double goodtotal
		, java.lang.Double paymenttotal
		, java.lang.Integer integralpay
		, java.lang.Integer diamondpay
		, java.lang.Boolean supportrefund
		) {
		super(entitystate);
		this.orderid = orderid;
		this.saleid = saleid;
		this.presplitid = presplitid;
		this.goodname = goodname;
		this.goodorigin = goodorigin;
		this.goodpicture = goodpicture;
		this.goodspec = goodspec;
		this.goodprice = goodprice;
		this.goodnumber = goodnumber;
		this.goodtotal = goodtotal;
		this.paymenttotal = paymenttotal;
		this.integralpay = integralpay;
		this.diamondpay = diamondpay;
		this.supportrefund = supportrefund;
	}
}