/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商户结算表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Merchantsettlements extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 商户标识。
	 */
	private java.lang.Long merchantid;
	/**
	 * 结算类型（1、自动结算，2、手动结算）。
	 */
	private java.lang.Integer settlementtype;
	/**
	 * 结算金额。
	 */
	private java.lang.Double settlementamount;
	/**
	 * 申请时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date applytime;
	/**
	 * 撤销时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date revoketime;
	/**
	 * 付款类型（1、人工打款，2、自动转账）。
	 */
	private java.lang.Integer paymenttype;
	/**
	 * 付款时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date paytime;
	/**
	 * 付款凭证。
	 */
	private java.lang.String payevidence;
	/**
	 * 取消原因。
	 */
	private java.lang.String cancelreason;
	/**
	 * 处理人。
	 */
	private java.lang.Long handler;

	/**
	 * 商户结算表构造方法。
	 * @param entitystate 实体状态（12、待处理，13、处理中，14、已撤销，15、结算完成，16、结算取消）。
	 * @param merchantid 商户标识。
	 * @param settlementtype 结算类型（1、自动结算，2、手动结算）。
	 * @param settlementamount 结算金额。
	 * @param applytime 申请时间。
	 * @param revoketime 撤销时间。
	 * @param paymenttype 付款类型（1、人工打款，2、自动转账）。
	 * @param paytime 付款时间。
	 * @param payevidence 付款凭证。
	 * @param cancelreason 取消原因。
	 * @param handler 处理人。
	 */
	public Merchantsettlements(
		  java.lang.Integer entitystate
		, java.lang.Long merchantid
		, java.lang.Integer settlementtype
		, java.lang.Double settlementamount
		, java.util.Date applytime
		, java.util.Date revoketime
		, java.lang.Integer paymenttype
		, java.util.Date paytime
		, java.lang.String payevidence
		, java.lang.String cancelreason
		, java.lang.Long handler
		) {
		super(entitystate);
		this.merchantid = merchantid;
		this.settlementtype = settlementtype;
		this.settlementamount = settlementamount;
		this.applytime = applytime;
		this.revoketime = revoketime;
		this.paymenttype = paymenttype;
		this.paytime = paytime;
		this.payevidence = payevidence;
		this.cancelreason = cancelreason;
		this.handler = handler;
	}
}