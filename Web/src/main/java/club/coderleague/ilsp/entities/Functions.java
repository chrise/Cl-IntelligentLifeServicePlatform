/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 功能表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Functions extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 功能名称。
	 */
	private java.lang.String funcname;
	/**
	 * 父级功能。
	 */
	private java.lang.Long parentfunc;
	/**
	 * 功能类型（1、模块，2、节点，3、按钮）。
	 */
	private java.lang.Integer functype;
	/**
	 * 显示图标。
	 */
	private java.lang.String showicon;
	/**
	 * 显示顺序。
	 */
	private java.lang.Integer showorder;
	/**
	 * 调用方法。
	 */
	private java.lang.String invokemethod;
	/**
	 * 右键支持。
	 */
	private java.lang.Boolean rightsupport;
	/**
	 * 授权标识。
	 */
	private java.lang.String authorityid;
	/**
	 * 功能描述。
	 */
	private java.lang.String funcdesc;

	/**
	 * 功能表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param funcname 功能名称。
	 * @param parentfunc 父级功能。
	 * @param functype 功能类型（1、模块，2、节点，3、按钮）。
	 * @param showicon 显示图标。
	 * @param showorder 显示顺序。
	 * @param invokemethod 调用方法。
	 * @param rightsupport 右键支持。
	 * @param authorityid 授权标识。
	 * @param funcdesc 功能描述。
	 */
	public Functions(
		  java.lang.Integer entitystate
		, java.lang.String funcname
		, java.lang.Long parentfunc
		, java.lang.Integer functype
		, java.lang.String showicon
		, java.lang.Integer showorder
		, java.lang.String invokemethod
		, java.lang.Boolean rightsupport
		, java.lang.String authorityid
		, java.lang.String funcdesc
		) {
		super(entitystate);
		this.funcname = funcname;
		this.parentfunc = parentfunc;
		this.functype = functype;
		this.showicon = showicon;
		this.showorder = showorder;
		this.invokemethod = invokemethod;
		this.rightsupport = rightsupport;
		this.authorityid = authorityid;
		this.funcdesc = funcdesc;
	}
}