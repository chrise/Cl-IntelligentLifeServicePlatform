/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 通知模板配置表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Notifytemplateconfigs extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 通知类型（1、手机绑定，2、身份认证，3、到账提醒，4、退款提醒，5、结算提醒）。
	 */
	private java.lang.Integer notifytype;
	/**
	 * 模板代码。
	 */
	private java.lang.String tplcode;
	/**
	 * 模板参数（JSON格式）。
	 */
	private java.lang.String tplparam;

	/**
	 * 通知模板配置表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param notifytype 通知类型（1、手机绑定，2、身份认证，3、到账提醒，4、退款提醒，5、结算提醒）。
	 * @param tplcode 模板代码。
	 * @param tplparam 模板参数（JSON格式）。
	 */
	public Notifytemplateconfigs(
		  java.lang.Integer entitystate
		, java.lang.Integer notifytype
		, java.lang.String tplcode
		, java.lang.String tplparam
		) {
		super(entitystate);
		this.notifytype = notifytype;
		this.tplcode = tplcode;
		this.tplparam = tplparam;
	}
}