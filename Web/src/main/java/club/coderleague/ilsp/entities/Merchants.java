/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商户表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@club.coderleague.data.jpa.annotation.EnableHibernateEncryption
public class Merchants extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 商户名。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String merchantname;
	/**
	 * 商户电话。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String merchantphone;
	/**
	 * 电话哈希。
	 */
	private java.lang.String phonehash;
	/**
	 * 商户微信。
	 */
	private java.lang.String merchantweixin;
	/**
	 * 身份证号。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String idnumber;
	/**
	 * 银行标识。
	 */
	private java.lang.Long bankid;
	/**
	 * 账户名称。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String accountname;
	/**
	 * 银行卡号。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String bcnumber;
	/**
	 * 身份证正面。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String idpositive;
	/**
	 * 身份证反面。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String idnegative;
	/**
	 * 银行卡。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String bankcard;
	/**
	 * 营业执照。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String businesslicense;
	/**
	 * 短信通知。
	 */
	private java.lang.Boolean smnotify;
	/**
	 * 自动结算。
	 */
	private java.lang.Boolean autosettle;
	/**
	 * 账户总额。
	 */
	private java.lang.Double totalamount;
	/**
	 * 锁定金额。
	 */
	private java.lang.Double lockedamount;
	/**
	 * 最近登录。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date lastlogin;

	/**
	 * 商户表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，3、冻结，99、删除）。
	 * @param merchantname 商户名。
	 * @param merchantphone 商户电话。
	 * @param phonehash 电话哈希。
	 * @param merchantweixin 商户微信。
	 * @param idnumber 身份证号。
	 * @param bankid 银行标识。
	 * @param accountname 账户名称。
	 * @param bcnumber 银行卡号。
	 * @param idpositive 身份证正面。
	 * @param idnegative 身份证反面。
	 * @param bankcard 银行卡。
	 * @param businesslicense 营业执照。
	 * @param smnotify 短信通知。
	 * @param autosettle 自动结算。
	 * @param totalamount 账户总额。
	 * @param lockedamount 锁定金额。
	 * @param lastlogin 最近登录。
	 */
	public Merchants(
		  java.lang.Integer entitystate
		, java.lang.String merchantname
		, java.lang.String merchantphone
		, java.lang.String phonehash
		, java.lang.String merchantweixin
		, java.lang.String idnumber
		, java.lang.Long bankid
		, java.lang.String accountname
		, java.lang.String bcnumber
		, java.lang.String idpositive
		, java.lang.String idnegative
		, java.lang.String bankcard
		, java.lang.String businesslicense
		, java.lang.Boolean smnotify
		, java.lang.Boolean autosettle
		, java.lang.Double totalamount
		, java.lang.Double lockedamount
		, java.util.Date lastlogin
		) {
		super(entitystate);
		this.merchantname = merchantname;
		this.merchantphone = merchantphone;
		this.phonehash = phonehash;
		this.merchantweixin = merchantweixin;
		this.idnumber = idnumber;
		this.bankid = bankid;
		this.accountname = accountname;
		this.bcnumber = bcnumber;
		this.idpositive = idpositive;
		this.idnegative = idnegative;
		this.bankcard = bankcard;
		this.businesslicense = businesslicense;
		this.smnotify = smnotify;
		this.autosettle = autosettle;
		this.totalamount = totalamount;
		this.lockedamount = lockedamount;
		this.lastlogin = lastlogin;
	}
}