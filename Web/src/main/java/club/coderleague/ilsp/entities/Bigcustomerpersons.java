/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 大客户人员表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@club.coderleague.data.jpa.annotation.EnableHibernateEncryption
public class Bigcustomerpersons extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 大客户标识。
	 */
	private java.lang.Long customerid;
	/**
	 * 人员姓名。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String personname;
	/**
	 * 人员电话。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String personphone;
	/**
	 * 电话哈希。
	 */
	private java.lang.String phonehash;
	/**
	 * 人员微信。
	 */
	private java.lang.String personweixin;
	/**
	 * 管理员标志。
	 */
	private java.lang.Boolean managerflag;
	/**
	 * 采购员标志。
	 */
	private java.lang.Boolean purchaserflag;
	/**
	 * 最近登录。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date lastlogin;

	/**
	 * 大客户人员表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效）。
	 * @param customerid 大客户标识。
	 * @param personname 人员姓名。
	 * @param personphone 人员电话。
	 * @param phonehash 电话哈希。
	 * @param personweixin 人员微信。
	 * @param managerflag 管理员标志。
	 * @param purchaserflag 采购员标志。
	 * @param lastlogin 最近登录。
	 */
	public Bigcustomerpersons(
		  java.lang.Integer entitystate
		, java.lang.Long customerid
		, java.lang.String personname
		, java.lang.String personphone
		, java.lang.String phonehash
		, java.lang.String personweixin
		, java.lang.Boolean managerflag
		, java.lang.Boolean purchaserflag
		, java.util.Date lastlogin
		) {
		super(entitystate);
		this.customerid = customerid;
		this.personname = personname;
		this.personphone = personphone;
		this.phonehash = phonehash;
		this.personweixin = personweixin;
		this.managerflag = managerflag;
		this.purchaserflag = purchaserflag;
		this.lastlogin = lastlogin;
	}
}