/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 退款商品表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Refundgoods extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 记录标识。
	 */
	private java.lang.Long recordid;
	/**
	 * 商品标识。
	 */
	private java.lang.Long goodid;

	/**
	 * 退款商品表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param recordid 记录标识。
	 * @param goodid 商品标识。
	 */
	public Refundgoods(
		  java.lang.Integer entitystate
		, java.lang.Long recordid
		, java.lang.Long goodid
		) {
		super(entitystate);
		this.recordid = recordid;
		this.goodid = goodid;
	}
}