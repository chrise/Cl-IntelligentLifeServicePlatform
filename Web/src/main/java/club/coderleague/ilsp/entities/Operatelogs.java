/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 操作日志表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Operatelogs extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 操作人。
	 */
	private java.lang.Long operator;
	/**
	 * 人员类型（1、管理员，2、会员，3、大客户，4、商户）。
	 */
	private java.lang.Integer operatortype;
	/**
	 * 操作时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date operatetime;
	/**
	 * 操作地址。
	 */
	private java.lang.String operateaddr;
	/**
	 * 操作描述。
	 */
	private java.lang.String operatedesc;
	/**
	 * 请求类型（1、主动，2、被动）。
	 */
	private java.lang.Integer requesttype;
	/**
	 * 调用方法。
	 */
	private java.lang.String invokemethod;
	/**
	 * 返回结果。
	 */
	private java.lang.Boolean returnresult;
	/**
	 * 请求数据。
	 */
	private java.lang.String requestdata;
	/**
	 * 响应数据。
	 */
	private java.lang.String responsedata;

	/**
	 * 操作日志表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param operator 操作人。
	 * @param operatortype 人员类型（1、管理员，2、会员，3、大客户，4、商户）。
	 * @param operatetime 操作时间。
	 * @param operateaddr 操作地址。
	 * @param operatedesc 操作描述。
	 * @param requesttype 请求类型（1、主动，2、被动）。
	 * @param invokemethod 调用方法。
	 * @param returnresult 返回结果。
	 * @param requestdata 请求数据。
	 * @param responsedata 响应数据。
	 */
	public Operatelogs(
		  java.lang.Integer entitystate
		, java.lang.Long operator
		, java.lang.Integer operatortype
		, java.util.Date operatetime
		, java.lang.String operateaddr
		, java.lang.String operatedesc
		, java.lang.Integer requesttype
		, java.lang.String invokemethod
		, java.lang.Boolean returnresult
		, java.lang.String requestdata
		, java.lang.String responsedata
		) {
		super(entitystate);
		this.operator = operator;
		this.operatortype = operatortype;
		this.operatetime = operatetime;
		this.operateaddr = operateaddr;
		this.operatedesc = operatedesc;
		this.requesttype = requesttype;
		this.invokemethod = invokemethod;
		this.returnresult = returnresult;
		this.requestdata = requestdata;
		this.responsedata = responsedata;
	}
}