/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 退款记录表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Refundrecords extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单标识。
	 */
	private java.lang.Long orderid;
	/**
	 * 申请类型（1、商户申请，2、用户申请，3、后台申请，4、系统申请）。
	 */
	private java.lang.Integer applytype;
	/**
	 * 申请时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date applytime;
	/**
	 * 申请描述。
	 */
	private java.lang.String applydesc;
	/**
	 * 部分退款。
	 */
	private java.lang.Boolean partialrefund;
	/**
	 * 退款总额。
	 */
	private java.lang.Double refundtotal;
	/**
	 * 积分退款。
	 */
	private java.lang.Integer integralrefund;
	/**
	 * 钻石退款。
	 */
	private java.lang.Integer diamondrefund;
	/**
	 * 处理时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date handletime;
	/**
	 * 处理描述。
	 */
	private java.lang.String handledesc;
	/**
	 * 交易号。
	 */
	private java.lang.String tradeno;
	/**
	 * 退款时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date refundtime;

	/**
	 * 退款记录表构造方法。
	 * @param entitystate 实体状态（12、待处理，14、已撤销，17、待退款，18、已退款）。
	 * @param orderid 订单标识。
	 * @param applytype 申请类型（1、商户申请，2、用户申请，3、后台申请，4、系统申请）。
	 * @param applytime 申请时间。
	 * @param applydesc 申请描述。
	 * @param partialrefund 部分退款。
	 * @param refundtotal 退款总额。
	 * @param integralrefund 积分退款。
	 * @param diamondrefund 钻石退款。
	 * @param handletime 处理时间。
	 * @param handledesc 处理描述。
	 * @param tradeno 交易号。
	 * @param refundtime 退款时间。
	 */
	public Refundrecords(
		  java.lang.Integer entitystate
		, java.lang.Long orderid
		, java.lang.Integer applytype
		, java.util.Date applytime
		, java.lang.String applydesc
		, java.lang.Boolean partialrefund
		, java.lang.Double refundtotal
		, java.lang.Integer integralrefund
		, java.lang.Integer diamondrefund
		, java.util.Date handletime
		, java.lang.String handledesc
		, java.lang.String tradeno
		, java.util.Date refundtime
		) {
		super(entitystate);
		this.orderid = orderid;
		this.applytype = applytype;
		this.applytime = applytime;
		this.applydesc = applydesc;
		this.partialrefund = partialrefund;
		this.refundtotal = refundtotal;
		this.integralrefund = integralrefund;
		this.diamondrefund = diamondrefund;
		this.handletime = handletime;
		this.handledesc = handledesc;
		this.tradeno = tradeno;
		this.refundtime = refundtime;
	}
}