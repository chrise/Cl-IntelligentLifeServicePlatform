/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 机构表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Organizations extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 机构名称。
	 */
	private java.lang.String orgname;
	/**
	 * 父级机构。
	 */
	private java.lang.Long parentorg;
	/**
	 * 机构描述。
	 */
	private java.lang.String orgdesc;

	/**
	 * 机构表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param orgname 机构名称。
	 * @param parentorg 父级机构。
	 * @param orgdesc 机构描述。
	 */
	public Organizations(
		  java.lang.Integer entitystate
		, java.lang.String orgname
		, java.lang.Long parentorg
		, java.lang.String orgdesc
		) {
		super(entitystate);
		this.orgname = orgname;
		this.parentorg = parentorg;
		this.orgdesc = orgdesc;
	}
}