/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 市场表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Markets extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 市场名称。
	 */
	private java.lang.String marketname;
	/**
	 * 市场描述。
	 */
	private java.lang.String marketdesc;

	/**
	 * 市场表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param marketname 市场名称。
	 * @param marketdesc 市场描述。
	 */
	public Markets(
		  java.lang.Integer entitystate
		, java.lang.String marketname
		, java.lang.String marketdesc
		) {
		super(entitystate);
		this.marketname = marketname;
		this.marketdesc = marketdesc;
	}
}