/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 用户授权表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Userauths extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户标识。
	 */
	private java.lang.Long userid;
	/**
	 * 角色标识。
	 */
	private java.lang.Long roleid;

	/**
	 * 用户授权表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效）。
	 * @param userid 用户标识。
	 * @param roleid 角色标识。
	 */
	public Userauths(
		  java.lang.Integer entitystate
		, java.lang.Long userid
		, java.lang.Long roleid
		) {
		super(entitystate);
		this.userid = userid;
		this.roleid = roleid;
	}
}