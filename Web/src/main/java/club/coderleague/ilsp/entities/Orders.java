/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 订单表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@club.coderleague.data.jpa.annotation.EnableHibernateEncryption
public class Orders extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员标识。
	 */
	private java.lang.Long memberid;
	/**
	 * 采购员标识。
	 */
	private java.lang.Long purchaserid;
	/**
	 * 商户标识。
	 */
	private java.lang.Long merchantid;
	/**
	 * 市场标识。
	 */
	private java.lang.Long marketid;
	/**
	 * 订单来源（1、线下，2、线上）。
	 */
	private java.lang.Integer orderorigin;
	/**
	 * 订单类型（1、普通，2、活动，3、充值，4、大客户）。
	 */
	private java.lang.Integer ordertype;
	/**
	 * 原始订单。
	 */
	private java.lang.Long originalorder;
	/**
	 * 收货人。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String receiver;
	/**
	 * 省份。
	 */
	private java.lang.Long province;
	/**
	 * 城市。
	 */
	private java.lang.Long city;
	/**
	 * 区县。
	 */
	private java.lang.Long county;
	/**
	 * 街道。
	 */
	private java.lang.Long street;
	/**
	 * 详细地址。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String detailaddress;
	/**
	 * 手机号码。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String mobilephone;
	/**
	 * 订单备注。
	 */
	private java.lang.String orderremark;
	/**
	 * 订单总额。
	 */
	private java.lang.Double ordertotal;
	/**
	 * 商品总额。
	 */
	private java.lang.Double goodtotal;
	/**
	 * 配送状态（1、待发货，2、已发货，3、已收货，4、已退货）。
	 */
	private java.lang.Integer deliverystate;
	/**
	 * 配送方式（1、自提，2、市场配送）。
	 */
	private java.lang.Integer deliverymode;
	/**
	 * 配送时间（1、标准时间，2、特定时间）。
	 */
	private java.lang.Integer deliverytime;
	/**
	 * 起送时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date deliverystarttime;
	/**
	 * 止送时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date deliveryendtime;
	/**
	 * 配送费用。
	 */
	private java.lang.Double deliverycost;
	/**
	 * 支付状态（1、待付款，2、已付款，3、已退款）。
	 */
	private java.lang.Integer paymentstate;
	/**
	 * 支付方式（1、微信，2、支付宝，3、银联）。
	 */
	private java.lang.Integer paymentmode;
	/**
	 * 支付总额。
	 */
	private java.lang.Double paymenttotal;
	/**
	 * 支付交易号。
	 */
	private java.lang.String paymenttradeno;
	/**
	 * 退款交易号。
	 */
	private java.lang.String refundtradeno;
	/**
	 * 交易创建时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date tradecreatetime;
	/**
	 * 交易完成时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date tradecompletetime;
	/**
	 * 退款截止时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date refundendtime;
	/**
	 * 积分支付。
	 */
	private java.lang.Integer integralpay;
	/**
	 * 钻石支付。
	 */
	private java.lang.Integer diamondpay;
	/**
	 * 下单时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date ordertime;
	/**
	 * 确认时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date confirmtime;
	/**
	 * 付款时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date paytime;
	/**
	 * 发货时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date sendtime;
	/**
	 * 收货时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date receivetime;
	/**
	 * 完成时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date completetime;
	/**
	 * 退款时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date refundtime;
	/**
	 * 拆分时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date splittime;
	/**
	 * 拆分原因。
	 */
	private java.lang.String splitreason;
	/**
	 * 部分退款。
	 */
	private java.lang.Boolean partialrefund;
	/**
	 * 退款总额。
	 */
	private java.lang.Double refundtotal;
	/**
	 * 积分退款。
	 */
	private java.lang.Integer integralrefund;
	/**
	 * 钻石退款。
	 */
	private java.lang.Integer diamondrefund;
	/**
	 * 取消类型（1、商户取消，2、用户取消，3、后台取消，4、系统取消）。
	 */
	private java.lang.Integer canceltype;
	/**
	 * 取消时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date canceltime;
	/**
	 * 取消原因。
	 */
	private java.lang.String cancelreason;
	/**
	 * 大客户结算。
	 */
	private java.lang.Boolean customerclose;
	/**
	 * 大客户结算时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date customerclosetime;

	/**
	 * 订单表构造方法。
	 * @param entitystate 实体状态（5、待确认，6、已确认，7、已完成，8、已拆分，9、已取消，99、删除）。
	 * @param memberid 会员标识。
	 * @param purchaserid 采购员标识。
	 * @param merchantid 商户标识。
	 * @param marketid 市场标识。
	 * @param orderorigin 订单来源（1、线下，2、线上）。
	 * @param ordertype 订单类型（1、普通，2、活动，3、充值，4、大客户）。
	 * @param originalorder 原始订单。
	 * @param receiver 收货人。
	 * @param province 省份。
	 * @param city 城市。
	 * @param county 区县。
	 * @param street 街道。
	 * @param detailaddress 详细地址。
	 * @param mobilephone 手机号码。
	 * @param orderremark 订单备注。
	 * @param ordertotal 订单总额。
	 * @param goodtotal 商品总额。
	 * @param deliverystate 配送状态（1、待发货，2、已发货，3、已收货，4、已退货）。
	 * @param deliverymode 配送方式（1、自提，2、市场配送）。
	 * @param deliverytime 配送时间（1、标准时间，2、特定时间）。
	 * @param deliverystarttime 起送时间。
	 * @param deliveryendtime 止送时间。
	 * @param deliverycost 配送费用。
	 * @param paymentstate 支付状态（1、待付款，2、已付款，3、已退款）。
	 * @param paymentmode 支付方式（1、微信，2、支付宝，3、银联）。
	 * @param paymenttotal 支付总额。
	 * @param paymenttradeno 支付交易号。
	 * @param refundtradeno 退款交易号。
	 * @param tradecreatetime 交易创建时间。
	 * @param tradecompletetime 交易完成时间。
	 * @param refundendtime 退款截止时间。
	 * @param integralpay 积分支付。
	 * @param diamondpay 钻石支付。
	 * @param ordertime 下单时间。
	 * @param confirmtime 确认时间。
	 * @param paytime 付款时间。
	 * @param sendtime 发货时间。
	 * @param receivetime 收货时间。
	 * @param completetime 完成时间。
	 * @param refundtime 退款时间。
	 * @param splittime 拆分时间。
	 * @param splitreason 拆分原因。
	 * @param partialrefund 部分退款。
	 * @param refundtotal 退款总额。
	 * @param integralrefund 积分退款。
	 * @param diamondrefund 钻石退款。
	 * @param canceltype 取消类型（1、商户取消，2、用户取消，3、后台取消，4、系统取消）。
	 * @param canceltime 取消时间。
	 * @param cancelreason 取消原因。
	 * @param customerclose 大客户结算。
	 * @param customerclosetime 大客户结算时间。
	 */
	public Orders(
		  java.lang.Integer entitystate
		, java.lang.Long memberid
		, java.lang.Long purchaserid
		, java.lang.Long merchantid
		, java.lang.Long marketid
		, java.lang.Integer orderorigin
		, java.lang.Integer ordertype
		, java.lang.Long originalorder
		, java.lang.String receiver
		, java.lang.Long province
		, java.lang.Long city
		, java.lang.Long county
		, java.lang.Long street
		, java.lang.String detailaddress
		, java.lang.String mobilephone
		, java.lang.String orderremark
		, java.lang.Double ordertotal
		, java.lang.Double goodtotal
		, java.lang.Integer deliverystate
		, java.lang.Integer deliverymode
		, java.lang.Integer deliverytime
		, java.util.Date deliverystarttime
		, java.util.Date deliveryendtime
		, java.lang.Double deliverycost
		, java.lang.Integer paymentstate
		, java.lang.Integer paymentmode
		, java.lang.Double paymenttotal
		, java.lang.String paymenttradeno
		, java.lang.String refundtradeno
		, java.util.Date tradecreatetime
		, java.util.Date tradecompletetime
		, java.util.Date refundendtime
		, java.lang.Integer integralpay
		, java.lang.Integer diamondpay
		, java.util.Date ordertime
		, java.util.Date confirmtime
		, java.util.Date paytime
		, java.util.Date sendtime
		, java.util.Date receivetime
		, java.util.Date completetime
		, java.util.Date refundtime
		, java.util.Date splittime
		, java.lang.String splitreason
		, java.lang.Boolean partialrefund
		, java.lang.Double refundtotal
		, java.lang.Integer integralrefund
		, java.lang.Integer diamondrefund
		, java.lang.Integer canceltype
		, java.util.Date canceltime
		, java.lang.String cancelreason
		, java.lang.Boolean customerclose
		, java.util.Date customerclosetime
		) {
		super(entitystate);
		this.memberid = memberid;
		this.purchaserid = purchaserid;
		this.merchantid = merchantid;
		this.marketid = marketid;
		this.orderorigin = orderorigin;
		this.ordertype = ordertype;
		this.originalorder = originalorder;
		this.receiver = receiver;
		this.province = province;
		this.city = city;
		this.county = county;
		this.street = street;
		this.detailaddress = detailaddress;
		this.mobilephone = mobilephone;
		this.orderremark = orderremark;
		this.ordertotal = ordertotal;
		this.goodtotal = goodtotal;
		this.deliverystate = deliverystate;
		this.deliverymode = deliverymode;
		this.deliverytime = deliverytime;
		this.deliverystarttime = deliverystarttime;
		this.deliveryendtime = deliveryendtime;
		this.deliverycost = deliverycost;
		this.paymentstate = paymentstate;
		this.paymentmode = paymentmode;
		this.paymenttotal = paymenttotal;
		this.paymenttradeno = paymenttradeno;
		this.refundtradeno = refundtradeno;
		this.tradecreatetime = tradecreatetime;
		this.tradecompletetime = tradecompletetime;
		this.refundendtime = refundendtime;
		this.integralpay = integralpay;
		this.diamondpay = diamondpay;
		this.ordertime = ordertime;
		this.confirmtime = confirmtime;
		this.paytime = paytime;
		this.sendtime = sendtime;
		this.receivetime = receivetime;
		this.completetime = completetime;
		this.refundtime = refundtime;
		this.splittime = splittime;
		this.splitreason = splitreason;
		this.partialrefund = partialrefund;
		this.refundtotal = refundtotal;
		this.integralrefund = integralrefund;
		this.diamondrefund = diamondrefund;
		this.canceltype = canceltype;
		this.canceltime = canceltime;
		this.cancelreason = cancelreason;
		this.customerclose = customerclose;
		this.customerclosetime = customerclosetime;
	}
}