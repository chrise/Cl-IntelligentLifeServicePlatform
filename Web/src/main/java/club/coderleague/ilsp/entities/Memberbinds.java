/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 会员绑定表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Memberbinds extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员标识。
	 */
	private java.lang.Long memberid;
	/**
	 * 绑定类型（1、微信，2、支付宝，3、银联）。
	 */
	private java.lang.Integer bindtype;
	/**
	 * 绑定标识。
	 */
	private java.lang.String bindid;

	/**
	 * 会员绑定表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效）。
	 * @param memberid 会员标识。
	 * @param bindtype 绑定类型（1、微信，2、支付宝，3、银联）。
	 * @param bindid 绑定标识。
	 */
	public Memberbinds(
		  java.lang.Integer entitystate
		, java.lang.Long memberid
		, java.lang.Integer bindtype
		, java.lang.String bindid
		) {
		super(entitystate);
		this.memberid = memberid;
		this.bindtype = bindtype;
		this.bindid = bindid;
	}
}