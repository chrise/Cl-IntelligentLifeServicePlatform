/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 区域表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Areas extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 区域名称。
	 */
	private java.lang.String areaname;
	/**
	 * 父级区域。
	 */
	private java.lang.Long parentarea;
	/**
	 * 区域描述。
	 */
	private java.lang.String areadesc;

	/**
	 * 区域表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param areaname 区域名称。
	 * @param parentarea 父级区域。
	 * @param areadesc 区域描述。
	 */
	public Areas(
		  java.lang.Integer entitystate
		, java.lang.String areaname
		, java.lang.Long parentarea
		, java.lang.String areadesc
		) {
		super(entitystate);
		this.areaname = areaname;
		this.parentarea = parentarea;
		this.areadesc = areadesc;
	}
}