/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 钻石记录表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Diamondrecords extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员标识。
	 */
	private java.lang.Long memberid;
	/**
	 * 记录类型（1、收入，2、支出）。
	 */
	private java.lang.Integer recordtype;
	/**
	 * 变化原因（1、过期失效，2、充值购买，3、订单支出）。
	 */
	private java.lang.Integer changedreason;
	/**
	 * 订单标识。
	 */
	private java.lang.Long orderid;
	/**
	 * 原因描述。
	 */
	private java.lang.String reasondesc;
	/**
	 * 变化钻石。
	 */
	private java.lang.Integer changeddiamond;
	/**
	 * 剩余钻石。
	 */
	private java.lang.Integer remaineddiamond;
	/**
	 * 锁定钻石。
	 */
	private java.lang.Integer lockeddiamond;
	/**
	 * 记录时间。
	 */
	@org.springframework.format.annotation.DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@com.fasterxml.jackson.annotation.JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private java.util.Date recordtime;

	/**
	 * 钻石记录表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param memberid 会员标识。
	 * @param recordtype 记录类型（1、收入，2、支出）。
	 * @param changedreason 变化原因（1、过期失效，2、充值购买，3、订单支出）。
	 * @param orderid 订单标识。
	 * @param reasondesc 原因描述。
	 * @param changeddiamond 变化钻石。
	 * @param remaineddiamond 剩余钻石。
	 * @param lockeddiamond 锁定钻石。
	 * @param recordtime 记录时间。
	 */
	public Diamondrecords(
		  java.lang.Integer entitystate
		, java.lang.Long memberid
		, java.lang.Integer recordtype
		, java.lang.Integer changedreason
		, java.lang.Long orderid
		, java.lang.String reasondesc
		, java.lang.Integer changeddiamond
		, java.lang.Integer remaineddiamond
		, java.lang.Integer lockeddiamond
		, java.util.Date recordtime
		) {
		super(entitystate);
		this.memberid = memberid;
		this.recordtype = recordtype;
		this.changedreason = changedreason;
		this.orderid = orderid;
		this.reasondesc = reasondesc;
		this.changeddiamond = changeddiamond;
		this.remaineddiamond = remaineddiamond;
		this.lockeddiamond = lockeddiamond;
		this.recordtime = recordtime;
	}
}