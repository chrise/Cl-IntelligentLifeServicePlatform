/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 收货地址表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
@club.coderleague.data.jpa.annotation.EnableHibernateEncryption
public class Receivingaddresses extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员标识。
	 */
	private java.lang.Long memberid;
	/**
	 * 收货人。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String receiver;
	/**
	 * 省份。
	 */
	private java.lang.Long province;
	/**
	 * 城市。
	 */
	private java.lang.Long city;
	/**
	 * 区县。
	 */
	private java.lang.Long county;
	/**
	 * 街道。
	 */
	private java.lang.Long street;
	/**
	 * 详细地址。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String detailaddress;
	/**
	 * 手机号码。
	 */
	@club.coderleague.data.jpa.annotation.SecureField
	private java.lang.String mobilephone;
	/**
	 * 默认地址。
	 */
	private java.lang.Boolean defaultaddress;
	/**
	 * 配送方式（1、自提，2、市场配送）。
	 */
	private java.lang.Integer deliverymode;
	/**
	 * 支付方式（1、微信，2、支付宝，3、银联）。
	 */
	private java.lang.Integer paymentmode;

	/**
	 * 收货地址表构造方法。
	 * @param entitystate 实体状态（1、有效，99、删除）。
	 * @param memberid 会员标识。
	 * @param receiver 收货人。
	 * @param province 省份。
	 * @param city 城市。
	 * @param county 区县。
	 * @param street 街道。
	 * @param detailaddress 详细地址。
	 * @param mobilephone 手机号码。
	 * @param defaultaddress 默认地址。
	 * @param deliverymode 配送方式（1、自提，2、市场配送）。
	 * @param paymentmode 支付方式（1、微信，2、支付宝，3、银联）。
	 */
	public Receivingaddresses(
		  java.lang.Integer entitystate
		, java.lang.Long memberid
		, java.lang.String receiver
		, java.lang.Long province
		, java.lang.Long city
		, java.lang.Long county
		, java.lang.Long street
		, java.lang.String detailaddress
		, java.lang.String mobilephone
		, java.lang.Boolean defaultaddress
		, java.lang.Integer deliverymode
		, java.lang.Integer paymentmode
		) {
		super(entitystate);
		this.memberid = memberid;
		this.receiver = receiver;
		this.province = province;
		this.city = city;
		this.county = county;
		this.street = street;
		this.detailaddress = detailaddress;
		this.mobilephone = mobilephone;
		this.defaultaddress = defaultaddress;
		this.deliverymode = deliverymode;
		this.paymentmode = paymentmode;
	}
}