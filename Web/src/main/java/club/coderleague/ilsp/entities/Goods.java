/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Goods extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 商户标识。
	 */
	private java.lang.Long merchantid;
	/**
	 * 分类标识。
	 */
	private java.lang.Long groupid;
	/**
	 * 商品名称。
	 */
	private java.lang.String goodname;
	/**
	 * 商品产地。
	 */
	private java.lang.String goodorigin;
	/**
	 * 商品描述。
	 */
	private java.lang.String gooddesc;

	/**
	 * 商品表构造方法。
	 * @param entitystate 实体状态（1、有效，2、无效，99、删除）。
	 * @param merchantid 商户标识。
	 * @param groupid 分类标识。
	 * @param goodname 商品名称。
	 * @param goodorigin 商品产地。
	 * @param gooddesc 商品描述。
	 */
	public Goods(
		  java.lang.Integer entitystate
		, java.lang.Long merchantid
		, java.lang.Long groupid
		, java.lang.String goodname
		, java.lang.String goodorigin
		, java.lang.String gooddesc
		) {
		super(entitystate);
		this.merchantid = merchantid;
		this.groupid = groupid;
		this.goodname = goodname;
		this.goodorigin = goodorigin;
		this.gooddesc = gooddesc;
	}
}