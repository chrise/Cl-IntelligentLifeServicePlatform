/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 订单预拆分表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Orderpresplits extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 订单标识。
	 */
	private java.lang.Long orderid;
	/**
	 * 商户标识。
	 */
	private java.lang.Long merchantid;
	/**
	 * 市场标识。
	 */
	private java.lang.Long marketid;
	/**
	 * 订单备注。
	 */
	private java.lang.String orderremark;

	/**
	 * 订单预拆分表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param orderid 订单标识。
	 * @param merchantid 商户标识。
	 * @param marketid 市场标识。
	 * @param orderremark 订单备注。
	 */
	public Orderpresplits(
		  java.lang.Integer entitystate
		, java.lang.Long orderid
		, java.lang.Long merchantid
		, java.lang.Long marketid
		, java.lang.String orderremark
		) {
		super(entitystate);
		this.orderid = orderid;
		this.merchantid = merchantid;
		this.marketid = marketid;
		this.orderremark = orderremark;
	}
}