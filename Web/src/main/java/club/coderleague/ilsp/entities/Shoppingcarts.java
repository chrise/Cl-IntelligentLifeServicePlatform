/**
 * Hibernate Tool generated code.
 */
package club.coderleague.ilsp.entities;

import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 购物车表。
 */
@Entity
@Getter
@Setter
@DynamicInsert
@DynamicUpdate
@NoArgsConstructor
public class Shoppingcarts extends club.coderleague.data.jpa.domain.Entity<java.lang.Long, club.coderleague.data.jpa.id.support.SnowflakeV4> {
	private static final long serialVersionUID = 1L;

	/**
	 * 会员标识。
	 */
	private java.lang.Long memberid;
	/**
	 * 商品销售标识。
	 */
	private java.lang.Long saleid;
	/**
	 * 商品数量。
	 */
	private java.lang.Double goodnumber;
	/**
	 * 选中状态。
	 */
	private java.lang.Boolean selectstate;
	/**
	 * 立即购买。
	 */
	private java.lang.Boolean buynow;

	/**
	 * 购物车表构造方法。
	 * @param entitystate 实体状态（1、有效）。
	 * @param memberid 会员标识。
	 * @param saleid 商品销售标识。
	 * @param goodnumber 商品数量。
	 * @param selectstate 选中状态。
	 * @param buynow 立即购买。
	 */
	public Shoppingcarts(
		  java.lang.Integer entitystate
		, java.lang.Long memberid
		, java.lang.Long saleid
		, java.lang.Double goodnumber
		, java.lang.Boolean selectstate
		, java.lang.Boolean buynow
		) {
		super(entitystate);
		this.memberid = memberid;
		this.saleid = saleid;
		this.goodnumber = goodnumber;
		this.selectstate = selectstate;
		this.buynow = buynow;
	}
}