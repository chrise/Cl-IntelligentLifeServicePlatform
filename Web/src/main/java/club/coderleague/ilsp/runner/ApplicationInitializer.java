/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：ApplicationInitializer.java
 * History:
 *         2019年5月8日: Initially created, Chrise.
 */
package club.coderleague.ilsp.runner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import club.coderleague.ilsp.cache.CacheManager;
import club.coderleague.ilsp.service.users.UserService;

/**
 * 应用程序初始化器。
 * @author Chrise
 */
@Component
public class ApplicationInitializer implements CommandLineRunner {
	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationInitializer.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private CacheManager cacheManager;
	
	/**
	 * @see org.springframework.boot.CommandLineRunner#run(java.lang.String[])
	 */
	@Override
	public void run(String... args) throws Exception {
		try {
			// 注册管理员
			this.userService.execRegisterAdminstrator();
			
			// 初始化缓存管理器
			this.cacheManager.initialize();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
