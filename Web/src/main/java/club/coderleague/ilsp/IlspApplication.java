/**
 * Copyright (c) 2019 Coder League
 * All rights reserved.
 *
 * File：IlspApplication.java
 * History:
 *         2019年4月30日: Initially created, Chrise.
 */
package club.coderleague.ilsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 应用程序。
 * @author Chrise
 */
@SpringBootApplication
@EnableScheduling
public class IlspApplication {
	/**
	 * 应用程序主入口。
	 * @author Chrise 2019年4月30日
	 * @param args 启动参数。
	 */
	public static void main(String[] args) {
		SpringApplication.run(IlspApplication.class, args);
	}
}
