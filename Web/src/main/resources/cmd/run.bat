@echo off

rem enable variable delayed expansion
setlocal enabledelayedexpansion

rem loop to start application automatic
:start

rem temp jar file exists
echo Checking application temp file...
if exist %cd%\temp\ilsp.jar (
	rem update time variable
	set timestamp=%date:~0,4%%date:~5,2%%date:~8,2%%time:~0,2%%time:~3,2%%time:~6,2%
	set timestamp=!timestamp: =0!
	
	rem backup old jar file
	echo Backuping old application file...
	copy /y %cd%\ilsp.jar %cd%\backup\ilsp-!timestamp!.jar
	
	rem copy temp jar file to run dir
	echo Copying new application file to run directory...
	copy /y %cd%\temp\ilsp.jar %cd%\ilsp.jar
	
	rem delete temp jar file
	echo Deleting application temp file...
	del %cd%\temp\ilsp.jar
)

rem run jar file
echo Starting application...
java -jar ilsp.jar

rem delay to restart application
echo The application will restart after 10s...
choice /t 10 /d y /n > nul

goto start
