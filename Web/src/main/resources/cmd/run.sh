#!/bin/bash

# loop to start application automatic
while :
do
	# temp jar file exists
	echo "Checking application temp file..."
	if [ -f "./temp/ilsp.jar" ]; then
		# update time variable
		timestamp=$(date "+%Y%m%d%H%M%S")
		
		# backup old jar file
		echo "Backuping old application file..."
		cp -f ./ilsp.jar ./backup/ilsp-$timestamp.jar
		
		# copy temp jar file to run dir
		echo "Copying new application file to run directory..."
		cp -f ./temp/ilsp.jar ./ilsp.jar
		
		# delete temp jar file
		echo "Deleting application temp file..."
		rm -f ./temp/ilsp.jar
	fi
	
	# run jar file
	echo "Starting application..."
	java -jar ilsp.jar
	
	# delay to restart application
	echo "The application will restart after 10s..."
	sleep 10
done
