//页面加载完成事件
$(function() {
	instance.init();
});

//全局实例对象
var instance = {
	file : null,
	username : null,
	password : null,
	filename : null,
	release : null,
	message : null,
	init : function() {
		if (!__supported) return;
		
		this.file = $("#file");
		this.username = $("#username");
		this.password = $("#password");
		this.filename = $("#filename");
		this.release = $("#release");
		this.message = $("#message");
		
		this.file.change(this.onFileChange);
		this.filename.dblclick(this.onFilepathDblClick);
		this.release.click(this.onReleaseClick);
	},
	onFileChange : function(e) {
		var file = e.currentTarget.files[0];
		if (!/^.+\.jar$/.test(file.name)) {
			instance.showMessage("不支持的文件类型");
			return;
		}
		
		instance.filename.val(file.name);
		instance.clearMessage();
	},
	onFilepathDblClick : function(e) {
		instance.file.click();
	},
	onReleaseClick : function(e) {
		var form = new FormData();
		form.append("username", instance.username.val());
		form.append("password", instance.password.val());
		form.append("file", instance.file[0].files[0]);
		
		var xhr = new XMLHttpRequest();
		xhr.upload.addEventListener("progress", instance.onProgress, false);
		xhr.addEventListener("load", instance.onCompleted, false);
		xhr.addEventListener("error", instance.onFailed, false);
		xhr.addEventListener("abort", instance.onFailed, false);
		xhr.open("post", window.location.href, true);
		xhr.send(form);
	},
	onProgress : function(e) {
		if (e.lengthComputable) {
			var percent = (e.loaded / e.total * 100.0).toFixed(2);
			instance.showMessage("正在上传发布包......" + percent + "%");
		}
	},
	onCompleted : function(e) {
		if (!e.target.response) instance.onFailed(e);
		else {
			instance.resetForm();
			var response = $.parseJSON(e.target.response);
			if (response.status) instance.showMessage("发布包已上传......5s后启动更新");
			else instance.showMessage("发布应用失败......" + response.result);
		}
	},
	onFailed : function(e) {
		instance.resetForm();
		instance.showMessage("发布应用失败......系统错误");
	},
	resetForm : function() {
		this.file.val(null);
		this.username.val(null);
		this.password.val(null);
		this.filename.val(null);
	},
	showMessage : function(text) {
		this.message.text(text);
	},
	clearMessage : function() {
		this.message.text("");
	}
};
