//全局初始化
var globalInit = function() {
	//初始化AJAX
	$.ajaxSetup({
		cache : false,
		timeout : 10000,
		type : "post",
		dataType : "json"
	});
}();

//系统全局对象
var sysGlobal = {
	// 请求响应-结果代码
	ResultCode : {
		Success : 1,
		Info : 2,
		Warn : 3,
		Error : 4
	},
	// 操作日志-请求类型
	OperateLogsRequestType : [
		{value : 1, text : '主动'},
		{value : 2, text : '被动'}
	],
	// 操作日志-返回结果
	OperateLogsReturnResult : [
		{value : true, text : '成功'},
		{value : false, text : '失败'}
	],
	EntityState : [
		{value : 1, text : '有效'},
		{value : 2, text : '无效'},
		{value : 3, text : '冻结'},
		{value : 4, text : '上架'},
		{value : 99, text : '删除'}
	],
	FuncType: [
		{text: '模块', value: '1'},
		{text: '节点', value: '2'},
		{text: '按钮', value: '3'}       
	],
	YesOrNo: [
		{text: '是', value: '1'},
	    {text: '否', value: '0'}
    ],
    AuditState: [
    	{text: '通过', value: 1},
	    {text: '否决', value: 2}
    ],
    Switch : [
    	{text: '开启', value: 1},
	    {text: '禁止', value: 0}
    ],
	EntityStateObj : {
		'1' : "有效", 
		'2' : "无效",
		'3' : "冻结",
		'4' : "上架",
		'5' : "待确认", 
		'6' : "已确认",
		'7' : "已完成",
		'8' : "已拆分",
		'9' : "已取消",
		'10' : "待审核",
		'11' : "未通过",
		'12' : "待处理",
		'13' : "处理中",
		'14' : "已撤销",
		'15' : "结算完成",
		'16' : "结算取消",
		'17' : "待退款",
		'18' : "已退款",
		'99' : "已删除"
	},
	//通知模板-通知类型
	NotifyType : [
		{text: '请选择', value: '-1'},
		{text: '手机绑定', value: '1'},
		{text: '身份认证', value: '2'},
		{text: '到账提醒', value: '3'},
		{text: '退款提醒', value: '4'},
		{text: '结算提醒', value: '5'}
	],
	TimeOutData : [
		{text: "10", value : 10},
		{text: "15", value : 15},
		{text: "20", value : 20}
	],
	ReceiveTimeoutData : [
		{text: "3", value : 3},
		{text: "5", value : 5}
	]
	
};

/**
 * 日期扩展-格式化
 * new Date('2018-12-29 12:00:00').format('yyyy年MM月dd日 HH时mm分ss秒')  ==> '2018年12月29日 12时00分00秒'
 * new Date(1546074271473).format('yyyy-MM-dd HH:mm:ss')                 ==> '2018-12-29 17:04:31'
 * new Date('2018-12-29').format('yyyy年MM月')                           ==> '2018年12月'
 * new Date().format('yyyy-MM-dd HH:mm:ss')                              ==> '2018-12-29 17:07:13'
 */
Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1,
		"d+" : this.getDate(),
		"H+" : this.getHours(),
		"m+" : this.getMinutes(),
		"s+" : this.getSeconds(),
		"q+" : Math.floor((this.getMonth() + 3) / 3),
		"S" : this.getMilliseconds()
	}

	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}

	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;
}

// 公共工具包
var CommonUtil = {
	/**
	 * 获取枚举文本值
	 * @param value 枚举值
	 * @param enums 枚举；字符串类型，在全局配置中获取枚举；数组类型，直接使用
	 */
	getEnumText : function(value, enums) {
		var globalenum = null;
		// 获取枚举对象
		if (Array.isArray(enums)) {
			globalenum = enums;
		} else if (typeof(enums) == 'string') {
			globalenum = sysGlobal[enums];
		} else {
			return null;
		}
		// 检查空枚举
		if (!globalenum || !Array.isArray(globalenum) || globalenum.length <= 0) {
			return null;
		}
		// 获取文本值
		var enumtext = null;
		$.each(globalenum, function() {
			if (value == this.value) {
				enumtext = this.text;
				return false;
			}
		})
		return enumtext;
	},
	/**
	 * URL添加参数
	 * @param url 地址
	 * @param params 参数集
	 * @return 组合后的地址
	 */
	urlHandler : function(url, params) {
		if (!url || !params) {
			return url;
		}
		var paramsArr = new Array();
		for (var key in params) {
			paramsArr.push(key + '=' + encodeURI(params[key]));
		}
		if (!url.startWith('/')) {
			url = ('/' + url);
		}
		return url + ((url.indexOf('?') != -1 ? '&' : '?') + paramsArr.join('&'));
	}
}