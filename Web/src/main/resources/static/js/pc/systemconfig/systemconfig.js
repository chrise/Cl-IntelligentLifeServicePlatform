search.parse();
var form = new Form("form");

var wxswitch = search.get("wxswitch");
var apswitch = search.get("apswitch");
var aynotify = search.get("aynotify");

//文件上传控件
var wxcerfileselect = $("#wxcerfileselect");
var wxverfileselect = $("#wxverfileselect");
var wxcerfile = search.get("wxcerfile");
var wxverfile = search.get("wxverfile");

var confirmtimeout = search.get("confirmtimeout");
var paytimeout = search.get("paytimeout");
var receivetimeout = search.get("receivetimeout");

//添加search-text点击弹出文件选择/上传控件变换事件
function setFileControlClick(){
	//text控件双击
	$(wxcerfile._textboxhtml[0]).css("cursor","pointer");
	$(wxverfile._textboxhtml[0]).css("cursor","pointer");
	$(wxcerfile._textboxhtml[0]).dblclick(function(){
		wxcerfileselect.click();
	});
	$(wxverfile._textboxhtml[0]).dblclick(function(){
		wxverfileselect.click();
	});
	//添加上传控件变换事件
	wxcerfileselect.change(filechange);
	wxverfileselect.change(filechange);
}

$(function(){
	setFileControlClick();
	
	wxswitch.loadData(sysGlobal.Switch);
	apswitch.loadData(sysGlobal.Switch);
	aynotify.loadData(sysGlobal.Switch);
	
	confirmtimeout.loadData(sysGlobal.TimeOutData);
	paytimeout.loadData(sysGlobal.TimeOutData);
	receivetimeout.loadData(sysGlobal.ReceiveTimeoutData);
	
    function tabs(tabTit,on,tabCon){
        $(tabTit).children().click(function(){
            $(this).addClass(on).siblings().removeClass(on);
            var index = $(tabTit).children().index(this);
           	$(tabCon).children().eq(index).show().siblings().hide();
    	});
	};
    tabs(".tab-hd","active",".tab-bd");
    $("body").find("ul[class=\"tab-hd\"]").children()[0].click();
    loadData();
});

function loadData(){
	pcGlobal.sendRequest(__ctx + "/system/getthesystemconfigs.xhtml", {}, function(data, status) {
		if (data.status) {
			if(data.result){
				if(!data.result.wxswitch){
					data.result.wxswitch = 0;
				}
				if(!data.result.apswitch){
					data.result.apswitch = 0;
				}
				if(!data.result.aynotify){
					data.result.aynotify = 0;
				}
				form.setData(data.result);
			}
		} else {
			search.error({content: data.result});
		}
	});
}

function saveData(){
	if (!form.validate()) {
		return;
	}
	
	if(wxcerfileselect[0].files[0] || wxverfileselect[0].files[0]){
		//先上传文件
		var formData = new FormData();
		if(wxcerfileselect[0].files[0]){
			formData.append("file", wxcerfileselect[0].files[0]);
		}
		if(wxverfileselect[0].files[0]){
			formData.append("file", wxverfileselect[0].files[0]);
		}
		$.ajax({
			url : __ctx + "/commonUpload/uploadthesystemfiles.xhtml",
			type : 'POST',
			data : formData,
			contentType:false,
			processData:false,
			success : function(result){
				//清空上传
				wxcerfileselect[0].value = '';
				wxverfileselect[0].value = '';
				
				if (result.status) {
					//再保存表单
					var fdata = form.getData();
					pcGlobal.sendRequest(__ctx + "/system/updatesystemconfigs.xhtml", fdata, function(data, status) {
						if (data.status) {
							if(data.result){
								search.info({content: data.result.msg});
							}
						} else {
							search.error({content: data.result});
						}
					});
				}else {
					search.error({content: result.result});
				}
			}
		});
	}else{
		var fdata = form.getData();
		pcGlobal.sendRequest(__ctx + "/system/updatesystemconfigs.xhtml", fdata, function(data, status) {
			if (data.status) {
				if(data.result){
					search.info({content: data.result.msg});
				}
			} else {
				search.error({content: data.result});
			}
		});
	}
}

//文件上传变化事件
function filechange(e){
	var file = e.currentTarget.files[0];
	var filecontrolid = e.currentTarget.id;
	if(filecontrolid == "wxcerfileselect"){
		if (!/^.+\.(p12)$/.test(file.name)) {
			search.warn({content : "不支持的文件类型！"});
			return;
		}
	}else if(filecontrolid == "wxverfileselect"){
		if (!/^.+\.(txt)$/.test(file.name)) {
			search.warn({content : "不支持的文件类型！"});
			return;
		}
	}else{
		return;
	}
	
	if(filecontrolid == "wxcerfileselect"){
		wxcerfile.setValue(file.name);
	}else{
		wxverfile.setValue(file.name);
	}
}

