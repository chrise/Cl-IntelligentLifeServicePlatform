search.parse();

var instance = {
	form : new Form("form"),
	parentorg : search.get("parentorg"),
	execute : "",
	loadData : function(data) {
		this.execute = data.action;
		if (data.action == "add") this.parentorg.setValue(data.entityid);
		else if (data.action == "edit") {
			pcGlobal.sendRequest(__ctx + "/orgmgr/getOrgObjByOrgid.xhtml", {entityid : data.entityid}, function(data, status) {
				if (data.status) {
					data.result.parentorg = (data.result.parentorg === null || data.result.parentorg == "") ? "-1" : data.result.parentorg;
					instance.form.setData(data.result);
				} else search.error({content: data.result});
			});
		}
	},
	save : function() {
		var data = instance.form.getData();
		data.entitystate = 1;
		if (data.entityid == null || data.entity == "") data.createtime == null;
		if (data.parentorg == "-1") data.parentorg = null;
		pcGlobal.sendRequest(__ctx + "/orgmgr/insertOrUpdateOrgObj.xhtml", $.extend({execute : instance.execute}, data), function(data, status) {
			if (data.status) {
				search.info({content: data.result.msg,funl:function(){
					if(data.result.state) window.closeWindow('ok');
				}});
			} else search.error({content: data.result});
		});
	}
};

/**
 * 保存数据。
 */
function saveData() {
	if (!instance.form.validate()) return;
	instance.save();
}

/**
 * 页面加载
 */
$(function(){
	instance.parentorg.url = __ctx + "/orgmgr/getParentOrgSelectList.xhtml";
	instance.parentorg.load();
});