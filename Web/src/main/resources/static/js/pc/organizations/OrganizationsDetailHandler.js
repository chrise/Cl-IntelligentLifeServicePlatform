search.parse();

var instance = {
	form : new Form("form"),
	loadData : function(data) {
		pcGlobal.sendRequest(__ctx + "/orgmgr/getOrganizationsDetailDataByOr.xhtml", {entityid : data.entityid}, function(data, status) {
			if (data.status) instance.form.setData(data.result);
			else search.error({content: data.result});
		});
	}
};