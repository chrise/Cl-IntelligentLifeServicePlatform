search.parse();

/**
 * 实例化控制器。
 */
var instance = {
	_Grid : search.get("dataGrid"),
	loadData : function () {
		this._Grid.url = __ctx + "/orgmgr/getOrgMgrList.xhtml";
		this._Grid.load({isrecycle: isrecycle});
	},
	updateState: function(text, entityid, execute) {
		search.confirm({content: "确定要执行" + text + "操作吗？", funl : function() {
			 　pcGlobal.sendRequest(__ctx + "/orgmgr/updateEntitystateByOrgid.xhtml", {entityid: entityid, execute: execute}, function(data, status) {
					if (data.status) {
						search.info({content: data.result.msg,funl:function(){
							instance._Grid.load();
						}});
					} else search.error({content: data.result});
				});
			  }
		});
	}
}

/**
 * 新增。
 */
function addOrg (params) {
	if(params.ids != null && params.ids != ""){
		var data = {
			url : __ctx + "/orgmgr/goUpdateOrgMgrPage.xhtml",
			title : "新增",
			width : 450,
			height : 300,
			onload : function(window){
				window.instance.loadData({action:"add", entityid: params.ids});
			},
			ondestroy : function(ation){
				if (ation == 'ok') instance.loadData();
			}
		}
	}else{
		var data = {
			url : __ctx + "/orgmgr/goUpdateOrgMgrPage.xhtml",
			title : "新增",
			width : 450,
			height : 300,
			onload : function(window){
				window.instance.loadData({action:"add", entityid: "-1"});
			},
			ondestroy : function(ation){
				if (ation == 'ok') instance.loadData();
			}
		}
	}
	top.search.popDialog(data);
}

/**
 * 编辑。
 */
function editOrg (params) {
	var data = {
		url : __ctx + "/orgmgr/goUpdateOrgMgrPage.xhtml",
		title : "编辑",
		width : 600,
		height : 520,
		onload : function(window){
			window.instance.loadData({action:"edit", entityid: params.ids});
		},
		ondestroy : function(data){
			if (data == 'ok') instance.loadData();
		}
	}
	top.search.popDialog(data);
}

/**
 * 删除
 * @param params
 */
function delData (params) {
	var execute = "del";
	if (isrecycle) execute = "delRecyce";
	instance.updateState("删除", params.ids, execute);
}

/**
 * 恢复。
 * @param params
 * @returns
 */
function restData(params) {
	instance.updateState("恢复", params.ids, "rest");
}

/**
 * 查询数据。
 * @returns
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	instance._Grid.load({keyword: keyword, isrecycle: isrecycle});
}

/**
 * 查看详情。
 * @param params 功能ID。
 * @returns
 */
function queryDetail(params){
	var entityid = params.origin.record.entityid;
	
	var data = { 
		url : __ctx + "/orgmgr/goOrganizationsDetailPage.xhtml", 
		title : "详情", 
		width : 600, 
		height : 500,
		onload : function(window) {
			window.instance.loadData({entityid: entityid});
		}, ondestroy : function(result){}
	};
	top.search.popDialog(data);
}

/*加载功能按钮*/
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(instance._Grid, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	restoreMethod : "restData",
	deleteMethod : "delData",
	validateRules : [
		{methodname : 'editOrg', conditions : [[1]], ismultiple : false},
		{methodname : 'delData', conditions : [[1],[2]], ismultiple : true},
		{methodname : 'restData', conditions : [[2]], ismultiple : true},
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['addOrg','queryDetail']
	}
});
/*加载功能按钮*/

/**
 * 加载完页面执行。
 * @returns
 */
$(function () {
	instance.loadData();
});