//PC端全局对象
var pcGlobal = {
	sendRequest : function(url, data, success, error) {	//发送请求
		$.ajax({
			url : url,
			data : data,
			success : function(data, status) {
				//请求成功处理
				if (success) success.call(pcGlobal, data, status);
				else {
					if (data.status) search.info({content : "操作成功！"});
					else search.warn({content : "操作失败！"});
				}
			},
			error : function(xhr, msg, e) {
				//请求错误处理
				if (error) error.call(pcGlobal, xhr, msg, e);
				else search.error({content : "网络请求错误！"});
			}
		});
	}
};