search.parse();
var form = new Form("form");

$(function(){
	var params = [
		{"url":__ctx+"/commonUpload/uploadGoodGroupsFile.xhtml","key":"fltp","limit":"jpg,jpeg,png,bmp,gif","num":1}
	];
	uploader.init(params, null, true);
})

function setData(data){
	if(data.action == "look"){
		pcGlobal.sendRequest(__ctx + "/goodgroups/getthegoodgrouplookinfopagedata.xhtml", {"entityid":data.id}, function(result, status) {
			if (result.status) {
				form.setData(result.result.formdata);
				uploader.loadFiles("fltp",result.result.fltp);
			} else {
				search.error({content: result.result});
			}
		});
	}
}
