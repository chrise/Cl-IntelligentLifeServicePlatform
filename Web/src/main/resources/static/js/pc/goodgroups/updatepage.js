search.parse();
var form = new Form("form");
var parentgroup = search.get("parentgroup");


$(function(){
	parentgroup.url =  __ctx + "/goodgroups/gettheupdatepageloadpraentgoods.xhtml";
	parentgroup.load({"entityid":_entityid});
	var params = [
		{"url":__ctx+"/commonUpload/uploadGoodGroupsFile.xhtml","key":"fltp","limit":"jpg,jpeg,png,bmp,gif","num":1}
	];
	uploader.init(params, saveData);
})

function setData(data){
	if(data.action == "edit"){
		pcGlobal.sendRequest(__ctx + "/goodgroups/getthegoodgroupeditpagedata.xhtml", {"entityid":data.id}, function(result, status) {
			if (result.status) {
				if(!result.result.formdata.parentgroup){
					result.result.formdata.parentgroup = -1;
				}
				form.setData(result.result.formdata);
				uploader.loadFiles("fltp",result.result.fltp);
			} else {
				search.error({content: result.result});
			}
		});
	}
}

function saveData(params){
	var formdata = form.getData();
	formdata["grouppicture"] = params["fltp"].uploads[0] ? params["fltp"].uploads[0].savePath : null;
	pcGlobal.sendRequest(__ctx + "/goodgroups/updatethegoodgroup.xhtml", formdata, function(data, status) {
		if (data.status) {
			search.info({content: data.result.msg,funl:function(){
				if(data.result.status){
					window.closeWindow({"isload":true});
				}
			}});
		} else {
			search.error({content: data.result});
		}
	});
}

function fupload(){
	if (!form.validate()) {
		return;
	}
//	var select = parentgroup.getSelectedNode();
//	if(select.record.pid){
//		if(!uploader.hasFile()){
//			search.info({content: "请上传附件!"});
//			return;
//		}
//	}
	uploader.upload();
}