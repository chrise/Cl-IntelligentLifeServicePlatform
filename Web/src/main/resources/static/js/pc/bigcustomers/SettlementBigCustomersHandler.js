search.parse();

/**
 * 实例化。
 */
var sbc = {
	_Grid : search.get("dataGrid"),
	id : null,
	loadData : function (param) {
		this.id = param.id;
		this._Grid.url = __ctx + "/bcc/getBigCustomerOrdersMgr.xhtml";
		this._Grid.load({entityid: param.id});
		search.get("keyword").url = __ctx + "/bcc/getBigCustomerList.xhtml";
		search.get("keyword").load();
	}	
}

/**
 * 查询
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	sbc._Grid.load({keyword: keyword, entityid: sbc.id});
}

/**
 * 保存数据。
 * @returns
 */
function saveData () {
	var ids = sbc._Grid.getValue();
	if (ids === null || ids === "") {
		search.warn({content:"请选择一条数据!"});
		return;
	}
	
	search.confirm({content: "确定要执行结算操作吗？", funl : function() {
		 　pcGlobal.sendRequest(__ctx + "/bcc/executeSettlementByIds.xhtml", {ids: ids}, function(data, status) {
				if (data.status) {
					search.info({content: data.result.msg,funl:function(){
						if(data.result.state) window.closeWindow('ok');
					}});
				}
				else search.error({content: data.result});
			});
		  }
	});
}
