search.parse();

/**
 * 实例化。
 */
var update = {
	form : new Form("form"),
	yesOrNo : [{text: '是', value: true},{text: '否', value: false}],
	execute : "",
	_tr_row_id : 1,
	_del_cont : 0,
	businesslicense : "",
	upload : function () {
		var params = [{"url": __ctx + "/commonUpload/uploadBigCustomersFile.xhtml", "key": "bcc", "limit": "jpg,jpeg,png,bmp,gif", "num": 1}];
		uploader.init(params, this.saveData);
	},
	loadData : function (param) { // 加载数据。
		this.execute = param.action;
		if (param.action === "add") update.generateTab(); // 生成表格。
		else if (param.action === "edit") {
			pcGlobal.sendRequest(__ctx + "/bcc/getBigCustomersObjByEntityid.xhtml", {entityid : param.id}, function(data, status) {
				if (data.status) {
					update.form.setData(data.result.bcc);
					update.businesslicense = data.result.bcc.businesslicense;
					uploader.loadFiles("bcc", data.result.businesslicense);
					// 生成标段表格。
					if(data.result.person && data.result.person.length > 0){
						var personList = data.result.person;
						for(var i = 0; i < personList.length; i++){
							var tr = '<tr id="sect_tr_' + i + '">'+
								'<td><div id="personname_' + i + '" rules="required" class="search-textbox" width="100" height="30" ></div></td> ' +
								'<td><div id="personphone_' + i + '" rules="mobile" class="search-textbox" width="100" height="30" ></div></td> ' +
								'<td><div id="managerflag_'+ i + '" class="search-select" width="90" height="30" idField="value" textField="text"></div></td> ' +
								'<td><div id="purchaserflag_' + i + '" class="search-select" width="90" height="30" idField="value" textField="text"></div></td> ' +
								'<td><i id="add_' + i+ '" class="fa fa-plus-circle" onclick="update.addTr(\'' + i + '\');"></i> '+
								'<i id="del_' + i + '" class="fa fa-trash-o" onclick="update.deleTr(\'' + i + '\');"></i>'+
								'<i id="restore_' + i + '" class="fa fa-reply" onclick="update.restoreTab(\'' + i + '\');" ></i>'+
								'<div id="pro_entityid_' + i + '" class="search-texthide"/></td></tr>';
							if(i == 0)$("#tr_bud").after(tr);
							else $("#sect_tr_"+ (i - 1)).after(tr);
							if(i != personList.length -1) $("#add_" + i ).hide();
							$("#restore_"+i).hide();
							search.parse();
							search.get("managerflag_" + i).loadData(update.yesOrNo);
							search.get("purchaserflag_" + i).loadData(update.yesOrNo);
							search.get("personname_" + i).setValue(personList[i].personname);
							search.get("personphone_" + i).setValue(personList[i].personphone);
							search.get("managerflag_" + i).setValue(personList[i].managerflag);
							search.get("purchaserflag_" + i).setValue(personList[i].purchaserflag);
							search.get("pro_entityid_" + i).setValue(personList[i].entityid);
							update._tr_row_id += 1;
						}
						update._tr_row_id -= 1; 
					} else update.generateTab(); // 生成表格。
				} else search.error({content: data.result});
			});
		}
	},
	generateTab : function () {
		var row = 0;
		var tr = '<tr id="sect_tr_' + row + '">'+
		'<td><div id="personname_' + row + '" rules="required" class="search-textbox" width="100" height="30" ></div></td> ' +
		'<td><div id="personphone_' + row + '" rules="mobile" class="search-textbox" width="100" height="30" ></div></td> ' +
		'<td><div id="managerflag_'+ row + '" class="search-select" width="90" height="30" idField="value" textField="text"></div></td> ' +
		'<td><div id="purchaserflag_' + row + '" class="search-select" width="90" height="30" idField="value" textField="text"></div></td> ' +
		'<td><i id="add_' + row+ '" class="fa fa-plus-circle" onclick="update.addTr(\'' + row + '\');"></i> '+
		'<i id="del_' + row + '" class="fa fa-trash-o" onclick="update.deleTr(\'' + row + '\');"></i>'+
		'<i id="restore_' + row + '" class="fa fa-reply" onclick="update.restoreTab(\'' + row + '\');" ></i>'+
		'<div id="pro_entityid_' + row + '" class="search-texthide"/></td></tr>';
		$("#tr_bud").after(tr);
		$("#restore_" + row).hide();
		search.parse();
		search.get("pro_entityid_" + row).setValue("add");
		search.get("managerflag_" + row).loadData(update.yesOrNo);
		search.get("managerflag_" + row).setValue(update.yesOrNo[1].value);
		search.get("purchaserflag_" + row).loadData(update.yesOrNo);
		search.get("purchaserflag_" + row).setValue(update.yesOrNo[1].value);
	},
	addTr : function(tr_id){ // 新增。
		var tr = '<tr id="sect_tr_' + this._tr_row_id + '">'+
			'<td><div id="personname_' + this._tr_row_id + '" rules="required" class="search-textbox" width="100" height="30" ></div></td> ' +
			'<td><div id="personphone_' + this._tr_row_id + '" rules="mobile" class="search-textbox" width="100" height="30" ></div></td> ' +
			'<td><div id="managerflag_'+ this._tr_row_id + '" class="search-select" width="90" height="30" idField="value" textField="text"></div></td> ' +
			'<td><div id="purchaserflag_' + this._tr_row_id + '" class="search-select" width="90" height="30" idField="value" textField="text"></div></td> ' +
			'<td><i id="add_' + this._tr_row_id + '" class="fa fa-plus-circle" onclick="update.addTr(\'' + this._tr_row_id + '\');"></i> '+
			'<i id="del_' + this._tr_row_id + '" class="fa fa-trash-o" onclick="update.deleTr(\'' + this._tr_row_id + '\');"></i>'+
			'<i id="restore_' + this._tr_row_id + '" class="fa fa-reply" onclick="update.restoreTab(\'' + this._tr_row_id + '\');" ></i>'+
			'<div id="pro_entityid_' + this._tr_row_id + '" class="search-texthide"/></td></tr>';
		$("#sect_tr_"+tr_id).after(tr);
		$("#add_"+tr_id).hide();
		$("#restore_"+this._tr_row_id).hide();
		search.parse();
		search.get("pro_entityid_" + this._tr_row_id).setValue("add");
		search.get("managerflag_" + this._tr_row_id).loadData(update.yesOrNo);
		search.get("managerflag_" + this._tr_row_id).setValue(update.yesOrNo[1].value);
		search.get("purchaserflag_" + this._tr_row_id).loadData(update.yesOrNo);
		search.get("purchaserflag_" + this._tr_row_id).setValue(update.yesOrNo[1].value);
		this._tr_row_id += 1;
	},
	deleTr : function(id){ // 删除。
		$("#del_"+id).hide();
		$("#restore_"+id).show();
		var entityid = search.get("pro_entityid_" + id).getValue() + "_del"
		search.get("pro_entityid_" + id).setValue(entityid);
		search.get("personname_" + id).setEnabled(false);
		search.get("personphone_" + id).setEnabled(false);
		search.get("managerflag_" + id).setEnabled(false);
		search.get("purchaserflag_" + id).setEnabled(false);
		search.get("personname_" + id).activateValidate(false);
		search.get("personphone_" + id).activateValidate(false);
		this._del_cont += 1;
	},
	restoreTab : function(id){ // 恢复。
		$("#del_" + id).show();
		$("#restore_" + id).hide();
		var entityid = search.get("pro_entityid_" + id).getValue();
		search.get("pro_entityid_" + id).setValue(entityid.split("_")[0]);
		search.get("personname_" + id).setEnabled(true);
		search.get("personphone_" + id).setEnabled(true);
		search.get("managerflag_"+ id).setEnabled(true);
		search.get("purchaserflag_" + id).setEnabled(true);
		search.get("personname_" + id).activateValidate(true);
		search.get("personphone_" + id).activateValidate(true);
		this._del_cont -= 1;
	},
	saveData : function (params) { // 保存数据。
		var data = update.form.getData();
		if (data.entityid == null || data.entityid == "") {
			data['createtime'] = null;
			data['entitystate'] = 1;
		}
		data['modifytime'] = null;
		if (params.bcc.uploads.length > 0) data['businesslicense'] = params.bcc.uploads[0].savePath;
		else data['businesslicense'] = update.businesslicense;
		
		var persons = [], phone = [];
		var _flag = false;
		for (var i = 0; i < update._tr_row_id; i++) {
			var entityid = search.get("pro_entityid_" + i).getValue();
			var _id = entityid.split("_");
			if (_id.length > 1 && "add" === _id[0] && "del" === _id[1]) continue; // 过滤新增删除的。
			if ("add" === _id[0] || (_id.length === 1 && "add" != _id[0])) { // 只检查有效数据里面是否有多个管理员或者手机号重复。
				var managerflag = search.get("managerflag_" + i).getValue();
				if (managerflag && _flag) {
					search.warn({content:"只能设置一个管理员!"});
					return;
				}
				phone.push(search.get("personphone_" + i).getValue()); // 查询手机号重复。
			}
			
			persons.push({
				entityid : search.get("pro_entityid_" + i).getValue(), 
				personname: search.get("personname_" + i).getValue(), 
				personphone: search.get("personphone_" + i).getValue(), 
				managerflag: managerflag, 
				purchaserflag: search.get("purchaserflag_" + i).getValue()
			});
			
			if (managerflag) _flag = true;
        }
		
		if (!_flag) {
			search.warn({content: "必须要设置一个管理员!"});
			return;
		}
		
		var _phone = phone.join(",") + ","; // 查询手机号重复。
		var _repeat = "";
		for(var i = 0;i < phone.length; i++) {
			if(_phone.replace(phone[i] + ",", "").indexOf(phone[i] + ",") > -1) {
				_repeat = phone[i];
				break;
			}
		}
		if (_repeat != "") {
			search.warn({content: "存在重复手机号【" + _repeat + "】!"});
			return;
		}
		var datas = {bigc: {entityid: data.entityid, createtime: data.createtime, entitystate: data.entitystate, 
			modifytime: data.modifytime, businesslicense: data.businesslicense, 
			customername: data.customername, contactsphone: data.contactsphone,customeraddress: data.customeraddress}, persons: persons};
		pcGlobal.sendRequest(__ctx + "/bcc/insertOrUpdateBigCustomersObj.xhtml", {data: JSON.stringify(datas), execute: update.execute}, function(data, status) {
			if (data.status) {
				search.info({content: data.result.msg,funl:function(){
					if(data.result.state) window.closeWindow('ok');
				}});
			} else search.error({content: data.result});
		});
	}
}

/**
 * 保存数据。
 */
function saveData () {
	if (!update.form.validate()) return;
	if (!uploader.hasFile()) {
		search.info({content: "请上传附件!"});
		return;
	}
	uploader.upload();
}

$(function () {
	update.upload();
});