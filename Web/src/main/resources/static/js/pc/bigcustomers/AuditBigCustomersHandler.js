search.parse();

/**
 * 实例化。
 */
var audit = {
	auditstate : search.get("auditstate"),
	auditrecord : search.get("auditrecord"),
	execute : "",
	customerid : "",
	loadData : function (params) {
		this.auditstate.loadData(sysGlobal.AuditState);
		this.auditstate.setValue(sysGlobal.AuditState[0].value);
		this.execute = params.action;
		this.customerid = params.id;
		console.log(params)
	}
}

/**
 * 保存。
 * @returns
 */
function saveData () {
	var auditstate = audit.auditstate.getValue();
	var auditrecord = audit.auditrecord.getValue();
	pcGlobal.sendRequest(__ctx + "/bcc/executeAuditBigCustomersObj.xhtml", {istate: auditstate, record: auditrecord, id: audit.customerid, execute: audit.execute}, function(data, status) {
		if (data.status) {
			search.info({content: data.result.msg,funl:function(){
				if(data.result.state) window.closeWindow('ok');
			}});
		} else search.error({content: data.result});
	});
}