search.parse();

/**
 * 实例化。
 */
var update = {
	form : new Form("form"),
	yesOrNo : [{text: '是', value: true},{text: '否', value: false}],
	execute : "",
	_tr_row_id : 1,
	_del_cont : 0,
	upload : function () {
		var params = [{"url": __ctx + "/commonUpload/uploadBigCustomersFile.xhtml", "key": "bcc", "limit": "jpg,jpeg,png,bmp,gif", "num": 1}];
		uploader.init(params, this.saveData);
	},
	loadData : function (param) { // 加载数据。
		this.execute = param.action;
		pcGlobal.sendRequest(__ctx + "/bcc/getBigCustomersDetailByEntityid.xhtml", {entityid : param.id}, function(data, status) {
			if (data.status) {
				update.form.setData(data.result.bcc);
				uploader.loadFiles("bcc", data.result.businesslicense);
				// 生成标段表格。
				if(data.result.person && data.result.person.length > 0){
					var personList = data.result.person;
					for(var i = 0; i < personList.length; i++){
						var tr = '<tr id="sect_tr_' + i + '">'+
							'<td align="center" style="background:#f9f9f9;">' + (personList[i].personname == null ? "" : personList[i].personname) + '</td> ' +
							'<td align="center" style="background:#f9f9f9;">' + (personList[i].personphone == null ? "" : personList[i].personphone) + '</td> ' +
							'<td align="center" style="background:#f9f9f9;">' + (personList[i].managerflag ? "是" : "否") + '</td> ' +
							'<td align="center" style="background:#f9f9f9;">' + (personList[i].purchaserflag ? "是" : "否") + '</td></tr>';
						if (i == 0) $("#tr_bud").after(tr);
						else $("#sect_tr_"+ (i - 1)).after(tr);
					}
				}
			} else search.error({content: data.result});
		});
	}
}

$(function () {
	update.upload();
});