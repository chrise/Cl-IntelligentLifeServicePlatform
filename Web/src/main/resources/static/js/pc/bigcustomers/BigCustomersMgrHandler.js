search.parse();

/**
 * 实例化控制。
 */
var _bigC = {
	_Grid : search.get("dataGrid"),
	customerid : search.get("keyword"),
	loadData : function () {
		this._Grid.url = __ctx + "/bcc/getBigCustomersMgr.xhtml";
		this._Grid.load({isrecycle: isrecycle});
	},
	updateEntityState : function (text, ids, execute) { // 修改状态。
		search.confirm({content: "确定要执行" + text + "操作吗？", funl : function() {
			 　pcGlobal.sendRequest(__ctx + "/bcc/updateEntitystateByEntityid.xhtml", {entityid: ids, execute: execute}, function(data, status) {
					if (data.status) {
						search.info({content: data.result.msg,funl:function(){
							_bigC._Grid.load();
						}});
					}
					else search.error({content: data.result});
				});
			  }
		});
	}
}

/**
 * 新增。
 * @returns
 */
function add (params) {
	var data = {
		url : __ctx + "/bcc/goUpdateBigCustomersPage.xhtml",
		title : "新增",
		width : 600,
		height : 520,
		onload : function(window){
			window.update.loadData({action: "add"});
		},
		ondestroy : function(ation){
			if (ation == 'ok') _bigC.loadData();
		}
	}
	top.search.popDialog(data);
}

/**
 * 编辑。
 * @param params 参数。
 * @returns
 */
function edit (params) {
	var data = {
		url : __ctx + "/bcc/goUpdateBigCustomersPage.xhtml",
		title : "编辑",
		width : 600,
		height : 520,
		onload : function(window){
			window.update.loadData({action: "edit", id: params.ids});
		},
		ondestroy : function(ation){
			if (ation == 'ok') _bigC.loadData();
		}
	}
	top.search.popDialog(data);
}

/**
 * 删除。
 * @param params
 * @returns
 */
function del (params) {
	var execute = "del";
	if (isrecycle)  execute = "delRecyce";
	_bigC.updateEntityState("删除", params.ids, execute);
}

/**
 * 恢复。
 * @param params
 * @returns
 */
function rest (params) {
	_bigC.updateEntityState("恢复", params.ids, 'rest');
}

/**
 * 审核。
 * @param params
 * @returns
 */
function audit (params) {
	var data = {
		url : __ctx + "/bcc/goAuditBigCustomersPage.xhtml",
		title : "审核",
		width : 450,
		height : 300,
		onload : function(window){
			window.audit.loadData({action: "audit", id: params.ids});
		},
		ondestroy : function(ation){
			if (ation == 'ok') _bigC.loadData();
		}
	}
	top.search.popDialog(data);
}

/**
 * 冻结。
 * @param params
 * @returns
 */
function freeze (params) {
	_bigC.updateEntityState("冻结", params.ids, 'freeze');
}

/**
 * 解冻。
 * @param params
 * @returns
 */
function unfreeze (params) {
	_bigC.updateEntityState("解冻", params.ids, 'unfreeze');
}

/**
 * 大客户结算。
 * @param params
 * @returns
 */
function settlement (params) {
	var data = {
		url : __ctx + "/bcc/goSettlementBigCustomersPage.xhtml",
		title : "结算",
		width : 750,
		height : 600,
		onload : function(window){
			window.sbc.loadData({id: params.ids});
		},
		ondestroy : function(ation){
			if (ation == 'ok') _bigC.loadData();
		}
	}
	top.search.popDialog(data);
}

/**
 * 查询数据。
 * @returns
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	_bigC._Grid.load({keyword: keyword, isrecycle: isrecycle});
}

/**
 * 查看详情。
 * @returns
 */
function queryDetail (params) {
	var data = { 
		url : __ctx + "/bcc/goBigCustomersDetailPage.xhtml", 
		title : "详情", 
		width : 600, 
		height : 500,
		onload : function(window) {
			window.update.loadData({id: params.ids});
		}, ondestroy : function(result){}
	};
	top.search.popDialog(data);
}

/**
 * 绘制单元格。
 * @param e
 * @returns
 */
function ondrawcell(e){
	var html = "";
	switch (e.column.field) {
		case "entitystate":
			html = sysGlobal.EntityStateObj[e.record.entitystate];
			break;
	}
	
	if (html) e.html = html;
};

/*加载功能按钮*/
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(_bigC._Grid, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	restoreMethod : "rest",
	deleteMethod : "del",
	validateRules : [
		{methodname : 'edit', conditions : [[1], [3]], ismultiple : false},
		{methodname : 'del', conditions : [[1],[2], [3], [11]], ismultiple : true},
		{methodname : 'rest', conditions : [[2]], ismultiple : true},
		{methodname : 'freeze', conditions : [[1]], ismultiple : true},
		{methodname : 'unfreeze', conditions : [[3]], ismultiple : true},
		{methodname : 'audit', conditions : [[10]], ismultiple : false},
		{methodname : 'settlement', conditions : [[1]], ismultiple : false}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['add', 'queryDetail']
	}
});
/*加载功能按钮*/

$(function () {
	_bigC.loadData();
	_bigC.customerid.url = __ctx + "/bcc/getBigCustomerList.xhtml";
	_bigC.customerid.load();
});