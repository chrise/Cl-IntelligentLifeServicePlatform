search.parse();

var grid = search.get('datagrid');
// 加载功能按钮
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(grid, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	updateEntitystateUrl : __ctx + '/users/updateEntitystate.xhtml',
	updateEntitystateMethodName : {
		frozen : {
			oper : 'frozen', 
			msg : '确定要冻结吗？'
		},
		thaw : {
			oper : 'thaw', 
			msg : '确定要解冻吗？'
		}
	},
	validateRules : [
		{methodname : 'edit', conditions : [[1], [3]], ismultiple : false},
		{methodname : 'del', conditions : [[1], [3]], ismultiple : true},
		{methodname : 'frozen', conditions : [[1]], ismultiple : true},
		{methodname : 'thaw', conditions : [[3]], ismultiple : true},
		{methodname : 'recycleRestore', conditions : [[2]], ismultiple : true},
		{methodname : 'recycleDelete', conditions : [[2]], ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['add', 'queryDetail']
	}
});

// 用户名
var username = search.get('username');
// 用户电话
var userphone = search.get('userphone');

// 页面加载完成执行
$(function() {
	grid.url = __ctx + '/users/findPageByParamsMap.xhtml';
	grid.load({isrecycle : isrecycle});
	
	$.post(__ctx + '/users/findAllUsernameAndUserphone.xhtml', {isrecycle : isrecycle}, function(result) {
		var selectData = result.result || new Array();
		selectData = [{entityid : '', username : '全部', userphone : '全部'}].concat(selectData);
		username.loadData(selectData);
		userphone.loadData(selectData);
	})
})

// 绘制表格单元格
function ondrawcell(e) {
	var field = e.column.field, record = e.record, fieldval = record[field];
	// 状态
	if (field == 'entitystate') {
		e.html = (fieldval == 1 ? '正常' : '冻结');
	}
}

// 新增
function add(e) {
	var data = {
		url : __ctx + "/users/goUsersEditPage.xhtml",
		title : "新增",
		width : 450,
		height : 450,
		onload : function(window) {
			window.loadData({action : 'add'});
		},
		ondestroy : function(result){
			if (result == 'ok')
				grid.reload();
		}
	}
	top.search.popDialog(data);
}

// 编辑
function edit(e) {
	var data = {
		url : __ctx + "/users/goUsersEditPage.xhtml",
		title : "编辑",
		width : 450,
		height : 450,
		onload : function(window) {
			window.loadData({action : 'edit', entityid : e.ids});
		},
		ondestroy : function(result){
			if (result == 'ok')
				grid.reload();
		}
	}
	top.search.popDialog(data);
}

// 查看详情
function queryDetail(e) {
	var data = {
		url : __ctx + "/users/goUsersViewPage.xhtml",
		title : "查看详情",
		width : 450,
		height : 400,
		onload : function(window) {
			window.loadData({entityid : e.ids});
		},
		ondestroy : function(result){
			if (result == 'ok')
				grid.reload();
		}
	}
	top.search.popDialog(data);
}

//查询
var queryParams = new Form('query-params');
function doSearch() {
	grid.load($.extend({isrecycle : isrecycle, usernametext : username.getText(), userphonetext : userphone.getText()}, queryParams.getData()));
}