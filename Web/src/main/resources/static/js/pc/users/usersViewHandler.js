search.parse();

// 表单对象
var form = new Form('users');

// 页面加载完成执行
$(function() {
})

// 父页面传值
function loadData(data) {
	parentData = data || {};
	
	$.post(__ctx + '/users/findExtensionByEntityid.xhtml', {entityid : parentData.entityid}, function(result) {
		if (result.status) {
			form.setData(result.result);
		} else {
			SearchUtils.msg(result.result);
		}
	})
}