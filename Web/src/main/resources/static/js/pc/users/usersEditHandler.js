search.parse();

// 表单对象
var form = new Form('users');
// 父页面值
var parentData;

var orgid = search.get('orgid');
var roleids = search.get('roleids');

SearchUtils.addLoadedSelecteds([orgid]);

// 页面加载完成执行
$(function() {
	orgid.url = __ctx + '/users/findAllOrganizations.xhtml';
	orgid.load();
	
	roleids.url = __ctx + '/users/findAuthRoles.xhtml';
	roleids.load();
})

// 父页面传值
function loadData(data) {
	parentData = data || {};
	
	if (parentData.action == 'edit') {
		search.get('loginname').setEnabled(false);
		search.get('loginpassword').rules = null;
		$.post(__ctx + '/users/findExtensionByEntityid.xhtml', {entityid : parentData.entityid}, function(result) {
			if (result.status) {
				form.setData(result.result);
			} else {
				SearchUtils.msg(result.result);
			}
		})
	}
}

// 保存
function save() {
	if (!form.validate()) {
		return;
	}
	$.post(__ctx + '/users/save.xhtml', $.extend({entityid : parentData.entityid, oper : parentData.action}, form.getData()), function(result) {
		if (result.status) {
			top.search.info({content : '操作成功', funl : function() {
				window.closeWindow('ok');
			}});
		} else {
			SearchUtils.msg(result.result);
		}
	})
}