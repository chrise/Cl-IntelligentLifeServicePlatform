search.parse();

/**
 * 实例化。
 */
var details = {
	_Grid : search.get("dataGrid"),
	settleid : "",
	loadData : function (param) {
		this.settleid = param.id;
		this._Grid.url = __ctx + "/goodsales/getGoodSalesDetailBySettleid.xhtml";
		this._Grid.load({settleid: param.id});
	},
}

/**
 * 查询。
 * @returns
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	details._Grid.load({keyword: keyword, settleid: details.settleid});
}

/**
 * 绘制单元格。
 * @returns
 */
function onDrawCell (e){
	var html = "";
	switch (e.column.field) {
		case "entitystate":
			html = e.record.entitystate === 4 ? "上架" : "有效";
			break;
	}
	
	if (html) e.html = html;
}