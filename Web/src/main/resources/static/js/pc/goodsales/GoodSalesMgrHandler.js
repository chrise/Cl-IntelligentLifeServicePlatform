search.parse();

var goodsales = {
	_Grid : search.get("dataGrid"),
	load : function () {
		this._Grid.url = __ctx + "/goodsales/getGoodSalesMgr.xhtml";
		this._Grid.load({isrecycle: isrecycle});
	},
}

/**
 * 新增。
 * @returns
 */
function add () {
	var data = {
		url : __ctx + "/goodsales/goUpdateGoodSalesPage.xhtml",
		title : "新增",
		width : 1000,
		height : 650,
		onload : function(window){
			window.update.loadData({action:"add"});
		},
		ondestroy : function(ation){
			if (ation == 'ok') goodsales._Grid.load();
		}
	}
	top.search.popDialog(data);
}

/**
 * 编辑。
 * @param param
 * @returns
 */
function edit (param) {
	var settleid = "";
	if (param.rows.length > 0) settleid = param.rows[0].record.settleid;
	var data = {
		url : __ctx + "/goodsales/goUpdateGoodSalesPage.xhtml",
		title : "编辑",
		width : 1000,
		height : 650,
		onload : function(window){
			window.update.loadData({action:"edit", id: settleid});
		},
		ondestroy : function(ation){
			if (ation == 'ok') goodsales._Grid.load();
		}
	}
	top.search.popDialog(data);
}

/**
 * 展开查询。
 * @returns
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	goodsales._Grid.load({keyword: keyword});
}

/**
 * 查看详情。
 * @param param 
 * @returns
 */
function queryDetail (param) {
	var data = { 
		url : __ctx + "/goodsales/goGoodSalesDetailPage.xhtml", 
		title : "详情", 
		width : 1000, 
		height : 500,
		onload : function(window) {
			window.details.loadData({id: param.rows[0].record.settleid});
		}, ondestroy : function(result){}
	};
	top.search.popDialog(data);
}

/**
 * 绘制单元格。
 * @returns
 */
function onDrawCell (e) {
	var html = "";
	switch (e.column.field) {
		case "totalNum":
			html = parseInt(e.record.validNum) + parseInt(e.record.putawayNum);
			break;
	}
	
	if (html) e.html = html;
}

//加载功能按钮
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(goodsales._Grid, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	restoreMethod : "restData",
	deleteMethod : "delData",
	validateRules : [
//		{methodname : 'edit', ismultiple : false},
		{methodname : 'rest', ismultiple : true},
		{methodname : 'del', ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['edit', 'queryDetail']
	}
});


$(function () {
	goodsales.load();
	// 绑定商户数据。
	search.get("keyword").url = __ctx + "/goods/getMerchantIdList.xhtml";
	search.get("keyword").load();
});