search.parse();

var update = {
	dataGrid1 : search.get("datagrid1"),
	dataGrid2 : search.get("datagrid2"),
	settleid : search.get("settleid"),
	rightbtn : $("#rightbtn"),
	leftbtn : $("#leftbtn"),
	_leftBtnEnabled : false,
	execute : "",
	_settleid : "",
	_RightData : null,
	delArr : new Array(),
	loadData : function (param) {
		this.execute = param.action;
		this.settleid.url = __ctx + "/goodsales/getSettleIdContr.xhtml";
		this.settleid.load();
		if (param.id != null && param.id != "") {
			this._settleid = param.id;
			this.loadRightData(this._settleid);
		}
	},
	loadRightData : function (settleid) {
		pcGlobal.sendRequest(__ctx + "/goodsales/getGoodSalesBySettleid.xhtml", {settleid: settleid}, function(data, status) {
			if (data.status) {
				update._RightData = JSON.parse(JSON.stringify(data.result)); // 缓存右侧原始数据。
				update.dataGrid2.loadData(data.result); // 重新载入数据。
			} else search.error({content: data.result});
		});
	},
	loadGoodData : function (param) {
		this.dataGrid1.url = __ctx + "/goodsales/getGoodByMerchantId.xhtml";
		this.dataGrid1.load({mid: param.mid, settleid: param.id});
	},
	addright : function () {
		var selects = this.dataGrid1.getSelecteds()
		if (selects.length <= 0) return;
		
		this.disableButton();
		
		var arry = new Array();
		var _LeftGrid = this.dataGrid1.getData(), settleid = this.settleid.getValue();
		$.each(selects, function (i, v) {
			var record = v.record;
			if (record.istate === 2 || record.istate === "2") {
				var goodsales = {};
				goodsales['entityid'] = record.id;
				goodsales['specid'] = record.id;
				goodsales['settleid'] = settleid;
				goodsales['pname'] = record.pname;
				goodsales['goodname'] = record.goodname;
				goodsales['goodorigin'] = record.goodorigin;
				goodsales['entitystate'] = 1;
				goodsales['id'] = record.id;
				goodsales['pid'] = record.pid;
				goodsales['goodorigins'] = record.goodorigins;
				goodsales['datatype'] = 'add';
				arry.push(goodsales);
				
				_LeftGrid = _LeftGrid.filter((item) => item.id !== record.id); // 删除数组里面被选中的对象。
			}
		});
		
		if (arry.length < 1) {
			search.warn({content : "请选择商品规格！"});
			return ;
		}
		
		var _dataGridArray2 = this.dataGrid2.getData();
		if (_dataGridArray2 != null && _dataGridArray2.length > 0) {
			$.each(arry, function (j, m) {
				var _flag = true;
				$.each(_dataGridArray2, function (i, v) {
					if (v.specid === m.specid && v.settleid === m.settleid) {
						_flag = false;
						return true;
					}
				});
				if (_flag) _dataGridArray2.push(m);
			});
			this.dataGrid2.loadData(_dataGridArray2); // 载入数据。
		}else this.dataGrid2.loadData(arry);
		
		// 去掉左面选择的数据。
		$.each(_LeftGrid, function (i, v) {
			var delState = true;
			if (v.pid == null || v.pid == "") { // 查询下面是否有子级。
				$.each(_LeftGrid, function (k, m) {
					if (m.pid != null && m.pid != "" && v.id === m.pid) {
						delState = false;
						return true;
					}
				});
				if (delState) _LeftGrid = _LeftGrid.filter((item) => item.id !== v.id); // 删除数组里面无效的父级对象。
			} 
		});
		this.dataGrid1.loadData(_LeftGrid);
	},
	removeright : function () {
		if (!this._leftBtnEnabled) return; // 右侧表格勾选中有在售商品。
		
		this.disableButton();
		
		var _RightGridArray = this.dataGrid2.getData();
		var _id = [], _DataGrid = this.dataGrid2.getSelecteds();
		$.each(_DataGrid, function (i, v) { // 过滤已上架数据。
			if (v.record.entitystate != 4) {
				_id.push(v.record.entityid);
				if (!v.record.hasOwnProperty('datatype')) update.delArr.push(v.record.entityid);
			}
		});
		var _dataGridArray2 = this.dataGrid2.getData();
		if (_id != null && _id.length > 0) {
			$.each(_dataGridArray2, function (j, m) {
				$.each(_id, function (i, v) {
					if (v == m.entityid) {
						_dataGridArray2 = _dataGridArray2.filter((item) => item.entityid !== v); // 删除选中数据。
						return true;
					}
				});
			});
			
			this.dataGrid2.loadData(_dataGridArray2); // 重新载入数据。
		}
		
		// 加载左侧数据。
		var _LeftGrid = this.dataGrid1.getData();
		
		$.each(_DataGrid, function (i, v) {
			var obj = v.record;
			if (obj.entitystate == 4 || obj.entitystate == '4') return true; // 在售商品不能去掉。
			var addState = false;
			var addObjs = [];
			if (_LeftGrid.length > 0) {
				$.each(_LeftGrid, function (k, m) {
					if (m.id === obj.pid) {
						addState = true;
						return false;
					}
				});
			}
			
			var addObjs = [];
			addObjs['id'] = obj.id;
			addObjs['pid'] = obj.pid;
			addObjs['goodname'] = obj.goodname;
			addObjs['goodorigin'] = obj.goodorigin;
			addObjs['goodorigins'] = obj.goodorigins;
			addObjs['istate'] = 2;
			addObjs['pname'] = obj.pname;
			_LeftGrid.push(addObjs);
			
			if (!addState) {
				var pidObjs = [];
				pidObjs['id'] = obj.pid;
				pidObjs['pid'] = null;
				pidObjs['goodname'] = obj.pname;
				pidObjs['goodorigin'] = obj.goodorigins;
				pidObjs['istate'] = 1;
				pidObjs['pname'] = null;
				_LeftGrid.push(pidObjs);
			}
		});
		
		this.dataGrid1.loadData(_LeftGrid); // 绑定左侧数据。
	},
	saveData : function () {
		var settleid = this.settleid.getValue();
		if (settleid === null || settleid === "") {
			search.warn({content : "请选择商户/入驻市场！"});
			return ;
		}
		
		var _dataGridArray2 = this.dataGrid2.getData();
		var id = new Array();
		if (_dataGridArray2.length > 0) {
			$.each(_dataGridArray2, function (i, v) {
				if (v.hasOwnProperty('datatype')) {
					var _flag = false;
					$.each(update._RightData, function (o, k) {
						if (k.specid === v.specid) {
							update.delArr.remove(k.entityid); // 删除记录id。
							_flag = true;
							return false;
						}
					});
					if (!_flag) id.push(v.entityid);
				}
			});
			
			if ((id === null || id.length === 0) && update.delArr.length === 0) {
				search.warn({content : "无数据变动！"});
				return ;
			}
		}
		
		pcGlobal.sendRequest(__ctx + "/goodsales/updateGoodSalesObj.xhtml", {settleid: settleid, specid: id.join(","), del: update.delArr.join(","), execute: update.execute}, function(data, status) {
			if (data.status) {
				search.info({content: data.result.msg,funl:function(){
					if(data.result.state) window.closeWindow('ok');
				}});
			} else search.error({content: data.result});
		});
	},
	onDatagrid1Selectchanged : function(e) {
		if (e.rows.length > 0 && this.rightbtn.hasClass("disabled")) this.rightbtn.removeClass("disabled");
		else if (e.rows.length <= 0 && !this.rightbtn.hasClass("disabled")) this.rightbtn.addClass("disabled");
	},
	onDatagrid2Selectchanged : function(e) {
		if (e.rows.length > 0) {
			this._leftBtnEnabled = true;
			$.each(e.rows, function (i, v) {
				var record = v.record;
				if (record.entitystate === 4 || record.entitystate === '4') {
					update._leftBtnEnabled = false;
					return false;
				}
			});
		} else this._leftBtnEnabled = false;
		
		if (this._leftBtnEnabled && this.leftbtn.hasClass("disabled")) this.leftbtn.removeClass("disabled");
		else if (!this._leftBtnEnabled && !this.leftbtn.hasClass("disabled")) this.leftbtn.addClass("disabled");
	},
	disableButton : function() {
		if (!this.rightbtn.hasClass("disabled")) this.rightbtn.addClass("disabled");
		if (!this.leftbtn.hasClass("disabled")) {
			this._leftBtnEnabled = false;
			this.leftbtn.addClass("disabled");
		}
		
		this.uncheckAll();
	},
	uncheckAll : function() {
		$.each($("thead").find("input[property='check']"), function(i, v) {
			if (v.checked) v.checked = false;
		});
	}
}

/**
 * 商户/入驻市场 下拉框值改变发生事件。
 * @param e
 * @returns
 */
function onValueChanged (e) {
	update.disableButton();
	
	if (e.value != "") {
		update.loadGoodData(update.settleid.getSelectedItem().record); // 加载左面商品树形数据。
		update.loadRightData(e.value);
	}
}

/**
 * 商户入驻加载成功绑定数据。
 * @param e
 * @returns
 */
function onLoaded (e) {
	if (update._settleid != "") {
		update.settleid.setValue(update._settleid);
	}
}

/**
 * 右侧绘制单元格。
 * @returns
 */
function onDrawCell (e) {
	var html = "";
	switch (e.column.field) {
		case "entitystate":
			html = e.record.entitystate === 4 ? "在售" : "正常";
			break;
	}
	
	if (html) e.html = html;
}

/**
 * 左侧表格选择变化事件。
 * @param e 事件参数。
 */
function datagrid1Selectchanged(e) {
	update.onDatagrid1Selectchanged(e);
}

/**
 * 右侧表格选择变化事件。
 * @param e 事件参数。
 */
function datagrid2Selectchanged(e) {
	update.onDatagrid2Selectchanged(e);
}

