search.parse();

var instance = {
	form : new Form("form"),
	entityid : null,
	loadData : function (data) {
		this.entityid = data.id;
	},
	saveData : function (params) {
		if (!this.form.validate()) return;
		var data = instance.form.getData();
		var payevidence = "";
		var data = {paytime: data.paytime, cancelreason: data.cancelreason, ids: instance.entityid};
		pcGlobal.sendRequest(__ctx + "/msmc/executeSettleCancel.xhtml", data, function(data, status) {
			if (data.status) {
				search.info({content: data.result.msg,funl:function(){
					if(data.result.state) window.closeWindow('ok');
				}});
			} else search.error({content: data.result});
		});
	}
}
