search.parse();

var addms = {
	_Grid : search.get("dataGrid"),
	loadData : function (param) {
		// 加载商户数据。
		search.get("keyword").url = __ctx + "/goods/getMerchantIdList.xhtml";
		search.get("keyword").load();
		// 绑定列表数据。
		this._Grid.url = __ctx + "/msmc/getMerchanstList.xhtml";
		this._Grid.load();
	}
}

/**
 * 保存数据。
 * @returns
 */
function saveData () {
	var ids = addms._Grid.getValue();
	if (ids === null || ids === "") {
		search.warn({content : "选择一条数据!"});
		return ;
	}
	
	var $url = __ctx + "/msmc/executeSaveMerchantSettlement.xhtml";
	var $form = $("<form></form>").attr("action", $url).attr("method", "post");
	$form.append($("<input></input>").attr("type", "hidden").attr("name", "ids").attr("value", ids));
	$form.appendTo('body').submit().remove();
	
//	pcGlobal.sendRequest(__ctx + "/msmc/executeSaveMerchantSettlement.xhtml", {ids: ids}, function(data, status) {
//		if (data.status) {
//			search.info({content: data.result.msg,funl:function(){
//				if(data.result.state) window.closeWindow('ok');
//			}});
//		} else search.error({content: data.result});
//	});
}

/**
 * 查询数据。
 * @returns
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	addms._Grid.load({keyword: keyword});
}