search.parse();

var instance = {
	form : new Form("form"),
	entityid : null,
	upload : function () {
		var params = [{"url": __ctx + "/commonUpload/uploadMerchantSettlementsFile.xhtml", "key": "msmc", "limit": "jpg,jpeg,png,bmp,gif", "num": 1}];
		uploader.init(params, this.saveData);
	},
	loadData : function (data) {
		this.entityid = data.id;
	},
	saveData : function (params) {
		var data = instance.form.getData();
		var payevidence = "";
		if (params.msmc.uploads.length > 0) payevidence = params.msmc.uploads[0].savePath;
		var data = {paytime: data.paytime, payevidence: payevidence, entityid: instance.entityid};
		pcGlobal.sendRequest(__ctx + "/msmc/executeSettleOver.xhtml", data, function(data, status) {
			if (data.status) {
				search.info({content: data.result.msg,funl:function(){
					if(data.result.state) window.closeWindow('ok');
				}});
			} else search.error({content: data.result});
		});
	}
}

/**
 * 保存数据。
 */
function saveData () {
	if (!instance.form.validate()) return;
	uploader.upload();
}

$(function () {
	instance.upload();
});