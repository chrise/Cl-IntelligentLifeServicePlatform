search.parse();

/**
 * 商户结算控制器。
 */
var _ms = {
	_Grid : search.get("dataGrid"),
	SettlementType : {"1": "自动结算","2": "手动结算"},
	PaymentType : {"1": "人工打款","2": "自动转账"},
	load : function () {
		this._Grid.url = __ctx + "/msmc/getMerchantSettlementsMgr.xhtml";
		this._Grid.load();
	}
}

/**
 * 自动结算。
 * @returns
 */
function automaticSettle () {
	var data = {
		url : __ctx + "/msmc/goMerchantAutomaticSettlementsPage.xhtml",
		title : "自动结算",
		width : 650,
		height : 550,
		onload : function(window){
			window.addms.loadData({action:"add"});
		},
		ondestroy : function(ation){
			_ms._Grid.load();
		}
	}
	top.search.popDialog(data);
}

/**
 * 手动结算。
 * @param param
 * @returns
 */
function manualSettle (param) {
	var $url = __ctx + "/msmc/executeManualSettleByIds.xhtml";
	var $form = $("<form></form>").attr("action", $url).attr("method", "post");
	$form.append($("<input></input>").attr("type", "hidden").attr("name", "ids").attr("value", param.ids));
	$form.appendTo('body').submit().remove();
}

/**
 * 导出文件。
 * @param param
 * @returns
 */
function exportExcel (param) {
	var $url = __ctx + "/msmc/executeExportExcelByIds.xhtml";
	var $form = $("<form></form>").attr("action", $url).attr("method", "post");
	$form.append($("<input></input>").attr("type", "hidden").attr("name", "ids").attr("value", param.ids));
	$form.appendTo('body').submit().remove();
}

/**
 * 结算完成。
 * @param param
 * @returns
 */
function settleOver (param) {
	var data = {
		url : __ctx + "/msmc/goMerchantSettlementOverPage.xhtml",
		title : "结算完成",
		width : 550,
		height : 450,
		onload : function(window){
			window.instance.loadData({id: param.ids});
		},
		ondestroy : function(ation){
			if (ation == 'ok') _ms._Grid.load();
		}
	}
	top.search.popDialog(data);
}

/**
 * 结算取消。
 * @param param
 * @returns
 */
function settleCancel (param) {
	var data = {
		url : __ctx + "/msmc/goMerchantSettlementCancelPage.xhtml",
		title : "结算取消",
		width : 450,
		height : 450,
		onload : function(window){
			window.instance.loadData({id: param.ids});
		},
		ondestroy : function(ation){
			if (ation == 'ok') _ms._Grid.load();
		}
	}
	top.search.popDialog(data);
}

/**
 * 绘制单元格。
 * @param e 
 * @returns
 */
function onDrawCell (e) {
	var html = "";
	switch (e.column.field) {
		case "entitystate":
			html = sysGlobal.EntityStateObj[e.record.entitystate];
			break;
		case "settlementtype":
			html = _ms.SettlementType[e.record.settlementtype];
			break;
		case "paymenttype":
			html = _ms.PaymentType[e.record.paymenttype];
			break;
	}
	
	if (html) e.html = html;
}

/**
 * 展开查询。
 * @returns
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	_ms._Grid.load({keyword: keyword});
}

/**
 * 加载功能按钮。
 */
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(_ms._Grid, {
	funcid : __param.funcid,
	isOpenDetails : false,
	isOpenQuery : true,
	isRecycle : false,
	validateRules : [
		{methodname : 'manualSettle', conditions : [[12]], ismultiple : true},
		{methodname : 'exportExcel', conditions : [[13]], ismultiple : true},
		{methodname : 'settleOver', conditions : [[13]], ismultiple : false},
		{methodname : 'settleCancel', conditions : [[13]], ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['automaticSettle', 'queryDetail']
	}
});

/**
 * 页面加载完执行。
 * @returns
 */
$(function () {
	_ms.load();
	search.get("keyword").url = __ctx + "/goods/getMerchantIdList.xhtml";
	search.get("keyword").load();
});