var isLogining = false;
var usernameCtl = $("#username"), passwordCtl = $("#password"), vericodeCtl = $("#vericode");

$(function() {
	//回车响应事件
	$(document).keydown(function(event) {
		if(event.keyCode == 13 && !isLogining) {
			onLoginClick();
		}
	});
});

//验证码点击事件
function onVericodeClick() {
	var src = __ctx + "/nr/vericode.xhtml?t=" + new Date().getTime();
	$("#codeimg").attr("src", src);
}

//登录点击事件
function onLoginClick() {
	isLogining = true;
	
	var data = {
		username : usernameCtl.val(), 
		password : passwordCtl.val(), 
		vericode : vericodeCtl.val()
	};
	pcGlobal.sendRequest(__ctx + "/nr/login.xhtml", data, function(data, status) {
		if (data.status) window.location.replace(__ctx + "/main.xhtml");
		else {
			search.warn({content : data.result, funl : function() {
				isLogining = false;
			}});
		}
	});
}

//重置点击事件
function onResetClick() {
	usernameCtl.val("");
	passwordCtl.val("");
	vericodeCtl.val("");
}