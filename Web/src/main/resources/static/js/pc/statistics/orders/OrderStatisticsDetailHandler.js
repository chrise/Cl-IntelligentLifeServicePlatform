searchComponents.parse();

var _detail = {
	_Grid : search.get("dataGrid"),
	_recordsGrid : search.get("RefundRecordsGrid"),
	form : new Form("order-form"),
	merchantname : search.get("merchantname"),
	customername : search.get("customername"),
	loadData : function (param) {
		this.merchantname.setValue((param.merchantname == "null") ? "" : decodeURIComponent(param.merchantname));
		this.customername.setValue((param.customer == "null") ? "" : decodeURIComponent(param.customer));
		this._recordsGrid.url = __ctx + "/orderstatistics/getRefundRecordByOrderId.xhtml";
		this._recordsGrid.load({orderid: param.entityid});
		this._Grid.url = __ctx + "/orderstatistics/getOrderGoodsByOrderId.xhtml";
		this._Grid.load({orderid: param.entityid});
		pcGlobal.sendRequest(__ctx + "/orderstatistics/getOrderStatisticsDetailByOrderId.xhtml", {id : param.entityid}, function(data, status) {
			if (data.status) {
				var obj = data.result.order;
				var province = (data.result.province) ? data.result.province + " " : "";
				var city = (data.result.city) ? data.result.city + " " : "";
				var county = (data.result.county) ? data.result.county + " " : "";
				var street = (data.result.street) ? data.result.street + " " : "";
				
				obj.detailaddress = province + city + county + street + (obj.detailaddress || "");
				obj.orderorigin = obj.orderorigin == 1 ? "线下" : "线上";
				obj.ordertype = obj.ordertype == 1 ? "普通" : (obj.ordertype == 2 ? "活动" : (obj.ordertype == 3 ? "充值" : "大客户"));
				obj.deliverystate = obj.deliverystate == 1 ? "待发货" : (obj.deliverystate == 2 ? "已发货" : (obj.deliverystate == 3 ? "已收货" : "已退货"));
				obj.deliverymode = obj.deliverymode == 1 ? "自提" : "市场配送";
				obj.paymentstate = obj.paymentstate == 1 ? "待付款" : (obj.paymentstate == 2 ? "已付款" : "已退款");
				obj.entitystate = sysGlobal.EntityStateObj[obj.entitystate];
				obj.deliverytime = (!obj.deliverytime || obj.deliverytime == 1) ? "标准时间" : "特定时间【" + obj.deliverystarttime + " 至 " + obj.deliveryendtime + "】";
				_detail.form.setData(data.result.order);
			} else search.error({content: data.result});
		});
	}
		
};

/**
 * 商品表格单元格绘制事件。
 * @param e 事件参数。
 */
function ongoodsdrawcell(e) {
	var html = "";
	switch (e.column.field) {
		case "refunded":
			html = (e.record.refunded == 18) ? "是" : "否";
			break;
	}
	if (html) e.html = html;
}

/**
 * 绘制单元格。
 * @param e
 * @returns
 */
function ondrawcell (e) {
	var html = "";
	switch (e.column.field) {
		case "partialrefund" :
			html = (e.record.partialrefund) ? "<span>是</span>" : "<span>否</span>";
			break;
		case "applytype":
			html = e.record.applytype == 1 ? "商户申请" : (e.record.applytype == 2 ? "用户申请" : (e.record.applytype == 3 ? "后台申请" : "系统申请"));
			break;
		case "entitystate":
			html = sysGlobal.EntityStateObj[e.record.entitystate];
			break;
	}
	
	if (html) e.html = html;
}