search.parse();

/**
 * 实例化
 */
var orderstatistics = {
	_Grid : search.get("dataGrid"),
	keyword : search.get("keyword"),
	merchantid : search.get("merchantid"),
	customerid : search.get("customerid"),
	entitystate : search.get("entitystate"), 
	_entityStateArr : [{val: 5, text: "待确认"}, {val: 6, text: "已确认"}, {val: 7, text: "已完成"}, {val: 9, text: "已取消"}, {val: 99, text: "已删除"}],
	orderorigin : search.get("orderorigin"), 
	_orderOriginArr : [{val: 1, text: "线下"}, {val: 2, text: "线上"}],
	ordertype : search.get("ordertype"), 
	_orderTypeArr : [{val: 1, text: "普通"}, {val: 2, text: "活动"}, {val: 3, text: "充值"}, {val: 4, text: "大客户"}],
	deliverystate : search.get("deliverystate"), 
	_deliveryStateArr : [{val: 1, text: "待发货"}, {val: 2, text: "已发货"}, {val: 3, text: "已收货"}, {val: 4, text: "已退货"}],
	deliverymode : search.get("deliverymode"), 
	_deliveryModeArr : [{val: 1, text: "自提"}, {val: 2, text: "市场配送"}],
	paymentstate : search.get("paymentstate"), 
	_paymentStateArr : [{val: 1, text: "待付款"}, {val: 2, text: "已付款"}, {val: 3, text: "已退款"}],
	loadData : function () {
		this._Grid.url = __ctx + "/orderstatistics/getOrderStatisticsMgr.xhtml";
		this._Grid.load();
	},
	executeModifyOrderState : function (ids) { // 修改状态。
		search.confirm({content: "确定执行标记发货操作吗？", funl : function() {
			 　pcGlobal.sendRequest(__ctx + "/orderstatistics/executeModifyOrderState.xhtml", {ids: ids, execute: "delivery"}, function(data, status) {
					if (data.status) {
						search.info({content: data.result.msg,funl:function(){
							orderstatistics._Grid.reload();
						}});
					} else search.error({content: data.result});
				});
			  }
		});
	}
};

/**
 * 处理退款。
 * @returns
 */
function refund (param) {
	var data = {
		url : __ctx + "/orderstatistics/goExecuteRefundPage.xhtml",
		title : "处理退款",
		width : 1000,
		height : 650,
		onload : function(window){
			var buyername = (param.rows[0].record.buyername == null || param.rows[0].record.buyername == "") ? param.rows[0].record.memberphone : param.rows[0].record.buyername + "【" + param.rows[0].record.customername +"】";
			var merchantname = (param.rows[0].record.merchantname == null || param.rows[0].record.merchantname == "") ? "" : param.rows[0].record.merchantname + "【" + param.rows[0].record.marketname +"】";
			window.instance.load({orderid: param.ids, customer: encodeURIComponent(buyername), merchantname: encodeURIComponent(merchantname)});
		},
		ondestroy : function(ation){
			if (ation == 'ok') orderstatistics._Grid.reload();
		}
	}
	top.search.popDialog(data);
}

/**
 * 发货。
 * @param param
 * @returns
 */
function delivery (param) {
	orderstatistics.executeModifyOrderState(param.ids);
}

/**
 * 查看详情。
 * @param param
 * @returns
 */
function queryDetail (params) {
	var data = { 
		url : __ctx + "/orderstatistics/goOrderStatisticsDetailPage.xhtml", 
		title : "详情", 
		width : 1000, 
		height : 650,
		onload : function(window) {
			var buyername = (params.rows[0].record.buyername == null || params.rows[0].record.buyername == "") ? params.rows[0].record.memberphone : params.rows[0].record.buyername + "【" + params.rows[0].record.customername +"】";
			var merchantname = (params.rows[0].record.merchantname == null || params.rows[0].record.merchantname == "") ? "" : params.rows[0].record.merchantname + "【" + params.rows[0].record.marketname +"】";
			window._detail.loadData({entityid: params.ids, customer: encodeURIComponent(buyername), merchantname: encodeURIComponent(merchantname)});
		}, ondestroy : function(result){}
	};
	top.search.popDialog(data);
}

/**
 * 查询。
 * @returns
 */
function onSearch () {
	var keyword = orderstatistics.keyword.getValue();
	var merchantid = orderstatistics.merchantid.getValue();
	var customerid = orderstatistics.customerid.getValue();
	var entitystate = orderstatistics.entitystate.getValue();
	var orderorigin = orderstatistics.orderorigin.getValue();
	var ordertype = orderstatistics.ordertype.getValue();
	var deliverystate = orderstatistics.deliverystate.getValue();
	var deliverymode = orderstatistics.deliverymode.getValue();
	var paymentstate = orderstatistics.paymentstate.getValue();
	
	orderstatistics._Grid.load({keyword: keyword, merchantid: merchantid, customerid: customerid, 
		entitystate: entitystate, orderorigin: orderorigin, ordertype: ordertype, deliverystate: deliverystate, deliverymode: deliverymode, paymentstate: paymentstate});
}

/**
 * 绘制单元格。
 * @param e
 * @returns
 */
function ondrawcell (e) {
	var html = "";
	switch (e.column.field) {
		case "merchantname" :
			html = (e.record.merchantname == null || e.record.merchantname == "") ? "<span></span>" : e.record.merchantname + "【" + e.record.marketname +"】";
			break;
		case "buyername":
			html = (e.record.buyername == null || e.record.buyername == "") ? e.record.memberphone : e.record.buyername + "【" + e.record.customername +"】";
			break;
		case "orderorigin":
			html = e.record.orderorigin == 1 ? "线下" : "线上";
			break;
		case "ordertype":
			html = e.record.ordertype == 1 ? "普通" : (e.record.ordertype == 2 ? "活动" : (e.record.ordertype == 3 ? "充值" : "大客户"));
			break;
		case "deliverystate":
			html = e.record.deliverystate == 1 ? "待发货" : (e.record.deliverystate == 2 ? "已发货" : (e.record.deliverystate == 3 ? "已收货" : "已退货"));
			break;
		case "deliverymode":
			html = e.record.deliverymode == 1 ? "自提" : "市场配送";
			break;
		case "paymentstate":
			html = e.record.paymentstate == 1 ? "待付款" : (e.record.paymentstate == 2 ? "已付款" : "已退款");
			break;
		case "refundstate":
			html = e.record.refundstate == 0 ? "<span></span>" : "待办理";
			break;
		case "deliverytime":
			html = (!e.record.deliverytime || e.record.deliverytime == 1) ? "标准时间" : "特定时间";
			break;
		case "entitystate":
			html = sysGlobal.EntityStateObj[e.record.entitystate];
			break;
	}
	
	if (html) e.html = html;
}

/**
 * 加载功能按钮。
 */
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(orderstatistics._Grid, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	validateRules : [
		{methodname : 'refund', fields:['refundstate'], conditions : [[1]], ismultiple : false},
		{methodname : 'delivery', fields:['entitystate','deliverystate','paymentstate'], conditions : [[6,1,2]], ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['queryDetail']
	}
});

$(function () {
	orderstatistics.loadData();
	orderstatistics.merchantid.url = __ctx + "/goods/getMerchantIdList.xhtml";
	orderstatistics.merchantid.load();
	orderstatistics.customerid.url = __ctx + "/bcc/getBigCustomerList.xhtml";
	orderstatistics.customerid.load();
	orderstatistics.entitystate.loadData(orderstatistics._entityStateArr);
	orderstatistics.orderorigin.loadData(orderstatistics._orderOriginArr);
	orderstatistics.ordertype.loadData(orderstatistics._orderTypeArr);
	orderstatistics.deliverystate.loadData(orderstatistics._deliveryStateArr);
	orderstatistics.deliverymode.loadData(orderstatistics._deliveryModeArr);
	orderstatistics.paymentstate.loadData(orderstatistics._paymentStateArr);
})