search.parse();

/**
 * 实例化。
 */
var instance = {
	form : new Form("form"),
	refundRecordsGrid : search.get("RefundRecordsGrid"),
	orderid : "",
	handledesc : search.get("handledesc"),
	load : function (param) {
		this.orderid = param.orderid;
		pcGlobal.sendRequest(__ctx + "/orderstatistics/getRefundMessageByOrderId.xhtml", {orderid : param.orderid}, function(data, status) {
			if (data.status) {
				var obj = data.result.order;
				var refundRecord = data.result.refundRecord;
				obj.customer = param.customer == "null" ? "" : decodeURIComponent(param.customer);
				obj.merchantname = param.merchantname == "null" ? "" : decodeURIComponent(param.merchantname);
				obj.applytime = new Date(refundRecord.applytime).format('yyyy-MM-dd HH:mm:ss') ;
				obj.partialrefund = refundRecord.partialrefund ? "是" : "否";
				obj.applydesc = refundRecord.applydesc;
				obj.refundtotal = refundRecord.refundtotal;
				obj.integralrefund = refundRecord.integralrefund;
				obj.diamondrefund = refundRecord.diamondrefund;
				obj.deliverystate = obj.deliverystate == 1 ? "待发货" : (obj.deliverystate == 2 ? "已发货" : (obj.deliverystate == 3 ? "已收货" : "已退货"));
				obj.paymentstate = obj.paymentstate == 1 ? "待付款" : (obj.paymentstate == 2 ? "已付款" : "已退款");
				obj.entitystate = sysGlobal.EntityStateObj[obj.entitystate];
				instance.refundRecordsGrid.loadData(data.result.refundGood);
				instance.form.setData(obj);
			} else search.error({content: data.result});
		});
	}
}

/**
 * 保存数据。
 * @returns
 */
function saveData (istate) {
	pcGlobal.sendRequest(__ctx + "/orderstatistics/executeRefundByOrderId.xhtml", {istate: istate, orderid: instance.orderid, handledesc: instance.handledesc.getValue()}, function(data, status) {
		if (data.status) {
			search.info({content: data.result.msg,funl:function(){
				if(data.result.state) window.closeWindow('ok');
			}});
		} else search.error({content: data.result});
	});
}