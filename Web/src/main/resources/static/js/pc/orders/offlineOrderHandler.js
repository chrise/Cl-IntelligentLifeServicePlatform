search.parse();

var markets = search.get('markets');
var merchants = search.get('merchants');
var goodnum = 5;

$(function() {
	search.get('goodsnum').setValue(goodnum);
	$.post(__ctx + '/nr/sim/orders/findMarketsData.xhtml', function(result) {
		if (result.status) {
			markets.loadData(result.result);
		}
	})
})

function loadGoods() {
	if (goodnum <= 0) {
		return;
	}
	$('.goodsone').remove();
	search.removeAll(['markets', 'merchants', 'goodsnum', 'totalprice']);
	for (var i = 1; i <= goodnum; i++) {
		var $goodsone = $('<tr class="goodsone">' +
	        	'<td class="lg"><label>商品：</label></td>' +
	            '<td class="lg">' +
	                '<div id="goods_' + i + '" class="search-select" idField="entityid" textField="goodname" onvaluechanged="goodschanged" width="220" height="26"></div>' +
	            '</td>' +
	            '<td class="lg"><label>价格(元)：</label></td>' +
	            '<td class="lg">' +
	                '<div id="goodprice_' + i + '" class="search-label" width="60" height="26"></div>' +
	            '</td>' +
	            '<td class="lg"><label>数量：</label></td>' +
	            '<td class="lg">' +
	                '<div id="goodnumber_' + i + '" class="search-textbox" enabled="false" onvaluechanged="goodnumberchanged" width="120" height="26"></div>' +
	            '</td>' +
	        '</tr>');
		$('#totalinfo').before($goodsone);
	}
	search.parse();
	merchantschanged();
}

function marketschanged() {
	var marketnode = markets.getSelectedItem();
	if (marketnode) {
		merchants.loadData(marketnode.record.merchants);
	}
}

function merchantschanged() {
	var merchantnode = merchants.getSelectedItem();
	if (merchantnode) {
		for (var i = 1; i <= goodnum; i++) {
			search.get('goods_' + i).loadData(merchantnode.record.goods);
		}
	}
}

function goodsnumchanged(e) {
	goodnum = e.value;
	loadGoods();
}

function goodschanged(e) {
	var index = this.id.split('_')[1];
	if (!e.value) {
		search.get('goodprice_' + index).setValue('');
		search.get('goodnumber_' + index).setValue('');
		search.get('goodnumber_' + index).setEnabled(false);
		return;
	}
	
	var goodsnode = this.getSelectedItem();
	if (goodsnode) {
		var record = goodsnode.record;
		search.get('goodprice_' + index).setValue(record.goodprice);
		search.get('goodnumber_' + index).setEnabled(true);
	}
}

function goodnumberchanged(e) {
	var totalprice = 0;
	for (var i = 1; i <= goodnum; i++) {
		if (search.get('goodprice_' + i) && search.get('goodnumber_' + i)) {
			var goodprice = search.get('goodprice_' + i).getValue();
			var goodnumber = search.get('goodnumber_' + i).getValue();
			if (goodprice && goodnumber) {
				totalprice += (goodprice * goodnumber);
			}
		}
	}
	if (search.get('totalprice')) {
		search.get('totalprice').setValue(totalprice);
	}
}

function confirm() {
	var requestparams = {}, rownum = 0;
	for (var i = 1; i <= goodnum; i++) {
		var saleid = search.get('goods_' + i).getValue();
		var goodnumber = search.get('goodnumber_' + i).getValue();
		if (saleid && goodnumber) {
			requestparams['offlinegoods[' + rownum + '].saleid'] = saleid;
			requestparams['offlinegoods[' + rownum + '].goodnumber'] = goodnumber;
			rownum++;
		}
	}
	if (rownum == 0) {
		top.search.info({content : '请选择商品'});
		return;
	}
	$.post(__ctx + '/nr/sim/orders/insertOfflineOrders.xhtml', requestparams, function(result) {
		if (result.status) {
			var qrcodesrc = CommonUtil.urlHandler(__ctx + '/nr/sim/orders/createQR.xhtml', {
				content : 'http://' + location.host + __ctx + '/nr/api/scanpay.xhtml?order=' + result.result
			});
			$('#qrcode-image').html('<img src="' + qrcodesrc + '"/>' +
					'<div><a href="javascript:void(0);" style="color: red;" onclick="ordersinfo(\'' + result.result + '\');">点击查看订单</a></div>');
		}
	})
}

function ordersinfo(ordersid) {
	var data = {
		url : __ctx + "/nr/sim/orders/goOrdersViewPage.xhtml",
		title : "订单详情",
		width : 650,
		height : 500,
		onload : function(window) {
			window.loadData({ordersid : ordersid});
		},
		ondestroy : function(result){
			if (result == 'ok')
				grid.reload();
		}
	}
	top.search.popDialog(data);
}