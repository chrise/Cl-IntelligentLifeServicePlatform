search.parse();

var form = new Form('orders');

function loadData(data) {
	$.post(__ctx + '/nr/sim/orders/findOrdersInfo.xhtml', {ordersid : data.ordersid}, function(result) {
		if (result.status) {
			var reqdata = result.result || {};
			reqdata.ordertime = reqdata.ordertime ? new Date(reqdata.ordertime).format('yyyy-MM-dd HH:mm:ss') : null;
			reqdata.confirmtime = reqdata.confirmtime ? new Date(reqdata.confirmtime).format('yyyy-MM-dd HH:mm:ss') : null;
			reqdata.paytime = reqdata.paytime ? new Date(reqdata.paytime).format('yyyy-MM-dd HH:mm:ss') : null;
			reqdata.completetime = reqdata.completetime ? new Date(reqdata.completetime).format('yyyy-MM-dd HH:mm:ss') : null;
			reqdata.canceltime = reqdata.canceltime ? new Date(reqdata.canceltime).format('yyyy-MM-dd HH:mm:ss') : null;
			reqdata.goodtotal += '元';
			reqdata.ordertotal += '元';
			reqdata.paymenttotal += '元';
			
			if (reqdata.ordersstate == 5) {
				reqdata.ordersstate = '待确认';
			} else if (reqdata.ordersstate == 6) {
				reqdata.ordersstate = '已确认';
			} else if (reqdata.ordersstate == 7) {
				reqdata.ordersstate = '已完成';
			} else if (reqdata.ordersstate == 9) {
				reqdata.ordersstate = '已取消';
			}
			form.setData(reqdata);
		}
	})
}