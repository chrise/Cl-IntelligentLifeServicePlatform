search.parse();

/**
 * 实例化控制器。
 */
var goods = {
	_Grid : search.get("dataGrid"),
	load : function () {
		this._Grid.url = __ctx + "/goods/getGoodsMgr.xhtml";
		this._Grid.load({isrecycle: isrecycle});
	},
	updateGoodState : function (text, entityid, execute) {
		search.confirm({content: "确定要执行" + text + "操作？", funl : function() {
			 　pcGlobal.sendRequest(__ctx + "/goods/updateEntitystateByGoodId.xhtml", {entityid: entityid, execute: execute}, function(data, status) {
					if (data.status) {
						search.info({content: data.result.msg,funl:function(){
							goods._Grid.load();
						}});
					} else search.error({content: data.result});
				});
			  }
		});
	}
};

/**
 * 新增。
 */
addGoods = function () {
	var data = {
		url : __ctx + "/goods/goUpdateGoodsPage.xhtml",
		title : "新增",
		width : 700,
		height : 600,
		onload : function(window){
			window.update.loadData({action:"add"});
		},
		ondestroy : function(ation){
			if (ation == 'ok') goods._Grid.load();
		}
	}
	top.search.popDialog(data);
}

/**
 * 编辑。
 */
editGoods = function (param) {
	var data = {
		url : __ctx + "/goods/goUpdateGoodsPage.xhtml",
		title : "编辑",
		width : 700,
		height : 600,
		onload : function(window){
			window.update.loadData({action:"edit", entityid: param.ids});
		},
		ondestroy : function(ation){
			if (ation == 'ok') goods._Grid.load();
		}
	}
	top.search.popDialog(data);
}

/**
 * 删除。
 */
function delData (param) {
	var execute = "del";
	if (isrecycle)  execute = "restDel"
	goods.updateGoodState("删除", param.ids, execute);
}

/**
 * 恢复。
 */
function restData (param) {
	goods.updateGoodState("恢复", param.ids, "rest");
}

/**
 * 展开查询。
 * @returns
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	goods._Grid.load({keyword: keyword, isrecycle: isrecycle});
}

/**
 * 查看详情。
 * @param params 行数据。
 * @returns
 */
function queryDetail(params){
	var data = { 
		url : __ctx + "/goods/goGoodsDetailPage.xhtml", 
		title : "详情", 
		width : 600, 
		height : 500,
		onload : function(window) {
			window.update.loadData({entityid: params.ids, merchantname: encodeURIComponent(params.rows[0].record.merchantname)});
		}, ondestroy : function(result){}
	};
	top.search.popDialog(data);
}

/**
 * 图文详情。
 * @param id
 * @returns
 */
function graphicDetail(id) {
	var data = { 
		url : __ctx + "/goods/goGoodGraphicDetailPage.xhtml", 
		title : "图文详情", 
		width : 600, 
		height : 500,
		onload : function(window) {
			window.detail.loadData({entityid: id});
		}, ondestroy : function(result){}
	};
	top.search.popDialog(data);
}

/**
 * 绘制单元格。
 * @param e
 * @returns
 */
function ondrawcell(e){
	var html = "";
	switch (e.column.field) {
		case "entitystate":
			html = sysGlobal.EntityStateObj[e.record.entitystate];
			break;
		case "operat":
			html = "<span style='color: #87CEFA;cursor: pointer;' onclick=\"graphicDetail('" + e.record.entityid + "')\">商品详情</span>";
			break;
	}
	
	if (html) e.html = html;
};

//加载功能按钮
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(goods._Grid, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	restoreMethod : "restData",
	deleteMethod : "delData",
	validateRules : [
		{methodname : 'editGoods', conditions : [[1]], ismultiple : false},
		{methodname : 'restData', conditions : [[2]], ismultiple : true},
		{methodname : 'delData', conditions : [[1],[2]], ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['addGoods', 'queryDetail']
	}
});


$(function () {
	goods.load();
});