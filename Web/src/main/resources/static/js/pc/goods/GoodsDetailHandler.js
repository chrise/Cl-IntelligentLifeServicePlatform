search.parse();

var update = {
	form : new Form("form"),
	settleid : search.get("settleid"),
	groupid : search.get("groupid"),
	merchantname : "",
	upload : function () {
		var params = [{"url": __ctx + "/commonUpload/uploadGoodPhotosFile.xhtml", "key": "goods", "limit": "jpg,jpeg,png,bmp,gif"},
			{"url": __ctx + "/commonUpload/uploadGoodPhotosFile.xhtml", "key": "coverphoto", "limit": "jpg,jpeg,png,bmp,gif", "num": 1}
		];
		uploader.init(params, this.saveData);
	},
	loadData : function (param) {
		this.merchantname = decodeURIComponent(param.merchantname);
		pcGlobal.sendRequest(__ctx + "/goods/getGoodsDetailByEntityid.xhtml", {entityid : param.entityid}, function(data, status) {
			if (data.status) {
				data.result.goods.merchantid = update.merchantname;
				update.form.setData(data.result.goods);
				uploader.loadFiles("goods", data.result.goodPhoto);
				uploader.loadFiles("coverphoto", data.result.coverPhoto);
				// 生成标段表格。
				if(data.result.goodsalebean && data.result.goodsalebean.length > 0){
					var goodsaleList = data.result.goodsalebean;
					for(var i = 0; i < goodsaleList.length; i++){
						var tr = '<tr id="sect_tr_' + i + '">'+
							'<td align="center" style="background:#f9f9f9;">' + (goodsaleList[i].specdefine == null ? "" : goodsaleList[i].specdefine) + '</td>'+
							'<td align="center" style="background:#f9f9f9;">' + (goodsaleList[i].meterunit == null ? "" : goodsaleList[i].meterunit) + '</td></tr>';
						if(i == 0)$("#tr_bud").after(tr);
						else $("#sect_tr_"+ (i - 1)).after(tr);
					}
				}
			}
			else search.error({content: data.result});
		});
	}
};


$(function () {
	update.upload();
});