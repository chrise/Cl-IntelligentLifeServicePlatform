search.parse();

var detail = {
	loadData : function (param) {
		pcGlobal.sendRequest(__ctx + "/goods/getGraphicDetailById.xhtml", {entityid : param.entityid}, function(data, status) {
			if (data.status) {
				$(window.document.body).append(data.result);
			} else search.error({content: data.result});
		});
	}
};
