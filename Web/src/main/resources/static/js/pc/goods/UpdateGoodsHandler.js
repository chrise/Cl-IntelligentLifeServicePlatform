search.parse();

var update = {
	form : new Form("form"),
	merchantid : search.get("merchantid"),
	groupid : search.get("groupid"),
	execute : "",
	_tr_row_id : 1,
	_del_cont : 0,
	layeditIndex : null,
	upload : function () {
		var params = [{"url": __ctx + "/commonUpload/uploadGoodPhotosFile.xhtml", "key": "goods", "limit": "jpg,jpeg,png,bmp,gif"},
			{"url": __ctx + "/commonUpload/uploadGoodPhotosFile.xhtml", "key": "coverphoto", "limit": "jpg,jpeg,png,bmp,gif", "num": 1}
		];
		uploader.init(params, this.saveData);
	},
	layui : function () {
		layui.use('layedit', function(){
			var layedit = layui.layedit;
			layedit.set({
				uploadImage: {
					url: __ctx + '/commonUpload/uploadGoodtextareaFile.xhtml', 
					accept: 'image',
					acceptMime: 'image/*',
					exts: 'jpg|png|gif|bmp|jpeg',
					size: 1024 * 10
				}
			});
			update.layeditIndex = layedit.build('gooddesc',{
			  height: 200
			});
		});
	},
	loadData : function (param) {
		this.execute = param.action;
		if (param.action === "add") update.generateTab(); // 生成表格。
		else if (param.action == "edit") {
			pcGlobal.sendRequest(__ctx + "/goods/getGoodsObjByEntityid.xhtml", {entityid : param.entityid}, function(data, status) {
				if (data.status) {
					update.form.setData(data.result.goods);
					uploader.loadFiles("goods", data.result.goodPhoto);
					uploader.loadFiles("coverphoto", data.result.coverPhoto);
					layui.layedit.setContent(update.layeditIndex, data.result.goods.gooddesc, false);
					
					// 生成标段表格。
					if(data.result.goodsalebean && data.result.goodsalebean.length > 0){
						var goodsaleList = data.result.goodsalebean;
						for(var i = 0; i < goodsaleList.length; i++){
							var tr = '<tr id="sect_tr_' + i + '">'+
								'<td><div id="specdefine_' + i + '" rules="required" class="search-textbox" width="200" height="30" ></div></td> ' +
								'<td><div id="meterunit_' + i + '" class="search-textbox" width="200" height="30" ></div></td> ' +
								'<td><i id="add_' + i+ '" class="fa fa-plus-circle" onclick="update.addTr(\'' + i + '\');"></i> '+
								'<i id="del_' + i + '" class="fa fa-trash-o" onclick="update.deleTr(\'' + i + '\');"></i>'+
								'<i id="restore_' + i + '" class="fa fa-reply" onclick="update.restoreTab(\'' + i + '\');" ></i>'+
								'<div id="pro_entityid_' + i + '" class="search-texthide"/></td></tr>';
							if(i == 0)$("#tr_bud").after(tr);
							else $("#sect_tr_"+ (i - 1)).after(tr);
							if(i != goodsaleList.length -1) $("#add_" + i ).hide();
							$("#restore_"+i).hide();
							search.parse();
							search.get("specdefine_" + i).setValue(goodsaleList[i].specdefine);
							search.get("meterunit_" + i).setValue(goodsaleList[i].meterunit);
							search.get("pro_entityid_" + i).setValue(goodsaleList[i].entityid);
							update._tr_row_id += 1;
						}
						update._tr_row_id -= 1; 
					} else update.generateTab(); // 生成表格。
				} else search.error({content: data.result});
			});
		}
	},
	generateTab : function () {
		var row = 0;
		var tr = '<tr id="sect_tr_' + row + '">'+
			'<td><div id="specdefine_' + row + '" rules="required" class="search-textbox" width="200" height="30" ></div></td> ' +
			'<td><div id="meterunit_' + row + '" rules="required" class="search-textbox" width="200" height="30" ></div></td> ' +
			'<td><i id="add_' + row+ '" class="fa fa-plus-circle" onclick="update.addTr(\'' + row + '\');"></i> '+
			'<i id="del_' + row + '" class="fa fa-trash-o" onclick="update.deleTr(\'' + row + '\');"></i>'+
			'<i id="restore_' + row + '" class="fa fa-reply" onclick="update.restoreTab(\'' + row + '\');" ></i>'+
			'<div id="pro_entityid_' + row + '" class="search-texthide"/></td></tr>';
		$("#tr_bud").after(tr);
		$("#restore_" + row).hide();
		search.parse();
		search.get("pro_entityid_" + row).setValue("add");
	},
	addTr : function(tr_id){ // 新增。
		var tr = '<tr id="sect_tr_' + this._tr_row_id + '">'+
			'<td><div id="specdefine_' + this._tr_row_id + '" rules="required" class="search-textbox" width="200" height="30" ></div></td> ' +
			'<td><div id="meterunit_' + this._tr_row_id + '" class="search-textbox" width="200" height="30" ></div></td> '+
			'<td><i id="add_' + this._tr_row_id + '" class="fa fa-plus-circle" onclick="update.addTr(\'' + this._tr_row_id + '\');"></i> '+
			'<i id="del_' + this._tr_row_id + '" class="fa fa-trash-o" onclick="update.deleTr(\'' + this._tr_row_id + '\');"></i>'+
			'<i id="restore_' + this._tr_row_id + '" class="fa fa-reply" onclick="update.restoreTab(\'' + this._tr_row_id + '\');" ></i>'+
			'<div id="pro_entityid_' + this._tr_row_id + '" class="search-texthide"/></td></tr>';
		$("#sect_tr_"+tr_id).after(tr);
		$("#add_"+tr_id).hide();
		$("#restore_"+this._tr_row_id).hide();
		search.parse();
		search.get("pro_entityid_" + this._tr_row_id).setValue("add");
		this._tr_row_id += 1;
	},
	deleTr : function(id){ // 删除。
		$("#del_"+id).hide();
		$("#restore_"+id).show();
		var entityid = search.get("pro_entityid_" + id).getValue() + "_del"
		search.get("pro_entityid_" + id).setValue(entityid);
		search.get("specdefine_"+id).setEnabled(false);
		search.get("specdefine_"+id).activateValidate(false);
		this._del_cont += 1;
	},
	restoreTab : function(id){ // 恢复。
		$("#del_" + id).show();
		$("#restore_" + id).hide();
		var entityid = search.get("pro_entityid_" + id).getValue();
		search.get("pro_entityid_" + id).setValue(entityid.split("_")[0]);
		search.get("specdefine_"+id).setEnabled(true);
		search.get("specdefine_"+id).activateValidate(true);
		this._del_cont -= 1;
	},
	saveData : function (params) {
		var data = update.form.getData();
		if (data.entityid == null || data.entity == "") {
			data.createtime == null;
			data.entitystate == 1;
		}
		var goodspecs = [];
		for (var i = 0; i < update._tr_row_id; i++) {
			goodspecs.push({
				entityid : search.get("pro_entityid_" + i).getValue(), 
				specdefine: search.get("specdefine_" + i).getValue(),
				meterunit: search.get("meterunit_" + i).getValue(),
			});
		}
		var del = params.goods.deletes.concat(params.coverphoto.deletes);
		
		var goods = {entityid: data.entityid, entitystate: 1, merchantid: data.merchantid, groupid: data.groupid, goodname: data.goodname, goodorigin: data.goodorigin, gooddesc: layui.layedit.getContent(update.layeditIndex)};
		var param = {goods: goods, goodphoto: params.goods.uploads, coverphoto: params.coverphoto.uploads, del: del.join(","), goodSpecsBean: goodspecs};
		pcGlobal.sendRequest(__ctx + "/goods/updateGoodsObj.xhtml", {data: JSON.stringify(param), execute: update.execute}, function(data, status) {
			if (data.status) {
				search.info({content: data.result.msg,funl:function(){
					if(data.result.state) window.closeWindow('ok');
				}});
			} else search.error({content: data.result});
		});
	}
};

/**
 * 保存数据。
 */
function saveData () {
	if (!update.form.validate()) return;
	if (!uploader.hasFile()) {
		search.info({content: "请上传附件!"});
		return;
	}
	uploader.upload();
}

$(function () {
	update.merchantid.url = __ctx + "/goods/getMerchantIdList.xhtml";
	update.merchantid.load();
	update.groupid.url = __ctx + "/goods/getGroupIdList.xhtml";
	update.groupid.load();
	update.upload();
	update.layui();
});