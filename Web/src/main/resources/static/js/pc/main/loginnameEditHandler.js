search.parse();

var form = new Form('loginname-form');

// 保存
function save() {
	if (!form.validate()) {
		return;
	}
	var formdata = form.getData();
	$.post(__ctx + '/main/updateLoginname.xhtml', formdata, function(result) {
		if (result.status) {
			top.search.info({content : '操作成功', funl : function() {
				window.closeWindow(formdata);
			}});
		} else {
			SearchUtils.msg(result.result);
		}
	})
}

// 验证登录名
function loginnameValidate(e) {
	if (!e.value) {
		e.pass = false;
		e.message = '不允许为空';
		return;
	}
	if(/^1[34578]\d{9}$/.test(e.value)){
		e.message = "不允许为手机号码";
		e.pass = false;
	}
}