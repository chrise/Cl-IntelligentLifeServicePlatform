search.parse();

// 表单对象
var form = new Form('password');

// 保存
function save() {
	if (!form.validate()) {
		return;
	}
	var formdata = form.getData();
	$.post(__ctx + '/main/updatePassword.xhtml', {oldpassword : formdata.oldpassword, newpassword : formdata.newpassword}, function(result) {
		if (result.status) {
			top.search.info({content : '操作成功', funl : function() {
				window.closeWindow('ok');
			}});
		} else {
			SearchUtils.msg(result.result);
		}
	})
}

// 新密码验证
function newpasswordValidate(e) {
	if (!e.value) {
		e.pass = false;
		e.message = '不允许为空';
		return;
	}
	var formdata = form.getData();
	if (e.value == formdata.oldpassword) {
		e.pass = false;
		e.message = '不能与原密码相同';
		return;
	}
}

// 确认密码验证
function againpasswordValidate(e) {
	if (!e.value) {
		e.pass = false;
		e.message = '不允许为空';
		return;
	}
	var formdata = form.getData();
	if (e.value != formdata.newpassword) {
		e.pass = false;
		e.message = '与新密码不一致';
		return;
	}
}