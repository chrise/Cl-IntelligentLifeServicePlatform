// 菜单容器
var $menu = $('#menu');
// 内容容器
var $content = $('#iframe-content');

// 页面加载完成执行
$(function() {
	loadmenu();
})

// 菜单展开/收缩
$menu.on('click', '.nav-item > a', function(){
	var $this = $(this);
    if ($this.next().is(':hidden')) {
        // 展开
        $('.nav-show ul').slideUp(300);
        $this.next('ul').slideDown(300);
        $this.parent('li').addClass('nav-show').siblings('li').removeClass('nav-show');
    } else {
        // 收缩
    	$this.next('ul').slideUp(300);
        $('.nav-item.nav-show').removeClass('nav-show');
    }
})

// 打开菜单页面
$menu.on('click', '.nav-item > ul > li', function() {
	var $this = $(this), nodedata = $this.data(), record = nodedata.record;
	$('.main-navigation').html(
		'<a>' + record.parentrecord.funcname + '</a>' +
		'<span>-</span>' +
		'<a>' + record.funcname + '</a>'
	);
	var requestParams = {funcid : record.entityid};
	$content.attr('src', __ctx + CommonUtil.urlHandler(record.invokemethod, requestParams));
})

// 加载菜单
function loadmenu() {
	$menu.empty();
	$.post(__ctx + '/main/findAuthByParentfuncAndFunctype.xhtml', {functype : [1, 2]}, function(result) {
		var funcs = result.result;
		var funcobjs = {};
		$.each(funcs, function(funcindex, func) {
			if (func.functype == 1) {
				// 模块
				var nodeobj = {record : func};
				nodeobj.html = $(
					'<li class="nav-item">' +
	                    '<a href="javascript:void(0);">' +
	                    	'<i class="' + func.showicon + '"></i>' +
	                    	'<span class="nav-one">' + func.funcname + '</span>' +
	                    	'<i class="fa fa-angle-right"></i>' +
	                    '</a>' +
	                    '<ul></ul>' +
	                '</li>'
				);
				funcobjs[func.entityid] = nodeobj;
			} else if (func.functype == 2) {
				// 节点
				var parentnodeobj = funcobjs[func.parentfunc];
				if (!parentnodeobj) {
					return true;
				}
				var $childcontainer = parentnodeobj.html.find('ul');
				var $childnode = $(
					'<li>' +
						'<a href="javascript:void(0);">' +
							'<span>' + func.funcname + '</span>' +
						'</a>' +
					'</li>'
				);
				$childnode.data('record', $.extend(func, {parentrecord : parentnodeobj.record}));
				$childcontainer.append($childnode);
			}
		})
		// 功能添加到页面上
		for (var funcobjkey in funcobjs) {
			$menu.append(funcobjs[funcobjkey].html);
		}
	})
}

// 退出登录
$('.wrap-top .fa-power-off').on('click', function() {
	top.search.confirm({content : "确定要退出吗？", funl : function() {
		window.location.href = __ctx + "/logout.xhtml";
	}});
})


// 修改密码
$('.wrap-top .fa-lock').on('click', function() {
	var data = {
		url : __ctx + "/main/goPasswordEditPage.xhtml",
		title : "修改密码",
		width : 300,
		height : 320,
		onload : function(window) {
		},
		ondestroy : function(result){
		}
	}
	top.search.popDialog(data);
})

// 更新登录名
function updateLoginname(thisdom) {
	var $parentdom = $(thisdom).parent();
	var data = {
		url : __ctx + "/main/goLoginnameEditPage.xhtml",
		title : "设置登录名",
		width : 300,
		height : 220,
		onload : function(window) {
		},
		ondestroy : function(result) {
			if (result == 'cancel') {
				return;
			}
			$parentdom.html(result.loginname);
		}
	}
	top.search.popDialog(data);
}

// 显示/隐藏用户信息
var $userinfo = $('.top-erji');
$(".wrap-top .fa-user").click(function(){
	if($userinfo.is(":hidden")){
		$userinfo.fadeIn(200);
	}else{
		$userinfo.slideUp(500);
	}
});

// 隐藏用户信息
$(document).on('click', function(e) {
	var filterselector = '.fa-user, .top-erji, .search_dialog_container, .search_message_container';
	if ($(e.target).is(filterselector) || $(e.target).parents(filterselector).length > 0) {
		// 排除元素
		return;
	}
	$userinfo.slideUp(500);
})

// 隐藏用户信息
var iframe = document.getElementById('iframe-content');
$content.ready(function() {
	iframe.contentDocument.onclick = function() {
		$userinfo.slideUp(500);
	};
})

// 隐藏用户信息
iframe.onload = function() {
	iframe.contentDocument.onclick = function() {
		$userinfo.slideUp(500);
	};
}