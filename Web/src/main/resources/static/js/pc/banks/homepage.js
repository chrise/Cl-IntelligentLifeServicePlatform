search.parse();
var tdg = search.get("datagrid");

/*加载功能按钮*/
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(tdg, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	restoreMethod : "recyclerest",
	deleteMethod : "recycledel",
	validateRules : [
		{methodname : 'edit', conditions : [[1]], ismultiple : false},
		{methodname : 'del', conditions : [[1],[2]], ismultiple : true},
		{methodname : 'recyclerest', conditions : [[2]], ismultiple : true},
		{methodname : 'recycledel', conditions : [[2]], ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['add','queryDetail']
	}
});
/*加载功能按钮*/

$(function(){
	tdg.url =  __ctx + "/banks/gettheallbanks.xhtml";
	tdg.load({"isrecycle": isrecycle});
})

function onSearch () {
	var keywordV = search.get("keyword").getValue();
	tdg.load({"key": keywordV});
}

function ondrawcell(e){
	if(e.column.field == "entitystate"){
		if(e.record.entitystate){
			if(e.record.entitystate == 1){
				e.html = "有效";
			}else if(e.record.entitystate == 2){
				e.html = "无效";
			}
		}
	}
}

function add(){
	var data = {
			url : __ctx + "/banks/goupdatepage.xhtml",
			title : "新增",
			width : 450,
			height : 300,
			onload : function(window){
				window.setData({action:"add"});
			},
			ondestroy : function(data){
				if(data && data.isload){
					tdg.reload();
				}
			}
	}
	top.search.popDialog(data);
}

function edit(params){
	var data = {
		url : __ctx + "/banks/goupdatepage.xhtml",
		title : "编辑",
		width : 450,
		height : 300,
		onload : function(window){
			window.setData({action:"edit", "id": params.ids});
		},
		ondestroy : function(data){
			if(data && data.isload){
				tdg.reload();
			}
		}
	}
	top.search.popDialog(data);
}

function del(params){
	changedistate(params.ids,2,"确定要删除吗?");
}

//查看详情
function queryDetail(params){
	var data = {
			url : __ctx + "/banks/golookinfopage.xhtml",
			title : "查看详情",
			width : 450,
			height : 300,
			onload : function(window){
				window.setData({action:"look", "id": params.ids});
			},
			ondestroy : function(data){
				if(data && data.isload){
					tdg.reload();
				}
			}
	}
	top.search.popDialog(data);
}

//回收站-恢复
function recyclerest(params){
	changedistate(params.ids,1,"确定要恢复吗?");
}
//回收站-删除
function recycledel(params){
	changedistate(params.ids,99,"确定要删除吗?");
}

function changedistate(ids,istate,msg){
	search.confirm({content: msg, funl : function() {
		 　pcGlobal.sendRequest(__ctx + "/banks/updatethebanksstate.xhtml", {"entityids": ids, "istate": istate}, function(data, status) {
				if (data.status) {
					search.info({content: data.result.msg,funl:function(){
						if(data.result.status){
							tdg.reload();
						}
					}});
				}
				else search.error({content: data.result});
			});
		  }
	});
}
