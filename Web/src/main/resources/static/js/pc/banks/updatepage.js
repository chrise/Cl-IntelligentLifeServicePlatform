search.parse();
var form = new Form("form");
var parentbank = search.get("parentbank");

$(function(){
	parentbank.url = __ctx + "/banks/gettheallparentbanks.xhtml";
	parentbank.load();
})

function setData(data){
	if(data.action == "edit"){
		pcGlobal.sendRequest(__ctx + "/banks/getthebankeditpagedata.xhtml", {"entityid":data.id}, function(result, status) {
			if (result.status) {
				if(!result.result.parentbank){
					result.result.parentbank = -1;	
				}
				form.setData(result.result);
			} else {
				search.error({content: result.result});
			}
		});
	}
}

function saveData(params){
	if (!form.validate()) {
		return;
	}
	var data = form.getData();
	
	pcGlobal.sendRequest(__ctx + "/banks/updatethebanks.xhtml", data, function(result, status) {
		if (result.status) {
			search.info({content: result.result.msg,funl:function(){
				if(result.result.status){
					window.closeWindow({"isload":true});
				}
			}});
		} else {
			search.error({content: result.result});
		}
	});
}

