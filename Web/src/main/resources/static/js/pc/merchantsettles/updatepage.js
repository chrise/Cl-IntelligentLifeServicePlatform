search.parse();
var form = new Form("form");
var marketid = search.get("marketid");
var merchantid = search.get("merchantid");

$(function(){
	marketid.url = __ctx + "/merchantsettles/gettheallmarkets.xhtml";
	marketid.load();
	merchantid.url = __ctx + "/merchantsettles/gettheallmerchants.xhtml";
	merchantid.load();
})

function emptyvalidate(e){
	if(!e.value || e.value == "-1"){
		e.pass = false;
		e.mssage = "请选择";
	}
}

function setData(data){
	if(data.action == "edit"){
		pcGlobal.sendRequest(__ctx + "/merchantsettles/getthemerchantsettleseditpagedata.xhtml", {"entityid":data.id}, function(result, status) {
			if (result.status) {
				form.setData(result.result);
			} else {
				search.error({content: result.result});
			}
		});
	}
}

function saveData(){
	if (!form.validate()) {
		return;
	}
	pcGlobal.sendRequest(__ctx + "/merchantsettles/updatethemerchantsettles.xhtml", form.getData(), function(data, status) {
		if (data.status) {
			search.info({content: data.result.msg,funl:function(){
				if(data.result.status){
					window.closeWindow({"isload":true});
				}
			}});
		} else {
			search.error({content: data.result});
		}
	});
}
