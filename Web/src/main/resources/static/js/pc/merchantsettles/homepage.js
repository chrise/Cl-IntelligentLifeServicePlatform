search.parse();
var tdg = search.get("datagrid");

$(function(){
	tdg.url =  __ctx + "/merchantsettles/gettheallmerchantsettles.xhtml";
	tdg.load({istate: 1});
})

function ondrawcell(e){
	if(e.column.field == "settlestall"){
		if(e.record.settlestall){
			e.html = "<a onclick=\"queryDetail("+e.record.entityid+")\" style=\"color:blue;\">"+e.record.settlestall+"</a>";
		}
	}
}

function add(){
	var data = {
			url : __ctx + "/merchantsettles/goupdatepage.xhtml",
			title : "新增",
			width : 600,
			height : 520,
			onload : function(window){
				window.setData({action:"add"});
			},
			ondestroy : function(data){
				if(data && data.isload){
					tdg.reload();
				}
			}
	}
	top.search.popDialog(data);
}

function edit(){
	var select = tdg.getSelecteds();
	if (!select || (select && select.length > 1)) {
		search.warn({content : '请选择一条数据!'});
		return;
	}
	var data = {
		url : __ctx + "/merchantsettles/goupdatepage.xhtml",
		title : "编辑",
		width : 600,
		height : 520,
		onload : function(window){
			window.setData({action:"edit", "id": select[0].record.entityid});
		},
		ondestroy : function(data){
			if(data && data.isload){
				tdg.reload();
			}
		}
	}
	top.search.popDialog(data);
}

function del(){
	var select = tdg.getSelecteds();
	if (!select) {
		search.warn({content : '请选择要删除的数据!'});
		return;
	}
	var ids = getselectids(select);
	changedistate(ids,2,"确定要删除吗?");
}

//查看详情
function queryDetail(id){
	var data = {
			url : __ctx + "/merchantsettles/golookinfopage.xhtml",
			title : "查看详情",
			width : 600,
			height : 520,
			onload : function(window){
				window.setData({action:"look", "id": id});
			},
			ondestroy : function(data){
				if(data && data.isload){
					tdg.reload();
				}
			}
	}
	top.search.popDialog(data);
}

//回收站-恢复
function recyclerest(){
	var select = tdg.getSelecteds();
	if (!select) {
		search.warn({content : '请选择要恢复的数据!'});
		return;
	}
	var ids = getselectids(select);
	changedistate(ids,1,"确定要恢复吗?");
}
//回收站-删除
function recycledel(){
	var select = tdg.getSelecteds();
	if (!select) {
		search.warn({content : '请选择要删除的数据!'});
		return;
	}
	var ids = getselectids(select);
	changedistate(ids,99,"确定要删除吗?");
}

function getselectids(select){
	var ids = [];
	$.each(select,function(i,v){
		ids.push(v.record.entityid);
	});
	return ids.join(",");
}

function changedistate(ids,istate,msg){
	search.confirm({content: msg, funl : function() {
		 　pcGlobal.sendRequest(__ctx + "/merchantsettles/updatethemerchantsettlesistate.xhtml", {"entityids": ids, "istate": istate}, function(data, status) {
				if (data.status) {
					search.info({content: data.result.msg,funl:function(){
						if(data.result.status){
							tdg.reload();
						}
					}});
				}
				else search.error({content: data.result});
			});
		  }
	});
}
