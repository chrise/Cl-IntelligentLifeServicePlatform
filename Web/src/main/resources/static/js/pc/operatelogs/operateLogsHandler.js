search.parse();

var grid = search.get('datagrid');
//加载功能按钮
var gridutil = new DatagridUtil(grid, {
	isOpenQuery : false
});

// 页面加载完成执行
$(function() {
	grid.url = __ctx + '/operatelogs/findPageByParamsMap.xhtml';
	grid.load();
})

// 表格单元格绘制
function ondrawcell(e) {
	var field = e.column.field, record = e.record, fieldval = record[field];
	// 操作时间
	if (field == 'operatetime') {
		e.html = new Date(fieldval).format('yyyy-MM-dd HH:mm:ss');
	}
	// 请求类型
	if (field == 'requesttype') {
		e.html = CommonUtil.getEnumText(fieldval, 'OperateLogsRequestType');
	}
	// 返回结果
	if (field == 'returnresult') {
		e.html = CommonUtil.getEnumText(fieldval, 'OperateLogsReturnResult');
	}
}

// 查看详情
function queryDetail(e) {
	var data = {
		url : __ctx + "/operatelogs/goOperateLogsViewPage.xhtml",
		title : "查看详情",
		width : 800,
		height : 550,
		onload : function(window) {
			window.loadData({entityid : e.ids});
		},
		ondestroy : function(result){
			if (result == 'ok')
				grid.reload();
		}
	}
	top.search.popDialog(data);
}

// 查询
var queryParams = new Form('query-params');
function doSearch() {
	if (!queryParams.validate()) {
		return;
	}
	grid.load(queryParams.getData());
}