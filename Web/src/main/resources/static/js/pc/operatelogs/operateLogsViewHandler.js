search.parse();

// 表单对象
var form = new Form('operatelogs');


// 页面加载完成执行
$(function() {
})

// 父页面传值
function loadData(data) {
	parentData = data || {};
	
	$.post(__ctx + '/operatelogs/findExtensionByEntityid.xhtml', {entityid : parentData.entityid}, function(result) {
		var result = result.result || {};
		result.requesttypeformat = CommonUtil.getEnumText(result.requesttype, 'OperateLogsRequestType');
		result.returnresultformat = CommonUtil.getEnumText(result.returnresult, 'OperateLogsReturnResult');
		form.setData(result);
	})
}