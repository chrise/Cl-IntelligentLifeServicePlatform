search.parse();
var form = new Form("form");
var notifytype = search.get("notifytype");

$(function(){
	notifytype.loadData(sysGlobal.NotifyType);
});

function setData(data){
	if(data.action == "edit"){
		pcGlobal.sendRequest(__ctx + "/notifytemplateconfigs/gettheeditpagedata.xhtml", {"entityid":data.id}, function(result, status) {
			if (result.status) {
				form.setData(result.result);
			} else {
				search.error({content: result.result});
			}
		});
	}
}

function saveData(){
	if (!form.validate()) {
		return;
	}
	pcGlobal.sendRequest(__ctx + "/notifytemplateconfigs/updatethetemplate.xhtml", form.getData(), function(data, status) {
		if (data.status) {
			search.info({content: data.result.msg,funl:function(){
				if(data.result.status){
					window.closeWindow({"isload":true});
				}
			}});
		} else {
			search.error({content: data.result});
		}
	});
}
