search.parse();
var tdg = search.get("datagrid");

/*加载功能按钮*/
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(tdg, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	restoreMethod : "recyclerest",
	deleteMethod : "recycledel",
	validateRules : [
		{methodname : 'edit', conditions : [[1]], ismultiple : false},
		{methodname : 'del', conditions : [[1],[2]], ismultiple : true},
		{methodname : 'recyclerest', conditions : [[2]], ismultiple : true},
		{methodname : 'recycledel', conditions : [[2]], ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['add','queryDetail']
	}
});
/*加载功能按钮*/

$(function(){
	tdg.url =  __ctx + "/notifytemplateconfigs/getthealltemplate.xhtml";
	tdg.load({"isrecycle": isrecycle});
})

function onSearch () {
	var keyword = search.get("keyword").getValue();
	tdg.load({"key": keyword, "isrecycle": isrecycle});
}

function ondrawcell(e){
	if(e.column.field == "notifytype"){
		if(e.record.notifytype == 1){
			e.html = "手机绑定";
		}else if(e.record.notifytype == 2){
			e.html = "身份认证";
		}else if(e.record.notifytype == 3){
			e.html = "到账提醒";
		}else if(e.record.notifytype == 4){
			e.html = "退款提醒";
		}else if(e.record.notifytype == 5){
			e.html = "结算提醒";
		}
	}
}

function add(){
	var data = {
			url : __ctx + "/notifytemplateconfigs/goupdatepage.xhtml",
			title : "新增",
			width : 600,
			height : 520,
			onload : function(window){
				window.setData({action:"add"});
			},
			ondestroy : function(data){
				if(data && data.isload){
					tdg.reload();
				}
			}
	}
	top.search.popDialog(data);
}

function edit(params){
	var data = {
		url : __ctx + "/notifytemplateconfigs/goupdatepage.xhtml",
		title : "编辑",
		width : 600,
		height : 520,
		onload : function(window){
			window.setData({action:"edit", "id": params.ids});
		},
		ondestroy : function(data){
			if(data && data.isload){
				tdg.reload();
			}
		}
	}
	top.search.popDialog(data);
}

function del(params){
	changedistate(params.ids,2,"确定要删除吗?");
}

//查看详情
function queryDetail(params){
	var data = {
			url : __ctx + "/notifytemplateconfigs/golookinfopage.xhtml",
			title : "查看详情",
			width : 600,
			height : 520,
			onload : function(window){
				window.setData({action:"look", "id": params.ids});
			},
			ondestroy : function(data){
				if(data && data.isload){
					tdg.reload();
				}
			}
	}
	top.search.popDialog(data);
}

//回收站-恢复
function recyclerest(params){
	changedistate(params.ids,1,"确定要恢复吗?");
}

//回收站-删除
function recycledel(params){
	changedistate(params.ids,99,"确定要删除吗?");
}

function changedistate(ids,istate,msg){
	search.confirm({content: msg, funl : function() {
		 　pcGlobal.sendRequest(__ctx + "/notifytemplateconfigs/updatethetemplateistate.xhtml", {"entityids": ids, "istate": istate}, function(data, status) {
				if (data.status) {
					search.info({content: data.result.msg,funl:function(){
						if(data.result.status){
							tdg.reload();
						}
					}});
				}
				else search.error({content: data.result});
			});
		  }
	});
}
