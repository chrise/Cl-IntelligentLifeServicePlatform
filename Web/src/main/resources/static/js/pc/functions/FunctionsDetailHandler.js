search.parse();

var instance = {
	form : new Form("form"),
	loadData : function (data) {
		pcGlobal.sendRequest(__ctx + "/funcmgr/getFunctionsDetailDataByFuncid.xhtml", {entityid : data.entityid}, function(data, status) {
			if (data.status) {
				data.result.rightsupport = data.result.rightsupport == 0 ? "否" : "是";
				instance.form.setData(data.result);
			}else search.error({content: data.result});
		});
	}
};