search.parse();

var instance = {
	form : new Form("form"),
	parentfunc : search.get("parentfunc"),
	functype : search.get("functype"),
	rightsupport : search.get("rightsupport"),
	action : "",
	loadData : function (data) {
		this.action = data.action;
		if (data.action == "add") this.parentfunc.setValue(data.entityid);
		else if (data.action == "edit") {
			pcGlobal.sendRequest(__ctx + "/funcmgr/getFuncObjByFuncid.xhtml", {entityid : data.entityid}, function(data, status) {
				if (data.status) instance.form.setData(data.result);
				else search.error({content: data.result});
			});
		}
	},
	saveFunc : function () {
		var data = instance.form.getData();
		data.entitystate = 1;
		if (data.entityid == null || data.entity == "") data.createtime == null;
		pcGlobal.sendRequest(__ctx + "/funcmgr/insertOrUpdateFuncObj.xhtml", $.extend({execute : instance.action}, data), function(data, status) {
			if (data.status) {
				search.info({content: data.result.msg,funl:function(){
					if(data.result.state) window.closeWindow('ok');
				}});
			} else search.error({content: data.result});
		});
	}
};

/**
 * 保存数据。
 */
function saveData () {
	if (!instance.form.validate()) return;
	instance.saveFunc();
}

/**
 * 类型联动右键支持。
 * @param e
 * @returns
 */
function onValueChanged (e) {
	if(e.value == 3){
		instance.rightsupport.setEditabled(true);
	}else{
		instance.rightsupport.setValue("");
		instance.rightsupport.setEditabled(false);
	}
}

/**
 * 页面加载
 */
$(function(){
	instance.parentfunc.url = __ctx + "/funcmgr/getParentFuncSelectList.xhtml";
	instance.parentfunc.load();
	instance.functype.loadData(sysGlobal.FuncType);
	instance.rightsupport.loadData(sysGlobal.YesOrNo);
});