search.parse();

/**
 * 实例化控制器。
 */
var instance = {
	_Grid : search.get("dataGrid"),
	loadData : function () {
		this._Grid.url = __ctx + "/funcmgr/getFuncMgrList.xhtml";
		this._Grid.load({isrecycle: isrecycle});
	},
	funcType : function (val) {
		var itype= sysGlobal.FuncType;
		var text = "";
		for (var i = 0 ; i < itype.length; i++) {
			if(val == itype[i].value) text = itype[i].text;
		}
		return text;
	},
	yesOrNo : function (val) {
		var itype= sysGlobal.YesOrNo;
		var text = "";
		for (var i = 0 ; i < itype.length; i++) {
			if(val == itype[i].value) text = itype[i].text;
		}
		return text;
	},
	updateFuncState: function(text, entityid, execute) {
		search.confirm({content: "确定要执行" + text + "操作吗？", funl : function() {
			 　pcGlobal.sendRequest(__ctx + "/funcmgr/updateEntitystateByFuncid.xhtml", {entityid: entityid, execute: execute}, function(data, status) {
					if (data.status) {
						search.info({content: data.result.msg,funl:function(){
							instance._Grid.load();
						}});
					} else search.error({content: data.result});
				});
			  }
		});
	}
}

/**
 * 新增。
 */
function addFunc (params) {
	if(params.ids != null && params.ids != ""){
		var data = {
			url : __ctx + "/funcmgr/goUpdateFuncMgrPage.xhtml",
			title : "新增",
			width : 600,
			height : 520,
			onload : function(window){
				window.instance.loadData({action:"add", entityid: params.ids});
			},
			ondestroy : function(ation){
				if (ation == 'ok') instance.loadData();
			}
		}
	}else{
		var data = {
			url : __ctx + "/funcmgr/goUpdateFuncMgrPage.xhtml",
			title : "新增",
			width : 600,
			height : 520,
			onload : function(window){
				window.instance.loadData({action:"add", entityid: "-1"});
			},
			ondestroy : function(ation){
				if (ation == 'ok') instance.loadData();
			}
		}
	}
	top.search.popDialog(data);
}

/**
 * 编辑。
 */
function editFunc (params) {
	var data = {
		url : __ctx + "/funcmgr/goUpdateFuncMgrPage.xhtml",
		title : "编辑",
		width : 600,
		height : 520,
		onload : function(window){
			window.instance.loadData({action:"edit", entityid: params.ids});
		},
		ondestroy : function(data){
			if (data == 'ok') instance.loadData();
		}
	}
	top.search.popDialog(data);
}

/**
 * 删除功能
 * @param params
 */
function delData (params) {
	var execute = "del";
	if (isrecycle) execute = "delRecyce";
	instance.updateFuncState("删除", params.ids, execute);
}

/**
 * 恢复。
 * @param params
 * @returns
 */
function restData(params) {
	instance.updateFuncState("恢复", params.ids, "rest");
}

/**
 * 查看详情。
 * @param params 功能ID。
 * @returns
 */
function queryDetail(params){
	var data = { 
		url : __ctx + "/funcmgr/goFunctionsDetailPage.xhtml", 
		title : "详情", 
		width : 600, 
		height : 500,
		onload : function(window) {
			window.instance.loadData({entityid: params.ids});
		}, ondestroy : function(result){}
	};
	top.search.popDialog(data);
}

/**
 * 查询数据。
 * @returns
 */
function onSearch () {
	var keyword = search.get("keyword").getValue();
	instance._Grid.load({keyword: keyword, isrecycle: isrecycle});
}

/**
 * 绘制单元格。
 * @param e
 * @returns
 */
function ondrawcell(e) {
	var html = "";
	switch (e.column.field) {
		case "functype":
			html = instance.funcType(e.record.functype);
			break;
		case "rightsupport":
			if (e.record.functype == 3) html = instance.yesOrNo(e.record.rightsupport);
			else html = "<span></span>";
			break;
	}
	
	if(html)e.html = html;
}


/*加载功能按钮*/
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(instance._Grid, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
//	recycleMethodName:'recy',
	restoreMethod : "restData",
	deleteMethod : "delData",
	validateRules : [
//		{methodname : 'addFunc',fields:['functype','entitystate'], conditions : [[3,1],[1,2]], ismultiple : false, allownull: true},
		{methodname : 'editFunc', conditions : [[1]], ismultiple : false},
		{methodname : 'delData', conditions : [[1],[2]], ismultiple : true},
		{methodname : 'restData', conditions : [[2]], ismultiple : true},
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['addFunc','queryDetail']
	}
});
/*加载功能按钮*/

/**
 * 加载完页面执行。
 * @returns
 */
$(function () {
	instance.loadData();
});