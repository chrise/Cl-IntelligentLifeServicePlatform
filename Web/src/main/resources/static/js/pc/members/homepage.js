search.parse();
var tdg = search.get("datagrid");

/*加载功能按钮*/
var gridutil = new DatagridUtil(tdg, {
	funcid : __param.funcid,
	validateRules : [
		{methodname : 'memberbinds', conditions : [[1]], ismultiple : false},
		{methodname : 'integralrecords', conditions : [[1]], ismultiple : false},
		{methodname : 'diamondrecords', conditions : [[1]], ismultiple : false},
		{methodname : 'receivingaddresses', conditions : [[1]], ismultiple : false}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['add','queryDetail']
	}
});
/*加载功能按钮*/

$(function(){
	tdg.url =  __ctx + "/members/gettheallmembers.xhtml";
	tdg.load();
})

function ondrawcell(e){
	if(e.column.field == "membertype"){
		if(e.record.membertype){
			if(e.record.membertype == 1){
				e.html = "普通会员";
			}else if(e.record.membertype == 2){
				e.html = "超级会员";
			}
		}
	}else if(e.column.field == "memberorigin"){
		if(e.record.memberorigin){
			if(e.record.memberorigin == 1){
				e.html = "微信关注";
			}else if(e.record.memberorigin == 2){
				e.html = "微信扫码";
			}else if(e.record.memberorigin == 3){
				e.html = "支付宝扫码";
			}else if(e.record.memberorigin == 4){
				e.html = "银联扫码";
			}
		}
	}
}

function queryDetail(params){
	
}

function memberbinds(params){
	var data = {
		url : __ctx + "/members/gomemberbindspage.xhtml?memberid="+params.ids,
		title : "会员绑定",
		width : 1100,
		height : 520
	}
	top.search.popDialog(data);
}

function integralrecords(params){
	var data = {
		url : __ctx + "/members/gointegralrecordspage.xhtml?memberid="+params.ids,
		title : "会员积分记录",
		width : 1100,
		height : 520
	}
	top.search.popDialog(data);
}

function diamondrecords(params){
	var data = {
		url : __ctx + "/members/godiamondrecordspage.xhtml?memberid="+params.ids,
		title : "会员钻石记录",
		width : 1100,
		height : 520
	}
	top.search.popDialog(data);
}

function receivingaddresses(params){
	var data = {
		url : __ctx + "/members/goreceivingaddressespage.xhtml?memberid="+params.ids,
		title : "收货地址",
		width : 1100,
		height : 520
	}
	top.search.popDialog(data);
}

