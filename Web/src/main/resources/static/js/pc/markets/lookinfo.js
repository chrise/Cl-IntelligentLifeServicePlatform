search.parse();
var form = new Form("form");

function setData(data){
	if(data.action == "look"){
		pcGlobal.sendRequest(__ctx + "/market/getthemarketeditpagedata.xhtml", {"entityid":data.id}, function(result, status) {
			if (result.status) {
				form.setData(result.result);
			} else {
				search.error({content: result.result});
			}
		});
	}
}
