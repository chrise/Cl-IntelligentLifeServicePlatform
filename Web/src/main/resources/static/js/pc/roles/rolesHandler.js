search.parse();

var grid = search.get('datagrid');
// 加载功能按钮
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(grid, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	updateEntitystateUrl : __ctx + '/roles/updateEntitystate.xhtml',
	validateRules : [
		{methodname : 'edit', conditions : [[1]], ismultiple : false},
		{methodname : 'del', conditions : [[1]], ismultiple : true},
		{methodname : 'recycleRestore', conditions : [[2]], ismultiple : true},
		{methodname : 'recycleDelete', conditions : [[2]], ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['add', 'queryDetail']
	}
});

// 页面加载完成执行
$(function() {
	grid.url = __ctx + '/roles/findPageByParamsMap.xhtml';
	grid.load({isrecycle : isrecycle});
})

// 绘制表格单元格
function ondrawcell(e) {
	var field = e.column.field, record = e.record, fieldval = record[field];
}

// 新增
function add(e) {
	var data = {
		url : __ctx + "/roles/goRolesEditPage.xhtml",
		title : "新增",
		width : 750,
		height : 450,
		onload : function(window) {
			window.loadData({action : 'add'});
		},
		ondestroy : function(result){
			if (result == 'ok')
				grid.reload();
		}
	}
	top.search.popDialog(data);
}

// 编辑
function edit(e) {
	var data = {
		url : __ctx + "/roles/goRolesEditPage.xhtml",
		title : "编辑",
		width : 750,
		height : 450,
		onload : function(window) {
			window.loadData({action : 'edit', entityid : e.ids});
		},
		ondestroy : function(result){
			if (result == 'ok')
				grid.reload();
		}
	}
	top.search.popDialog(data);
}

// 查看详情
function queryDetail(e) {
	var data = {
		url : __ctx + "/roles/goRolesViewPage.xhtml",
		title : "查看详情",
		width : 450,
		height : 550,
		onload : function(window) {
			window.loadData({entityid : e.ids});
		},
		ondestroy : function(result){
			if (result == 'ok')
				grid.reload();
		}
	}
	top.search.popDialog(data);
}

//查询
var queryParams = new Form('query-params');
function doSearch() {
	grid.load($.extend({isrecycle : isrecycle}, queryParams.getData()));
}