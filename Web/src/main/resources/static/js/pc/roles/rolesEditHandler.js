search.parse();

// 表单对象
var form = new Form('roles');
// 父页面值
var parentData;

// 授权功能
var funcids = search.get('funcids');

// 页面加载完成执行
$(function() {
	funcids.url = __ctx + '/main/findAuthByParentfuncAndFunctype.xhtml';
	funcids.load();
})

// 父页面传值
function loadData(data) {
	parentData = data || {};
	
	if (parentData.action == 'edit') {
		$.post(__ctx + '/roles/findMapByEntityid.xhtml', {entityid : parentData.entityid}, function(result) {
			if (result.status) {
				var resultdata = result.result;
				funcids.setValue(resultdata.funcids);
				form.setData(resultdata);
			} else {
				SearchUtils.msg(result.result);
			}
		})
	}
}

// 保存
function save() {
	if (!form.validate()) {
		return;
	}
	$.post(__ctx + '/roles/save.xhtml', $.extend({entityid : parentData.entityid, oper : parentData.action}, form.getData(), {funcids : funcids.getValue()}), function(result) {
		if (result.status) {
			top.search.info({content : '操作成功', funl : function() {
				window.closeWindow('ok');
			}});
		} else {
			SearchUtils.msg(result.result);
		}
	})
}