search.parse();

// 表单对象
var form = new Form('roles');

//授权功能
var funcids = search.get('funcids');

// 页面加载完成执行
$(function() {
	funcids.url = __ctx + '/roles/findAuthFunctionsByRoleid.xhtml';
})

// 父页面传值
function loadData(data) {
	parentData = data || {};
	funcids.load({roleid : parentData.entityid});
	$.post(__ctx + '/roles/findMapByEntityid.xhtml', {entityid : parentData.entityid}, function(result) {
		if (result.status) {
			form.setData(result.result);
		} else {
			SearchUtils.msg(result.result);
		}
	})
}