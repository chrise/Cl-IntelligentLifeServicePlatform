search.parse();
var form = new Form("form");
var parentbank = search.get("parentbank");
var bankid = search.get("bankid");

$(function(){
	var params = [
		{"url":__ctx+"/commonUpload/uploadMerchantsFile.xhtml","key":"idpositive","limit":"jpg,jpeg,png,bmp,gif","num":1}
		,{"url":__ctx+"/commonUpload/uploadMerchantsFile.xhtml","key":"idnegative","limit":"jpg,jpeg,png,bmp,gif","num":1}
		,{"url":__ctx+"/commonUpload/uploadMerchantsFile.xhtml","key":"bankcard","limit":"jpg,jpeg,png,bmp,gif","num":1}
		,{"url":__ctx+"/commonUpload/uploadMerchantsFile.xhtml","key":"businesslicense","limit":"jpg,jpeg,png,bmp,gif","num":1}
	];
	uploader.init(params, saveData);
	parentbank.url = __ctx+"/merchants/gettheallparentbanks.xhtml";
	parentbank.load();
});

function parentbankBlur(){
	var text = parentbank.getText();
	if(text != parentbank.getSelectedItem().record.bankname){
		parentbank.setValue("");
		parentbank.setText(text);
	}
}

function parentbankChanged(e){
	if(e.value){
		bankid.url = __ctx+"/merchants/gettheallsonbanks.xhtml";
		bankid.load({"entityid": e.value});
	}else{
		bankid.loadData([]);
	}
}

function parentbankValidate(e){
	if(!e.value && !parentbank.getText()){
		e.message = "不能为空";
		e.pass = false;
	}
}


function bankValidate(e){
	if(!e.value && !bankid.getText()){
		e.message = "不能为空";
		e.pass = false;
	}
}

function generateTab(){
	var row = 0;
	var tr = '<tr id="sect_tr_' + row + '">'+
		'<td><div id="marketid_' + row + '" class="search-select" idField="id" textField="name" width="150" height="30" defaultValue="-1" onvalidate="emptyvalidate" ></div></td> ' +
		'<td><div id="settlestall_' + row + '" rules="required" class="search-textbox" width="150" height="30" ></div></td> ' +
		'<td style="text-align:center">'+
		'<i style="cursor:pointer;" id="add_' + row+ '" class="fa fa-plus-circle" onclick="add(\'' + row + '\');"></i> '+
		'<i style="cursor:pointer;" id="del_' + row + '" class="fa fa-trash-o" onclick="del(\'' + row + '\');"></i>'+
		'<i style="cursor:pointer;display:none;" id="rest_' + row + '" class="fa fa-reply" onclick="rest(\'' + row + '\');" ></i>'+
		'<div id="pro_entityid_' + row + '" class="search-texthide"/></td></tr>';
	$("#table_bud").append(tr);
	search.parse();
	search.get("marketid_0").loadData(marketlist);
}

function loadtab(params){
	if(params && params.length > 0){
		$.each(params,function(i,v){
			var row = i;
			var tr = '<tr id="sect_tr_' + row + '">'+
				'<td><div id="marketid_' + row + '" class="search-select" idField="id" textField="name" width="150" height="30" defaultValue="-1" onvalidate="emptyvalidate" ></div></td> ' +
				'<td><div id="settlestall_' + row + '" rules="required" class="search-textbox" width="150" height="30" ></div></td> ' +
				'<td style="text-align:center">'+
				'<i style="cursor:pointer;" id="add_' + row+ '" class="fa fa-plus-circle" onclick="add(\'' + row + '\');"></i> '+
				'<i style="cursor:pointer;" id="del_' + row + '" class="fa fa-trash-o" onclick="del(\'' + row + '\');"></i>'+
				'<i style="cursor:pointer;display:none;" id="rest_' + row + '" class="fa fa-reply" onclick="rest(\'' + row + '\');" ></i>'+
				'<div id="pro_entityid_' + row + '" class="search-texthide"/></td></tr>';
			$("#table_bud").append(tr);
			search.parse();
			search.get("marketid_"+row).loadData(marketlist);
			search.get("pro_entityid_"+row).setValue(v.entityid);
			search.get("marketid_"+row).setValue(v.marketid);
			search.get("settlestall_"+row).setValue(v.settlestall);
			if(i != params.length - 1){
				$("#add_"+row).hide();
			}
		});
	}else{
		generateTab();
	}
}

function add(row){
	if(!search.get("settlestall_"+row).validate() || !search.get("marketid_"+row).validate()){
		return;
	}
	$("#add_"+row).hide();
	var rownext = parseInt(row) + 1;
	var tr = '<tr id="sect_tr_' + rownext + '">'+
	'<td><div id="marketid_' + rownext + '" class="search-select" idField="id" textField="name" width="150" height="30" defaultValue="-1" onvalidate="emptyvalidate" ></div></td> ' +
	'<td><div id="settlestall_' + rownext + '" rules="required" class="search-textbox" width="150" height="30" ></div></td> ' +
	'<td style="text-align:center">'+
	'<i style="cursor:pointer;" id="add_' + rownext+ '" class="fa fa-plus-circle" onclick="add(\'' + rownext + '\');"></i> '+
	'<i style="cursor:pointer;" id="del_' + rownext + '" class="fa fa-trash-o" onclick="del(\'' + rownext + '\');"></i>'+
	'<i style="cursor:pointer;display:none;" id="rest_' + rownext + '" class="fa fa-reply" onclick="rest(\'' + rownext + '\');" ></i>'+
	'<div id="pro_entityid_' + rownext + '" class="search-texthide"/></td></tr>';
	$("#table_bud").append(tr);
	search.parse();
	search.get("marketid_"+rownext).loadData(marketlist);
}

function emptyvalidate(e){
	if(!e.value || e.value == "-1"){
		e.pass = false;
		e.message = "请选择";
	}
}

function del(row){
	$("#del_"+row).hide();
	$("#rest_"+row).show();
	search.get("settlestall_"+row).setEnabled(false);
	search.get("settlestall_"+row).activateValidate(false);
	search.get("marketid_"+row).setEnabled(false);
	search.get("marketid_"+row).activateValidate(false);
}

function rest(row){
	$("#del_"+row).show();
	$("#rest_"+row).hide();
	search.get("settlestall_"+row).setEnabled(true);
	search.get("settlestall_"+row).activateValidate(true);
	search.get("marketid_"+row).setEnabled(true);
	search.get("marketid_"+row).activateValidate(true);
}

function setData(data){
	if(data.action == "edit"){
		pcGlobal.sendRequest(__ctx + "/merchants/getthemerchantseditpagedata.xhtml", {"entityid":data.id}, function(result, status) {
			if (result.status) {
				parentbank.setValue(result.result.parentbankid);
				bankid.loaded = function(){
					form.setData(result.result.formdata);
					bankid.loaded = null;
				}
				loadtab(result.result.settlelist);
				uploader.loadFiles("idpositive",result.result.files.idpositive);
				uploader.loadFiles("idnegative",result.result.files.idnegative);
				uploader.loadFiles("bankcard",result.result.files.bankcard);
				uploader.loadFiles("businesslicense",result.result.files.businesslicense);
			} else {
				search.error({content: result.result});
			}
		});
	}else{
		generateTab();
	}
}

function getTheStallData(){
	var array = [];
	var _row = $("#table_bud").find("tr").length - 1;
	for(var index = 0; index < _row; index++){
		if(search.get("settlestall_"+index).getEnabled() && search.get("settlestall_"+index).getValue() && search.get("marketid_"+index).getValue() != "-1"){
			var obj = new Object();
			obj["entityid"] = search.get("pro_entityid_"+index).getValue();
			obj["marketid"] = search.get("marketid_"+index).getValue();
			obj["marketname"] = search.get("marketid_"+index).getSelectedItem().record.name;
			obj["settlestall"] = search.get("settlestall_"+index).getValue();
			array.push(obj);
		}
	}
	return array;
}

function saveData(params){
	var data = form.getData();
	if(!data["entityid"]){
		data["entityid"] = null;
	}
	var bankobj = {};
	if(!data["bankid"]){
		bankobj["parentname"] = parentbank.getText();
		bankobj["sonname"] = bankid.getText();
	}
	data["idpositive"] = params["idpositive"].uploads[0] ? params["idpositive"].uploads[0].savePath : null;
	data["idnegative"] = params["idpositive"].uploads[0] ? params["idnegative"].uploads[0].savePath : null;
	data["bankcard"] = params["idpositive"].uploads[0] ? params["bankcard"].uploads[0].savePath : null;
	data["businesslicense"] = params["idpositive"].uploads[0] ? params["businesslicense"].uploads[0].savePath : null;
	var array = getTheStallData();
	pcGlobal.sendRequest(__ctx + "/merchants/updatemerchants.xhtml", {"data":JSON.stringify(data),"array":JSON.stringify(array),"bank":JSON.stringify(bankobj)}, function(result, status) {
		if (result.status) {
			search.info({content: result.result.msg,funl:function(){
				if(result.result.status){
					window.closeWindow({"isload":true});
				}
			}});
		} else {
			search.error({content: result.result});
		}
	});
}

function fupload(){
	getTheStallData();
	if (!form.validate()) {
		return;
	}
	if(!uploader.hasFile()){
		search.info({content: "请上传附件!"});
		return;
	}
	uploader.upload();
}
