search.parse();
var tdg = search.get("datagrid");
var merchantname = search.get("merchantname");
var merchantphone = search.get("merchantphone");

/*加载功能按钮*/
var isrecycle = __param.recycle ? true : false;
var gridutil = new DatagridUtil(tdg, {
	funcid : __param.funcid,
	isRecycle : isrecycle,
	restoreMethod : "recyclerest",
	deleteMethod : "recycledel",
	validateRules : [
		{methodname : 'edit', conditions : [[1]], ismultiple : false},
		{methodname : 'del', conditions : [[1],[2]], ismultiple : true},
		{methodname : 'start', conditions : [[3]], ismultiple : true},
		{methodname : 'ban', conditions : [[1]], ismultiple : true},
		{methodname : 'recyclerest', conditions : [[2]], ismultiple : true},
		{methodname : 'recycledel', conditions : [[2]], ismultiple : true}
	],
	validateOptions : {
		fields : ['entitystate'],
		ignoretypes : ['add','queryDetail']
	}
});
/*加载功能按钮*/

$(function(){
	tdg.url =  __ctx + "/merchants/gettheallmerchants.xhtml";
	tdg.load({"isrecycle": isrecycle});
	merchantname.url = __ctx + "/merchants/gettheallsearchmerchant.xhtml";
	merchantname.load();
	merchantphone.url = __ctx + "/merchants/gettheallsearchmerchantphonehash.xhtml";
	merchantphone.load();
})

function onSearch () {
	var merchantnameV = search.get("merchantname").getValue();
	var merchantphoneV = search.get("merchantphone").getValue();
	tdg.load({"searchnameentityid": merchantnameV, "searchphonehash": merchantphoneV, "isrecycle": isrecycle});
}

function ondrawcell(e){
	if(e.column.field == "entitystate"){
		if(e.record.entitystate){
			if(e.record.entitystate == 1){
				e.html = "有效";
			}else if(e.record.entitystate == 2){
				e.html = "无效";
			}else if(e.record.entitystate == 3){
				e.html = "冻结";
			}
		}
	}
}

function add(){
	var data = {
			url : __ctx + "/merchants/goupdatepage.xhtml",
			title : "新增",
			width : 600,
			height : 520,
			onload : function(window){
				window.setData({action:"add"});
			},
			ondestroy : function(data){
				if(data && data.isload){
					tdg.reload();
				}
			}
	}
	top.search.popDialog(data);
}

function edit(params){
	var data = {
		url : __ctx + "/merchants/goupdatepage.xhtml",
		title : "编辑",
		width : 600,
		height : 520,
		onload : function(window){
			window.setData({action:"edit", "id": params.ids});
		},
		ondestroy : function(data){
			if(data && data.isload){
				tdg.reload();
			}
		}
	}
	top.search.popDialog(data);
}

function del(params){
	changedistate(params.ids,2,"确定要删除吗?");
}

function ban(params){
	changedistate(params.ids,3,"确定要冻结吗?");
}

function start(params){
	changedistate(params.ids,1,"确定要解冻吗?");
}

//查看详情
function queryDetail(params){
	var data = {
			url : __ctx + "/merchants/golookinfopage.xhtml",
			title : "查看详情",
			width : 600,
			height : 520,
			onload : function(window){
				window.setData({action:"look", "id": params.ids});
			},
			ondestroy : function(data){
				if(data && data.isload){
					tdg.reload();
				}
			}
	}
	top.search.popDialog(data);
}

//回收站-恢复
function recyclerest(params){
	changedistate(params.ids,1,"确定要恢复吗?");
}
//回收站-删除
function recycledel(params){
	changedistate(params.ids,99,"确定要删除吗?");
}

function changedistate(ids,istate,msg){
	search.confirm({content: msg, funl : function() {
		 　pcGlobal.sendRequest(__ctx + "/merchants/updatemerchantsstate.xhtml", {"entityids": ids, "istate": istate}, function(data, status) {
				if (data.status) {
					search.info({content: data.result.msg,funl:function(){
						if(data.result.status){
							tdg.reload();
						}
					}});
				}
				else search.error({content: data.result});
			});
		  }
	});
}
