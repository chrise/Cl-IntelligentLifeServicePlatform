search.parse();
var form = new Form("form");
var parentbankname = search.get("parentbankname");
var bankname = search.get("bankname");

$(function(){
	var params = [
		{"url":__ctx+"/commonUpload/uploadMerchantsFile.xhtml","key":"idpositive","limit":"jpg,jpeg,png,bmp,gif","num":1}
		,{"url":__ctx+"/commonUpload/uploadMerchantsFile.xhtml","key":"idnegative","limit":"jpg,jpeg,png,bmp,gif","num":1}
		,{"url":__ctx+"/commonUpload/uploadMerchantsFile.xhtml","key":"bankcard","limit":"jpg,jpeg,png,bmp,gif","num":1}
		,{"url":__ctx+"/commonUpload/uploadMerchantsFile.xhtml","key":"businesslicense","limit":"jpg,jpeg,png,bmp,gif","num":1}
	];
	uploader.init(params, null, true);
});

function loadtab(params){
	if(params && params.length > 0){
		$.each(params,function(i,v){
			var row = i;
			var tr = '<tr id="sect_tr_' + row + '">'+
				'<td><div id="marketid_' + row + '" class="search-label" width="200"></div></td> ' +
				'<td><div id="settlestall_' + row + '" class="search-label" width="200" ></div></td> ' +
				'</tr>';
			$("#table_bud").append(tr);
			search.parse();
			search.get("marketid_"+row).setValue(v.marketname);
			search.get("settlestall_"+row).setValue(v.settlestall);
			if(i != params.length - 1){
				$("#add_"+row).hide();
			}
		});
	}
}


function setData(data){
	if(data.action == "look"){
		pcGlobal.sendRequest(__ctx + "/merchants/getthemerchantslookinfopagedata.xhtml", {"entityid":data.id}, function(result, status) {
			if (result.status) {
				form.setData(result.result.formdata);
				if(result.result.bank){
					parentbankname.setValue(result.result.bank.parentbankname);
					bankname.setValue(result.result.bank.bankname);
				}
				loadtab(result.result.settlelist);
				uploader.loadFiles("idpositive",result.result.files.idpositive);
				uploader.loadFiles("idnegative",result.result.files.idnegative);
				uploader.loadFiles("bankcard",result.result.files.bankcard);
				uploader.loadFiles("businesslicense",result.result.files.businesslicense);
			} else {
				search.error({content: result.result});
			}
		});
	}
}
