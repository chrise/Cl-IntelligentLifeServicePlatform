search.parse();

// 表单对象
var form = new Form('areas');
// 父页面值
var parentData;

// 父级区域
var parentarea = search.get('parentarea');

SearchUtils.addLoadedSelecteds([parentarea]);

// 页面加载完成执行
$(function() {
	$.post(__ctx + '/areas/findAll.xhtml', function(result) {
		var resultDatas = result.result || new Array();
		var parentareaDatas = [{entityid : -1, parentarea : null, areaname : '顶层区域'}];
		$.each(resultDatas, function() {
			if (!this.parentarea) {
				this.parentarea = -1;
			}
			parentareaDatas.push(this);
		})
		parentarea.loadData(parentareaDatas);
	})
})

// 父页面传值
function loadData(data) {
	parentData = data || {};
	
	if (parentData.action == 'edit') {
		$.post(__ctx + '/areas/findOneByEntityid.xhtml', {entityid : parentData.entityid}, function(result) {
			if (result.status) {
				var resultdata = result.result || {};
				resultdata.parentarea = resultdata.parentarea || -1;
				form.setData(resultdata);
			} else {
				SearchUtils.msg(result.result);
			}
		})
	} else if (parentData.action == 'add' && parentData.parentarea) {
		parentarea.setValue(parentData.parentarea);
	}
}

// 保存
function save() {
	if (!form.validate()) {
		return;
	}
	$.post(__ctx + '/areas/save.xhtml', $.extend({entityid : parentData.entityid, oper : parentData.action}, form.getData()), function(result) {
		if (result.status) {
			top.search.info({content : '操作成功', funl : function() {
				window.closeWindow('ok');
			}});
		} else {
			SearchUtils.msg(result.result);
		}
	})
}