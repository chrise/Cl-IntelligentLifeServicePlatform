$(function(){
	$(".open-popup").click(function(){
		window.top.shaixuan();
	})
})

//查看详情
function lookinfo(entityid){
	window.top.half(entityid);
}

//撤销
function bindsclick(entityid){
	//阻止详情弹框弹出
	var e = window.event || arguments.callee.caller.arguments[0];
	if (e.stopPropagation) {
		e.stopPropagation();
	} else e.cancelBubble = true;
	
	window.top.$.confirm({
		text: '你确定要撤销该结算吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/updatethemerchantsettlementstoback.xhtml",{"entityid":entityid}, "数据加载中", function(result){
				if(result.status){
					if(result.result.status){
						window.top.mobileGlobal.info(result.result.msg);
						window.location.reload();
					}else{
						window.top.mobileGlobal.warn(result.result.msg);
					}
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

