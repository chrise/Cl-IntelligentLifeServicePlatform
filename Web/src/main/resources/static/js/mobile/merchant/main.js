$(function(){
	var mainWidth = $(".weui-tab").outerWidth();
	var rightWidth = $(".weui-navbar").outerWidth();
	$(".weui-tab__bd").width(mainWidth - rightWidth);
	
	infoClick(0);
});

//打开商户信息
function infoClick(index){
	$("#merchantinfo").attr("src", __merchantctx + "/merchantinfo.xhtml");
	$($(".weui-navbar__item")[index]).addClass("weui-bar__item--on").siblings().removeClass("weui-bar__item--on");
}

//打开商品管理
function itemClick(index){
	$("#merchantinfo").attr("src", __merchantctx + "/merchantitem.xhtml");
	$($(".weui-navbar__item")[index]).addClass("weui-bar__item--on").siblings().removeClass("weui-bar__item--on");
}

//打开商户结算
var checkstate = [];
function accountClick(index){
	$("#merchantinfo").attr("src", __merchantctx + "/merchantaccount.xhtml?istate="+checkstate.join(","));
	$($(".weui-navbar__item")[index]).addClass("weui-bar__item--on").siblings().removeClass("weui-bar__item--on");
}

//打开资金记录
function fundClick(index) {
	$("#merchantinfo").attr("src", __merchantctx + "/merchantfund.xhtml");
	$($(".weui-navbar__item")[index]).addClass("weui-bar__item--on").siblings().removeClass("weui-bar__item--on");
}

/*资金记录-详情*/
function popupRecordDetail(recordid){
	//$("#fund").popup();
	$('.weui-screening-categories').addClass('weui-screening-categories-show');
	$.showLoading("数据加载中");
	$("#RecordDetail").load(
		__ctx + "/mobile/merchant/getRecordDetailByRecordId.xhtml",
		{recordid: recordid},
		function(response,status,xhr){
			$.hideLoading();
		}
	);
}

/*资金记录-关闭弹框*/
function close(){
	$('.weui-screening-categories').removeClass('weui-screening-categories-show');
}

/*资金记录-筛选框*/
function popupRecordSearch () {
	checkstate = [];
	$("#popup-capitalrecord-search").popup();
}

/*资金记录-筛选框-确定*/
function submiRecordSearch () {
	$.closePopup();
	var obj = $("#merchantinfo").eq(0);
	var iframeWin = obj[0].contentWindow;
	iframeWin.instance.loadCapitalRecord({recordtype: checkstate.join(",")});
}

/*商户结算-详情*/
function half(entityid){
	$("#half").load(
		__ctx + "/mobile/mobilemanager/getthemerchantsettlementinfo.xhtml",
		{"entityid": entityid},
		function(response,status,xhr){
			$("#half").popup();
		}
	);
}

/*筛选*/
function shaixuan(){
	checkstate = [];
	$("#shaixuan").popup();
}

/**
 * 商品管理-库存。
 * @param saleid 销售id。
 * @param goodstock 库存。
 * @returns
 */
function masking(saleid, goodstock){
	mobileGlobal.popInputWindow("库存", "库存数量", goodstock , function(value){
		var param = {saleid: saleid, goodstock: value};
		mobileGlobal.sendRequest(__ctx + "/mobilegoods/executeModifyGoodstockBySaleid.xhtml",  param, "数据加载中", function(result){
			if(result.status){
				var obj = $("#merchantinfo").eq(0);
				var iframeWin = obj[0].contentWindow;
				iframeWin.instance.modifyPageData();
			} else mobileGlobal.error("网络异常!");
		});
	});
}

/**
 * 商品管理-调价。
 * @param saleid 销售id。
 * @param goodprice 单价。
 * @returns
 */
function readjust(saleid, goodprice){
	mobileGlobal.popInputWindow("调价", "调价金额", goodprice , function(value){
		var param = {saleid: saleid, goodprice: value};
		mobileGlobal.sendRequest(__ctx + "/mobilegoods/executeModifyGoodpriceBySaleid.xhtml",  param, "数据加载中", function(result){
			if(result.status){
				var obj = $("#merchantinfo").eq(0);
				var iframeWin = obj[0].contentWindow;
				iframeWin.instance.modifyPageData();
			} else mobileGlobal.error("网络异常!");
		});
	});
}

/*商户信息-结算*/
function accounts(money){
	mobileGlobal.popInputWindow("结算", "结算金额", money , function(value){
		if(parseFloat(value) > parseFloat(money)){
			mobileGlobal.warn("结算金额超出");
			return;
		}
		if(parseFloat(value) <= parseFloat(0)){
			mobileGlobal.warn("结算金额不能小于等于0");
			return;
		}
		mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/updatethemerchantmoeny.xhtml",{"moeny":value}, "数据加载中", function(result){
			if(result.status){
				if(result.result.status){
					mobileGlobal.info(result.result.msg);
					accountClick(2);
				}else{
					mobileGlobal.warn(result.result.msg);
				}
			}
		});
	});
}

/*商户信息-查看*/
function info(path){
	$(".weui-photo-browser-modal").remove();
	var pb1 = $.photoBrowser({
        items: [
        	path
        ],
	});
	pb1.open();
}

//结算撤销
function toback(entityid){
	$.confirm({
		text: '你确定要撤销该结算吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/updatethemerchantsettlementstoback.xhtml",{"entityid":entityid}, "数据加载中", function(result){
				if(result.status){
					$.closePopup();
					if(result.result.status){
						mobileGlobal.info(result.result.msg);
						accountClick(2);
					}else{
						mobileGlobal.warn(result.result.msg);
					}
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

//筛选选择
function shaixuancheck(e,istate){
	$(e).toggleClass("weui-curt");
	if($(e).is(".weui-curt")){
		checkstate.push(istate);
	}else{
		checkstate.splice($.inArray(istate,checkstate),1);
	}
}

//筛选重置
function resetSearch($id){
	$.each($($id).find("a[class='weui-curt']"),function(i,v){
		$(v).removeClass("weui-curt");
	});
	checkstate = [];
}

//筛选搜索
function shaixuanSearch(){
	$.closePopup();
	accountClick(2);
}