$(function(){
	var mainHeight = $(".weui-tab__bd-item").height();
	var titleHeight = $(".weui-cell_select").outerHeight();
	$(".weui-user-list").height(mainHeight - titleHeight);
})

/**
 * 实例化。
 */
var instance = {
	loadGoodMgrData : function (param) { // 加载商品数据。
		$.showLoading("数据加载中");
		$("#goodMgr_List").load(
				__ctx + "/mobile/merchant/getGoodMgrByMerchantid.xhtml",
				param,
				function(response,status,xhr){
					$.hideLoading();
				}
		);
	},
	showSelect : function ($id) { // 显示商品管理-顶部的分类、市场、状态。
		$("ul[class='erji']").each(function(i, v){
			if ($(this).attr("id") != $id) $(this).hide();
	    });
		if ($("#" +$id).css("display") == "none") $("#" +$id).show();
		else $("#" +$id).hide();
	},
	onSearchGroup : function (groupid) { // 按分类搜索。
		var param = {};
		if (groupid != "all") {
			param['groupid'] = groupid ;
			$("#pidGroup").text($("#" + groupid).text());
			$("#pidGroupVal").val(groupid);
		} else {
			$("#pidGroupVal").val("all");
			$("#pidGroup").text("全部分类");
		}
		if ($("#pidMarketVal").val() != "all") param['marketid'] = $("#pidMarketVal").val() ;
		if ($("#pidStateVal").val() != "all") param['salestate'] = $("#pidStateVal").val() ;
		this.loadGoodMgrData(param);
		$("#group").hide();
	},
	onSearchMarket : function (marketid) { // 按市场搜索。
		var param = {};
		if (marketid != "all") {
			param['marketid'] = marketid ;
			$("#pidMarket").text($("#" + marketid).text());
			$("#pidMarketVal").val(marketid);
		} else {
			$("#pidMarketVal").val("all");
			$("#pidMarket").text("全部市场");
		}
		if ($("#pidGroupVal").val() != "all") param['groupid'] = $("#pidGroupVal").val() ;
		if ($("#pidStateVal").val() != "all") param['salestate'] = $("#pidStateVal").val() ;
		this.loadGoodMgrData(param);
		$("#market").hide();
	},
	onSearchIstate : function (salestate) {
		var param = {};
		if (salestate != "all") {
			param = {salestate: salestate} ;
			$("#pidState").text($("#istate_" + salestate).text());
			$("#pidStateVal").val(salestate);
		} else {
			$("#pidStateVal").val("all");
			$("#pidState").text("全部");
		}
		if ($("#pidGroupVal").val() != "all") param['groupid'] = $("#pidGroupVal").val() ;
		if ($("#pidMarketVal").val() != "all") param['marketid'] = $("#pidMarketVal").val() ;
		this.loadGoodMgrData(param);
		$("#goodstate").hide();
	},
	evenMore : function ($id) { // 弹出更多。
		$("div[class='weui-cmtbox']").each(function(i, v){
			if ($(this).attr("id") != $id) $(this).hide();
	    });
		if ($("#" + $id).css("display") === "none") $("#" + $id).show();
		else $("#" + $id).hide();
	},
	popupInventory : function (saleid) { // 库存。
		window.top.masking(saleid, $("#goodstock_" + saleid).text());
	},
	popupPricing : function (saleid) { // 调价。
		window.top.readjust(saleid, $("#goodprice_" + saleid).text());
	},
	executeModifyGoodsaleState : function (saleid, salestate) { // 修改商品销售状态。
		var istate = ($("#salestate_" + saleid).val() === 4 || $("#salestate_" + saleid).val() === "4") ? 1 : 4;
		if (istate == 4 && ($("#goodstock_" + saleid).text() === "0" ||  $("#goodstock_" + saleid).text() === 0)) {
			top.mobileGlobal.info("该商品库存为0，不能上架"); 
			return;
		}
		var param = {saleid: saleid, istate: istate};
		mobileGlobal.sendRequest(__ctx + "/mobilegoods/executeModifyGoodStateBySaleid.xhtml",  param, "数据加载中", function(result){
			if(result.status){
				var param = {};
				if ($("#pidGroupVal").val() != "all") param['groupid'] = $("#pidGroupVal").val() ;
				if ($("#pidMarketVal").val() != "all") param['marketid'] = $("#pidMarketVal").val() ;
				if ($("#pidStateVal").val() != "all") param['salestate'] = $("#pidStateVal").val() ;
				instance.loadGoodMgrData(param);
			} else top.mobileGlobal.error("网络异常!");
		});
	},
	modifyPageData: function () {
		var param = {};
		if ($("#pidGroupVal").val() != "all") param['groupid'] = $("#pidGroupVal").val() ;
		if ($("#pidMarketVal").val() != "all") param['marketid'] = $("#pidMarketVal").val() ;
		if ($("#pidStateVal").val() != "all") param['salestate'] = $("#pidStateVal").val() ;
		instance.loadGoodMgrData(param);
	}
}

$(function () {
	instance.loadGoodMgrData();
})