
//结算
function settlement(e){
	var money = $(e).attr("data");
	window.top.accounts(money);
}

//点击查看图片
function lookpic(e){
	var path = $(e).attr("data");
	window.top.info(path);
}

//切换
function toggle(e,itype){
	var ischeck = $(e).is(':checked');
	mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/updatethemerchantsmnotifyorautosettle.xhtml",{"istate":ischeck, "itype": itype}, "数据加载中", function(result){
		if(result.status){
			if(itype == 2 && ischeck){
				$("#weui-settle-accounts").hide();
			}else{
				$("#weui-settle-accounts").show();
			}
			window.top.mobileGlobal.info(result.result.msg);
		}
	});
}