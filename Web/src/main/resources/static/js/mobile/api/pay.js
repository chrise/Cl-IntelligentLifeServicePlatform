//页面加载完成事件
$(function() {
	instance.wakeupCashier();
});

//查看订单点击事件
function viewClick() {
	instance.gotoOrderList();
}

//全局实例对象
var instance = {
	payresult : false,
	payresultCtl : $("#payresult"),
	resulttextCtl : $("#resulttext"),
	resultamountCtl : $("#resultamount"),
	wakeupCashier : function() {
		switch (__trade.pay_api) {
			case "weixin":
				this.wakeupWeixinCashier();
				break;
			case "alipay":
				this.wakeupAlipayCashier();
				break;
			case "unionpay":
				break;
			default:
				mobileGlobal.warn("未知支付接口");
				break;
		}
	},
	wakeupWeixinCashier : function() {
		if (typeof(WeixinJSBridge) === "undefined") {
			if (document.addEventListener) {
				document.addEventListener("WeixinJSBridgeReady", this.onWeixinBridgeReady, false);
			} else if (document.attachEvent) {
				document.attachEvent("WeixinJSBridgeReady", this.onWeixinBridgeReady);
				document.attachEvent("onWeixinJSBridgeReady", this.onWeixinBridgeReady);
			} else mobileGlobal.warn("支付接口未就绪");
		} else this.onWeixinBridgeReady();
	},
	onWeixinBridgeReady : function() {
		if (__trade.prepay_id === "false") instance.paySuccess();
		else {
			var params = {
				appId : __trade.appId,
				timeStamp : __trade.timeStamp,
				nonceStr : __trade.nonceStr,
				package : __trade.package,
				signType : __trade.signType,
				paySign : __trade.paySign
			};
			WeixinJSBridge.invoke("getBrandWCPayRequest", params, function(data) {
				if (data.err_msg === "get_brand_wcpay_request:ok") instance.paySuccess();
				else instance.payFailed();
			});
		}
	},
	wakeupAlipayCashier : function() {
		if (!__trade.trade_no) this.paySuccess();
		else {
			AlipayJSBridge.call("tradePay", {tradeNO : __trade.trade_no}, function(data) {
				switch (data.resultCode) {
					case "9000":
						instance.paySuccess();
						break;
					case "8000":
						instance.payPending();
						break;
					default:
						instance.payFailed();
						break;
				}
			});
		}
	},
	paySuccess : function() {
		this.showResult(true, "支付成功");
	},
	payFailed : function() {
		this.showResult(false, "支付失败");
	},
	payPending : function() {
		this.showResult(false, "支付处理中");
	},
	showResult : function(success, text) {
		this.payresult = success;
		
		if (success) this.resultamountCtl.removeClass("hide");
		this.resulttextCtl.text(text);
		this.payresultCtl.removeClass("hide");
	},
	gotoOrderList : function() {
		var tabindex = (this.payresult) ? 1 : 2;
		window.location.replace(__ctx + "/mobile/member/mobileorderm.xhtml?tabindex=" + tabindex);
	}
};