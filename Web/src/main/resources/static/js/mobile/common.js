//移动端全局对象
var mobileGlobal = {
	sendRequest : function(url, data, loading, success, error) {	//发送请求
		if (loading != null) {
			var text = loading || "请稍后...";
			$.showLoading(text);
		}
		
		$.ajax({
			url : url,
			data : data,
			success : function(data, status) {
				//请求成功处理
				if (success) {
					success.call(mobileGlobal, data, status);
					if (loading != null) $.hideLoading();
				} else {
					if (loading != null) $.hideLoading();
					if (data.status) mobileGlobal.info("操作成功");
					else mobileGlobal.warn("操作失败");
				}
			},
			error : function(xhr, msg, e) {
				//请求错误处理
				if (error) {
					error.call(mobileGlobal, xhr, msg, e);
					if (loading != null) $.hideLoading();
				} else {
					if (loading != null) $.hideLoading();
					mobileGlobal.error("网络请求错误");
				}
			}
		});
	},
	showMessage : function(message, type) {
		$.toptip(message, type);
	},
	info : function(message) {
		this.showMessage(message, "success");
	},
	warn : function(message) {
		this.showMessage(message, "warning");
	},
	error : function(message) {
		this.showMessage(message, "error");
	},
	//刷新参数
	pageFreshParams : {
		//第一次加载最大数
		"firstLimitMax" : 20,
		//每次刷新加载数
		"everyTimeFreshNum" : 10
	},
	deliveryMode : {
		1: "自提", 2: "市场配送"
	},
	paymentMode : {
		1: "微信", 2: "支付宝", 3: "银联"
	},
	invoicesMode : {
		1: "普通发票", 2: "电子发票"
	},
	popInputWindow : function(title, label, value, okfunc){
		var html = $("<div class='weui-masking'>"+
					"<div class='weui-stock'>"+
						"<div class='weui-stock-title'><h2>"+ title +"<i class='fa fa-close'></i></h2></div>"+
						"<div class='weui-stock-content'>"+
							"<label>"+label+"：</label>"+
							"<input type='text' style='width:200px; height:30px; line-height:30px; border-radius: 10px; border:1px #e1e1e1 solid;'/>"+
						"</div>"+
						"<ul class='weui-stock-btn'>"+
							"<li class='weui-first-child'><a>确认</a></li>"+
							"<li class='weui-last-child'><a>取消</a></li>"+
						"</ul>"+
					"</div>"+
					"</div>");
		
		var input = html.find("input");
		input.val(value);
		
		html.find(".weui-first-child").click(function(){
			if(okfunc){
				if (!/^(0|([1-9]\d*))(\.\d{1,2})?$/.test(input.val())) {
					mobileGlobal.warn("无效数字，请重新输入！");
					return;
				}
				okfunc.call(html,input.val());
			}
			html.remove();
		});
		html.find(".weui-last-child").click(function(){
			html.remove();
		});
		html.find("i").click(function(){
			html.remove();
		});
		
		$(document.body).append(html);
	}
};