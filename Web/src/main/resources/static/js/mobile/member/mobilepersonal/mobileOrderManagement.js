//$(function(){
//	var ITEM_ON = "weui-bar__item--on";
//
//	  var showTab = function(a) {
//	    var $a = $(a);
//	    if($a.hasClass(ITEM_ON)) return;
//	    var href = $a.attr("href");
//
//	    if(!/^#/.test(href)) return ;
//
//	    $a.parent().parent().find("."+ITEM_ON).removeClass(ITEM_ON);
//	    $a.addClass(ITEM_ON);
//
//	    var bd = $a.parents(".weui-tab").find(".weui-tab__bd");
//
//	    bd.find(".weui-tab__bd-item--active").removeClass("weui-tab__bd-item--active");
//
//	    $(href).addClass("weui-tab__bd-item--active");
//	  }
//
//	  $.showTab = showTab;
//
//	  $(document).off("click",".weui-navbar__item, .weui-tabbar__item").on("click", ".weui-navbar__item", function(e) {
//	    var $a = $(e.currentTarget);
////	    var href = $a.attr("href");
//	    if($a.hasClass(ITEM_ON)) return;
////	    if(!/^#/.test(href)) return;
//
//	    e.preventDefault();
//
//	    showTab($a);
//	  });
//});

$(function(){
	var $clickdom;
	$.each($(".weui-navbar").find("a"),function(i,v){
		//绑定点击事件
		$(v).bind("click",function(){
			tabclick($(v));
		});
		if(!tabindex){
			if(i==0){
				$clickdom = $(v);
			}
		}else{
			if(tabindex == $(v).attr("data")){
				$clickdom = $(v);
			}
		}
	});
	if($clickdom) $clickdom.click();
});

//tab点击事件
function tabclick(e){
	if(e.hasClass("weui-bar__item--on")) return;
	e.parent().parent().find(".weui-bar__item--on").removeClass("weui-bar__item--on");
    e.addClass("weui-bar__item--on");
    tabindex = e.attr("data");
    loadData(e.attr("data"));
}

//加载订单
function loadData(tabIndex){
	if(tabIndex){
		$.showLoading("数据加载中");
		$("#orderlist").load(
			__ctx + "/mobile/mobilemanager/getthesomeorders.xhtml",{"tabIndex": tabIndex},
			function(response,status,xhr){
				$.hideLoading();
			}
		);
	}
}

//删除订单
function deleteorder(orderid){
	window.top.$.confirm({
		text: '你确定要删除吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/deletetheorder.xhtml",{"orderid":orderid}, "数据加载中", function(result){
				if(result.status){
					window.location.replace(__memberctx +"/mobileorderm.xhtml?tabindex="+tabindex);
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

//取消订单
function cancleorder(orderid){
	window.top.$.confirm({
		text: '你确定要取消吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/cancletheorder.xhtml",{"orderid":orderid}, "数据加载中", function(result){
				if(result.status){
					window.location.replace(__memberctx +"/mobileorderm.xhtml?tabindex="+tabindex);
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

//再次购买
function payagainorder(orderid){
	window.top.$.confirm({
		text: '你确定要再次购买吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/payagainorder.xhtml",{"orderid":orderid}, "数据加载中", function(result){
				if(result.status){
					window.location.replace(__memberctx +"/mobileshoppingcart.xhtml?backUrl=/mobileorderm.xhtml?tabindex="+tabindex);
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

//确定收货
function finishorder(orderid){
	window.top.$.confirm({
		text: '你确定要收货吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/finishtheorder.xhtml",{"orderid":orderid}, "数据加载中", function(result){
				if(result.status){
					window.location.replace(__memberctx +"/mobileorderm.xhtml?tabindex="+tabindex);
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

//支付
function gopay(orderid,paymodel,orderorigin){
	//线下支付
	if(orderorigin == 1){
		if(paymodel){
			window.location.replace(__ctx + "/nr/api/" + paymodel + "/pay.xhtml?order=" + orderid);
		}else{
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/updatetheorderpaytype.xhtml",{"orderid":orderid}, "", function(result){
				if(result.status){
					window.location.replace(__ctx + "/nr/api/" + result.result + "/pay.xhtml?order=" + orderid);
				}
			});
		}
	}else{
		window.location.replace(__ctx + "/nr/api/" + paymodel + "/pay.xhtml?order=" + orderid);
	}
}

//申请退款
function refundmoney(orderid){
	RefundWindow.popupRefundWindow(orderid);
}

//订单详情
function goinfo(orderid,orderstate){
	window.location.replace(__memberctx + "/mobileorderdetail.xhtml?orderid="+orderid+"&orderstate="+orderstate+"&tabindex="+tabindex);
}

//返回
function back(){
	window.location.replace(__memberctx + "/mobilepersonal.xhtml");
}

//退款框刷新
function refreshData(){
	window.location.replace(__memberctx +"/mobileorderm.xhtml?tabindex="+tabindex);
}
