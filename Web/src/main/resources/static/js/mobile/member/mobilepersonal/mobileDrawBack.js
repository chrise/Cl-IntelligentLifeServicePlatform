$(function(){
	var lbHeight = $(".weui-tab-main-order").height();
	var titleHeight = $(".weui-navbar").outerHeight();
	$(".weui-tab__bd").height(lbHeight - titleHeight);
})

/**
 * 实例化退款申请。
 */
var drawback = {
	orderid : null,
	searchIstate : "",
	loadDrawBack : function (keyword) { // 绑定退款申请列表数据。
		this.searchIstate = "refundapply";
		$.showLoading("数据加载中");
		$("#refundapply").load(__ctx + "/mobiledrawback/getRefundApplyByMemberId.xhtml",
			{keyword: keyword},function(response,status,xhr){$.hideLoading();}
		);
	},
	loadBeingProcessed : function (keyword) { // 绑定处理中数据。
		this.searchIstate = "beingprocessed";
		$.showLoading("数据加载中");
		$("#beingprocessed").load(__ctx + "/mobiledrawback/getRefundHandleByMemberId.xhtml",
			{keyword: keyword},function(response,status,xhr){$.hideLoading();}
		);
	},
	loadRefundRecord : function (keyword) { // 绑定退款记录数据。
		this.searchIstate = "refundrecord";
		$.showLoading("数据加载中");
		$("#refundrecord").load(__ctx + "/mobiledrawback/getRefundRecordByMemberId.xhtml",
			{keyword: keyword},function(response,status,xhr){$.hideLoading();}
		);
	},
	drawBackDetails : function (orderid) { // 退款处理中详情。
		window.location.replace(__memberctx + "/mobiledbd.xhtml?orderid=" + orderid + "&pagetype=beingprocessed");
	},
	drawBackRecordDetails : function (orderid) { // 退款记录详情。
		window.location.replace(__memberctx + "/mobiledbdr.xhtml?orderid=" + orderid + "&pagetype=refundrecord");
	},
	backMain : function () { // 返回个人主页。
		window.location.replace(__memberctx + "/mobilepersonal.xhtml");
	}
}

/**
 * 刷新数据。
 * @returns
 */
function refreshData () {
	drawback.loadDrawBack(); // 退款申请。
}

/**
 * 搜索。
 * @returns
 */
function onSearch () {
	var keyword = $("#searchcontent").val();
	if (drawback.searchIstate === "refundapply") drawback.loadDrawBack(keyword);
	else if (drawback.searchIstate === "beingprocessed") drawback.loadBeingProcessed(keyword);
	else if (drawback.searchIstate === "refundrecord") drawback.loadRefundRecord(keyword);
}

/**
 * 加载完页面执行。
 * @returns
 */
$(function () {
	if (pagetype === null || pagetype === "") {
		$("#rapply").addClass("weui-bar__item--on");
		$("#refundapply").addClass("weui-tab__bd-item--active");
		drawback.loadDrawBack(); // 退款申请。
	} else if (pagetype === "beingprocessed") {
		$("#rhand").addClass("weui-bar__item--on");
		$("#beingprocessed").addClass("weui-tab__bd-item--active");
		drawback.loadBeingProcessed(); // 处理中。
	} else if (pagetype === "refundrecord") {
		$("#rrecord").addClass("weui-bar__item--on");
		$("#refundrecord").addClass("weui-tab__bd-item--active");
		drawback.loadRefundRecord(); // 退款记录。
	}
})