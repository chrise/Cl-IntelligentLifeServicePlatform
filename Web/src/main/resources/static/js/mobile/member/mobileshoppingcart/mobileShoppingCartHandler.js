/*购物车*/
var MAX = 99, MIN = 1;

/**
 * 减少数量。
 * @param e
 * @returns
 */
$('.weui-count__decrease').click(function (e) {
	var $input = $(e.currentTarget).parent().find('.weui-count__number');
	var number = parseInt($input.val() || "0") - 1
	if (number < MIN) number = MIN;
	var $id = ($(e.currentTarget).parent().find('.weui-count__number').attr("id")).split("_")[1];
	var $shoppingcartid = $("#shoppingcartid_" + $id);
	if (number != $input.val()) {
		var $goodprice = $("#goodprice_" + $id).text();
		var $total = $("#total").text();
		var total = (parseFloat($total) - parseFloat($goodprice)).toFixed(2);
		shoppingcart.executeAmendGoodNumber($shoppingcartid.val(), number, total); // 根据购物车id修改数量。
	}
	$input.val(number);
});

/**
 * 增加数量。
 * @param e
 * @returns
 */
$('.weui-count__increase').click(function (e) {
	var $input = $(e.currentTarget).parent().find('.weui-count__number');
	var number = parseInt($input.val() || "0") + 1
	if (number > MAX) number = MAX;
	var $id = ($(e.currentTarget).parent().find('.weui-count__number').attr("id")).split("_")[1];
	var $goodstock = parseFloat($("#goodstock_" + $id).val());
	if ($goodstock < number) return;
	var $shoppingcartid = $("#shoppingcartid_" + $id);
	if (number != $input.val()) {
		var $goodprice = $("#goodprice_" + $id).text();
		var $total = $("#total").text();
		var total = (parseFloat($total) + parseFloat($goodprice)).toFixed(2);
		shoppingcart.executeAmendGoodNumber($shoppingcartid.val(), number, total); // 根据购物车id修改数量。
	}
	$input.val(number);
});

/**
 * 监听键盘开启事件。
 * @param event
 * @returns
 */
$(".weui-count__number").keyup(function(event){
	if ($(this).val() === null || $(this).val() === "" || $(this).val() === 0) {
		$(this).val(1);
		mobileGlobal.warn("商品数量至少大于0"); 
	}
});

/**
 * 删除。
 * @returns
 */
$('.delete-swipeout').click(function () {
	var $id = ($(this).attr("id")).split("_")[1];
	var $shoppingcartid = $("#shoppingcartid_" + $id).val();
	mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/executeDelShoppingCartGood.xhtml",  {shoppingcartid: $shoppingcartid}, "数据加载中", function(result){
		if(result.status){
			$(this).parents('.weui-cell').remove();
			window.location.reload();
		}
	});
	
})

$('.close-swipeout').click(function () {
	$(this).parents('.weui-cell').swipeout('close')
})

$('.weui-cell_swiped').swipeout('open');
$('.weui-cell_swiped').swipeout('close');


/**
 * 购物车实例化控制器。
 */
var shoppingcart = {
	total_id : $("#total"),
	select_all : $("#selete-all"),
	clickSingleSelect : function (index) { // 单选事件。
		var $total = parseFloat(this.total_id.text()); // 合计金额。
		var _price = $("#goodprice_" + index).text(); // 单价。
		var _number = $("#goodnumber_" + index).val(); // 数量。
		var $id = index.split("-"); // 获取商户索引标识。
		var $merchantName = $("#merchantname" + $id);
		if ($("#checkbox_" + index).prop("checked")) {
			$total += parseFloat(_price) * parseFloat(_number); // 点击选中。累加总金额。
			var flag = true;
			$("input[merchantname='merchantname" + $id[0] + "']").each(function(i, v){
				if (!$(this).prop("checked")) {
					flag = false;
					return false;
				}
		    });
			if (flag) $merchantName.removeClass("weui-icon-circle").addClass("weui-icon-success");
		} else {
			$total = $total - (parseFloat(_price) * parseFloat(_number)); // 取消选中。
			$merchantName.removeClass("weui-icon-success").addClass("weui-icon-circle");
		}
		this.total_id.text($total.toFixed(2));
		var selectstate = $("#checkbox_" + index).prop("checked") ? true : false;
		this.executeAmendSelectState($("#shoppingcartid_" + index).val(), selectstate);
	},
	clickAllSelect : function () { // 全选事件。
		var $selete_all = this.select_all.attr("class");
		var flag = $selete_all.indexOf("weui-icon-circle") != -1 ? true : false;
		var $total = parseFloat(this.total_id.text());
		var shoppingcartidArr = new Array;
		$("input[type='checkbox']").each(function(i, v){
			var $index = $(this).context.id.split("_")[1]; // 获取生成每条商品信息的索引值。
			var _price = $("#goodprice_" + $index).text();
			var _number = $("#goodnumber_" + $index).val();
			if (flag && !$(this).prop("checked")) { // 检查是否勾选, 无勾选打勾。
				$(this).prop("checked", "checked"); 
				$total += parseFloat(_price) * parseFloat(_number);
				shoppingcartidArr.push($("#shoppingcartid_" + $index).val());
			} else if (!flag && $(this).prop("checked")) { // 去掉全选, 清楚商品已打勾项。
				$(this).removeAttr("checked");
				shoppingcartidArr.push($("#shoppingcartid_" + $index).val());
			} else return true;
	    });
		
		$("i[checkbox='mni']").each(function(i, v){ // 检查商户是否勾选。
			var $flag = $(this).attr("class").indexOf("weui-icon-circle") != -1 ? true : false; //商户是否勾选。
			if (flag && $flag) $(this).removeClass("weui-icon-circle").addClass("weui-icon-success");
			else if (!flag && !$flag) $(this).removeClass("weui-icon-success").addClass("weui-icon-circle");
			else return true;
	    });
		
		if (flag) { // 触发全选。
			this.total_id.text($total.toFixed(2));
			this.select_all.removeClass("weui-icon-circle").addClass("weui-icon-success");
		} else {
			this.select_all.removeClass("weui-icon-success").addClass("weui-icon-circle");
			this.total_id.text("0.00");
		}
		
		this.executeAmendSelectState(shoppingcartidArr.join(","), flag); // 修改选中状态。
	},
	clickSettlementShoppingCart : function () { // 结算购物车。
		var $flag = 0; // 1、有无商品。2、勾选商品是否有库存不足、无货、停售。
		$("input[type='checkbox']").each(function(i, v){ // 遍历已勾选的商品。
			var $index = $(this).attr("id").split("_")[1]; // 获取生成每条商品信息的索引值。
			if ($(this).prop("checked")) { // 是否勾选。
				var $goodstock = parseFloat($("#goodstock_" + $index).val()); // 商品库存。
				var $salestate = $("#salestate_" + $index).val(); // 商品状态。 1、停售，4、在售。
				var $goodnumber = parseFloat($("#goodnumber_" + $index).val()); // 商品库存。
				if ($salestate === "1" || $goodstock === 0 || $goodstock < $goodnumber) {
					$flag = 2;
					return false;
				}
				$flag = 1; // 有勾选商品。
			}
	    });
		if ($flag === 2) {
			mobileGlobal.warn("勾选的商品里包含有无货、停售及库存不足的商品，请取消勾选！");
			return;
		}
		if ($flag === 0) {
			mobileGlobal.warn("至少勾选一件商品进行结算操作！");
			return;
		}
		var $url = __memberctx + "/mobilesubmito.xhtml";
		var $form = $("<form></form>").attr("action", $url).attr("method", "post");
		$form.append($("<input></input>").attr("type", "hidden").attr("name", "buynow").attr("value", false));
		$form.appendTo('body').submit().remove();
	},
	clickMerchantName : function ($index) { // 点击商户全选或者取消全选。
		var $merchantName = $("#merchantname" + $index);
		var $class = $("#merchantname" + $index).attr("class");
		var $total = parseFloat(this.total_id.text());
		var flag = $class.indexOf("weui-icon-circle") != -1 ? true : false; //  有无选中。
		var shoppingcartidArr = new Array;
		if (flag) { // 勾选。
			$("input[merchantname='merchantname" + $index + "']").each(function(i, v){
				if (!$(this).prop("checked")) { // 勾选无选中的。
					$(this).prop("checked", "checked"); 
					var $index = $(this).context.id.split("_")[1]; // 获取生成每条商品信息的索引值。
					var _price = $("#goodprice_" + $index).text();
					var _number = $("#goodnumber_" + $index).val();
					$total += parseFloat(_price) * parseFloat(_number);
					shoppingcartidArr.push($("#shoppingcartid_" + $index).val());
				}
		    });
			$merchantName.removeClass("weui-icon-circle").addClass("weui-icon-success");
		} else {
			$("input[merchantname='merchantname" + $index + "']").each(function(i, v){
				if ($(this).prop("checked")) {
					$(this).removeAttr("checked");
					var $index = $(this).context.id.split("_")[1]; // 获取生成每条商品信息的索引值。
					var _price = $("#goodprice_" + $index).text();
					var _number = $("#goodnumber_" + $index).val();
					$total = $total - (parseFloat(_price) * parseFloat(_number)); // 取消选中。
					shoppingcartidArr.push($("#shoppingcartid_" + $index).val());
				}
		    });
			$merchantName.removeClass("weui-icon-success").addClass("weui-icon-circle");
		}
		
		this.total_id.text($total.toFixed(2));
		this.executeAmendSelectState(shoppingcartidArr.join(","), flag);
	},
	executeAmendSelectState : function (shoppingcartid, selectstate) { // 修改商品的选中状态。
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/executeAmendSelectState.xhtml",  {shoppingcartid: shoppingcartid, selectstate: selectstate}, "数据加载中", function(result){
			if(result.status){
			}
		});
	},
	executeAmendGoodNumber : function (shoppingcartid, goodnumber, total) { // 修改商品数量。
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/executeAmendGoodNumber.xhtml",  {shoppingcartid: shoppingcartid, goodnumber: goodnumber}, "数据加载中", function(result){
			if(result.status){
				$("#total").text(total);
			}
		});
	},
	goBack : function () {
		var _url = _isbackUrl.replace(/@/g,'&');
		window.location.replace(__memberctx + _url);
	}
};

/**
 * 检查是否全选中。
 * @returns
 */
$(function () {
	var flag = true;
	$("i[checkbox='mni']").each(function(i, v){ // 检查商户是否勾选。
		if ($(this).attr("class").indexOf("weui-icon-circle")  != -1 ) {
			flag = false;
			return false;
		}
    });
	
	if (flag) {
		shoppingcart.select_all.removeClass("weui-icon-circle").addClass("weui-icon-success");
	}
	
	if (_isbackUrl != null && _isbackUrl != "") { // 修改样式。
		$(".weui-tab-home-shopping").css("bottom","50px");
		$(".weui-payment-bar").css("bottom","0px");
	}
});