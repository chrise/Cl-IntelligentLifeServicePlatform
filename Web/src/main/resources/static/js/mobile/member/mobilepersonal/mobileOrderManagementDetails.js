function back(){
	window.location.replace(__memberctx +"/mobileorderm.xhtml?tabindex="+tabindex);
}

//删除订单
function deleteorder(orderid){
	window.top.$.confirm({
		text: '你确定要删除吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/deletetheorder.xhtml",{"orderid":orderid}, "数据加载中", function(result){
				if(result.status){
					window.location.replace(__memberctx +"/mobileorderm.xhtml?tabindex="+tabindex);
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

//取消订单
function cancleorder(orderid){
	window.top.$.confirm({
		text: '你确定要取消吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/cancletheorder.xhtml",{"orderid":orderid}, "数据加载中", function(result){
				if(result.status){
					window.location.replace(__memberctx +"/mobileorderm.xhtml?tabindex="+tabindex);
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

//再次购买
function payagainorder(orderid,orderstate){
	window.top.$.confirm({
		text: '你确定要再次购买吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/payagainorder.xhtml",{"orderid":orderid}, "数据加载中", function(result){
				if(result.status){
					window.location.replace(__memberctx +"/mobileshoppingcart.xhtml?backUrl=/mobileorderdetail.xhtml?orderid="+orderid+"@orderstate="+orderstate+"@tabindex="+tabindex);
				}
			});
		},
		onCancel: function(){
		}
	});
}

//确定收货
function finishorder(orderid){
	window.top.$.confirm({
		text: '你确定要收货吗?',
		onOK: function () {
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/finishtheorder.xhtml",{"orderid":orderid}, "数据加载中", function(result){
				if(result.status){
					window.location.replace(__memberctx +"/mobileorderm.xhtml?tabindex="+tabindex);
				}
			});
		},
		onCancel: function(){
			
		}
	});
}

//支付
function gopay(orderid,paymodel,orderorigin){
	//线下支付
	if(orderorigin == 1){
		if(paymodel){
			window.location.replace(__ctx + "/nr/api/" + paymodel + "/pay.xhtml?order=" + orderid);
		}else{
			mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/updatetheorderpaytype.xhtml",{"orderid":orderid}, "", function(result){
				if(result.status){
					window.location.replace(__ctx + "/nr/api/" + result.result + "/pay.xhtml?order=" + orderid);
				}
			});
		}
	}else{
		window.location.replace(__ctx + "/nr/api/" + paymodel + "/pay.xhtml?order=" + orderid);
	}
}

//申请退款
function refundmoney(orderid){
	RefundWindow.popupRefundWindow(orderid);
}

//退款框刷新
function refreshData(){
}

//更多
$(function(){
	$(".prosonal-ordermd-more>ul>li").click(function(){
		$(this).children("ul").toggle(); 
	});
});