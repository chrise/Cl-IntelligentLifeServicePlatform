!function(window) {
    "use strict"; //ECMAscript 5 添加的（严格）运行模式提高编译器效率，增加运行速度

    var doc = window.document
      , ydui = {};

    $(window).on('load', function() {});

    var util = ydui.util = {

        parseOptions: function(string) {},

    };

    function storage(ls) {}

    if (typeof define === 'function') {
        define(ydui);
    } else {
        window.YDUI = ydui;
    }

    function ScrollTab(element, options) {
        this.$element = $(element);
        this.options = $.extend({}, ScrollTab.DEFAULTS, options || {});
        this.init();
    }

    ScrollTab.DEFAULTS = {
        navItem: '.scrolltab-itemize',
        content: '.scrolltab-content',
        contentItem: '.scrolltab-content-item',
        initIndex: 0
    };

    ScrollTab.prototype.init = function() {
        var _this = this
          , $element = _this.$element
          , options = _this.options;

        _this.$navItem = $element.find(options.navItem);
        _this.$content = $element.find(options.content);
        _this.$contentItem = $element.find(options.contentItem);

        _this.scrolling = false;
        _this.contentOffsetTop = _this.$content.offset().top;

        _this.bindEvent();

        _this.movePosition(_this.options.initIndex, false);
    }
    ;

    ScrollTab.prototype.bindEvent = function() {
        var _this = this;

        _this.$content.on('resize.ydui.scrolltab scroll.ydui.scrolltab', function() {
            _this.checkInView();
        });

        _this.$navItem.on('click.ydui.scrolltab', function() {
            _this.movePosition($(this).index(), true);
        });
    }
    ;

    ScrollTab.prototype.movePosition = function(index, animate) {
        var _this = this;

        if (_this.scrolling)
            return;
        _this.scrolling = true;

        _this.$navItem.removeClass('crt');
        _this.$navItem.eq(index).addClass('crt');

        var $item = _this.$contentItem.eq(index);
        if (!$item[0])
            return;

        var offset = $item.offset().top;

        var top = offset + _this.$content.scrollTop() - _this.contentOffsetTop + 1;

        _this.$content.stop().animate({
            scrollTop: top
        }, animate ? 200 : 0, function() {
            _this.scrolling = false;
        });
    }
    ;

    ScrollTab.prototype.checkInView = function() {
        var _this = this;

        if (_this.scrolling)
            return;

        if (_this.isScrollTop()) {
            _this.setClass(0);
            return;
        }

        if (_this.isScrollBottom()) {
            _this.setClass(_this.$navItem.length - 1);
            return;
        }

        _this.$contentItem.each(function() {
            var $this = $(this);

            if ($this.offset().top <= _this.contentOffsetTop) {
                _this.setClass($this.index());
            }
        });
    }
    ;

    function Plugin(option) {
        var args = Array.prototype.slice.call(arguments, 1);

        return this.each(function() {
            var target = this
              , $this = $(target)
              , scrollTab = $this.data('ydui.scrolltab');

            if (!scrollTab) {
                $this.data('ydui.scrolltab', (scrollTab = new ScrollTab(target,option)));
            }

            if (typeof option == 'string') {
                scrollTab[option] && scrollTab[option].apply(scrollTab, args);
            }
        });
    }

    $(window).on('load.ydui.scrolltab', function() {
        $('[data-ydui-scrolltab]').each(function() {
            var $this = $(this);
            $this.scrollTab(window.YDUI.util.parseOptions($this.data('ydui-scrolltab')));
        });
    });

    $.fn.scrollTab = Plugin;

}(window);

function changedgroups(entityid){
	$.showLoading("数据加载中");
	$(".scrolltab-content").load(
			__ctx + "/mobile/member/reloadmobileItem.xhtml",
			{"entityid":entityid},
			function(response,status,xhr){
				$.hideLoading();
			}
	);
}

function showGoodList(entityid){
	window.location.replace(__memberctx + "/mainlist.xhtml?limitMin=0&limitMax="+mobileGlobal.pageFreshParams.firstLimitMax+"&goodgroupid="+entityid);
}

//搜索商品
function search(e,dom){
	var _value = $("#searchcontent").val();
	if(_value){
		window.location.replace(__memberctx + "/mainlist.xhtml?limitMin=0&limitMax="+mobileGlobal.pageFreshParams.firstLimitMax+"&key="+_value);
	}else{
		mobileGlobal.info("请输入您喜欢的产品");
	}
	document.activeElement.blur();
}
