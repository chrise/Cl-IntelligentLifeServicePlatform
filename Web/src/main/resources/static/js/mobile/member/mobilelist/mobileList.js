//筛选搜索参数
var classSearch = [], classSearchText = [], merchatSearch = [], merchatSearchText = [], marketSearch = [], marketSearchText = [];

//筛选重置
function resetSearch(){
	$.each($("#shaixuan").find("a[class*='weui-curt']"),function(i,v){
		$(v).removeClass("weui-curt");
	});
	classSearch = [], classSearchText = [], merchatSearch = [], merchatSearchText = [], marketSearch = [], marketSearchText = [];
	$("#shaixuan").find("span[class='searchtext']").html("");
}

//筛选搜索
function shaixuanSearch(){
	$.showLoading("数据加载中");
	$("#goodslist").load(
		__ctx + "/mobile/member/refreshmobilemainlist.xhtml",
		{"goodgroupid": goodgroupid, "key": key,"limitMin": 0,"limitMax": mobileGlobal.pageFreshParams.firstLimitMax, "classSearch":classSearch.join(","), "merchatSearch":merchatSearch.join(","),"marketSearch":marketSearch.join(",")},
		function(response,status,xhr){
			if(response.replace(/\s/g, "") == ""){
				$(".weui-nomerchandise").show();
			}else{
				$(".weui-nomerchandise").hide();
			}
			$.hideLoading();
			//关闭弹框
			$.closePopup();
		}
	);
}

//筛选选择
function shaixuanClick(e,itype){
	$(e).toggleClass("weui-curt");
	var entityid = $(e).attr("data");
	var name = $(e).html();
	var $text = $(e).parent().find(".searchtext");
	var changeText;
	if($(e).is(".weui-curt")){
		changeText = shaixuanParamChange(true, itype, entityid, name);
	}else{
		changeText = shaixuanParamChange(false, itype, entityid, name);
	}
	$text.html(changeText);
}

//筛选参数变更
function shaixuanParamChange(isPush, itype, entityid, name){
	var changeText;
	if(isPush){
		if(itype == 1){
			classSearch.push(entityid);
			classSearchText.push(name);
			changeText = classSearchText.join(",");
		}else if(itype == 2){
			merchatSearch.push(entityid);
			merchatSearchText.push(name);
			changeText = merchatSearchText.join(",");
		}else if(itype == 3){
			marketSearch.push(entityid);
			marketSearchText.push(name);
			changeText = marketSearchText.join(",");
		}
	}else{
		if(itype == 1){
			classSearch.splice($.inArray(entityid,classSearch),1);
			classSearchText.splice($.inArray(name,classSearchText),1);
			changeText = classSearchText.join(",");
		}else if(itype == 2){
			merchatSearch.splice($.inArray(entityid,merchatSearch),1);
			merchatSearchText.splice($.inArray(name,merchatSearchText),1);
			changeText = merchatSearchText.join(",");
		}else if(itype == 3){
			marketSearch.splice($.inArray(entityid,marketSearch),1);
			marketSearchText.splice($.inArray(name,marketSearchText),1);
			changeText = marketSearchText.join(",");
		}
	}
	return changeText;
}

//查看详情
function lookinfo(goodentityid, merchantsettleid){
	showgoodinfo(goodentityid, merchantsettleid);
}

//返回商品分类页面
function toback(){
	window.location.replace(__memberctx + "/mobileItem.xhtml");
}

/*加载更多*/
var pageMin = parseInt(0);
var pageMax = parseInt(mobileGlobal.pageFreshParams.firstLimitMax);
var ispullloading = true;
var ishavedata = true;
$(".infinite").infinite().on("infinite", function() {
	if(!ispullloading || !ishavedata){
		return;
	}else{
		//重置显示
		$(".weui-loading").show();
		$(".weui-loadmore__tips").html("正在加载");
		
		pageMin = pageMax + 1;
		pageMax = pageMin + mobileGlobal.pageFreshParams.everyTimeFreshNum;
		$(".weui-loadmore").show();
		ispullloading = false;
		mobileGlobal.sendRequest(__ctx + "/mobile/mobilegoodgroups/getthesinglegoodsextension.xhtml",{"limitMin":pageMin, "limitMax":pageMax, "goodgroupid":goodgroupid, "key": key, "classSearch":classSearch.join(","), "merchatSearch":merchatSearch.join(","),"marketSearch":marketSearch.join(",")}, null, function(result){
			if(result.status){
				if(result.result.length > 0){
					$(".weui-loadmore").hide();
					$.each(result.result,function(i,v){
						var _salenumber = v.salenumber ? v.salenumber : 0;
						$("#goodslist").append("<div class='weui-cell' onclick=\"lookinfo('"+v.goodentityid+"','"+v.goodentityid+"')\">"+
								"<div class='weui-cell__hd'><img style=\"width: 80px;height: 65px;\" src='"+v.goodphoto+"'/></div>"+
								"<div class='weui-cell__bd'>"+
								"<p class='cell-title'>"+v.goodname+"</p>"+
								"<p class='cell-title-small'>月售"+_salenumber+"件</p>"+
								"<p class='cell-monly'>￥"+v.goodprice+"</p>"+
								"</div>"+
								"<div class='weui-cell__ft'><i class='fa fa-plus-circle'></i></div>"+
						"</div>");
					});
				}else{
					$(".weui-loading").hide();
					$(".weui-loadmore__tips").html("已经全部加载");
					ishavedata = false;
				}
			}
			ispullloading = true;
		});;
	}
});

//重置页面数据
function loadPageData(loadPageMin, loadPageMax){
	pageMin = parseInt(loadPageMin);
	pageMax = parseInt(loadPageMax);
	ishavedata = true;
	ispullloading = true;
}

//下拉刷新
var ispushloading = false;
$("#listwrap").pullToRefresh({
	onRefresh: function () { 
		/*当下拉刷新触发的时候执行的回调 */
		$("#goodslist").load(
			__ctx + "/mobile/member/refreshmobilemainlist.xhtml",
			{"goodgroupid": goodgroupid, "key": key,"limitMin": 0,"limitMax": mobileGlobal.pageFreshParams.firstLimitMax, "classSearch":classSearch.join(","), "merchatSearch":merchatSearch.join(","),"marketSearch":marketSearch.join(",")},
			function(response,status,xhr){
				if(response.replace(/\s/g, "") == ""){
					$(".weui-nomerchandise").show();
				}else{
					$(".weui-nomerchandise").hide();
				}
				
				//重置页面数据
				loadPageData(0, mobileGlobal.pageFreshParams.firstLimitMax);
				
				$("#top_refresh").hide();
				ispushloading = false;
				$("#listwrap").pullToRefreshDone(); // 重置下拉刷新
			}
		);
	},
	onPull: function (percent) {
		/*用户下拉过程中会触发，接收一个百分比表示用户下拉的比例 */ 
		if(!ispushloading){
			ispushloading = true;
			$("#top_refresh").show();
		}
	},
	/* 下拉刷新的触发距离， 注意，如果你重新定义了这个值，那么你需要重载一部分CSS才可以，请参考下面的自定义样式部分 */
	distance: 50 
});

//筛选分类弹框
function shaixuanPopClick(e,itype){
	//公共参数
	var $text = $(e).parent().find(".searchtext");
	var name;
	var data;
	var checkArray;
	var checkGroupName;
	if(itype == 1){
		name = "选择类别";
		data = filter.goodgroupList;
		checkArray = classSearch;
		checkGroupName = "lb";
	}else if(itype == 2){
		name = "选择商家";
		data = filter.merchantList;
		checkArray = merchatSearch;
		checkGroupName = "sj";
	}else if(itype == 3){
		name = "选择市场";
		data = filter.marketList;
		checkArray = marketSearch;
		checkGroupName = "sc";
	}
	var _title = $("<div class=\"weui-sc-title\"><h2>"+name+"</h2></div>");
	var _closeBtn = $("<span class=\"weui-sc-title-btn\">确认</span>");
	//绑定关闭点击事件
	_closeBtn.bind("click",function(){
		$(".weui-sc-content").empty(); //关闭时清空内容,否则页面锁死
		$('.weui-screening-categories').removeClass('weui-screening-categories-show');
	});
	_title.append(_closeBtn);
	$(".weui-sc-content").append(_title);
	//添加items
	var _content = $("<div class=\"weui-sc-contents\"></div>");
	var _check = $("<div class=\"weui-cells weui-cells-sc weui-cells_checkbox\"></div>");
	$.each(data,function(i,obj){
		var _item;
		var $id = obj.entityid+""; //转字符串  checkArray存的字符串  否则无法inArray
		var $btnItem = $(e).parent().parent().find(".btnitem[data='"+$id+"']"); //对应外层快速选择按钮
		if($.inArray($id,checkArray) >= 0){
			_item = $(
			"<label class=\"weui-cell weui-check__label\" for=\""+(checkGroupName+i)+"\">" +
				"<div class=\"weui-cell__bd weui-bdsc\"><p>"+obj.name+"</p></div>" +
				"<div class=\"weui-cell__hd\">" +
					"<input type=\"checkbox\" class=\"weui-check\" name=\"checkbox1\" id=\""+(checkGroupName+i)+"\" checked=\"checked\">" +
					"<i class=\"weui-icon-checked\"></i>" +
				"</div>" +
			"</label>");
		}else{
			_item = $(
			"<label class=\"weui-cell weui-check__label\" for=\""+(checkGroupName+i)+"\">" +
				"<div class=\"weui-cell__bd weui-bdsc\"><p>"+obj.name+"</p></div>" +
				"<div class=\"weui-cell__hd\">" +
				"<input type=\"checkbox\" class=\"weui-check\" name=\"checkbox1\" id=\""+(checkGroupName+i)+"\" >" +
				"<i class=\"weui-icon-checked\"></i>" +
				"</div>" +
			"</label>");
		}
		//绑定check变化事件
		_item.find("input[class='weui-check']").change(function(){
			var changeText;
			if($(this).is(':checked')){
				changeText = shaixuanParamChange(true, itype, $id, obj.name);
			}else{
				changeText = shaixuanParamChange(false, itype, $id, obj.name);
			}
			if($btnItem.length == 1){
				$btnItem.toggleClass("weui-curt");
			}
			$text.html(changeText);
		});
		_check.append(_item);
	});
	_content.append(_check);
	$(".weui-sc-content").append(_content);
	$('.weui-screening-categories').addClass('weui-screening-categories-show');
}




