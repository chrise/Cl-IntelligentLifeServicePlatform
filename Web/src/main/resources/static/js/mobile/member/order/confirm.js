//钻石点击事件
function diamondClick(o) {
	instance.switchSelection($(o), "diamond");
}

//确认支付点击事件
function confirmClick() {
	instance.confirmOrder();
}

//全局实例对象
var instance = {
	deduction : null,
	paymenttotalCtl : $("#paymenttotal"),
	switchSelection : function(obj, type) {
		var checked = obj.siblings().children("i.fa-check-circle-o");
		if (checked.length > 0) checked.removeClass("fa-check-circle-o").addClass("fa-circle-thin");
		
		var i = obj.children("i");
		if (i.hasClass("fa-check-circle-o")) {
			this.deduction = null;
			this.paymenttotalCtl.text(__ordertotal.toFixed(2));
			
			i.removeClass("fa-check-circle-o").addClass("fa-circle-thin");
		} else {
			var amount = 0;
			switch (type) {
				case "diamond":
					amount = __diamondpay;
					this.deduction = {type : type, amount : __diamondpay};
					break;
			}
			this.paymenttotalCtl.text(((__ordertotal * 100 - amount * 100) / 100).toFixed(2));
			
			i.removeClass("fa-circle-thin").addClass("fa-check-circle-o");
		}
	},
	confirmOrder : function() {
		if (!__needconfirm) {
			this.gotoPay();
			return;
		}
		
		var data = {order : __order, diamondpay : 0};
		if (this.deduction) {
			switch (this.deduction.type) {
				case "diamond":
					data.diamondpay = __diamondpay;
					break;
			}
		}
		
		mobileGlobal.sendRequest(__ctx + "/mobile/member/order/confirm.xhtml", data, "正在确认订单", function(data, status) {
			if (data.status) instance.gotoPay();
			else {
				if (data.result.code == sysGlobal.ResultCode.Info) {
					mobileGlobal.warn(data.result.message);
					setTimeout(function() { window.location.reload(); }, 3000);
					return;
				}
				mobileGlobal.warn("确认订单失败");
			}
		});
	},
	gotoPay : function() {
		window.location.replace(__ctx + "/nr/api/" + __api + "/pay.xhtml?order=" + __order);
	}
};
