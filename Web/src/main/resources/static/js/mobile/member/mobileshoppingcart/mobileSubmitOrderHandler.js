/*购物车*/
var MAX = 99, MIN = 1;

/**
 * 减少数量。
 * @param e
 * @returns
 */
$('.weui-count__decrease').click(function (e) {
	var $input = $(e.currentTarget).parent().find('.weui-count__number');
	var number = parseInt($input.val() || "0") - 1
	if (number < MIN) number = MIN;
	var $id = ($(e.currentTarget).parent().find('.weui-count__number').attr("id")).split("_")[1];
	var $shoppingcartid = $("#shoppingcartid_" + $id);
	if (number != $input.val()) {
		var $goodprice = $("#goodprice_" + $id).text();
		var $total = $("#total").text();
		var $goodNum = $("#goodNum").text();
		var total = (parseFloat($total) - parseFloat($goodprice)).toFixed(2);
		submitOrder.executeAmendGoodNumber($shoppingcartid.val(), number, total, parseFloat($goodNum) - 1); // 根据购物车id修改数量。
	}
	$input.val(number);
});

/**
 * 增加数量。
 * @param e
 * @returns
 */
$('.weui-count__increase').click(function (e) {
	var $input = $(e.currentTarget).parent().find('.weui-count__number');
	var number = parseInt($input.val() || "0") + 1
	if (number > MAX) number = MAX;
	var $id = ($(e.currentTarget).parent().find('.weui-count__number').attr("id")).split("_")[1];
	var $shoppingcartid = $("#shoppingcartid_" + $id);
	if (number != $input.val()) {
		var $goodprice = $("#goodprice_" + $id).text();
		var $total = $("#total").text();
		var $goodNum = $("#goodNum").text();
		var total = (parseFloat($total) + parseFloat($goodprice)).toFixed(2);
		submitOrder.executeAmendGoodNumber($shoppingcartid.val(), number, total, parseFloat($goodNum) + 1); // 根据购物车id修改数量。
	}
	$input.val(number);
});

/**
 * 监听键盘开启事件。
 * @param event
 * @returns
 */
$(".weui-count__number").keyup(function(event){
	if ($(this).val() === null || $(this).val() === "" || $(this).val() === 0) {
		$(this).val(1);
		mobileGlobal.warn("商品数量至少大于0"); 
	}
});

/**
 * 确认订单控制器。
 */
var submitOrder = {
	receiver_addresse : null,
	$popupState : 0, // 默认值为0;
	closeIFrame : function(){
        $("#addressMgr").removeAttr("src"); // 删掉属性，
        $("#addressMgr").hide(); // 关闭IFrame。
    },
	clickAddress : function () { // 跳转至地址管理页面。
		$("#addressMgr").show();
		$("#addressMgr").attr("src", __memberctx + "/mobileaddress.xhtml");
	},
	loadAddresses : function (addresseid) {
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/getReceiverAddressById.xhtml",  {entityid: addresseid}, "数据加载中", function(result){
			if(result.status){
				if (result.result == null) return;
				var $addresses = result.result;
				submitOrder.receiver_addresse = $addresses.entityid;
				$("#receiver").text($addresses.receiver);
				$("#mobilephone").text(submitOrder.replacePhone($addresses.mobilephone));
				$("#areas").text($addresses.provincename + " " + $addresses.cityname + " " + $addresses.countyname + " " + $addresses.streetname);
				$("#detailaddress").text($addresses.detailaddress);
				if ($addresses.deliverymode != null) { // 配送方式。
					$("#deliverymode_val").val("dm" + $addresses.deliverymode);
					$("#deliverymode").text(mobileGlobal.deliveryMode[$addresses.deliverymode]);
				};
//				if ($addresses.paymentmode != null) { // 付款方式。
//					$("#paymentmode_val").val("pm" + $addresses.paymentmode);
//					$("#paymentmode").text(mobileGlobal.paymentMode[$addresses.paymentmode]);
//				};
			}
		});
	},
	replacePhone : function (val) {
		if (Number(val) && String(val).length === 11) {
		    var mobile = String(val);
	        var reg = /^(\d{3})\d{4}(\d{4})$/;
	        return mobile.replace(reg, '$1****$2');
	    } else return val;
	},
	clickDiamond : function () { // 点击钻石事件。
		if ($("#diamond_val").text() === 0 || $("#diamond_val").text() == "0") {
			mobileGlobal.warn("可用钻石为0,不可优惠!"); 
			return;
		}
		var diamondClass =  $("#diamond").attr("class");
		var flag = diamondClass.indexOf("weui-icon-circle") != -1 ? true : false;
		if (flag) $("#diamond").removeClass("weui-icon-circle").addClass("weui-icon-success");
		else $("#diamond").removeClass("weui-icon-success").addClass("weui-icon-circle");
	},
	clickIntegral : function () { // 点击积分事件。
		var integralClass =  $("#integral").attr("class");
		var flag = integralClass.indexOf("weui-icon-circle") != -1 ? true : false;
		if (flag) $("#integral").removeClass("weui-icon-circle").addClass("weui-icon-success");
		else $("#integral").removeClass("weui-icon-success").addClass("weui-icon-circle");
	},
	saveOrder : function () { // 提交订单。
		if (this.receiver_addresse === null || this.receiver_addresse === "") {
			mobileGlobal.warn("请填写正确收货人信息!");
			return;
		}
		// 商户。
		var $mid = new Array();
		$("input[mid='mid']").each(function(i, v){ // 遍历留言。
			var mid = $(this).val();
			var $index = $(this).attr("id").split("_")[1]; // 获取生成每条商户信息的索引值。
			if ($("#leave_" + $index).val() != null && $("#leave_" + $index).val() != "") {
				mid += "_" + $("#leave_" + $index).val();
			} else mid += "_@";
			
			var $goodSale = new Array();
			// 获取商品销售id、数量、单价。
			$("input[mid='mid_" + $index + "']").each(function(i, v){
				var $saleidInputId = $(this).attr("id").split("_")[1];
				var saleid = $(this).val(); // 销售id。
				var goodprice = $("#goodprice_" + $saleidInputId).text(); // 销售id。
				var goodnumber = $("#goodnumber_" + $saleidInputId).val(); // 销售id。
				$goodSale.push(saleid + "#" + goodprice + "#" + goodnumber);
			});
			mid += "_" + $goodSale.join("&");
			$mid.push(mid); // 存入商品信息。
	    });
		
		// 钻石。
		var $diamond = $("#diamond").attr("class").indexOf("weui-icon-success") != -1 ? true : false;
		var diamond = 0;
		if ($diamond)  diamond = $("#diamond_val").text();
		// 积分。
		var integral = 0;
		// 配送方式。
		var $deliverymode = $("#deliverymode_val").val();
		var dm = $deliverymode.substr($deliverymode.length-1, 1);
		// 支付方式。
//		var $paymentmode = $("#paymentmode_val").val();
//		var pm = $paymentmode.substr($paymentmode.length-1, 1);
		var pm = 1;
		// 配送时间。
		var dt = $("#deliverytime_val").val();
		var params = {addressid: submitOrder.receiver_addresse, mid: $mid.join(","), dm: dm, pm: pm, dt: dt, buynow: $buynow, diamond: diamond, integral: integral};
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/executeSubmitOrder.xhtml",  params, "数据加载中", function(result){
			if (result.status) {
				window.location.replace(__ctx + "/nr/api/" + result.result.paymentkey + "/pay.xhtml?order=" + result.result.orderid);
			} else mobileGlobal.warn(result.result.message);
		});
	},
	executeAmendGoodNumber : function (shoppingcartid, goodnumber, total, goodNum) { // 修改商品数量。
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/executeAmendGoodNumber.xhtml",  {shoppingcartid: shoppingcartid, goodnumber: goodnumber, pagestate: true}, "数据加载中", function(result){
			if(result.status){
				$("#total").text(total);
				$("#goodTotalPrice").text(total);
				$("#goodNum").text(goodNum);
			}
		});
	}
};

/**
 * 弹出窗公共方法。
 */
var commonClick = {
	_flag : null,
	clickCloseHalfPopup : function (id) { // 关闭半层弹出窗(支付方式、配送方式)。
		$(id).removeClass('weui-screening-categories-shows');
	},
	popupDeliveryMode : function () { // 弹出配送方式。
		$('#popup-deliverymode').addClass('weui-screening-categories-shows');
		$("#" + $("#deliverymode_val").val()).prop("checked", "checked");
	}
	,
	popupDeliveryTime : function () { // 弹出配送时间方式。
		$('#popup-deliverytime').addClass('weui-screening-categories-shows');
		$("#" + $("#deliverytime_id").val()).prop("checked", "checked");
	},
	popupPaymentMode : function () { // 弹出支付方式
		$('#popup-paymentmode').addClass('weui-screening-categories-shows');
		$("#" + $("#paymentmode_val").val()).prop("checked", "checked");
	},
	popupInvoiceMode : function () { // 弹出开票方式。
		$('#popup-invoicemode').addClass('weui-screening-categories-shows');
	},
	clickDeliverymode : function () { // 点击配送方式完成事件。
		var $deliverymode;
		var $flag = false;
		$("input[dm='dm']").each(function(i, v){ // 获取配送方式下拉选址。
			if ($(this).prop("checked")) {
				$deliverymode = $(this).attr("id");
				$flag = true;
				return false;
			}
	    });
		if ($flag) {
			$("#deliverymode").text(mobileGlobal.deliveryMode[$deliverymode.substr($deliverymode.length-1, 1)]);
			$("#deliverymode_val").val($deliverymode);
		}
		$('#popup-deliverymode').removeClass('weui-screening-categories-shows');
	},
	clickDeliveryTime : function () { // 点击配送时间完成事件。
		var $deliverytime;
		var $flag = false;
		$("input[dt='dt']").each(function(i, v){ // 获取配送方式下拉选址。
			if ($(this).prop("checked")) {
				$deliverytime = $(this).attr("id");
				$flag = true;
				return false;
			}
	    });
		if ($flag) {
			$("#deliverytime").text($("#pid_" + $deliverytime.split("_")[0]).text() + ": " + $("#p-" + $deliverytime).text());
			$("#deliverytime_val").val($deliverytime.split("_")[0] + "_" + $("#p-" + $deliverytime).text());
			$("#deliverytime_id").val($deliverytime);
		}
		$('#popup-deliverytime').removeClass('weui-screening-categories-shows');
	},
	clickPaymentmode : function () { // 点击支付方式完成事件。
		var $paymentmode;
		var $flag = false;
		$("input[pm='pm']").each(function(i, v){ // 获取配送方式下拉选址。
			if ($(this).prop("checked")) {
				$paymentmode = $(this).attr("id");
				$flag = true;
				return false;
			}
	    });
		if ($flag) {
			$("#paymentmode").text(mobileGlobal.paymentMode[$paymentmode.substr($paymentmode.length-1, 1)]);
			$("#paymentmode_val").val($paymentmode);
		}
		$('#popup-paymentmode').removeClass('weui-screening-categories-shows');
	},
//	clickInvoicemode : function () { // 点击开票方式。
//		var invoices;
//		$("input[invoices='invoices']").each(function(i, v){ 
//			if ($(this).prop("checked")) {
//				if ($(this).context.id === "x16") {
//					invoices = 1;
//					return false;
//				} else if ($(this).context.id === "x17") {
//					invoices = 2;
//					return false;
//				}
//			}
//	    });
//		$("#invoices").text(mobileGlobal.invoicesMode[invoices]);
//		$("#invoices_val").val(invoices);
//		$('#popup-invoicemode').removeClass('weui-screening-categories-shows');
//	},
	goBackShoppingCart : function () { // 返回购物车页面。
		if (_isbackUrl != null && _isbackUrl != "") { // 修改样式。
			var _url = _isbackUrl.replace(/@/g,'&');
			window.location.replace(__memberctx + _url);
			return;
		}
		window.location.replace(__memberctx + "/mobileshoppingcart.xhtml");
	}
}

/**
 * 加载完执行。
 * @returns
 */
$(function () {
	submitOrder.loadAddresses(); // 绑定收货地址。
});