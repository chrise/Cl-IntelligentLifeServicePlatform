//加载商品规格数据
var loadobj = {"isfristload": true, "goodid": null, "merchantsettleid": null}; //加载标识
var loadgoodspacedata = []; //加载规格数据
var checkspaceobject = {}; //选择规格对象
function loadGoodSpaces(goodid,merchantsettleid,itype){
	if(loadobj.isfristload || loadobj.goodid != goodid || loadobj.merchantsettleid != merchantsettleid){
		loadobj.goodid = goodid;
		loadobj.merchantsettleid = merchantsettleid;
		loadobj.isfristload = false;
		mobileGlobal.sendRequest(__ctx + "/mobile/mobilegoodgroups/getheallgoodspecs.xhtml",{"goodid":goodid,"merchantsettleid":merchantsettleid}, "数据加载中", function(result){
			if(result.status){
				if(result.result){
					loadgoodspacedata = result.result;
					checkspaceobject = {"goodprice": result.result[0].goodprice, "goodnumber": result.result[0].goodnumber, "index": 0};
					createGoodSpacesHtml(loadgoodspacedata, itype);
				}
			}
		});
	}else{
		createGoodSpacesHtml(loadgoodspacedata, itype);
	}
}

//生成商品规格html
function createGoodSpacesHtml(data,itype){
	if(!data){
		return;
	}
	var _cart = $(".weui-add-cart");
	//创建商品信息行
	var _title = $("" +
			"<div class=\"weui-add-cart-title\">" +
			"<i class=\"fa fa-close\"></i>" +
			"<div class=\"weui-cell\">" +
			"<div class=\"weui-cell__hd\"><img style=\"width:90px;height:90px;\" src=\""+data[0].goodphoto+"\"/></div>" +
			"<div class=\"weui-cell__bd\" style=\"padding-top:20px;\">" +
			"<p class=\"cart-monly\">￥"+checkspaceobject.goodprice+"</p>" +
			"<p class=\"cart-carts\">商品编号：<span>"+checkspaceobject.goodnumber+"</span></p>" +
			"</div>" +
			"</div>" +
	"</div>");
	_title.find("i").bind("click",function(){
		_cart.empty(); //关闭时清空内容,否则页面锁死
		$('.weui-add-cart-carts').removeClass('weui-add-cart-carts-show');
	});
	
	//创建规格选择行
	var _content = $("<div class=\"weui-add-cart-content\"></div>");
	var _spaceHead = $("" +
		"<div class=\"weui-cell-tk-carts\">" +
			"<div class=\"weui-cell-carts-title\"><h3>规格</h3></div>" +
		"</div>");
	$.each(data,function(i,obj){
		var _spaces = $("<a data=\""+obj.goodsaleid+"\">"+obj.specdefine+"</a>");
		if(i == checkspaceobject.index){
			_spaces.addClass('weui-curt');
		}
		//判断是否有库存
		if(obj.goodstock && obj.goodstock > 0){
			_spaces.bind("click",function(){
				//未被点击
				if(!$(this).is(".weui-curt")){
					//修改checkspaceobject
					checkspaceobject = {"goodprice": obj.goodprice, "goodnumber": obj.goodnumber, "index": i};
					//取消其他点击样式
					_spaceHead.find("a[class='weui-curt']").removeClass('weui-curt');
					//添加点击样式
					$(this).addClass('weui-curt');
					//修改金额 和 编号
					_title.find("p[class='cart-monly']").html("￥"+obj.goodprice);
					_title.find("p[class='cart-carts']").find("span").html(obj.goodnumber);
					//修改父级页面 金额、库存
					$(".swiper-title").find("p[class='cell-title-small']").find("span").html(obj.goodstock);
					$(".swiper-title").find("p[class='cell-monly']").html("￥"+obj.goodprice);
					//修改购买数量
					$("#buynum").val(1);
				}
			});
		}else{
			_spaces.css("color","#CCC");
		}
		_spaceHead.append(_spaces);
	});
	_content.append(_spaceHead);
	
	//创建数量选择行
	var _num = $("" +
	"<div class=\"weui-cell\">" +
		"<div class=\"weui-cell__bd\">" +
			"<p class=\"weui-cart-bd\">数量</p>" +
		"</div>" +
		"<div class=\"weui-cell__ft\">" +
			"<div class=\"weui-count\">" +
				"<a class=\"weui-count__btn weui-count__decrease\"></a>" +
				"<input id=\"buynum\" class=\"weui-count__number\" type=\"number\" value=\"1\"/>" +
				"<a class=\"weui-count__btn weui-count__increase\"></a>" +
			"</div>" +
		"</div>" +
	"</div>");
	_num.find(".weui-count__decrease").bind("click",function(){
		var _input = _num.find(".weui-count__number");
		var number = parseInt(_input.val() || "0") - 1;
		if(number < 1){
			number = 1;
		}
		_input.val(number);
	});
	_num.find(".weui-count__increase").bind("click",function(){
		var _input = _num.find(".weui-count__number");
		var number = parseInt(_input.val() || "0") + 1;
		if(number > 99){
			number = 99;
		}
		_input.val(number);
	});
	_content.append(_num);
	
	//确定取消按钮事件绑定
	var _footBtn = $("" +
		"<div class=\"weui-add-cart-btn\">" +
			"<a class=\"weui-cart-btn-close\">取消</a>" +
			"<a class=\"weui-cart-btn-ok\">确认</a>" +
		"</div>");
	_footBtn.find("a[class='weui-cart-btn-close']").bind("click",function(){
		_cart.empty(); //关闭时清空内容,否则页面锁死
		$('.weui-add-cart-carts').removeClass('weui-add-cart-carts-show');
	});
	_footBtn.find("a[class='weui-cart-btn-ok']").bind("click",function(){
		//获取 商品销售id
		var goodsaleid = _spaceHead.find("a[class='weui-curt']").attr("data");
		//获取 购买数量
		var num = parseFloat(_num.find(".weui-count__number").val());
		//直接购买或者添加至购物车
		payOrAddShoppingCar(goodsaleid,num,itype);
		_cart.empty(); //关闭时清空内容,否则页面锁死
		$('.weui-add-cart-carts').removeClass('weui-add-cart-carts-show');
	});
	
	//添加所有生成的行信息
	_cart.append(_title);
	_cart.append(_content);
	_cart.append(_footBtn);
	
	//弹框弹出
	$('.weui-add-cart-carts').addClass('weui-add-cart-carts-show');
}

//直接购买或者添加至购物车
function payOrAddShoppingCar(goodsaleid,num,itype){
	if(itype == 1){ //加入购物车
		mobileGlobal.sendRequest(__ctx + "/mobile/mobilegoodgroups/updategoodintotheshoppingcartsbackisaddnum.xhtml",{"goodsalesid":goodsaleid, "num": num}, "数据加载中", function(result){
			if(result.status){
				mobileGlobal.info("成功加入购物车");
				if(result.result){
					$.each($(".carnum"), function(i,v){
						$(v).html(parseInt($(v).html()) + 1);
					});
				}
			}
		});
	}else if(itype == 2){ //立即购买
		mobileGlobal.sendRequest(__ctx + "/mobile/mobilegoodgroups/updatepaygoods.xhtml",{"goodsalesid":goodsaleid, "num": num}, "数据加载中", function(result){
			if(result.status){
				if(key){
					window.location.replace(__memberctx + "/mobilesubmito.xhtml?backUrl=/mainlist.xhtml?limitMin=0@limitMax="+mobileGlobal.pageFreshParams.firstLimitMax+"@key="+key);
				}else{
					window.location.replace(__memberctx + "/mobilesubmito.xhtml?backUrl=/mainlist.xhtml?goodgroupid="+goodgroupid+"@limitMin=0@limitMax="+mobileGlobal.pageFreshParams.firstLimitMax);
				}
			}
		});
	}
}

//加载商品详情内容
function showgoodinfo(goodentityid, merchantsettleid){
	$.showLoading("数据加载中");
	$("#gooddetail").load(
		__ctx + "/mobile/mobilegoodgroups/refreshmobilegooddetailinfo.xhtml",
		{"goodentityid":goodentityid, "merchantsettleid":merchantsettleid},
		function(response,status,xhr){
			$.hideLoading();
			$("#gooddetail_pop").popup();
			
			//初始化详情图片滚动
			$(".swiper-container").swiper({
		        loop: true,
		        autoplay: 3000
		    });
		}
	);
}

//前往购物车页面
function gocar(){
	if(key){
		window.location.replace(__memberctx + "/mobileshoppingcart.xhtml?backUrl=/mainlist.xhtml?limitMin=0@limitMax="+mobileGlobal.pageFreshParams.firstLimitMax+"@key="+key);
	}else{
		window.location.replace(__memberctx + "/mobileshoppingcart.xhtml?backUrl=/mainlist.xhtml?goodgroupid="+goodgroupid+"@limitMin=0@limitMax="+mobileGlobal.pageFreshParams.firstLimitMax);
	}
}

//详情弹窗关闭
function detailclose(){
	$.closePopup();
}

