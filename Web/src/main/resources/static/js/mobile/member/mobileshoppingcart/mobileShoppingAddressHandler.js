/**
 * 实例化收货地址列表控制器。
 */
var addresses = {
	$popupState : 0,
	entityid : null,
	add : function () { // 新增地址。
		$.showLoading("数据加载中");
		this.$popupState = 1;
		$('#region').citySelect("emptyCache");
		$('#popup-add-address').addClass('weui-screening-categories-show');
		$.hideLoading();
	},
	edit : function (id) { // 编辑地址。
		this.$popupState = 1;
		this.entityid = id;
		$.showLoading("数据加载中");
		$('#popup-add-address').addClass('weui-screening-categories-show');
		addressAdd.load(id);
		$.hideLoading();
	},
	del : function (id) { // 删除地址。
		if (!_isHome) return;
		$.confirm({
			text: '你确定删除该地址吗?',
			onOK: function () {
				mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/deleteAddressesObjByEntityId.xhtml",  {entityid: id}, "数据加载中", function(result){
					if (result.status) {
						$.showLoading("数据加载中");
						$("#load_addresslist").load(
							__memberctx + "/executeLoadAddress.xhtml",
							null,function(response,status,xhr){$.hideLoading();}
						);
					} else top.mobileGlobal.error("网络异常!");
				});
			},
			onCancel: function(){
				
			}
		});
	},
	backSubmieOrder : function (id) {
		if (_isHome) { // 返回个人中心。
			if (typeof id !== 'undefined' && id != null && id != "") return;
			window.location.replace(__memberctx + "/mobilepersonal.xhtml");
			return;
		}
		if (this.$popupState != 0) return;
		if (typeof id !== 'undefined' && id != null && id != "") window.parent.submitOrder.loadAddresses(id); // 绑定收货地址。
		window.parent.submitOrder.closeIFrame(); // 返回上个页面。
	}
}

/**
 * 新增地址控制器。
 */
var addressAdd = {
	username : $("#username"),
	userphone : $("#userphone"),
	region : $("#region"),
	detailed : $("#detailed"),
	checkbox : $("#checkboxClick"),
	deliverymode : $("#dm_val"),
//	paymentmode : $("#pm_val"),
	emptyAddress : function() { //清空表单数据。
		this.username.val("");
		this.userphone.val("");
		this.detailed.val("");
		var defaultAddress = $("#checkboxClick").prop("checked") ? true : false; 
		if (defaultAddress) $("#checkboxClick").removeAttr("checked");
		$("#addressid").val("");
		$('#region').val("");
	},
	load : function (id) { // 绑定数据。
		if (id === null || id === "") return;
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/getReceiverAddressById.xhtml",  {entityid: id}, "数据加载中", function(result){
			if(result.status){
				addressAdd.username.val(result.result.receiver);
				addressAdd.userphone.val(result.result.mobilephone);
				addressAdd.detailed.val(result.result.detailaddress);
				var $target = $('#region'); // 绑定地址选择器的值。
				$target.val(result.result.provincename + " " + result.result.cityname + " "+ result.result.countyname + " "+ result.result.streetname);
				var $_id = result.result.province + "-" + result.result.city + "-"+ result.result.county + "-"+ result.result.street;
			    $target.citySelect({
			    	country: result.result.provincename,
			        provance: result.result.cityname,
			        city: result.result.countyname,
			        area: result.result.streetname,
			        id: $_id
			    });
			    $("#addressid").val($_id);
				// 配送方式。
			    if (result.result.deliverymode != null) { // 配送方式。
					$("#dm_val").val("dm" + result.result.deliverymode);
					$("#dm").text(mobileGlobal.deliveryMode[result.result.deliverymode]);
				};
//				if (result.result.paymentmode != null) { // 付款方式。
//					$("#pm_val").val("pm" + result.result.paymentmode);
//					$("#pm").text(mobileGlobal.paymentMode[result.result.paymentmode]);
//				};
				// 默认地址。
				if (result.result.defaultaddress) addressAdd.checkbox.prop("checked", "checked"); 
				$("#number").text(200 - result.result.detailaddress.length) ;
			}
		});
	},
//	clickInvoices : function () { // 发票方式。
//		var invoices, invoices_val;
//		$("input[invoices='invoices']").each(function(i, v){ // 获取配送方式下拉选址。
//			if ($(this).prop("checked")) {
//				if ($(this).context.id === "x16") { // 自提方式。
//					invoices = "普通";
//					invoices_val = 1;
//					return false;
//				} else if ($(this).context.id === "x17") {// 市场配送。
//					invoices = "增值";
//					invoices_val = 2;
//					return false;
//				}
//			}
//	    });
//		$("#invoices").text(invoices);
//		$("#invoices_val").val(invoices_val);
//	},
	save : function () {
		var defaultAddress = this.checkbox.prop("checked") ? true : false; // 是否设置为默认地址。
		if (this.username.val() === "") {
			mobileGlobal.warn("请填写收货人名称！");
			return;
		}
		if (!/^1[34578]\d{9}$/.test(this.userphone.val())) {
			mobileGlobal.warn("无效手机号码!");
			return;
		}
		if (this.region.val() === "") {
			mobileGlobal.warn("请选择收货城市!");
			return;
		}
		if (this.detailed.val() === "") {
			mobileGlobal.warn("请填写详细地址!");
			return;
		}
		var addressIdArr = $('#addressid').val().split("-");
		var $deliverymode = $("#dm_val").val();
		var dm = $deliverymode.substr($deliverymode.length-1, 1);
//		var $paymentmode = $("#pm_val").val();
//		var pm = $paymentmode.substr($paymentmode.length-1, 1);
		var pm = 1;
		var param = {entityid: addresses.entityid, receiver: this.username.val(), mobilephone: this.userphone.val(), defaultaddress: defaultAddress, province: addressIdArr[0], city: addressIdArr[1],
				county: addressIdArr[2], street: addressIdArr[3], detailaddress: this.detailed.val(), deliverymode: dm, paymentmode: pm, createtime: null, entitystate: 1};
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/insertOrUpdateAddressesObj.xhtml",  {data: JSON.stringify(param)}, "数据加载中", function(result){
			if(result.status){
				addresses.entityid = null;
				addressAdd.emptyAddress();
				if (_isHome) { // 返回个人中心。
					$("#popup-add-address").removeClass('weui-screening-categories-show');
					$.showLoading("数据加载中");
					$("#load_addresslist").load(
							__memberctx + "/executeLoadAddress.xhtml",
							null,
							function(response,status,xhr){
								$.hideLoading();
							}
					);
					return;
				}
				if (addresses.$popupState === 2) { // 返回订单页面并默认加载收货人详情。
					addresses.$popupState = 0;
					addresses.backSubmieOrder(result.result.entityid);
				} else if (addresses.$popupState === 1){ // 返回地址列表页面。
					addresses.$popupState = 0;
					$("#popup-add-address").removeClass('weui-screening-categories-show');
					$.showLoading("数据加载中");
					$("#load_addresslist").load(
							__memberctx + "/executeLoadAddress.xhtml",
							null,
							function(response,status,xhr){
								$.hideLoading();
							}
					);
				}
			}
		});
	}
};

/**
 * 设置默认值
 */
!function () {
    var $target = $('#region');
//	$target.val('重庆市 重庆市 江北区 华新街');
//
//    $target.citySelect({
//    	country: '重庆市',
//        provance: '重庆市',
//        city: '江北区',
//        area: '华新街',
//        id: '5353873674985472-5358137277276160-24264402455633920-24264501306990592'
//    });
//	
//    $("#addressid").val("5353873674985472-5358137277276160-24264402455633920-24264501306990592");
    
    $target.on('click', function (event) {
        event.stopPropagation();
        $target.citySelect('open');
    });

    $target.on('done.ydui.cityselect', function (ret) {
        $(this).val(ret.country + ' ' + ret.provance + ' ' + ret.city + ' ' + ret.area);
    });
}();

/**
 * 弹出窗公共方法。
 */
var commonClick = {
	clickCloseHalfPopup : function (id) { // 关闭半层弹出窗(支付方式、配送方式)。
		$(id).removeClass('weui-screening-categories-shows');
	},
	clickCloseFullPopup : function (id) { // 关闭全层弹出窗(新增页面、地址列表页面)。
		window.top.$.confirm({
			text: '你确定离开此页面吗?',
			onOK: function () {
				addresses.entityid = null;
				addresses.$popupState = 0;
				addressAdd.emptyAddress(); // 清空表单数据。
				$(id).removeClass('weui-screening-categories-show');
			},
			onCancel: function(){
				
			}
		});
	},
	popupDeliveryMode : function (flag) { // 弹出配送方式。
		$('#popup-deliverymode').addClass('weui-screening-categories-shows');
		$("#" + $("#dm_val").val()).prop("checked", "checked");
	},
	popupPaymentMode : function (flag) { // 弹出支付方式
		$('#popup-paymentmode').addClass('weui-screening-categories-shows');
		$("#" + $("#pm_val").val()).prop("checked", "checked");
	},
	popupInvoiceMode : function (flag) { // 弹出开票方式。
		$('#popup-invoicemode').addClass('weui-screening-categories-shows');
		$("#" + $("#invoicesmode_val").val()).prop("checked", "checked");
	},
	clickDeliverymode : function () { // 点击配送方式完成事件。
		var $deliverymode;
		var $flag = false;
		$("input[dm='dm']").each(function(i, v){ // 获取配送方式下拉选址。
			if ($(this).prop("checked")) {
				$deliverymode = $(this).attr("id");
				$flag = true;
				return false;
			}
	    });
		if ($flag) {
			$("#dm").text(mobileGlobal.deliveryMode[$deliverymode.substr($deliverymode.length-1, 1)]);
			$("#dm_val").val($deliverymode);
		}
		$('#popup-deliverymode').removeClass('weui-screening-categories-shows');
	},
	clickPaymentmode : function () { // 点击支付方式完成事件。
		var $paymentmode;
		var $flag = false;
		$("input[pm='pm']").each(function(i, v){ // 获取配送方式下拉选址。
			if ($(this).prop("checked")) {
				$paymentmode = $(this).attr("id");
				$flag = true;
				return false;
			}
	    });
		if ($flag) {
			$("#pm").text(mobileGlobal.paymentMode[$paymentmode.substr($paymentmode.length-1, 1)]);
			$("#pm_val").val($paymentmode);
		}
		$('#popup-paymentmode').removeClass('weui-screening-categories-shows');
	},
	clickInvoicemode : function () { // 点击开票方式。
		var invoices;
		$("input[invoices='invoices']").each(function(i, v){ 
			if ($(this).prop("checked")) {
				if ($(this).context.id === "x16") {
					invoices = 1;
					return false;
				} else if ($(this).context.id === "x17") {
					invoices = 2;
					return false;
				}
			}
	    });
		$("#invoices").text(mobileGlobal.invoicesMode[invoices]);
		$("#invoices_val").val(invoices);
		$('#popup-invoicemode').removeClass('weui-screening-categories-shows');
	},
	goBackShoppingCart : function () { // 返回购物车页面。
		window.location.replace(__memberctx + "/mobileshoppingcart.xhtml");
	}
}

$(function () {
	loadAddress();
});

function loadAddress () {
	if (_isHome) { // 个人中心
		$.showLoading("数据加载中");
		$("#load_addresslist").load(
				__memberctx + "/executeLoadAddress.xhtml",
				null,
				function(response,status,xhr){
					$.hideLoading();
				}
		);
	} else { // 购物车。
		if (window.parent.submitOrder.receiver_addresse != null && window.parent.submitOrder.receiver_addresse != "") {
			$.showLoading("数据加载中");
			$("#load_addresslist").load(
					__memberctx + "/executeLoadAddress.xhtml",
					null,
					function(response,status,xhr){
						$.hideLoading();
					}
			);
		} else {
			addresses.$popupState = 2;
			$('#popup-add-address').addClass('weui-screening-categories-show');
		}
	}
}

///**
// * 勾选默认地址事件。
// * @returns
// */
//$("#checkboxClick").click(function () {
//	if ($(this).prop("checked")) {
//		mobileGlobal.warn("是否确认为默认收货地址！");
//	}
//});

/**
 * 详细地址输入字数限制。
 * @returns
 */
$(".weui-textarea").keyup(function(){
    if ($(".weui-textarea").val().length > 200) {
        $(".weui-textarea").val($(".weui-textarea").val().substring(0, 200));
    }
    $("#number").text(200 - $(".weui-textarea").val().length) ;
 });
