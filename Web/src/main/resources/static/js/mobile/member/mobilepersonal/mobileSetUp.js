$(function(){
	$("#name").on("click",function(){
		$(".weui-masking").css("display","block");
	});
	
	$(".fa-close").on("click",function(){
		$(".weui-masking").css("display","none");
	});
	
	$(".weui-last-child").on("click",function(){
		$(".weui-masking").css("display","none");
	});
});

//修改昵称
function changenick(){
	var nick = $("#nickname").val();
	mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/updatethemembernickname.xhtml",{"nickname":nick}, "数据加载中", function(result){
		if(result.status){
			mobileGlobal.info(result.result.msg);
			window.location.reload();
		}
	});
}

//解除绑定
function removebind(e){
	var entityid = $(e).attr("data");
	if(entityid){
		$.confirm({
			text: '你确定要取消绑定吗?',
			onOK: function () {
				mobileGlobal.sendRequest(__ctx + "/mobile/mobilemanager/updatethemembertoremovebing.xhtml",{"entityid":entityid}, "数据加载中", function(result){
					if(result.status){
						if(result.result.status){
							mobileGlobal.info(result.result.msg);
							window.location.reload();
						}
					}
				});
			},
			onCancel: function(){
				
			}
		});
	}
}

//返回
function back(){
	window.location.replace(__memberctx + "/mobilepersonal.xhtml");
}
