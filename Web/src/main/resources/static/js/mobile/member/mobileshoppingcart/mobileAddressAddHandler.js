/**
 * 新增地址控制器。
 */
var addressAdd = {
	username : $("#username"),
	userphone : $("#userphone"),
	region : $("#region"),
	detailed : $("#detailed"),
	checkbox : $("#checkboxClick"),
	deliverymode : $("#deliverymode_val"),
	paymentmode : $("#paymentmode_val"),
	load : function () { // 绑定数据。
		if ($entityid === null || $entityid === "") {
			$("#deliverymode1").prop("checked", "checked"); 
			$("#paymentmode1").prop("checked", "checked");  
			return;
		}
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/getReceiverAddressById.xhtml",  {entityid: $entityid}, "数据加载中", function(result){
			if(result.status){
				addressAdd.username.val(result.result.receiver);
				addressAdd.userphone.val(result.result.mobilephone);
				addressAdd.detailed.val(result.result.detailaddress);
				// 配送方式。
				$("#deliverymode").text(mobileGlobal.deliveryMode[result.result.deliverymode]);
				$("#deliverymode_val").val(result.result.deliverymode);
				$("#deliverymode" + result.result.deliverymode).prop("checked", "checked"); 
				// 支付方式。
				$("#paymentmode").text(mobileGlobal.paymentMode[result.result.paymentmode]);
				$("#paymentmode_val").val(result.result.paymentmode);
				$("#paymentmode" + result.result.paymentmode).prop("checked", "checked"); 
				// 默认地址。
				if (result.result.defaultaddress) addressAdd.checkbox.prop("checked", "checked"); 
			}
		});
	},
	clickDeliverymode : function () { // 点击配送方式完成事件。
		var deliverymode;
		$("input[deliverymode='deliverymode']").each(function(i, v){ // 获取配送方式下拉选址。
			if ($(this).prop("checked")) {
				if ($(this).context.id === "deliverymode1") { // 自提方式。
					deliverymode = 1;
					return false;
				} else if ($(this).context.id === "deliverymode2") {// 市场配送。
					deliverymode = 2;
					return false;
				}
			}
	    });
		$("#deliverymode").text(mobileGlobal.deliveryMode[deliverymode]);
		$("#deliverymode_val").val(deliverymode);
	},
	clickPaymentmode : function () { // 点击支付方式完成事件。
		var paymentmode;
		$("input[paymentmode='paymentmode']").each(function(i, v){ // 获取配送方式下拉选址。
			if ($(this).prop("checked")) {
				if ($(this).context.id === "paymentmode1") { // 自提方式。
					paymentmode = 1;
					return false;
				} else if ($(this).context.id === "paymentmode2") {// 市场配送。
					paymentmode = 2;
					return false;
				} else if ($(this).context.id === "paymentmode3") {// 市场配送。
					paymentmode = 3;
					return false;
				}
			}
	    });
		$("#paymentmode").text(mobileGlobal.paymentMode[paymentmode]);
		$("#paymentmode_val").val(paymentmode);
	},
//	clickInvoices : function () { // 发票方式。
//		var invoices, invoices_val;
//		$("input[invoices='invoices']").each(function(i, v){ // 获取配送方式下拉选址。
//			if ($(this).prop("checked")) {
//				if ($(this).context.id === "x16") { // 自提方式。
//					invoices = "普通";
//					invoices_val = 1;
//					return false;
//				} else if ($(this).context.id === "x17") {// 市场配送。
//					invoices = "增值";
//					invoices_val = 2;
//					return false;
//				}
//			}
//	    });
//		$("#invoices").text(invoices);
//		$("#invoices_val").val(invoices_val);
//	},
	save : function () {
		var defaultAddress = this.checkbox.prop("checked") ? true : false; // 是否设置为默认地址。
		if (this.username.val() === "") {
			mobileGlobal.warn("请填写收货人名称！");
			return;
		}
		if (!/^1[34578]\d{9}$/.test(this.userphone.val())) {
			mobileGlobal.warn("无效手机号码!");
			return;
		}
		if (this.region.val() === "") {
			mobileGlobal.warn("请选择收货城市!");
			return;
		}
		if (this.detailed.val() === "") {
			mobileGlobal.warn("请填写详细地址!");
			return;
		}
		var regionArr = (this.region.val()).split(" ");
		var param = {entityid: $entityid, receiver: this.username.val(), mobilephone: this.userphone.val(), defaultaddress: defaultAddress, province: null, city: null,
				county: null, detailaddress: this.detailed.val(), deliverymode: this.deliverymode.val(), paymentmode: this.paymentmode.val(), createtime: null, entitystate: 1};
		mobileGlobal.sendRequest(__ctx + "/mobileshoppingcart/insertOrUpdateAddressesObj.xhtml",  {data: JSON.stringify(param)}, "数据加载中", function(result){
			if(result.status){
				console.log();
			}
		});
	}
};

/**
 * 勾选默认地址事件。
 * @returns
 */
$("#checkboxClick").click(function () {
	if ($(this).prop("checked")) {
		mobileGlobal.warn("是否确认为默认收货地址！");
	}
});

/**
 * 详细地址输入字数限制。
 * @returns
 */
$(".weui-textarea").keyup(function(){
    if ($(".weui-textarea").val().length > 200) {
        $(".weui-textarea").val($(".weui-textarea").val().substring(0, 200));
    }
    $("#number").text(200 - $(".weui-textarea").val().length) ;
 });

$(function () {
	addressAdd.load();
});
