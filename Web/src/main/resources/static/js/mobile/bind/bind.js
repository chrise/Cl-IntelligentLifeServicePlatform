//输入按键抬起事件
function inputKeyup() {
	instance.checkChanged();
}

//发送点击事件
function sendClick() {
	instance.sendCode();
}

//绑定点击事件
function bindClick() {
	instance.bindPhone();
}

//全局实例对象
var instance = {
	phoneCtl : $("#phone"),
	sendCtl : $("#send"),
	codeCtl : $("#code"),
	submitCtl : $("#submit"),
	sendWait : false,
	cachedPhone : null,
	checkChanged : function() {
		var phone = this.phoneCtl.val(), code = this.codeCtl.val();
		if (!phone || !code) {
			this.submitCtl.addClass("disabled");
		} else this.submitCtl.removeClass("disabled");
	},
	sendCode : function() {
		if (this.sendWait) return;
		
		var phone = this.phoneCtl.val();
		if (!this.verifyPhone(phone)) return;
		
		this.cachedPhone = null;
		mobileGlobal.sendRequest(__ctx + "/nr/api/aliyun/bindvercode.xhtml", {phone : phone}, "正在发送...", function(data, status) {
			if (data.status) {
				instance.readSecond(60);
				instance.sendWait = true;
				instance.cachedPhone = phone;
				mobileGlobal.info("验证码已发送");
			} else mobileGlobal.info("验证码发送失败");
		}, function(xhr, msg, e) {
			mobileGlobal.info("验证码发送失败");
		});
	},
	bindPhone : function() {
		var phone = this.phoneCtl.val(), code = this.codeCtl.val();
		if (!phone || !code) return;
		
		if (!this.verifyPhone(phone)) return;
		
		if (phone != this.cachedPhone) {
			mobileGlobal.warn("手机号与验证码不匹配");
			return;
		}
		
		mobileGlobal.sendRequest(__ctx + "/nr/bind.xhtml", {phone : phone, code : code}, "正在绑定...", function(data, status) {
			if (data.status) {
				window.location.replace(data.result);
			} else mobileGlobal.warn(data.result);
		});
	},
	verifyPhone : function(phone) {
		if (!/^1\d{10}$/.test(phone)) {
			mobileGlobal.warn("手机号无效");
			return false;
		}
		return true;
	},
	readSecond : function(remain) {
		if (remain <= 0) {
			this.sendWait = false;
			this.sendCtl.html("获取验证码");
			return;
		}
		
		this.sendCtl.html("<span style='color:red;'>" + remain + "</span><span style='color:#ccc;'>&nbsp;秒后重发</span>");
		setTimeout(function() {
			instance.readSecond(remain - 1);
		}, 1000);
	}
};